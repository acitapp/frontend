<?php

use App\Models\User;
use Carbon\Carbon;
use App\Models\Event;
use App\Models\Page;
use App\Models\Category;
use App\Models\EventTicket;
use App\Models\EventSalesChannel;
use App\Models\EventSalesChannelTicket;
use App\Models\Schedule;
use App\Models\Setting;
use App\Models\Order;
use Jenssegers\Agent\Agent;
use App\Models\Country;

/**
 * Detect Agent
 */
function mobile() {
	$agent = new Agent();
	if($agent->isDesktop()){
		return false;
	}else{
		return true;
	}
}
/**
 * Get Country
 */
function getCountries() {
    return Cache::remember('countries',1440,function() {
		$countries   = Country::undeleted()->pluck('name','id');
        return $countries;
    });
}
function getCountryLists() {
    return Cache::remember('countries',1440,function() {
		$countries   = Country::undeleted()->get();
        return $countries;
    });
}
/**
 * Get Page Type
 */
function getPageType($type) {
    return Cache::remember('page-'.$type,1440,function()  use ($type) {
        $pages = Page::latest('created_at')->withTranslation()->undeleted()->where('type',$type)->get();
        return $pages;
    });
}
/**
 * Get Category
 */
function getCategory($limit) {
        $categories = Category::latest('created_at')->undeleted()->asmenu()->limit($limit)->get();
        return $categories;
}
/**
 * Get Category Events
 */
function getCategoryItem($id,$limit) {
    return Cache::remember('category-'.$id,1440,function()  use ($id,$limit) {
        $category = Category::undeleted()->find($id);
        if ($category) {
	        $category = $category->events()->live()->simplePaginate($limit);
	        return $category;
        }
	    return [];
    });
}
/**
 * Get Category Events
 */
function getCategoryLimit($id,$limit) {
    return Cache::remember('category-'.$id,1440,function()  use ($id,$limit) {
        $category = Category::undeleted()->find($id);
        if ($category) {
	        $category = $category->events()->with('media')->with('schedule')->withTranslation()->live()->latest('created_at')->limit($limit)->get();
	        return $category;
        }
	    return [];
    });
}
/**
 * Get Featured Events
 */
function getFeaturedEvent($limit) {
   return Cache::remember('getFeaturedEvent-'.$limit,1440,function()  use ($limit) {
        $events = Event::with('media')->live()->where('recomended',1)->withTranslation()->latest('created_at')->limit($limit)->get();
        return $events;
   });
}
/**
 * Get Latest Events
 */
function getLatestEvent($limit) {
    return Cache::remember('getLatestEvent-'.$limit,1440,function()  use ($limit) {
        $events = Event::live()->latest('created_at')->limit($limit)->get();
        return $events;
    });
}
/**
 * Get Asset Image
 */
function getPath($path) {
        $url = env('MEDIA_URL');
        return $url.'/'.$path;
}
/**
 * Get Thumbnail
 */
function getThumbnail($data, $collection, $size) {
	return Cache::remember('thumbnail-'.$collection.'-'.$size.'-'.$data->id,1440,function()  use ($data, $collection, $size) {
	      $url = env('MEDIA_URL');
	      if (count($data->getMedia($collection))){
	      	$image = $url.'/media/'.$data->getMedia($collection)->first()->id.'/conversions/'.$size.'.jpg';
			$file_headers = @get_headers($image);
			if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
	      		return url('/').'/images/default_'.$size.'.jpg';
			}
			else {
	     	 	return $image;
			}
	      }
	      else{
	      	return url('/').'/images/default_'.$size.'.jpg';
	      }
	});
}

function unique_multidim_array($array, $key) { 
    $temp_array = array(); 
    $i = 0; 
    $key_array = array(); 
    
    foreach($array as $val) { 
        if (!in_array($val[$key], $key_array)) { 
            $key_array[$i] = $val[$key]; 
            $temp_array[$i] = $val; 
        } 
        $i++; 
    } 
    return $temp_array; 
} 
/**
 *  Get Range Date
 */
function getRangeDate($schedules) {
  if($schedules){
	  	$lowers = $schedules->sortBy('started_at')->first();
	  	$highers = $schedules->sortByDesc('started_at')->first();
	  	$count = $schedules->count();
	  	if ($count < 1) {
		 	$lower =  eventDateFormat($lowers->started_at);
		 	$higher = eventDateFormat($highers->started_at);
		 	$start = '<span class="date start-date">'.$lower.'</span>';
		 	$end = ' - <span class="date start-date">'.$higher.'</span>';
		 	if ($lower == $higher) {
		  		return $start;
		 	}
		  	return $start.$end;
	  	}else{
	  		if (is_null($lowers->ended_at)) {
		 		return '<span class="date start-date">'.eventDateFormat($lowers->started_at).'</span>';
	  		}
		 	return '<span class="date start-date">'.eventDateFormat($lowers->started_at).'</span>'.' - '.'<span class="date start-date">'.eventDateFormat($lowers->ended_at).'</span>';
	  	}
  }
  return '';
}
/**
 *  Get Range Date
 */
function notExpired($eventID) {
  if($eventID){
  	$end = Schedule::where('event_id',$eventID)->orderBy('started_at', 'desc')->first();;
  	if ($end) {
  		if (is_null($end->ended_at)) {
  			if (Carbon::now() >= $end->started_at) {
  				return false;
  			}
  		}elseif(Carbon::now() >= $end->ended_at){
  				return false;
  		}
  		return true;
  	}
  }
  return true;
}
/**
 *  Get Range Price
 */
function getRangePrice($schedule) {
  if($schedule){
	  $tickets = $schedule->tickets;
	  if ($tickets) {
	  	$count = $schedule->tickets->count();
	  	$lower = $schedule->tickets->sortBy('value')->first();
	  	$higher = $schedule->tickets->sortByDesc('value')->first();
	  	//
          $now = Carbon::now();
          $start_date = Carbon::createFromFormat('d/m/Y h:i A',$lower->started_at);

          $endlow = Carbon::createFromFormat('d/m/Y h:i A',$lower->ended_at);
          $endhigh = Carbon::createFromFormat('d/m/Y h:i A',$higher->ended_at);
	  	//
	 	$lower = 'Rp.'.number_format($lower->value ?? 0);
	 	$higher = 'Rp.'.number_format($higher->value ?? 0);
	 	$price = $lower.' - '.$higher;
	 	if($count <= 1){
	  		return $lower;
	 	}elseif ($now < $start_date) {
	  		return $lower;
	 	}elseif($now > $endlow) {
	 		return $higher;
	 	}elseif($now > $endhigh) {
	 		return $lower;
	 	}
	  	return $price;
	  }
  }
  return 0;
}
/**
 *  Get Current Url
 */
function currentUrl($number) {
  $currentpage = Request::segment($number);
  return $currentpage;
}
/**
 * Limit Wordss
 */
function limitWord($str, $limit) {
	$word = Str::words($str, $limit, '...');
	return $word;
}
/**
 * Get Options  Site
 */
function getOption($key) {
    return Cache::remember('option-'.$key,1440,function()  use ($key) {
		$setting = Setting::where('key', $key)->first();
		if ($setting) {
			if ($setting->value == 'yes') {
				return true;
			}elseif ($setting->value == 'no') {
				return false;
			}else{
				return $setting->value;
			}
		}
		return false;
    });
}
function getSetting($id) {
    return Cache::remember('setting-'.$id,1440,function()  use ($id) {
		 $settings = Setting::get()->toArray();
		   foreach ($settings as $key => $val) {
		       if ($val['key'] === $id) {
		           return $val['value'];
		       }
		   }
		   return '';
    });
}

/**
 * Get Current Language
 */
function lang() {

	$current = Request::segment(1);
	if ($current == 'id'  || $current == 'en') {
		App::setLocale($current);
		$locale = App::getLocale();
	}else{
		$locale = Lang::locale();
	}
	return $locale;
}
/**
 * Get Current Language ID
 */
function langID() {
	$url = Request::url();
	$url = str_replace('/en', '/id', $url);
	return $url;
}
/**
 * Get Current Language EN
 */
function langEn() {
	$url = Request::url();
	$url = str_replace('/id', '/en', $url);
	return $url;
}
/**
 * Human Date Carbon 
 */
function humandate($date) {
	$date = \Carbon\Carbon::parse($date)->diffForHumans();
	return $date;
}
/**
 * Indonesian Date
 */
function formatdate($date) {
	$date = Carbon::parse($date)->format('d/m/Y h:i A');
	return $date;
}

/**
 * Format Date
 */
function formatdateday($date) {
	$date = Carbon::parse($date)->format('d/m/Y');
	return $date;
}

function formatdMY($date) {
	$date = Carbon::parse($date)->format('d M Y');
	return $date;
}


/**
 * Indonesian Date
 */
function eventDateFormat($date) {
	$date = Carbon::parse($date)->formatLocalized('%A, %d %B %Y');
	$date = str_replace('Monday', 'Senin', $date);
	$date = str_replace('Tuesday', 'Selasa', $date);
	$date = str_replace('Wednesday', 'Rabu', $date);
	$date = str_replace('Thursday', 'Kamis', $date);
	$date = str_replace('Friday', "Jum'at", $date);
	$date = str_replace('Saturday', 'Sabtu', $date);
	$date = str_replace('Sunday', 'Minggu', $date);
	$date = str_replace('January', 'Januari', $date);
	$date = str_replace('February', 'Februari', $date);
	$date = str_replace('March', 'Maret', $date);
	$date = str_replace('April', 'April', $date);
	$date = str_replace('May', 'Mei', $date);
	$date = str_replace('June', 'Juni', $date);
	$date = str_replace('July', 'Juli', $date);
	$date = str_replace('August', 'Agustus', $date);
	$date = str_replace('September', 'September', $date);
	$date = str_replace('October', 'Oktober', $date);
	$date = str_replace('November', 'November', $date);
	$date = str_replace('December', 'Desember', $date);
	return $date;
}

/**
 * Filter EveryDay
 */
function everyDay($date) {
    $day = [0,1,2,3,4,5,6];
    $set = explode(',', $date);
    $result =  array_diff($day, $set);
    $result = implode(", ", $result); 
	return $result;
}
/**
 * Rate
 */
function rate($val) {
	$val = $val/5;
	return $val;
}
/**
 * Check Attractions
 */
function thisAttraction($event) {
	$array = $event->categories->pluck('id','title')->toArray();
	$search =((array_search(2,$array)) != NULL);
	return $search;
}
/**
 * Generate Order No
 */
function orderNo() {
	$order_no 	=DB::select(DB::raw('select getOrderNo() as order_no'));
	$order_no 	= $order_no[0]->order_no;
	return $order_no;
}
/**
 * GetPrice
 */
function getPrice($ticket_id) {
        $ticket = EventTicket::find($ticket_id);
        return $ticket->value;
    }
function getTicket($ticket_id) {
        $ticket = EventTicket::find($ticket_id);
        return $ticket;
    }
/**
 * GetQuantity
 */
function GetQuantity($event_id,$ticket_id) {
	    // ID 1 -> kiosTIx.com Only
        $saleschannel = EventSalesChannel::where('event_id',$event_id)->where('sales_channel_id', 1)->first();
        if ($saleschannel) {
	        $quantity = EventSalesChannelTicket::where('event_sales_channel_id',$saleschannel->id)
	        		    ->where('event_ticket_id',$ticket_id)
	        		    ->first();
	        if ($quantity) {
		        if ($quantity->available_quantity === 0) {
		        	return false;
		        }
		        return $quantity->available_quantity;
	        } 		    
        }
		return false;
    }
/**
 * Get Period
 */
function getPeriod($period_id) {
        $period = Periodable::find($period_id);
        return $period;
    }
/**
 * Search Array
 */
function searchArray($array, $key, $value)
	{
	    $results = array();

	    if (is_array($array)) {
	        if (isset($array[$key]) && $array[$key] == $value) {
	            $results[] = $array;
	        }
	        foreach ($array as $subarray) {
	            $results = array_merge($results, searchArray($subarray, $key, $value));
	        }
	    }

	    return $results;
	}


function generateQrcode($string,$config,$path,$name)
{
    return QrCode::format($config['format'])
    ->size($config['size'])
    ->margin((!empty($config['margin']))?$config['margin']:0)
    ->generate($string,$path.$name.'.png');
}

function generateBarcode($eventId)
{
    $number = mt_rand(1000000, 9999999);
    if (checkBarcode($number)) return generateBarcode($eventId);
    return date('y').$eventId.$number;
}

function generatePdf($transaction)
{

    $barcode= $transaction->order_no;
    $config['format']='png'; $config['size']=200;
	$path = env('STORAGE_QRCODE_PATH').$transaction->event_id.'/';
	if(!File::exists($path)) {
	    File::makeDirectory($path, $mode = 0777, true, true);
	}
    generateQrcode($barcode,$config,$path,$barcode);

    $ticketData=Order::join('event_tickets','orders.event_ticket_id','event_tickets.id')
    ->join('events','event_tickets.event_id','events.id')
    ->where('transaction_id',$transaction->id)
    ->groupBy('event_tickets.schedule_id')
    ->select(
        'orders.transaction_id',
        'event_tickets.event_id',
        'events.type as ticket_type',
        'event_tickets.name as ticket_name',
        'event_tickets.schedule_id as schedule_id',
        'orders.event_ticket_id'
    )
    ->get();
    	// dd($ticketData);
    foreach ($ticketData as $key => $values) {
    	$transaction_id = $values->transaction_id;
    	$schedule_id = $values->schedule_id;
        if($values->ticket_type=='["E-Voucher"]'){
            $ticketFolder=env('STORAGE_EVOUCHER_PATH').$transaction->event->id;
			if(!File::exists($ticketFolder)) {
			    File::makeDirectory($ticketFolder, $mode = 0777, true, true);
			}
            $ticketPath=env('STORAGE_EVOUCHER_PATH').$transaction->event->id.'/E-Voucher_'.$transaction->order_no.'.pdf';
            $view='app.order.evoucher';
        }elseif($values->ticket_type=='["Voucher Electronic"]'){
            $ticketFolder=env('STORAGE_EVOUCHER_PATH').$transaction->event->id;
			if(!File::exists($ticketFolder)) {
			    File::makeDirectory($ticketFolder, $mode = 0777, true, true);
			}
            $ticketPath=env('STORAGE_EVOUCHER_PATH').$transaction->event->id.'/E-Voucher_'.$transaction->order_no.'.pdf';
            $view='app.order.voucher_electronic';
        }else{
            $ticketFolder=env('STORAGE_ETICKET_PATH').$transaction->event->id;
			if(!File::exists($ticketFolder)) {
			    File::makeDirectory($ticketFolder, $mode = 0777, true, true);
			}
            $ticketPath=env('STORAGE_ETICKET_PATH').$transaction->event->id.'/E-Ticket_'.$transaction->order_no.'.pdf';
            $view='app.order.eticket';
        }
	//dd($view);
	//return view($view,compact('values'));
         $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
	        $fontDirs = $defaultConfig['fontDir'];
	        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
	        $fontData = $defaultFontConfig['fontdata'];
	        $mpdf= new \Mpdf\Mpdf([
	            'margin-left'=>10,
	            'margin-right'=>5,
	            'margin-top'=>2,
	            'margin-bottom'=>2,
	            'margin-header'=>0,
	            'margin-footer'=>0,
	            'fontDir' => array_merge($fontDirs, [
	               public_path('fonts'),
	            ]),
	            'fontdata' => $fontData + [
	                'lato' => [
	                    'R' => 'Lato-Regular.ttf',
	                    'I' => 'Lato-Italic.ttf',
	                    'B' => "Lato-Bold.ttf",
	                ]
	            ],
	            'default_font' => 'lato'

	        ]);
        $mpdf->showImageErrors = false;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->list_indent_first_level = 0;
		$mpdf->WriteHTML(view($view,compact('values')));
		$mpdf->Output($ticketPath,'F');
		if ($mpdf) {
			return true;
		}
		return false;
    }
}

function checkBarcode($number)
{
    return Order::where('orders.barcode',$number)->exists();
}


function checkRole($role, $userId)
{
	$array =  User::role($role)->pluck('id')->toArray();
	$search =((array_search($userId, $array)) != NULL);
    return $search;
}
/**
 * Check Status
 */
function checkStatus($status) {
	switch ($status) {
	    case 1000:
			return  "Disabled";
	        break;
	    case 1001:
			return  "Enabled";
	        break;
	    case 1002:
			return  true;
	        break;
	    case 1003:
			return  false;
	        break;
	    case 1004:
			echo  "<label class='label label-info'>Draft</label>";
	        break;
	    case 1005:
			echo  "<label class='label label-info'>Live</label>";
	        break;
	    case 1006:
			echo  "<label class='label label-warning'>Hold</label>";
	        break;
	    case 1007:
			echo  "<label class='label label-primary'>Unhold<label>";
	        break;
	    case 1008:
			echo  "<label class='label label-success'>Paid</label>";
	        break;
	    case 1009:
			echo  "<label class='label label-danger'>Expired</label>";
	        break;
	    case 1010:
			echo  "<label class='label label-warning'>Unpaid</label>";
	        break;
	    case 1011:
			echo  "<label class='label label-info'>In Cart</label>";
	        break;
	    case 1012:
			echo  "<label class='label label-warning'>Issued</label>";
	        break;
	    case 1013:
			echo  "<label class='label label-warning'>Return</label>";
	        break;
	    case 1016:
			echo  "<label class='label label-danger'>Fail</label>";
	        break;
	    case 'deny':
			echo  "<label class='label label-danger'>Expired</label>";
	        break;
	    case 'capture':
			echo  "<label class='label label-success'>Paid</label>";
	        break;
	    case 'settlement':
			echo  "<label class='label label-success'>Paid</label>";
	        break;
	    case 'pending':
			echo  "<label class='label label-warning'>Unpaid</label>";
	        break;
	    case 'expired':
			echo  "<label class='label label-danger'>Expired</label>";
	        break;
	    default:
			echo  "<label class='label label-info'>Default</label>";
	}
}

//Parameter OVO
function getTid(){
	$tid = "37860001";
	return $tid;
}

function getMid(){
	$mid = "HHGJJKTREJK0003";
	return $mid;
}

function getMerchantId(){
	$merchantId = "3786";
	return $merchantId;
}

function getStoreCode(){
	$storeCode = "KIOSTIXWEB";
	return $storeCode;
}

function getAppId(){
	$appId = "kiostix";
	return $appId;
}

function generateHmac($random){
	$key = "ee179beb1f8f85a88539cc648e28a17cbf2b367f5152cf26e2fd2c5c7fd790f4";
	$data = getAppId().$random;
	$hmac = hash_hmac("sha256", $data, $key);
}


//Paypal Parameter
function urlStaging() {
	$url = "https://api.sandbox.paypal.com/v1/payments/payment";
	return $url;
}

function username(){
	$username = "AbkSVVK2-q1tNYozim2IY-KCYjRMe-_0FNn5UimjR4OKtWJHFOo4LPj7f_vhfnZDMgU10RLLHM9AYoXE";
	return $username;
}

function password(){
	$password = "ELHO4jqtdleYhdwbaJxztDXMLh6Ffe_oXvfsctBZv3Abj0FwL5d6osqtMGRzM6pJWFWx0LFVzX3_tNfd";
	return $password;
}
