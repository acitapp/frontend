<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	public function customer(){
		return $this->belongsTo('App\Models\User','customer_id','id');
	}
    public function userInformation() {
        return $this->belongsTo('App\Models\UserInformation');
    }

	public function sales(){
		return $this->belongsTo('App\Models\User','sales_id','id');
	}

	public function payment(){
		return $this->belongsTo('App\Models\PaymentMethod','payment_method_id','id');
	}

	public function event(){
		return $this->belongsTo('App\Models\Event');
	}

	public function venue(){
		return $this->belongsTo('App\Models\Venue');
	}

	public function delivery(){
		return $this->belongsTo('App\Models\DeliveryMethod','delivery_method_id','id');
	}
    public function orders() {
        return $this->hasMany('App\Models\Order');
    }
	public function paymentResponse(){
		return $this->hasOne('App\Models\PaymentResponse','order_id','order_no');
	}
	public function scopeExpiredStatus($query) {
		$query->where('status', 1009);
	}
	public function scopeInCartStatus($query) {
		$query->where('status', 1011);
	}
	public function scopeIssuedStatus($query) {
		$query->where('status', 1012);
	}
	public function scopePaidStatus($query) {
		$query->where('status', 1008);
	}
	public function scopeUnpaidStatus($query) {
		$query->where('status', 1010);
	}
}