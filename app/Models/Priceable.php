<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Priceable extends Model
{
	protected $fillable = ['period_name','start_date','end_date'];

    public function priceable()
    {
        return $this->morphTo();
    }
	public function section() {
		return $this->belongsTo('App\Models\Sectionable','sectionable_id');
	}
}
