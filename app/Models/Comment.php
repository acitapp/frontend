<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	protected $fillable = ['user_id', 'commentable_id', 'commentable_type', 'content'];

	public function user() {
		return $this->belongsTo('App\Models\User');
	}
    public function events()
    {
        return $this->morphTo('App\Models\Event');
    }
    public function commentable()
    {
        return $this->morphTo();
    }
	public function parents() {
		return $this->hasMany(Comment::class , 'comment_id');
	}
	public function parent() {
		return $this->belongTo(Comment::class , 'comment_id');
	}
	public function isParent() {
		return !!$this->parent;
	}

}
