<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Seat extends Model
{
	protected $fillable=['seat_layout_id','transaction_id','tiket_id','code_canvas','title','top','left','width','height','color_seat','color_text'];

    public function tickets()
    {
        return $this->belongsTo('App\Models\EventTicket','event_ticket_id');
    }
    public function seatLayout(){
    	return $this->belongsTo('App\Models\SeatLayout','seat_layout_id');
    }
	
	public function scopeUndeleted($query) {
		$query->where('trash', false);
	}
	public function scopeDeleted($query) {
		$query->where('trash', true);
	}
	
}