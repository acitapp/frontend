<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Venue extends Model implements HasMediaConversions 
{
    use Sluggable;
    use HasMediaTrait;
    use SluggableScopeHelpers;
    
	protected $fillable = ['title','keyword','description','status'];

    function info(){
        return $this->hasOne('App\Models\VenueInformation')->with('country')->with('state')->with('city');
    }
    public function registerMediaConversions(Media $media = null){
		$this->addMediaConversion('small')->crop(Manipulations::CROP_CENTER, 300, 200)->performOnCollections('venue');
		$this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 600, 400)->performOnCollections('venue');
		$this->addMediaConversion('large')->crop(Manipulations::CROP_CENTER, 1280, 400)->performOnCollections('venue');
	}
	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate' => true
            ]
        ];
    }
    public function scopeEnabled($query) {
        $query->where('status', 1001);
    }
    public function events()
    {
        return $this->morphedByMany('App\Models\Event', 'venueable');
    }
    public function schedules()
    {
        return $this->morphMany('App\Models\Schedule', 'scheduleable');
    }
}
