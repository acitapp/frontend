<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VenueInformation extends Model
{
	protected $fillable = ['venue_id','owner','capacity','phone','city','address','province','longitude','latitude','country_id','state_id','city_id'];

	public function country() {
		return $this->belongsTo('App\Models\Country');
	}
	public function state() {
		return $this->belongsTo('App\Models\State');
	}
	public function city() {
		return $this->belongsTo('App\Models\City');
	}
	public function venue() {
		return $this->belongsTo('App\Models\Venue');
	}
}
