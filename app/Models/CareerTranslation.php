<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class CareerTranslation extends Model {
	use Sluggable;
	use SluggableScopeHelpers;

	//slug
	public function sluggable() {
		return [
			'slug'    => [
				'source' => 'position',
                'onUpdate' => true
			]
		];
	}

	protected $fillable = ['position', 'slug', 'locale', 'description'];

}
