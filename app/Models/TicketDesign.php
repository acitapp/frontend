<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketDesign extends Model
{
	protected $fillable = ['type','template'];

    public function ticket_designable()
    {
        return $this->morphTo();
    }
}
