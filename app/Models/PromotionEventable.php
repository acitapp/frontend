<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionEventable extends Model
{
    public function promo()
    {
        return $this->belongsTo('App\Models\PromotionEvent','promotion_event_id');
    }
}
