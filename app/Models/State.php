<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class State extends Model implements HasMediaConversions {	
	use Sluggable;
	use SluggableScopeHelpers;
	use HasMediaTrait;
	//slug
	public function sluggable() {
		return [
			'slug'    => [
				'source' => 'name',
                'onUpdate' => true
			]
		];
	}

	protected $fillable = ['code','name','slug','trash','country_id'];


	public function scopeUndeleted($query) {
		$query->where('trash', false);
	}
	public function scopeDeleted($query) {
		$query->where('trash', true);
	}
	public function scopeHasEvent($query) {
		$query->where('count','!=',0);
	}
	public function country() {
		return $this->belongsTo('App\Models\Country');
	}
    public function cities() {
        return $this->hasMany('App\Models\City');
    }
    public function venueinformations() {
        return $this->hasMany('App\Models\VenueInformation');
    }

	public function registerMediaConversions(Media $media = null){
		$this->addMediaConversion('small')->crop(Manipulations::CROP_CENTER, 300, 200)->performOnCollections('states');
		$this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 230, 330)->performOnCollections('states');
		$this->addMediaConversion('large')->crop(Manipulations::CROP_CENTER, 1280, 400)->performOnCollections('states');
	}
}
