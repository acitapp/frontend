<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSalesChannelable extends Model
{
	public function eventsaleschannelable()
    {
        return $this->morphTo();
    }

	public function event_sales_channel()
    {
        return $this->belongsTo('App\Models\EventSalesChannel');
    }
}
