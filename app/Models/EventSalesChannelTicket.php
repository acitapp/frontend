<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSalesChannelTicket extends Model
{
    protected $fillable = ['schedule','venue','section','payment','price','quantity'];


    public function sales_channel(){
    	return $this->belongsTo('App\Models\SalesChannel');
    }

    public function schedule()
    {
        return $this->belongsTo('App\Models\Schedule');
    }

    public function ticket()
    {
        return $this->belongsTo('App\Models\EventTicket');
    }

}
