<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model {

	public function user() {
        return $this->belongsTo('App\Models\User');
	}
    public function events()
    {
        return $this->morphTo('App\Models\Event');
    }
    public function ratingable()
    {
        return $this->morphTo();
    }
}
