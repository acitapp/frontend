<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
	protected $fillable=['user_information_id','address','province','city_id','state_id','country_id','zip_code'];

	public function country() {
		return $this->belongsTo('App\Models\Country');
	}
	public function state() {
		return $this->belongsTo('App\Models\State');
	}
	public function city() {
		return $this->belongsTo('App\Models\City');
	}

}