<?php

namespace App\Models;
use Conner\Tagging\Taggable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Carbon\Carbon;
class Schedule extends Model implements HasMediaConversions{
    
    use Taggable;
    use HasMediaTrait;

	protected $fillable = ['name','type','started_at','every_day','every_date','ended_at'];

    public function scheduleable()
    {
        return $this->morphTo();
    }
	public function event() {
		return $this->belongsTo('App\Event');
	}
    public function sessions() {
        return $this->hasMany('App\Models\Sessionable');
    }
    public function sections()
    {
        return $this->morphMany('App\Models\Sectionable', 'sectionable');
    }
    public function scopeNotExpired($query) {
        $query->where('ended_at', '<=', Carbon::now())->where('status',1001);
    }
    public function scopeEnable($query) {
        $query->where('status',1001);
    }
    public function scopeExpired($query) {
        $query->where('ended_at', '>', Carbon::now());
    }
    public function periodes() {
        return $this->morphMany('App\Models\Periodable', 'periodable');
    }
    public function venue() {
        return $this->belongsTo('App\Models\Venue','scheduleable_id')->with('info');
    }
    public function tickets() {
        $tickets = $this->hasMany('App\Models\EventTicket');
        return $tickets;
    }
    public function lowTickets() {
        $tickets = $this->hasMany('App\Models\EventTicket')->orderBy('value', 'asc');
        return $tickets;
    }
    public function highTickets() {
        $tickets = $this->hasMany('App\Models\EventTicket')->orderBy('value', 'desc');
        return $tickets;
    }

    public function registerMediaConversions(Media $media = null){
        $this->addMediaConversion('small')->crop(Manipulations::CROP_CENTER, 200, 200)->performOnCollections('tickets');
        $this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 600, 600)->performOnCollections('tickets');
        $this->addMediaConversion('large')->crop(Manipulations::CROP_CENTER, 800, 800)->performOnCollections('tickets');
    }
}
