<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Periodable extends Model
{
	protected $fillable = ['period_name','start_date','end_date','max_purchase','event_ticket_id'];

    public function periodable()
    {
        return $this->morphTo();
    }
    public function prices()
    {
        return $this->morphMany('App\Models\Priceable', 'priceable');
    }
    public function price() {
        return $this->hasOne('App\Models\Priceable');
    }
    public function ticket()
    {
    	return $this->belongsTo('App\Models\EventTicket','event_ticket_id','id');
    }
    public function scopeInPeriode($query) {
        $query->where('start_date', '<=',Carbon::now())->where('end_date', '>=',Carbon::now());
    }

}
