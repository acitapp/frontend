<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = ['title','status','term_customer','term_event','term_payment_method','unit','value','minimum','maximum','quantity'];

	public function payment_methods()
	{
		return $this->belongsToMany('App\Models\PaymentMethod', 'promotion_payment_methods', 'promotion_id', 'payment_method_id')->withTimestamps();
	}

    public function event() {
		return $this->belongsToMany('App\Models\PromotionEvent', 'promotion_events', 'promotion_id', 'event_id');
	}
}
