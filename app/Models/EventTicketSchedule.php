<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventTicketSchedule extends Model
{
    public function schedule(){
    	return $this->belongsTo('App\Models\Schedule');
    }

    public function tickets()
    {
        return $this->belongsTo('App\Models\EventTicket','event_ticket_id');
    }
}
