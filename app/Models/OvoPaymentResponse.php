<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OvoPaymentResponse extends Model
{
    protected $table = 'ovo_payment_responses';
}
