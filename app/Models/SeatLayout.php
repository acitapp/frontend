<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SeatLayout extends Model
{
	protected $fillable=['name','venue_id','event_id','tiket_id','number_seat','start_seat','color_seat','color_text_seat','trash','status'];
    protected $table = 'seats_layout';
   
    public function event(){
    	return $this->belongsTo('App\Models\Event','event_id');
    }
    public function venue(){
		
    	return $this->belongsTo('App\Models\Venue','venue_id');
    }
	public function seat(){
		return $this->hasMany('App\Models\Seat');
	}
	public function scopeUndeleted($query) {
		$query->where('trash', false);
	}
	public function scopeDeleted($query) {
		$query->where('trash', true);
	}
	
}