<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Sessionable extends Model
{
	protected $fillable = ['time'];
	
	public function schedule() {
		return $this->belongsTo('App\Models\Schedule');
	}
}
