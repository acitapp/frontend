<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ApprovalLog extends Model 
{
  
  public function event() {
    return $this->belongsTo('App\Models\Event');
  }
  public function user() {
    return $this->belongsTo('App\Models\User');
  }
  
  

}
