<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Carbon\Carbon;

class UserInformation extends Model implements HasMedia
{
	use HasMediaTrait;

	protected $fillable=['user_id','fullname','firstname','lastname','company','gender','pob','dob','phone','biography','point'];

    public function addresses() {
        return $this->hasMany('App\Models\UserAddress');
    }
    public function address() {
        return $this->hasOne('App\Models\UserAddress');
    }
    public function user() {
        return $this->belongsTo('App\Models\User');
    }
    public function getDobAttribute($date) {
        return Carbon::parse($date)->format('d/m/Y');
    }

}