<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    
    public function promotion() {
        return $this->belongsTo('App\Models\Promotion');
    }
}
