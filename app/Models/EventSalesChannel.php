<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSalesChannel extends Model
{
    protected $fillable = ['schedule','venue','section','payment','price','quantity'];

    public function event(){
    	return $this->belongsTo('App\Models\Event');
    }

    public function sales_channel(){
    	return $this->belongsTo('App\Models\SalesChannel');
    }

    public function venues()
    {
        return $this->morphedByMany('App\Models\Venueable', 'event_sales_channelable')->withTimestamps();
    }

    public function schedules()
    {
        return $this->morphedByMany('App\Models\Schedule', 'event_sales_channelable')->withTimestamps();
    }

    public function sections()
    {
        return $this->morphedByMany('App\Models\Sectionable', 'event_sales_channelable')->withTimestamps();
    }

    public function prices()
    {
        return $this->morphedByMany('App\Models\Priceable', 'event_sales_channelable')->withTimestamps();
    }

    public function payment_methods()
    {
        return $this->morphedByMany('App\Models\Paymentable', 'event_sales_channelable')->withTimestamps();
    }
}
