<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaypalPaymentResponse extends Model
{
    protected $fillable = ['invoice_id', 'amount','invoice_description','payment_status','created_at','updated_at'];

    public function getPaidAttribute() {
    	if ($this->payment_status == 'Invalid') {
    		return false;
    	}
    	return true;
    }
}
