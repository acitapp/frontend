<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionPaymentMethod extends Model
{
    protected $fillable = ['promotion_id','payment_method_id'];

    public function promotion(){
    	return $this->belongsTo('App\Models\Promotion');
    }

    public function payment_method(){
    	return $this->belongsTo('App\Models\PaymentMethod');
    }
}
