<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PaymentMethod extends Model
{
    protected $table = 'payment_methods';
	protected $fillable = ['name','slug','type','description'];

	public function fees()
    {
        return $this->morphMany('App\Models\AdditionalFee','additional_feeable');
    }
    public function promotions() {
        $promotion = $this->belongsToMany('App\Models\Promotion')->latest();
        return $promotion;
    }
    public function getCreatedAtAttribute($date) {
        return Carbon::parse($date)->format('Y/m/d');
    }
    public function scopeEnabled($query) {
        $query->where('status', 1001);
    }
    public function scopeDeleted($query) {
        $query->where('status', 1014);
    }
}
