<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryMethod extends Model
{
    protected $table = 'delivery_methods';
	protected $fillable = ['name','status'];

	public function age()
    {
        return $this->morphToMany('App\Models\AdditionalFee','additional_feeable');
    }
}
