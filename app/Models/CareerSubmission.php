<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CareerSubmission extends Model {

	protected $fillable = ['career_id','name','email','phone','address','cv','trash'];

	public function scopeUndeleted($query) {
		$query->where('trash', 1);
	}
	public function scopeDeleted($query) {
		$query->where('trash', 0);
	}

}
