<?php
namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Conner\Tagging\Taggable;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Carbon\Carbon;

class Post extends Model implements HasMediaConversions {

	use Translatable;
	use HasMediaTrait;
	use Taggable;

	public $translatedAttributes = ['title', 'summary', 'slug', 'description'];

	protected $fillable = ['template','trash','keyword','type','published_at'];

	public function registerMediaConversions(Media $media = null){
		$this->addMediaConversion('small')->crop(Manipulations::CROP_CENTER, 300, 200)->performOnCollections('posts');
		$this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 600, 400)->performOnCollections('posts');
		$this->addMediaConversion('large')->crop(Manipulations::CROP_CENTER, 1280, 400)->performOnCollections('posts');
	}

	public function scopeUndeleted($query) {
		$query->where('trash', false);
	}
	public function scopeDeleted($query) {
		$query->where('trash', true);
	}

	public function scopePublished($query) {
		$query->where('published_at', '<=', Carbon::now());
	}
	public function scopeUnpublished($query) {
		$query->where('published_at', '>', Carbon::now());
	}
	public function setPublishedAtAttribute($date) {
		$this->attributes['published_at'] = Carbon::parse($date);
	}
	public function getPublishedAtAttribute($date) {
		return Carbon::parse($date)->format('Y-m-d h:i A');
	}

	public function user() {
		return $this->belongsTo('App\Models\User');
	}
	public function categories() {
		return $this->belongsToMany('App\Category')->withTimestamps();
	}
	public function medias() {
		return $this->belongsToMany('Spatie\Medialibrary\Media')->withPivot('id')->withTimestamps();
	}
	public function getCategoryListAttribute() {
		return $this->categories->pluck('id')->all();
	}

}
