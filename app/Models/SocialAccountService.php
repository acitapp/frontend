<?php

namespace App\Models;

use Laravel\Socialite\Contracts\Provider;
use App\Models\UserInformation;
use App\Models\UserAddress;
use App\Models\User;
use Carbon\Carbon;

class SocialAccountService {
	public function createOrGetUser(Provider $provider) {

		$providerUser = $provider->user();
		$providerName = class_basename($provider);

		$account = SocialAccount::whereProvider($providerName)
			->whereProviderUserId($providerUser->getId())
			->first();
		//if account exist, do.....
		if ($account) {
			return $account->user;
		} else {

			$account = new SocialAccount([
					'provider_user_id' => $providerUser->getId(),
					'provider'         => $providerName
				]);
			$providerid    = $providerUser->getId();
			$user          = User::whereEmail($providerUser->getEmail())->first();
			$hasprovider   = SocialAccount::where('provider_user_id', $providerid)->first();
			$provideremail = $providerUser->getEmail();

			if (is_null($user) && is_null($hasprovider)) {
		        $now = Carbon::now()->format('my');
		        $lastID = User::latest('created_at')->first()->customer_id;
				$user = User::create([
           				'customer_id' => $this->generateID($lastID),
						'email'  => $providerUser->getEmail(),
						'name'   => $providerUser->getName(),
						'status'   => 1001,
					]);
       				$user->syncRoles(['Customer']);
			        $profile = new UserInformation;
        			$profile->user_id = $user->id;
        			$profile->avatar = $providerUser->getAvatar();
			        $profile->save(); 
			        $address = new UserAddress;
        			$address->user_information_id = $profile->id;
			        $address->save(); 
			}

			$account->user()->associate($user);
			$account->save();

			return $user;

		}

	}
    private function generateID($lastID){
        $now = Carbon::now()->format('my');
        $ddyy    = substr($lastID, 0, 4);
        $number  = substr($lastID, 4);
        if ($ddyy == $now) {
            $no =  str_pad($number+1, 5, "0", STR_PAD_LEFT);  //00002
        }else{
            $number  = 00000;
            $no =  str_pad($number+1, 5, "0", STR_PAD_LEFT);  //00002
        }
        return $now.$no;
    }
}
