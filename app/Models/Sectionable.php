<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Sectionable extends Model
{
	protected $fillable = ['name','type','entrance','quantity','tnc','status'];

    public function sectionable()
    {
        return $this->morphTo();
    }
    public function prices() {
        return $this->hasMany('App\Models\Priceable');
    }
    public function price() {
        return $this->hasOne('App\Models\Priceable');
    }
}
