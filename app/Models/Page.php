<?php
namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Page extends Model implements HasMediaConversions {

	use Translatable;
	use HasMediaTrait;

	public $translatedAttributes = ['title', 'summary', 'slug', 'description'];

	protected $fillable = ['template','trash','keyword','type'];

	public function registerMediaConversions(Media $media = null){
		$this->addMediaConversion('small')->crop(Manipulations::CROP_CENTER, 300, 200)->performOnCollections('pages');
		$this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 600, 400)->performOnCollections('pages');
		$this->addMediaConversion('large')->crop(Manipulations::CROP_CENTER, 1280, 400)->performOnCollections('pages');
	}

	public function scopeUndeleted($query) {
		$query->where('trash', false);
	}
	public function scopeDeleted($query) {
		$query->where('trash', true);
	}

}
