<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Media extends Model
{
	protected $table = 'media';
	//protected $fillable = ['featured','keyword','mark_adult','rate_average','status'];
	public function model()
    {
        return $this->morphTo();
    }
}
