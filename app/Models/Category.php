<?php
namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Carbon\Carbon;

class Category extends Model implements HasMediaConversions {

	use Translatable;
	use HasMediaTrait;

	public $translatedAttributes = ['title', 'slug', 'description'];

	protected $fillable = ['trash','keyword'];

	public function registerMediaConversions(Media $media = null){
		$this->addMediaConversion('small')->crop(Manipulations::CROP_CENTER, 300, 200)->performOnCollections('categories');
		$this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 600, 400)->performOnCollections('categories');
		$this->addMediaConversion('large')->crop(Manipulations::CROP_CENTER, 1280, 400)->performOnCollections('categories');
	}

	public function scopeUndeleted($query) {
		$query->where('trash', false);
	}
	public function scopeDeleted($query) {
		$query->where('trash', true);
	}
	public function scopeAsmenu($query) {
		$query->where('status', false);
	}
	
    public function posts()
    {
        return $this->morphedByMany('App\Models\Post', 'categoryable');
    }
    public function events()
    {
        return $this->morphedByMany('App\Models\Event', 'categoryable');
    }
}
