<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable  implements HasMediaConversions{
    use Notifiable;
    use HasMediaTrait;
    use HasRoles;

    protected $guard_name = 'web';
    
    protected $fillable = [
        'name','lastname', 'email', 'password','status','profile','customer_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts() {
        return $this->hasMany('App\Models\Post');
    }
    public function ratings() {
        return $this->hasMany('App\Models\Rating');
    }
    public function events() {
        return $this->hasMany('App\Models\Event');
    }
    public function informations() {
        return $this->hasMany('App\Models\UserInformation');
    }
    public function information() {
        return $this->hasOne('App\Models\UserInformation');
    }
    public function transactions(){
        return $this->hasMany('App\Models\Transaction','customer_id','id');
    }

    public function registerMediaConversions(Media $media = null){
        $this->addMediaConversion('small')->crop(Manipulations::CROP_CENTER, 300, 200)->performOnCollections('avatar');
        $this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 600, 400)->performOnCollections('avatar');
        $this->addMediaConversion('square')->crop(Manipulations::CROP_CENTER, 300, 300)->performOnCollections('avatar');
    }

}