<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	public function transaction(){
		return $this->belongsTo('App\Models\Transaction');
	}

	public function schedule(){
		return $this->belongsTo('App\Models\Schedule');
	}
	public function session(){
		return $this->belongsTo('App\Models\Sessionable','session_id','id');
	}

	public function ticket(){
		return $this->belongsTo('App\Models\EventTicket','event_ticket_id','id');
	}
}