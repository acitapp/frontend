<?php
namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Conner\Tagging\Taggable;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

class Event extends Model implements HasMediaConversions {

	use Translatable;
	use HasMediaTrait;
	use Taggable;
	use SearchableTrait;

	public $translatedAttributes = ['title', 'summary', 'slug', 'description','terms'];

	protected $fillable = ['mark_adult','status','keyword','featured','published_at','sales_date','live_date'];

    protected $searchable = [
        'columns' => [
            'events.keyword' => 40,
            'event_translations.title' => 40,
            'event_translations.description' => 20,
            'venues.title' => 10,
        ],
        'joins' => [
            'event_translations' => ['events.id','event_translations.event_id'],
            'schedules' => ['events.id','schedules.event_id'],
            'venues' => ['schedules.scheduleable_id','venues.id'],
        ],
    ];

	public function registerMediaConversions(Media $media = null){
		$this->addMediaConversion('small')->crop(Manipulations::CROP_CENTER, 300, 200)->performOnCollections('events');
		$this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 600, 400)->performOnCollections('events');
		$this->addMediaConversion('large')->crop(Manipulations::CROP_CENTER, 1280, 400)->performOnCollections('events');
	}

	public function scopeUndeleted($query) {
		$query->where('trash', 1);
	}
	public function scopeDeleted($query) {
		$query->where('trash', 0);
	}
	public function scopeEnabled($query) {
		$query->where('status', 1001);
	}
	public function scopeLive($query) {
		$query->where('status', 1005)->where('trash', false)->where('live_date', '<=', Carbon::now());
	}
	public function scopeFeatured($query) {
		$query->where('featured', 1);
	}
	public function scopeRecommended($query) {
		$query->where('recomended', 1);
	}

	public function user() {
		return $this->belongsTo('App\Models\User');
	}
	public function event() {
		return $this->belongsTo('App\Models\Event');
	}
	public function categories() {
		 return $this->morphToMany('App\Models\Category', 'categoryable');
	}
	public function getCategoryListAttribute() {
		return $this->categories->pluck('id')->all();
	}
	public function payments() {
		 return $this->morphToMany('App\Models\PaymentMethod', 'paymentable')->where('online',1);
	}
	public function getPaymentListAttribute() {
		return $this->payments->pluck('id')->all();
	}
	public function deliverys() {
		 return $this->morphToMany('App\Models\DeliveryChannel', 'deliveryable');
	}
	public function getDeliveryListAttribute() {
		return $this->deliverys->pluck('id')->all();
	}
	function translation(){
		return $this->hasOne('App\Models\EventTranslation');
	}

    function schedule(){
        return $this->hasOne('App\Models\Schedule')->with('venue')->with('tickets');
    }
    
    public function schedules() {
        return $this->hasMany('App\Models\Schedule')->with('tickets');
    }
    public function schedulesLowTickets() {
        return $this->hasMany('App\Models\Schedule')->with('lowTickets');
    }
    public function schedulesHighTickets() {
        return $this->hasMany('App\Models\EventTicket')->orderBy('value', 'desc');
    }

    public function sections()
    {
        return $this->morphMany('App\Models\Sectionable', 'sectionable');
    }
    public function prices()
    {
        return $this->morphMany('App\Models\Priceable', 'priceable');
    }
    public function venues()
    {
        return $this->morphToMany('App\Models\Venue', 'venueable');
    }
    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }
    public function ratings()
    {
        return $this->morphMany('App\Models\Rating', 'ratingable');
    }
}
