<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentResponse extends Model
{
	public function transaction(){
		return $this->belongsTo('App\Models\Transaction','order_id','order_no');
	}
	public function scopeCaptured($query) {
		$query->where('transaction_status', 'capture');
	}
	public function scopePending($query) {
		$query->where('transaction_status', 'pending');
	}
	public function scopeExpired($query) {
		$query->where('transaction_status', 'expired');
	}
}
