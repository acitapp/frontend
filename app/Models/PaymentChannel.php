<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class PaymentChannel extends Model implements HasMedia
{
	use HasMediaTrait;
	protected $fillable = ['status','name','description'];

	public function method()
    {
        return $this->hasMany('App\Models\PaymentMethod','payment_channel_id');
    }
    public function events()
    {
        return $this->morphedByMany('App\Models\Event', 'paymentable');
    }
}
