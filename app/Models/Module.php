<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
	protected $fillable = ['status','module_id','position','slug','icon','name','source'];
}