<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model 
{

  protected $fillable = ['email','message'];

}
