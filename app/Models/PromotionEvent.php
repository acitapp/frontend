<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionEvent extends Model
{
    public function promotion(){
    	return $this->belongsTo('App\Models\Promotion');
    }

    public function event(){
    	return $this->belongsTo('App\Models\Event');
    }

    public function priceable()
    {
        return $this->morphedByMany('App\Models\Priceable', 'promotion_eventable');
    }
}
