<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EventTicket extends Model
{
	protected $fillable=['event_id','type','name','event_ticket_template_id','cc_bin','started_at','ended_at','max_purchase','quantity','available_quantity','value'.'description','show_description','group','status','sales_channel','sales_channel_id','schedule_id'];
    public function setStartedAtAttribute($date) {
        $this->attributes['started_at'] = Carbon::parse($date);
    }
    public function getStartedAtAttribute($date) {
        return Carbon::parse($date)->format('d/m/Y h:i A');
    }
    public function setEndedAtAttribute($date) {
        $this->attributes['ended_at'] = Carbon::parse($date);
    }
    public function getEndedAtAttribute($date) {
        if (is_null($date)) {
            return null;
        }
        return Carbon::parse($date)->format('d/m/Y h:i A');
    }
    public function getCreatedAtAttribute($date) {
        return Carbon::parse($date)->format('d/m/Y h:i A');
    }
    public function event(){
    	return $this->belongsTo('App\Models\Event');
    }
    public function saleschannel(){
        return $this->belongsTo('App\Models\SalesChannel','sales_channel_id');
    }
    public function quantities() {
        return $this->hasMany('App\Models\EventSalesChannelTicket');
    }
    public function schedule()
    {
        return $this->belongsTo('App\Models\Schedule');
    }

    public function getScheduleListAttribute()
    {
        return $this->schedules->pluck('id')->all();
    }
    public function scopeGetTicket($query,$date,$schedule_id) {
        $query->where('started_at','<=',$date);
        $query->where('ended_at','>=',$date);
        $query->where('status','1001');
        $query->where('schedule_id',$schedule_id);
    }
}
