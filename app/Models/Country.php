<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Country extends Model implements HasMediaConversions {	
	use Sluggable;
	use SluggableScopeHelpers;
	use HasMediaTrait;
	//slug
	public function sluggable() {
		return [
			'slug'    => [
				'source' => 'name',
                'onUpdate' => true
			]
		];
	}

	protected $fillable = ['code','name','slug','trash'];


	public function scopeUndeleted($query) {
		$query->where('trash', false);
	}
	public function scopeDeleted($query) {
		$query->where('trash', true);
	}
	public function scopeHasEvent($query) {
		$query->where('count','!=',0);
	}
    public function states() {
        return $this->hasMany('App\Models\State');
    }
    public function venueinformations() {
        return $this->hasMany('App\Models\VenueInformation');
    }

	public function registerMediaConversions(Media $media = null){
		$this->addMediaConversion('small')->crop(Manipulations::CROP_CENTER, 300, 200)->performOnCollections('countries');
		$this->addMediaConversion('medium')->crop(Manipulations::CROP_CENTER, 230, 330)->performOnCollections('countries');
		$this->addMediaConversion('large')->crop(Manipulations::CROP_CENTER, 1280, 400)->performOnCollections('countries');
	}
}
