<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
       '/send',
       '/subscribe',
       '/unsubscribe',
       '/like',
       '/payment/complete',
       '/payment/fail',
       '/payment/error',
       '/order/complete',
       '/order/fail',
       '/order/error',
    ];
}
