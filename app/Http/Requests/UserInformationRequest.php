<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Rules\OlderThan;
class UserInformationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'fullname' => 'required|min:3|max:255',
          'phone' => 'required|min:3',
          'dob' => 'required|olderThan:15',
          'avatar' => 'max:2000|mimes:jpeg,bmp,png',
        ];
    }
    public function messages()
    {
        return [
            'fullname.required'    => 'Mohon isi nama lengkap anda',
            'phone.required'  => 'Mohon isi nomor telepon anda dengan benar',
            'dob.older_than'  => 'Minimal umur anda 15 tahun',
        ];
    }
}
