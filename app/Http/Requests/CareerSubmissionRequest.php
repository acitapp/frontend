<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CareerSubmissionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'career_id' => 'required',
          'name' => 'required|min:3|max:255',
          'email' => 'required|email',
          'phone' => 'required|min:3',
        //  'address' => 'required|min:3',
          'cv' => 'required|max:2000|mimes:pdf',
        ];
    }
    public function messages()
    {
        return [
            'career_id.required'    => 'Please select your position',
            'name.required'  => 'Please enter your name',
            'email.required'  => 'Please enter your email',
            'phone.required'  => 'Please enter your phone',
          //  'address.required'  => 'Please enter your address',
            'cv.required'  => 'Please upload your cv',
            'cv.mimes'  => 'Please use PDF Format',
            'cv.max'  => 'Maximum Size 2Mb',
        ];
    }
}
