<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          // 'fullname' => 'required|min:3|max:255',
           'visit_date' => 'required',
          // 'email' => 'required|email|max:255|unique:users',
        ];
    }
    public function messages()
    {
        return [
            'visit_date.required'    => 'Mohon pilih tanggal berkunjung terlebih dahulu.',
        ];
    }
}
