<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BookingInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'firstname' => 'required|min:3|max:255',
          'lastname' => 'required|min:3|max:255',
          'phone' => 'required|min:3',
          'gender' => 'required|min:3|max:255',
          'dob' => 'required|olderThan:15',
        ];
    }
    public function messages()
    {
        return [
            'firstname.required'    => 'Mohon isi nama depan anda',
            'lastname.required'    => 'Mohon isi nama belakang anda',
            'phone.required'  => 'Mohon isi nomor telepon anda dengan benar',
            'dob.older_than'  => 'Minimal umur anda 15 tahun',
            'gender.required'  => 'Pilih jenis kelamin anda',
        ];
    }
}
