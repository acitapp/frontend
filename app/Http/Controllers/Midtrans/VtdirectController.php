<?php

namespace App\Http\Controllers\Midtrans;
use Illuminate\Http\Request;
use App\Http\Requests\PaymentRequest;
use App\Http\Requests;
use App\Models\Transaction;
use App\Models\PaymentResponse;
use App\Models\PaymentMethod;
use App\Models\Voucher;
use App\Models\VoucherUser;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Mail;
use DB;

use App\Veritrans\Veritrans;

class VtdirectController extends Controller
{
    public function __construct()
    {   
        Veritrans::$serverKey = getOption('midtrans_serverkey');
        Veritrans::$isProduction = getOption('midtrans_production');
    }
    public function vtdirect() 
    {
        return view('checkout'); 
    }

    public function checkout_process(PaymentRequest $request)
    {
        $key = decrypt($request->key);
        $transactions = DB::table('orders')
                            ->leftJoin('transactions','orders.transaction_id','transactions.id')
                            ->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
                            ->where('transactions.order_no',$key)
                            ->groupBy('event_tickets.name')
                            ->select(DB::raw('count(*) as qty'),DB::raw('sum(orders.order_value) as total_value'),'event_tickets.name as name','orders.visit_date as visit_date','orders.visit_time as visit_time','orders.event_ticket_id as event_ticket_id','orders.order_value as order_value','orders.periode as periode')
                            ->get();
        $transaction  = Transaction::where('order_no',$key)->first();
        if($transaction){
            $event = $transaction->event;
            $payment = PaymentMethod::find($request->payment_method_id);
            $voucher = Voucher::where('code',$request->voucher_code)->first();
            $fee_fixed = $payment->fees()->where('unit','fixed')->first();
            $percent = $payment->fees()->where('unit','percent')->first();
            $mdr_fixed = $payment->fees()->where('unit','mdr_fixed')->first();
            $mdr_percent = $payment->fees()->where('unit','mdr_percent')->first();

            $subtotal = $transaction->value_sub_total;
            //Get Fee Percent
            $fee_percent = ($subtotal/100)*$percent->value;

            $transaction->payment_method_id = $payment->id;
            $transaction->fee_convenience_percent = $fee_percent;
            $transaction->fee_convenience_fixed = $fee_fixed->value;
            //Total Convenience Fee
            $transaction->fee_convenience_total = $fee_fixed->value+$fee_percent;

            //Get MDR Percent
            $mdrPercent = ($subtotal/100)*$mdr_percent->value;
            $transaction->mdr_percent = $mdrPercent;
            $transaction->mdr_fixed = $mdr_fixed->value;
            //Total MDR 
            $totalMdr = $mdrPercent+$mdr_fixed->value;
            //Grand Total
            $grand_total = $subtotal+$fee_fixed->value+$fee_percent;
            $transaction->value_grand_total = $grand_total;
            if ($voucher) {
                $transaction->voucher_code = $request->voucher_code;
                if ($voucher->promotion->unit == 'fixed') {
                     $transaction->value_promo = $voucher->promotion->value;
                }elseif($voucher->promotion->unit == 'percent'){
                     $transaction->value_promo = ($transaction->value_grand_total/100)*$voucher->promotion->value;
                }
                $grand_total = $subtotal+$fee_fixed->value+$fee_percent-$transaction->value_promo;
                $transaction->value_grand_total = $grand_total;
                $voucher->status = 1000;
                $voucher->update();
                $voucherUser = new VoucherUser;
                $voucherUser->user_id = $transaction->customer_id;
                $voucherUser->voucher_id = $voucher->id;
                $voucherUser->save();
            }
            //Paid Value
            $transaction->paid_value = $grand_total-$totalMdr;
            $transaction->update();
            generatePdf($transaction);
            $vt = new Veritrans;
            $transaction_data = $this->transactionData($request,$transaction);
            // dd($transaction_data);
            $response = null;
            try{
                
                $response = $vt->vtdirect_charge($transaction_data);
            } catch (Exception $e) {
                return $e->getMessage; 
            }
            if($response)
            {
                if (!in_array($response->status_code, array(200, 201, 202, 407))) {
                    return view('app.order.message',compact('response','event'));

                }else if($response->transaction_status == "capture")
                { 
                    $transaction->status = 1012;
                    $transaction->update();
                    generatePdf($transaction);
                    $paymentResponse = $this->saveResponse($response);
                    $this->sendMailPdf($transactions,$transaction,'[kiosTix] Your transaction with order number '.$transaction->order_no.' Success');
                    flash()->success('Transaksi berhasil');
                }
                else if($response->transaction_status == "settlement")
                {
                    $transaction->status = 1012;
                    $transaction->update();
                    $paymentResponse = $this->saveResponse($response);
                    generatePdf($transaction);
                    $this->sendMailPdf($transactions,$transaction,'[kiosTix] Your transaction with order number '.$transaction->order_no.' Success');
                    flash()->success('Transaksi berhasil');
                }
                else if($response->transaction_status == "deny")
                {
                    //deny
                    $transaction->status = 1016;
                    $transaction->update();
                    $paymentResponse = $this->saveResponse($response);
                    flash()->success('Transaksi ditolak');
                }
                else if($response->transaction_status == "challenge")
                {
                    //challenge
                    $transaction->status = 1010;
                    $transaction->update();
                    $paymentResponse = $this->saveResponse($response);
                    flash()->success('Transaksi challenge');
                }
                else if(($response->transaction_status == "pending") && ($response->payment_type == "cimb_clicks")){
                    $transaction->status = 1010;
                    $transaction->update();
                    $paymentResponse = $this->saveResponse($response);
                    return redirect($response->redirect_url);
                    // $this->sendMailPdf($transactions,$transaction,'Transaksi Sukses & E-Voucher, Konfirmasi Pembayaran');
                    // flash()->success('Transaksi ');
                }
                else if(($response->transaction_status == "pending") && ($response->payment_type == "mandiri_ecash")){
                    $transaction->status = 1010;
                    $transaction->update();
                    $paymentResponse = $this->saveResponse($response);
                    return redirect($response->redirect_url);
                    // $this->sendMailPdf($transactions,$transaction,'Transaksi Sukses & E-Voucher, Konfirmasi Pembayaran');
                    // flash()->success('Transaksi ');
                }
                else
                {
                    $transaction->status = 1010;
                    $transaction->update();
                    $paymentResponse = $this->saveResponse($response);
                    $this->sendOnlyMail($transactions,$transaction,'[kiosTix] You are one step away from getting your tickets!');
                    flash()->success('Transaksi berhasil');
                }   
            }

            $date = Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
            $currentDate = strtotime($date);
            $futureDate = $currentDate+(60*180);
            $expTime = date("Y-m-d H:i:s", $futureDate);
            $expTimeView = date("Y/m/d H:i:s", $futureDate);
            $expTimeViewDate = date("Y/m/d", $futureDate);
            $expTimeViewTime = date("H:i:s", $futureDate);
            $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
            return view('app.order.thanks',compact('transaction','event','paymentResponse','expired_date','expTime','expTimeViewDate','expTimeViewTime','expTimeView'));
        }

    }
    private function  transactionData($request,$transaction)
    {
        $key = decrypt($request->key);
        $token = $request->input('token_id');
        $transaction_details = array(
            'order_id'          => $key,
            'gross_amount'  => $transaction->value_grand_total
        );
        if ($request->payment_type == 'credit_card') {
            $transaction_data = array(
                'payment_type'          => $request->payment_type, 
                'credit_card'           => array(
                    'token_id'          => $token,
                     'bank'              => 'cimb'
                 ),
                'transaction_details'   => $transaction_details,
                'item_details'          => $this->productItems($transaction),
                'customer_details'      => $this->customerDetail($transaction),
            );
             return $transaction_data;
        }elseif($request->payment_type == 'cstore'){
            $transaction_data = array(
                'payment_type'          => $request->payment_type, 
                'cstore'                => array(
                    'store'             => 'Indomaret',
                    'message'           => 'Purchase - '.$transaction->event->getTranslation('id')->title,
                 ),
                'transaction_details'   => $transaction_details,
                'item_details'          => $this->productItems($transaction),
                'customer_details'      => $this->customerDetail($transaction),
                'custom_expiry'         => array (
                                          "order_time" => Carbon::now()->format('Y-m-d H:i:s O'),
                                          "expiry_duration" => 3,
                                          "unit" => "hour",
                ),
            );
             return $transaction_data;
        }elseif($request->payment_type == 'bank_transfer'){
            $transaction_data = array(
                'payment_type'              => $request->payment_type, 
                'bank_transfer'             => array(
                    'bank'                  => 'permata',
                    'permata'               => array(
                        'recipient_name'    => 'PT. PISON TICKETTECH',
                     ),
                 ),
                'transaction_details'   => $transaction_details,
                'item_details'          => $this->productItems($transaction),
                'customer_details'      => $this->customerDetail($transaction),
                'custom_expiry'         => array (
                                          "order_time" => Carbon::now()->format('Y-m-d H:i:s O'),
                                          "expiry_duration" => 3,
                                          "unit" => "hour",
                ),
            );
             return $transaction_data;
        }elseif($request->payment_type == 'cimb_clicks'){
            $transaction_data = array(
                'payment_type'              => $request->payment_type, 
                'cimb_clicks'               => array(
                    'description'           => 'Purchase - '.$transaction->event->getTranslation('id')->title,
                 ),
                'transaction_details'   => $transaction_details,
                'item_details'          => $this->productItems($transaction),
                'customer_details'      => $this->customerDetail($transaction),
            );
             return $transaction_data;
        }elseif($request->payment_type == 'echannel'){
            $transaction_data = array(
                'payment_type'              => $request->payment_type, 
                'echannel'               => array(
                    'bill_info1'           => 'Payment For: - '.$transaction->event->getTranslation('id')->title,
                    'bill_info2'            => 'debt'
                ),
                'transaction_details'   => $transaction_details,
                'item_details'          => $this->productItems($transaction),
                'customer_details'      => $this->customerDetail($transaction),
            );
            return $transaction_data;
        } elseif($request->payment_type == 'mandiri_ecash'){
            $transaction_data = array(
                'payment_type'              => $request->payment_type, 
                'mandiri_ecash'               => array(
                    'description'           => 'Purchase - '.$transaction->event->getTranslation('id')->title,
                 ),
                'transaction_details'   => $transaction_details,
                'item_details'          => $this->productItems($transaction),
                'customer_details'      => $this->customerDetail($transaction),
            );
            return $transaction_data;
        }
        elseif($request->payment_type == 'mandiri_clickpay'){
            $tokenid = $request->input('token_id');
            $input3 = $request->input('input3');
            $token = $request->input('token');
            $transaction_data = array(
                'payment_type'              => $request->payment_type, 
                'mandiri_clickpay'               => array(
                    "token_id" =>   $tokenid,
                    "input3" =>  $input3,
                    "token" =>  $token
                 ),
                'transaction_details'   => $transaction_details,
                'item_details'          => $this->productItems($transaction),
                'customer_details'      => $this->customerDetail($transaction),
            );
            return $transaction_data;
        }
        elseif ($request->payment_type == 'cicilan') {
            $transaction_data = array(
                'payment_type'          => $request->payment_type, 
                'transaction_details'   => $transaction_details,
                'credit_card'           => array(
                    'token_id'          => $token,
                    'bank'              => 'bni',
                    'installment_term'  => $request->month_installment,
                   
                 ),
            );
             return $transaction_data;
        }
    }
    private function  productItems($transaction)
    {
        $productItems = $transaction->orders->toArray();
        $fee = array(array(
                    'id'            => uniqid(),
                    'price'         => $transaction->fee_convenience_total,
                    'quantity'      => 1,
                    'name'          => 'Convenience Fee'
                ));
        $voucher = array(array(
                    'id'            => uniqid(),
                    'price'         => -$transaction->value_promo,
                    'quantity'      => 1,
                    'name'          => 'Voucher '.$transaction->voucher_code
                ));
        $productItems = array_map(function($item) {
                $transaction  = Transaction::find($item['transaction_id']);
                return array(
                    'id'            => $item['id'],
                    'price'         => $item['order_value'],
                    'quantity'      => 1,
                    'name'          => $transaction->event->getTranslation('id')->title.' ('.$item['periode'].')'
                );
            }, $productItems);
        $productItems = array_merge($productItems,$fee);
        $productItems = array_merge($productItems,$voucher);
        return $productItems;
    }
    private function  customerDetail($transaction)
    {
        $address = array(
            'first_name'        => $transaction->customer->name ?? '',
            'last_name'         => '',
            'address'           => '',
            'city'              => '',
            'postal_code'       => 15415,
            'phone'             => $transaction->userInformation->phone ?? '',
            'country_code'      => 'IDN'
            );
        // Populate customer's Info
        $customer_details = array(
            'first_name'            => $transaction->customer->name ?? '',
            'last_name'             => '',
            'email'                     => $transaction->customer->email ?? '',
            'phone'                     => $transaction->userInformation->phone ?? '',
            'billing_address' => $address,
            'shipping_address'=> $address
            );
        return $customer_details;
    }
    private function  saveResponse($response)
    {
            $record = new PaymentResponse;
            $record->status_code = $response->status_code ?? null;
            $record->status_message = $response->status_message ?? null;
            $record->transaction_id = $response->transaction_id ?? null;
            $record->order_id = $response->order_id ?? null;
            $record->gross_amount = $response->gross_amount ?? 0;
            $record->payment_type = $response->payment_type ?? null;
            $record->payment_channel = 'Midtrans';
            $record->transaction_time = $response->transaction_time ?? null;
            $record->transaction_status = $response->transaction_status ?? null;
            $record->fraud_status = $response->fraud_status ?? null;
            $record->approval_code = $response->approval_code ?? null;
            $record->eci = $response->eci ?? null;
            $record->masked_card = $response->masked_card ?? null;
            $record->bank = $response->bank ?? null;
            $record->channel_response_code = $response->channel_response_code ?? null;
            $record->channel_response_message = $response->channel_response_message ?? null;
            $record->permata_va_number = $response->permata_va_number ?? null;
            $record->payment_code = $response->payment_code ?? null;
            $record->bill_key = $response->bill_key ?? null;
            $record->biller_code = $response->biller_code ?? null;
            $record->store = $response->store ?? null;
            $record->save();
            return $record;
    }
    private function sendOnlyMail($transactions,$transaction, $subject)
    {
        $email = $transaction->customer->email;
        $event = $transaction->event;
        $response = PaymentResponse::where('order_id',$transaction->order_no)->first();
        $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
        Mail::send('email.order_'.$transaction->payment->type, compact('transactions','transaction','event','response','expired_date'), function ($message) use ($email, $subject) {
             $message->from('booking@kiostix.com', 'kiosTix');
             $message->to($email)->subject($subject);
         });
    }
    private function sendMailPdf($transactions,$transaction, $subject)
    {
        $email = $transaction->customer->email;
        $event = $transaction->event;
        $response = PaymentResponse::where('order_id',$transaction->order_no)->first();
        $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
        if ($transaction->event->type == '["E-Voucher"]') {
            $attach=env('STORAGE_EVOUCHER_PATH').$transaction->event->id.'/E-Voucher_'.$transaction->order_no.'.pdf';
        }elseif($transaction->event->type == '["Voucher Electronic"]'){
            $attach=env('STORAGE_EVOUCHER_PATH').$transaction->event->id.'/E-Voucher_'.$transaction->order_no.'.pdf';
        }else{
            $attach=env('STORAGE_ETICKET_PATH').$transaction->event->id.'/E-Ticket_'.$transaction->order_no.'.pdf';
        }
        Mail::send('email.order_'.$transaction->payment->type, compact('transactions','transaction','event','response','expired_date'), function ($message) use ($email, $subject, $attach) {
             $message->from('booking@kiostix.com', 'kiosTix');
             $message->attach($attach);
             $message->to($email)->subject($subject);
        });
    }
}    