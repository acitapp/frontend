<?php
namespace App\Http\Controllers\Midtrans;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Veritrans\Midtrans;
use App\Veritrans\Veritrans;
use App\Models\PaymentResponse;
use App\Models\Transaction;
use App\Models\EventSalesChannel;
use App\Models\EventSalesChannelTicket;
use App\Models\Seat;
use Carbon\Carbon;
use DB;
use Mail;
use Cookie;
use Auth;

class SnapController extends Controller
{
    public function __construct()
    {   
        Veritrans::$serverKey = getOption('midtrans_serverkey');
        Veritrans::$isProduction = getOption('midtrans_production');
    }

    public function snap()
    {
        return view('snap_checkout');
    }

    public function token() 
    {
        error_log('masuk ke snap token dri ajax');
        $midtrans = new Midtrans;

        $transaction_details = array(
            'order_id'      => uniqid(),
            'gross_amount'  => 200000
        );

        // Populate items
        $items = [
            array(
                'id'        => 'item1',
                'price'     => 100000,
                'quantity'  => 1,
                'name'      => 'Adidas f50'
            ),
            array(
                'id'        => 'item2',
                'price'     => 50000,
                'quantity'  => 2,
                'name'      => 'Nike N90'
            )
        ];

        // Populate customer's billing address
        $billing_address = array(
            'first_name'    => "Andri",
            'last_name'     => "Setiawan",
            'address'       => "Karet Belakang 15A, Setiabudi.",
            'city'          => "Jakarta",
            'postal_code'   => "51161",
            'phone'         => "081322311801",
            'country_code'  => 'IDN'
            );

        // Populate customer's shipping address
        $shipping_address = array(
            'first_name'    => "John",
            'last_name'     => "Watson",
            'address'       => "Bakerstreet 221B.",
            'city'          => "Jakarta",
            'postal_code'   => "51162",
            'phone'         => "081322311801",
            'country_code'  => 'IDN'
            );

        // Populate customer's Info
        $customer_details = array(
            'first_name'      => "Andri",
            'last_name'       => "Setiawan",
            'email'           => "andrisetiawan@asdasd.com",
            'phone'           => "081322311801",
            'billing_address' => $billing_address,
            'shipping_address'=> $shipping_address
            );

        // Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit'       => 'hour', 
            'duration'   => 2
        );
        
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $items,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );
    
        try
        {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            //return redirect($vtweb_url);
            echo $snap_token;
        } 
        catch (Exception $e) 
        {   
            return $e->getMessage;
        }
    }

    public function finish(Request $request)
    {
        $result = $request->input('result_data');
        $result = json_decode($result);
        echo $result->status_message . '<br>';
        echo 'RESULT <br><pre>';
        var_dump($result);
        echo '</pre>' ;
    }

    public function notification()
    {
        $midtrans = new Midtrans;
        $json_result = file_get_contents('php://input');
        $json_decode = json_decode($json_result);
        $result = json_decode(json_encode($json_decode), true);

        Log::debug(['Midtrans' => $result]);
        $transaction = $result["transaction_status"];
        $payment_type = $result["payment_type"];
        $type = $result["payment_type"];
        $order_id = $result["order_id"];

        if ($transaction == 'capture') {
          // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card'){
                $fraud = $result["fraud_status"];
                if($fraud == 'challenge'){
                // TODO set payment status in merchant's database to 'Challenge by FDS'
                // TODO merchant should decide whether this transaction is authorized or not in MAP
                    $getTransaction  = Transaction::where('order_no',$order_id)->first();
                    $seat = Seat::where('transaction_id', $getTransaction->id)->first();
                    if($seat){
                        Seat::Where('transaction_id',$getTransaction->id)->update(['status_order'=>2]);
                    }
                    echo "Transaction order_id: " . $order_id ." is challenged by FDS";
                } 
                else {
                // TODO set payment status in merchant's database to 'Success'
                    $getTransaction  = Transaction::where('order_no',$order_id)->first();
                    $seat = Seat::where('transaction_id', $getTransaction->id)->first();
                    if($seat){
                        Seat::Where('transaction_id',$getTransaction->id)->update(['status_order'=>2]);
                    }
                    echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
                }
            }
        }
        else if ($transaction == 'deny') {

            $paymentResponse = PaymentResponse::where('order_id',$order_id)->first();
            $paymentResponse->transaction_status = $transaction;
            $paymentResponse->update();

            $transactions = DB::table('transactions')->where('order_no',$order_id)->update(['status'=> "1016"]);
            $getTransaction  = Transaction::where('order_no',$order_id)->first();
            $seat = Seat::where('transaction_id', $getTransaction->id)->first();
            if($seat){
                Seat::Where('transaction_id',$getTransaction->id)->update(['status_order'=>0,'transaction_id'=>0,'customer_id'=>0]);
            }
           
            // TODO set payment status in merchant's database to 'Denied'
            echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
        }
        else if($transaction == 'pending'){
            //update status in payment_response as pending
            $paymentResponse = PaymentResponse::where('order_id',$order_id)->first();
            $paymentResponse->transaction_status = $transaction;
            $paymentResponse->update();

            //update status in DB transactions as pending
            $transactions = DB::table('transactions')->where('order_no',$order_id)->update(['status'=> "1010"]);
            $getTransaction  = Transaction::where('order_no',$order_id)->first();
            $seat = Seat::where('transaction_id', $getTransaction->id)->first();
            if($seat){
                Seat::Where('transaction_id',$getTransaction->id)->update(['status_order'=>2]);
            }
               
            // TODO set payment status in merchant's database to 'Pending'
            echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
        } 
        else if ($transaction == 'expire') {
            Log::debug(['Midtrans Expired' => $result]); 
            $paymentResponse = PaymentResponse::where('order_id',$order_id)->first();
            $paymentResponse->transaction_status = $transaction;
            $paymentResponse->update();

            $transaction = Transaction::where('order_no',$order_id)->where('status',1010)->first();
            Log::debug(['Transaction Expired' => $transaction]);
            if ($transaction) {
                $transaction->status = 1009;
                $transaction->update();
                foreach ($transaction->orders as $order) {
                    $saleschannel = EventSalesChannel::where('event_id',$transaction->event_id)->where('sales_channel_id',1)->first();
                    $saleschannelTicket = EventSalesChannelTicket::where('event_sales_channel_id',$saleschannel->id)->where('event_ticket_id', $order->event_ticket_id)->first();
                    $saleschannelTicket->increment('available_quantity', 1);
                }
                $seat = Seat::where('transaction_id', $transaction->id)->first();
                if($seat){
                    Seat::Where('transaction_id',$transaction->id)->update(['status_order'=>0,'transaction_id'=>0,'customer_id'=>0]);
                }
                $this->sendMailExpired($transaction,'[kiosTix] We’re sorry but your order '.$order_id.' has expired');
                Log::debug(['Transaction Expired Email' => 'Sukses Send Expired']);
            }
            // TODO set payment status in merchant's database to 'Denied'
            echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is expired.";
        }
        else if ($transaction == 'settlement'){
            //update status in payment_response as pending
            $paymentResponse = PaymentResponse::where('order_id',$order_id)->first();
            Log::debug(['paymentResponse' => $paymentResponse]);
            $paymentResponse->transaction_status = $transaction;
            $paymentResponse->update();

            //update status in DB transactions as paid
            $transactions = DB::table('transactions')->where('order_no',$order_id)->update(['status'=> "1012"]);
            Log::debug(['Transactions' => $transactions]);
            $loopDetailTransaction = DB::table('orders')
                                    ->leftJoin('transactions','orders.transaction_id','transactions.id')
                                    ->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
                                    ->where('transactions.order_no',$order_id)
                                    ->groupBy('event_tickets.name')
                                    ->select(DB::raw('count(*) as qty'),DB::raw('sum(orders.order_value) as total_value'),'event_tickets.name as name','orders.visit_date as visit_date','orders.visit_time as visit_time','orders.event_ticket_id as event_ticket_id','orders.order_value as order_value','orders.periode as periode')
                                    ->get();
             Log::debug(['loopDetailTransaction' => $loopDetailTransaction]);
             $getTransaction  = Transaction::where('order_no',$order_id)->first();
             generatePdf($getTransaction);
             $seat = Seat::where('transaction_id', $getTransaction->id)->first();
             if($seat){
                Seat::Where('transaction_id',$getTransaction->id)->update(['status_order'=>1]);
             }
             Log::debug(['getTransaction' => $getTransaction]);

            //send e
            $this->sendMail($loopDetailTransaction,$getTransaction,'[kiosTix] Your transaction with order number '.$getTransaction->order_no.' Success');
             Log::debug(['Pembayaran Sukses' => 'Pembayaran Sukses']);
             session(['ORDER_ID' => $order_id]);
             $cookie = request()->cookie('ORDER_ID');
             Log::debug(['Cookie ORDER_ID' => $cookie]);

            // return redirect('/payment/complete')->with('ORDER_ID', [$order_id]);
            // if($payment_type == "bank_transfer" || $payment_type == "cstore" || $payment_type == "cimb_clicks"){
                
            // }
            // TODO set payment status in merchant's database to 'Settlement'
           // echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
        } 
   
    }

    public function complete(Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            $transaction  = Transaction::latest('created_at')->where('customer_id', $user->id)->first();
            if($transaction){
                if($transaction->status == 1012){
                    $paymentResponse = PaymentResponse::where('order_id',$transaction->order_no)->first();
                    $event = $transaction->event;
                    $date = Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
                    $currentDate = strtotime($date);
                    $futureDate = $currentDate+(60*180);
                    $expTime = date("Y-m-d H:i:s", $futureDate);
                    $expTimeView = date("Y/m/d H:i:s", $futureDate);
                    $expTimeViewDate = date("Y/m/d", $futureDate);
                    $expTimeViewTime = date("H:i:s", $futureDate);
                    $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
                    return view('app.order.complete',compact('transaction','event','paymentResponse','expired_date','expTime','expTimeViewDate','expTimeViewTime','expTimeView'));
                }
            }
        }
        return redirect('/order/fail');
    }
    public function fail() {
        if (Auth::check()) {
            $user = Auth::user();
            $transaction  = Transaction::latest('created_at')->where('customer_id', $user->id)->first();
            if($transaction){
                $paymentResponse = PaymentResponse::where('order_id',$transaction->order_no)->first();
                $event = $transaction->event;
                $date = Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
                $currentDate = strtotime($date);
                $futureDate = $currentDate+(60*180);
                $expTime = date("Y-m-d H:i:s", $futureDate);
                $expTimeView = date("Y/m/d H:i:s", $futureDate);
                $expTimeViewDate = date("Y/m/d", $futureDate);
                $expTimeViewTime = date("H:i:s", $futureDate);
                $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
                return view('app.order.fail',compact('transaction','event','paymentResponse','expired_date','expTime','expTimeViewDate','expTimeViewTime','expTimeView'));
            }
        }
        return redirect('/');
    }
    public function error() {
        if (Auth::check()) {
            $user = Auth::user();
            $transaction  = Transaction::latest('created_at')->where('customer_id', $user->id)->first();
            if($transaction){
                $paymentResponse = PaymentResponse::where('order_id',$transaction->order_no)->first();
                $event = $transaction->event;
                $date = Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
                $currentDate = strtotime($date);
                $futureDate = $currentDate+(60*180);
                $expTime = date("Y-m-d H:i:s", $futureDate);
                $expTimeView = date("Y/m/d H:i:s", $futureDate);
                $expTimeViewDate = date("Y/m/d", $futureDate);
                $expTimeViewTime = date("H:i:s", $futureDate);
                $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
                return view('app.order.fail',compact('transaction','event','paymentResponse','expired_date','expTime','expTimeViewDate','expTimeViewTime','expTimeView'));
            }
        }
        return redirect('/');
    }
    public function orderFail(){
        if (Auth::check()) {
            $user = Auth::user();
            $transaction  = Transaction::latest('created_at')->where('customer_id', $user->id)->first();
            if($transaction){
                $paymentResponse = PaymentResponse::where('order_id',$transaction->order_no)->first();
                $event = $transaction->event;
                $date = Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
                $currentDate = strtotime($date);
                $futureDate = $currentDate+(60*180);
                $expTime = date("Y-m-d H:i:s", $futureDate);
                $expTimeView = date("Y/m/d H:i:s", $futureDate);
                $expTimeViewDate = date("Y/m/d", $futureDate);
                $expTimeViewTime = date("H:i:s", $futureDate);
                $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
                return view('app.order.fail',compact('transaction','event','paymentResponse','expired_date','expTime','expTimeViewDate','expTimeViewTime','expTimeView'));
            }
        }
        return redirect('/');
    }
    private function sendMail($transactions,$transaction, $subject)
    {
        $email = $transaction->customer->email;
        $event = $transaction->event;
        $response = PaymentResponse::where('order_id',$transaction->order_no)->first();
        $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
        $payment_date = Carbon::parse($transaction->updated_at)->addDays(1)->format('d/m/Y h:i A');
        if ($transaction->event->type == '["E-Voucher"]') {
            $attach=env('STORAGE_EVOUCHER_PATH').$transaction->event->id.'/E-Voucher_'.$transaction->order_no.'.pdf';
        }elseif($transaction->event->type == '["Voucher Electronic"]'){
            $attach=env('STORAGE_EVOUCHER_PATH').$transaction->event->id.'/E-Voucher_'.$transaction->order_no.'.pdf';
        }else{
            $attach=env('STORAGE_ETICKET_PATH').$transaction->event->id.'/E-Ticket_'.$transaction->order_no.'.pdf';
        }
        Mail::send('email.complete_order_'.$transaction->payment->type, compact('transactions','transaction','event','response','expired_date','payment_date'), function ($message) use ($email, $subject, $attach) {
             $message->from('booking@kiostix.com', 'Transaction');
             $message->attach($attach);
             $message->to($email)->subject($subject);
        });
    }

    private function sendMailExpired($transaction, $subject)
    {
        $email = $transaction->customer->email;
        $event = $transaction->event;
        $response = PaymentResponse::where('order_id',$transaction->order_no)->first();
        $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
        $payment_date = Carbon::parse($transaction->updated_at)->addDays(1)->format('d/m/Y h:i A');
        Mail::send('email.expired', compact('transaction','event','response','expired_date'), function ($message) use ($email, $subject) {
             $message->from('booking@kiostix.com', 'kiosTix');
             $message->to($email)->subject($subject);
         });
    }
}    