<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserInformation;
use App\Models\UserAddress;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    
    protected function redirectTo()
    {
        return url()->previous();
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $message = [
            'fullname.required'    => 'Mohon isi nama lengkap anda',
            'email.required'  => 'Mohon isi email anda dengan benar',
            'email.email'  => 'Mohon isi email anda dengan benar',
            'email.unique'  => 'Email anda sudah pernah digunakan sebelumnya',
            'phone.required'  => 'Mohon isi nomor telepon anda dengan benar',
            'dob.older_than'  => 'Minimal umur anda 15 tahun',
            'password.required'  => 'Mohon isi password anda',
            'password.min:6'  => 'Password minimal 6 karakter',
            'password.confirmed'  => 'Password anda tidak sama',
            'country_id.required'  => 'Mohon pilih negara asal anda',
        ];
        return Validator::make($data, [
            'fullname' => 'required|max:255',
            'phone' => 'required|max:255',
            'country_id' => 'required',
            'dob' => 'required|olderThan:15',
            'email' => 'required|email|max:255|unique:users|confirmed',
            'password' => 'required|min:6|confirmed',
        ], $message);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $lastID = User::latest('created_at')->first()->customer_id;
        $user = User::create([
            'name' => $data['fullname'],
            'customer_id' => $this->generateID($lastID),
            'email' => $data['email'],
            'status'   => 1001,
            'profile'   => true,
            'password' => bcrypt($data['password']),
        ]);
        $information = new UserInformation;
        $information->user_id = $user->id;
        $information->phone = $data['phone'];
        $information->phone_code = $data['phone_code'];
        // $information->email = $data['email'];
        $information->dob = Carbon::createFromFormat('d/m/Y',$data['dob']);
        $information->save();
        $address = new UserAddress;
        $address->user_information_id = $information->id;
        $address->country_id = $data['country_id'];
        $address->save();
        $user->syncRoles(['Customer']);
        return  $user;
    }

    private function generateID($lastID){
        $now = Carbon::now()->format('my');
        $ddyy    = substr($lastID, 0, 4);
        $number  = substr($lastID, 4);
        if ($ddyy == $now) {
            $no =  str_pad($number+1, 5, "0", STR_PAD_LEFT);  //00002
        }else{
            $number  = 00000;
            $no =  str_pad($number+1, 5, "0", STR_PAD_LEFT);  //00002
        }
        return $now.$no;
    }
}
