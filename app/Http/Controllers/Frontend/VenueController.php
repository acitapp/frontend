<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Auth;

use DB;
use App\Models\Venue;
use Carbon\Carbon;
use Request;
use Response;

class VenueController extends Controller {
	/**
	 * View
	 */
	public function view($locale,$slug) {
		$venue = Venue::findBySlug($slug);
		$latests = Venue::latest('created_at')->enabled()->limit(6)->get();
		if($venue){
			return view('app.venue.view',compact('venue','latests'));
		}
		return redirect('/');
	}
}
