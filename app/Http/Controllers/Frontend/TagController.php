<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Auth;

use DB;
use App\Models\Event;
use App\Models\TaggingTag;
use Conner\Tagging\Model\Tag;
use Conner\Tagging\Model\TagGroup;
use Carbon\Carbon;
use Request;
use Response;

class TagController extends Controller {
	/**
	 * View
	 */
	public function view($locale,$slug) {
		$tag = TaggingTag::findBySlug($slug);
		$tags = TaggingTag::limit(15)->get()->sortBy('count');
		if($tag){
			$events = Event::withTranslation()->with('media')->with('schedule')->with('schedules')->withAnyTag([$slug])->simplePaginate(16);
			return view('app.event.tag',compact('tag','events','tags'));
		}
		return redirect('/');
	}
}
