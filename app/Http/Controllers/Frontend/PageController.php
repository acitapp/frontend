<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use App\Http\Requests\CareerSubmissionRequest;
use Auth;

use DB;
use App\Models\Page;
use App\Models\Career;
use App\Models\CareerSubmission;
use Carbon\Carbon;
use Request;
use Response;
use Mail;

class PageController extends Controller {
	/**
	 * View
	 */
	public function view($locale,$id,$slug) {
		$page  = Page::find($id);
		if($page){
			$careers = Career::undeleted()->latest('created_at')->get();
			return view('app.page.'.$page->template,compact('page','careers'));
		}
		return redirect('/');
	}
	public function careerApply($locale) {
			$careers = Career::listsTranslations('position')->pluck('position', 'id');
			return view('app.page.career-apply',compact('careers'));
	}
	public function careerSubmission(CareerSubmissionRequest $request) {
		$now             = Carbon::now();
		$datenow         = $now->format('YmdHis');
		$file            = $request->file('cv');
		$filename        = $file->getClientOriginalName();
		$imgname         = preg_replace('/\s+/', '', $filename);
		$filename        = $datenow.'_'.$imgname;
		$extension       = $file->getClientOriginalExtension();
		$realpath        = $file->getRealPath();
		$size            = $file->getSize();
		$mimetype        = $file->getMimeType();
		$destinationPath = public_path().'/uploads/cv';
		$file->move($destinationPath, $filename);
		$submission          = new CareerSubmission;
		$submission->career_id    = $request->career_id;
		$submission->name    = $request->name;
		$submission->email   = $request->email;
		$submission->phone   = $request->phone;
		//$submission->address = $request->address;
		$submission->cv      = $filename;
		$submission->save();
		flash()->success('Thank you, We will contact you as soon as possible.');
		return redirect()->back();
	}

	public function send() {
		if (Request::ajax()) {
			$name     = $_POST['name'];
			$email    = $_POST['email'];
			$phone    = $_POST['phone'];
			$subject  = $_POST['subject'];
			$messages  = $_POST['message'];

			Mail::send('email.contact', compact('name','email','subject','messages','phone'), function ($message) use ($name,$email,$phone,$subject,$messages) {
					$message->from($email, $name);
					$message->to('cs@kiostix.com')->subject($subject);
				});

			// SEND To inquiry@grdsteel.com
			return Response::json('Thank You');
		}
	}
}
