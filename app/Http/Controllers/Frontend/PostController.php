<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Auth;

use DB;
use App\Models\Post;
use Carbon\Carbon;
use Request;
use Response;

class PostController extends Controller {

	/**
	 * Index
	 */
	public function index() {
		$posts  = Post::latest('created_at')->undeleted()->simplePaginate(8);
		return view('app.article.index',compact('posts'));
	}
	/**
	 * View
	 */
	public function view($locale,$id,$slug) {
		$post  = Post::find($id);
		$latests  = Post::latest('created_at')->undeleted()->limit(5)->get();
		if($post){
			return view('app.article.view',compact('post','latests'));
		}
		return redirect('/');
	}
}
