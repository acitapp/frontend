<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Models\Event;
use App\Models\EventTicket;
use App\Models\Schedule;
use Jenssegers\Agent\Agent;
use App\Models\TaggingTag;
use Conner\Tagging\Model\Tag;
use Request;
use Response;
use App\Models\Category;
use App\Models\Categoryable;
use Carbon\Carbon;
use DB;

class FrontendController extends Controller {
	
	public function __construct() {
		$this->middleware('email');
	}


	/**
	 * Homepage
	 */
	
	public function index() {
		
			return redirect('/id');
	}
	public function login($id) {
			$user = Auth::loginUsingId($id);
			if ( ! $user)
			{
			    throw new Exception('Error logging in');
			}
			return redirect('/id');
	}
	public function maintenance() {
		return view('app.page.maintenance');
	}
	public function home($locale) {
		if (Auth::check()) {
			if (Auth::user()->status == 1001) {
				if(mobile()){
					return view('mobile.home');
				}
				return view('app.page.home');
			}
			return redirect('/update-profile');
		}
		if(mobile()){
			return view('mobile.home');
		}
		return view('app.page.home');
	}
	/**
	 * Search
	 */
	public function search($locale) {
		$search     = \Request::get('q');//<-- we use global request to get the param of URI
		$filter     = \Request::get('filter');//<-- we use global request to get the param of URI
		$category  = Category::find(1);
		if ($filter) {
			switch ($filter) {
			    case 'terbaru':
					$events = Event::withTranslation()->with('media')->with('schedule')->with('schedules')->live()->latest('created_at')->get();
					return view('app.event.filter-result',compact('filter','events'));
			        break;
			    case 'terpopuler':
					$events = Event::withTranslation()->with('media')->with('schedule')->with('schedules')->live()->orderBy('view', 'desc')->latest('created_at')
					->get();
					return view('app.event.filter-result',compact('filter','events'));
			        break;
			    case 'termurah':
					$events = $category->events()->withTranslation()->with('media')->with('schedule')->with('schedules')->join('schedules', 'schedules.event_id', '=', 'events.id')
					->join('event_tickets', 'event_tickets.schedule_id', '=', 'schedules.id')
					->orderBy('event_tickets.value','asc')
					->where('events.status',1005)
					->where('events.trash', false)
					->where('events.live_date', '<=', Carbon::now())
					->groupBy('events.id')
	                ->select(
	                    'events.id AS id'
	                )->get();
					return view('app.event.filter-result',compact('filter','events'));
			        break;
			    case 'terdekat':
					$events = Event::withTranslation()->with('media')->with('schedule')->with('schedules')->join('event_translations','event_translations.event_id','events.id')
           		    ->join('schedules', 'schedules.event_id', '=', 'events.id')
					->where('events.status',1005)
					->where('events.trash', false)
					->where('events.live_date', '<=', Carbon::now())
					->orderBy(DB::raw('ABS(DATEDIFF(schedules.started_at, NOW()))'))
					->groupBy('events.id')
	                ->select(
	                    'events.id AS id'
	                )->get();
					return view('app.event.filter-result',compact('filter','events'));
			        break;
			    case 'termahal':
					$events = Event::withTranslation()->with('media')->with('schedule')->with('schedules')->join('schedules', 'schedules.event_id', '=', 'events.id')
					->join('event_tickets', 'event_tickets.schedule_id', '=', 'schedules.id')
					->orderBy('event_tickets.value','desc')
					->where('events.status',1005)
					->where('events.trash', false)
					->where('events.live_date', '<=', Carbon::now())
					->groupBy('events.id')
	                ->select(
	                    'events.id AS id'
	                )->get();
					return view('app.event.filter-result',compact('filter','events'));
			        break;
			    default:
					return view('app.event.filter-result',compact('filter','events'));
			}
		}
		$events      = Event::withTranslation()->with('media')->with('schedule')->with('schedules')->whereTranslationLike('title', "%$search%")
						->orWhereTranslationLike('description', "%$search%")
						->latest('created_at')
						->where('status', 1005)
						->where('trash', false)
						->where('live_date', '<=', Carbon::now())
						->simplePaginate(24);
		$title      = 'Hasil pencarian: '.$search;
		$tags = TaggingTag::limit(15)->get()->sortBy('count');
		return view('app.event.search', compact('events', 'title','tags'));
	}
	/**
	 * Search
	 */
	public function filter($locale) {
		$request     = Input::all();
		if ($request['start_date'] && $request['end_date'] != null) {
			$start 		= Carbon::createFromFormat('d/m/Y',$request['start_date']);
			$end 		= Carbon::createFromFormat('d/m/Y',$request['end_date']);
			$events      = Event::live()->whereBetween('live_date', [$start,  $end])->simplePaginate(8);
			$title      = 'Hasil pencarian event tanggal '.$request['start_date'].' sampai '.$request['end_date'];
			return view('app.event.search', compact('events', 'title'));
		}
		return redirect()->back();
	}

	public function sortBy(\Illuminate\Http\Request $request){
		$value = $request->value;
		$category_id = $request->category;
		$lang = $request->lang;
		$category = Category::find($category_id);
		if ($value == "terbaru") {
			$events = $category->events()->live()->latest('created_at')->simplePaginate(16);
			$title      = $category->getTranslation($lang)->title.' berdasarkan yang '.$value;
			return view('app.event.loop-list',compact('category','events','title'));
		}
		else if($value == "termurah"){
			$events = $category->events()->live()->simplePaginate(16);
			$title      = $category->getTranslation($lang)->title.' berdasarkan yang '.$value;
			return view('app.event.loop-list',compact('category','events','title','lang'));
		}
		else if($value == "termahal"){
			$events = $category->events()->live()->simplePaginate(16);
			$title      = $category->getTranslation($lang)->title.' berdasarkan yang '.$value;
			return view('app.event.loop-list',compact('category','events','title','lang'));
		}
		else if($value == "terpopuler"){
			$events = $category->events()->live()->where('recomended',1)->simplePaginate(16);
			$title      = $category->getTranslation($lang)->title.' berdasarkan yang '.$value;
			return view('app.event.loop-list',compact('category','events','title','lang'));
		}
	}

	public function find(Request $request)
	{
		$search     = \Request::get('q');//<-- we use global request to get the param of URI
		$events =	Event::search($search)
					->join('media', 'events.id', '=', 'media.model_id')
					->join('event_translations', 'events.id', '=', 'event_translations.event_id')
					->join('schedules', 'events.id', '=', 'schedules.event_id')
					->join('venues', 'venues.id', '=', 'schedules.scheduleable_id')
	                ->join('venue_informations','venues.id','venue_informations.id')
	                ->join('countries','venue_informations.country_id','countries.id')
	                ->join('states','venue_informations.state_id','states.id')
	                ->join('cities','venue_informations.city_id','cities.id')
      				->where('event_translations.locale','id')
      				->where('media.model_type','App\Models\Event')
	                ->where('events.status',1005)
					->where('events.trash', false)
					->where('events.live_date', '<=', Carbon::now())
	                ->select(
	                    'events.id AS id',
	                    'events.url AS url',
	                    'events.status AS status',
	                    'media.id AS media_id',
	                    'events.live_date AS live_date',
	                    'schedules.started_at AS start_date',
	                    'schedules.ended_at AS end_date',
	                    'venues.title AS venue',
	                    'countries.name AS country',
	                    'states.name AS state',
	                    'cities.name AS city'
	                )->get();
		$unique = $events->unique('id')->toArray();
	    return $unique;
	}
}
