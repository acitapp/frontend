<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Request;
use Response;
use Auth;

use App\Models\Transaction;

class StateController extends Controller {
	/**
	 * Search Data.
	 **/
	public function search() {
		if (Request::ajax()) {
			$id =	$_POST['id'];
			$state = State::find($id);
			$img = count($state->getMedia('states'));
			$cities = $state->cities()->undeleted()->pluck('name','id');
			$data = array(
				'data' =>  $cities,
				'search' => 'state',
				'img' => $img,
			);
			return Response::json($data);
		}
	}

	public function generator($transaction_id){
		//dd(env('ASSETS_URL')."-".env('MEDIA_URL')."-".env('STORAGE_QRCODE_URL'));
		$transaction=Transaction::where('order_no',$transaction_id)->first();
		generatePdf($transaction);
	}
}
