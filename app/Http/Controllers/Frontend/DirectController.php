<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Veritrans\Midtrans;
use App\Models\PaymentResponse;
use App\Models\Transaction;
use Carbon\Carbon;
use DB;
use Mail;
use Storage;
use File;
use PDF;

class DirectController extends Controller {
    public function __construct()
    {   

        //live
        Veritrans::$serverKey = 'VT-server-F_0ydHwCA3WIlpQTB3j4WsE9';
        Veritrans::$isProduction = true;
        //sanbox
        // Veritrans::$serverKey = 'VT-server-RJ4oQLxaMHap300gJh2ozmOY';
        // Veritrans::$isProduction = false;
    }
	public function complete(Request $request) {

		$value = $request->session()->pull('transaction');
		
        $midtrans = new Midtrans;
        $json_result = file_get_contents('php://input');
        $json_decode = json_decode($json_result);
        $result = json_decode(json_encode($json_decode), true);

        Log::debug($result);
        Log::debug($value);
        if ($result) {
			$value = session('transaction');
			if ($value) {
		        $key = decrypt($value);
		        $transaction  = Transaction::where('order_no',$key)->first();
		        $event = $transaction->event;
		        $paymentResponse = PaymentResponse::where('order_id',$key)->first();
		        $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
		        return view('app.order.thanks',compact('transaction','event','paymentResponse','expired_date'));
			}
        }
		return redirect('/order/fail');
	}
	public function fail() {
		$value = $request->session()->pull('transaction');
		if ($value) {
	        $key = decrypt($value);
	        $transaction  = Transaction::where('order_no',$key)->first();
	        $event = $transaction->event;
	        $paymentResponse = PaymentResponse::where('order_id',$key)->first();
	        $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
	        return view('app.order.fail',compact('transaction','event','paymentResponse','expired_date'));
		}
		return redirect('/');
	}
	public function error() {
		$value = $request->session()->pull('transaction');
		if ($value) {
	        $key = decrypt($value);
	        $transaction  = Transaction::where('order_no',$key)->first();
	        $event = $transaction->event;
	        $paymentResponse = PaymentResponse::where('order_id',$key)->first();
	        $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
	        return view('app.order.error',compact('transaction','event','paymentResponse','expired_date'));
		}
		return redirect('/');
	}
	public function orderFail(){
		$value = session('transaction');
		if ($value) {
	        $key = decrypt($value);
	        $transaction  = Transaction::where('order_no',$key)->first();
	        $event = $transaction->event;
	        $paymentResponse = PaymentResponse::where('order_id',$key)->first();
	        $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
	        return view('app.order.error',compact('transaction','event','paymentResponse','expired_date'));
		}
		$event = null;
	    return view('app.order.fail',compact('event'));
	}
}
