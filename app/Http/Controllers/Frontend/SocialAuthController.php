<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use App\Models\SocialAccountService;
use Illuminate\Http\Request;
use Redirect;
use Socialite;

class SocialAuthController extends Controller {
	public function callback(SocialAccountService $service, $provider,Request $request) {
		if (! $request->input('code')) {
	        return redirect('login')->withErrors('Login failed: '.$request->input('error').' - '.$request->input('error_reason'));
	    }
		$user = $service->createOrGetUser(Socialite::driver($provider));
		auth()->login($user);
		if ($user->profile == true) {
			return redirect('/');
		}
		return redirect('/update-profile');
	}
	public function redirect($provider) {
		return Socialite::driver($provider)->redirect();
	}
}
