<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Models\Event;
use App\Models\EventTicket;
use App\Models\Schedule;
use Jenssegers\Agent\Agent;
use App\Models\TaggingTag;
use Conner\Tagging\Model\Tag;
use Request;
use Response;
use App\Models\Category;
use App\Models\Categoryable;
use Carbon\Carbon;
use DB;

class CustomController extends Controller {
	
	public function __construct() {
		$this->middleware('email');
	}
	/**
	 * Homepage
	 */
	public function index($locale,$custom) {
		//
		$category = Category::whereTranslation('slug', $custom)->first();
		$page     = \Request::get('page');//<-- we use global request to get the param of URI
		if($category){
			if($page){
				$events = $category->events()->live()->latest('created_at')->get();
				
				return view('custom.'.$custom.'.'.$page,compact('category','events','custom','page'));
			}
			$events = $category->events()->live()->latest('created_at')->get();
			return view('custom.'.$custom.'.index',compact('category','events','custom'));
		}
		return redirect('/');
	}
}
