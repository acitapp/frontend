<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;

use App\Models\Comment;
use App\Models\Event;
use Auth;
use Request;
use Response;

class CommentController extends Controller {

	public function comment() {
		if (Request::ajax()) {
			if (Auth::check()) {
				$postId    = $_POST['postId'];
				$parrent = $_POST['commentId'];
				if ($parrent == 0) {
					$parrent = null;
				}
				$post           = Event::find($postId);
				$message 		= $_POST['commentMessage'];	
				if (!empty($message)) {
					$data = $this->createComment($message,$post,$parrent);
					return Response::json($data);
				} else {
					$data = array(
						'status' => 3,
					);
					return Response::json($data);
				}

			}
			$data = array(
				'status' => 3,
			);
			return Response::json($data);
		}
	}
	public function likeComment() {
		if (Request::ajax()) {

			if (Auth::check()) {
				$commentId = $_POST['commentId'];
				$comment   = Comment::find($commentId);
				$comment->increment('like', 1);
				$data = array(
					'comment_id' => $commentId,
				);
			} else {
				$data = array(
					'status' => 3,
				);
				return Response::json($data);
			}
			return Response::json($data);
		}
	}
	public function dislikeComment() {
		if (Request::ajax()) {
			if (Auth::check()) {
				$commentId = $_POST['commentId'];
				$comment   = Comment::find($commentId);
				$comment->increment('dislike', 1);
				$data = array(
					'comment_id' => $commentId,
				);
			} else {
				$data = array(
					'status' => 3,
				);
				return Response::json($data);
			}
			return Response::json($data);
		}
	}
	public function reportComment() {
		if (Request::ajax()) {

			if (Auth::check()) {

				$commentId = $_POST['commentId'];
				$comment   = Comment::find($commentId);
				$comment->increment('report', 1);
				$data = array(
					'comment_id' => $commentId,
				);
			} else {
				$data = array(
					'status' => 3,
				);
				return Response::json($data);
			}
			return Response::json($data);
		}
	}
	private function  createComment($message,$post,$parrent)
	{
			$user   = Auth::user();
			$avatar = "images/default_square.jpg";
			if (count($user->getMedia('avatar'))) {
				$avatar = $user->getMedia('avatar')->first();
				$avatar = "media/".$avatar->id."/conversions/square.jpg";
			}
			$post->increment('comment', 1);
			$comment             = new Comment;
			$comment->user_id    = $user->id;
			$comment->content    = $message;
			$comment->like       = 0;
			$comment->dislike    = 0;
			$comment->report     = 0;
			$comment->comment_id = $parrent;
			$post->comments()->save($comment);
			$data = array(
				'name'       => $user->name,
				'userslug'   => $user->slug,
				'comment'    => $message,
				'date'       => $comment->created_at,
				'avatar'     => $avatar,
				'post_id'    => $post->id,
				'comment_id' => $parrent,
				'parent_id'  => $parrent,
			);
			return $data;
	}
}
