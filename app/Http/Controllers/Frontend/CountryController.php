<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Auth;

use DB;
use App\Models\Country;
use App\Models\Category;
use App\Models\Venue;
use App\Models\Venueable;
use App\Models\Event;
use Carbon\Carbon;
use Request;
use Response;

class CountryController extends Controller {
	/**
	 * View
	 */
	public function view($locale,$slug) {
		$category  = Category::find(7);
		if($slug == 'international'){
			if($category){
				$events = $category->events()->latest('created_at')->simplePaginate(16);
				$countries = Country::undeleted()->hasEvent()->latest('created_at')->limit(12)->get();
				return view('app.event.attactions',compact('category','events','countries'));
			}
		}
		$country  = Country::findBySlug($slug);
		if($country){
			$countries = $country->venueinformations()->simplePaginate(16);
			$venues = $country->venueinformations()->pluck('venue_id')->toArray();
			$events = Event::latest('created_at')->simplePaginate(16);
			if(count($venues)){
				$eventId = Venueable::where('venue_id',$venues)->pluck('venueable_id')->toArray();
				$events = Event::whereIn('id',$eventId)->latest('created_at')->simplePaginate(16);
			}
			if($slug == 'indonesia'){
				$states = $country->states()->undeleted()->hasEvent()->latest('created_at')->limit(12)->get();
				return view('app.event.indonesia',compact('country','events','states','category'));
			}
			return view('app.event.country',compact('country','events','category'));
		}
		return redirect('/');
	}
	/**
	 * Search Data.
	 **/
	public function search() {
		if (Request::ajax()) {
			$id =	$_POST['id'];
			$country = Country::find($id);
			$img = count($country->getMedia('countries'));
			$countries = $country->states()->undeleted()->pluck('name','id');
			$data = array(
				'data' =>  $countries,
				'search' => 'country',
				'img' => $img,
			);
			return Response::json($data);
		}
	}
}
