<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\OrderRequest;
use Auth;
use App\Models\Event;
use App\Models\EventTicket;
use App\Models\Schedule;
use App\Models\Transaction;
use App\Models\Page;
use App\Models\Seat;
use App\Models\Venue;
use App\Models\Order;
use Jenssegers\Agent\Agent;
use App\Models\TaggingTag;
use Conner\Tagging\Model\Tag;
use Request;
use Response;
use App\Models\Category;
use App\Models\Categoryable;
use Carbon\Carbon;
use DB;
use URL;
use Cache;
use File;
use PDF;

class AsiangamesController extends Controller {
	
	public function __construct() {
		$this->middleware('email');
	}
	/**
	 * Homepage
	 */
	public function master() {

		
		$que = getOption('app_queue_url');
		$queueittoken     = \Request::get('queueittoken');//<-- we use global request to get the param of URI
		if ($queueittoken) {
	        $custom='asian-games-2018';
			$category = Category::whereTranslation('slug', $custom)->first();
			
			if($category){
				$events = $category->events()->where('parent_id',0)->live()->latest('created_at')->get();
				
				return view('custom.'.$custom.'.index',compact('category','events','custom'));
			}
		}
		return redirect($que);

    }
	public function index($locale) {
		//
		
        $custom='asian-games-2018';
		$category = Category::whereTranslation('slug', $custom)->first();
		
		if($category){
			$events = $category->events()->where('parent_id',0)->live()->latest('created_at')->get();
			
			return view('custom.'.$custom.'.index',compact('category','events','custom'));
		}
		return redirect('/');
    }
    /**
	 * Homepage
	 */
	public function schedules($locale) {
        //
        $category = Category::whereTranslation('slug', 'asian-games-2018')->first();
		$custom='asian-games-2018';
		$events = $category->events()->live()->latest('created_at')->get();
		return view('custom.asian-games-2018.schedules',compact('category','events','custom'));
		
    }
    /**
	 * location page
	 */
	public function location($locale) {
        //
		$custom='asian-games-2018';
		$venue = Venue::with('info')
		->leftJoin('venue_informations','venues.id','venue_informations.venue_id')
		->leftJoin('cities','venue_informations.city_id','cities.id')
		->leftJoin('venueables','venues.id','venueables.venue_id')
		->leftJoin('categoryables','venueables.venueable_id','categoryables.categoryable_id')
		->leftJoin('category_translations','categoryables.category_id','category_translations.category_id')
		->where('category_translations.slug',$custom)
		->groupBy('venues.id')
		->select(DB::raw('venues.*'))
		->paginate(12);
		// dd($venue);
        // $venue = Venue::latest('created_at')->with('events')->enabled()->paginate(12);
        
		return view('custom.asian-games-2018.location',compact('venue','custom'));
		
    }
     /**
	 * Detail  venue
	 */
	public function viewVenue($locale,$slug) {
		//
		$custom='asian-games-2018';
		
		$venue = Venue::findBySlug($slug);
	
        $latests = Venue::latest('created_at')->enabled()->limit(6)->get();
		if($venue){
			return view('custom.asian-games-2018.location-view',compact('venue','latests','custom'));
		}
		return redirect('/');
    }
     /**
	 * Detail  event
	 */
	public function viewEvent($locale,$id,$slug) {
		$custom='asian-games-2018';
        $event = Cache::remember('event-'.$id,1440,function()  use ($id) {
			return Event::withTranslation()->with('venues')->with('categories')->with('schedules')->find($id);
	    });
		$agent = new Agent();
		$seat = new Seat();
		if($event){
			
			$eventChild = Event::where('parent_id',$event->id)->live()->get();
			$venues = $event->venues;
			
			//$comments = $event->comments()->simplePaginate(5);
			if(count($eventChild)>0){
				return view('custom.asian-games-2018.category-sport',compact('event','eventChild','agent','custom'));
			}

			
			if ($event->template) {
			
				return view('custom.asian-games-2018.event-view',compact('event','venues','agent','seat','custom'));
			}
			if (Auth::check()) {
				$user = Auth::user();
				$date = new \DateTime();
                $date->modify('-30 minutes');
				$formatted_date = $date->format('Y-m-d H:i:s');
				
				$updatestatusseat = Seat::where(function($query) use ($formatted_date){
					$query->where('status_order', '3');
					$query->where('date_order', '<',$formatted_date);
				})->orWhere(function($query) use ($user){
					$query->where('status_order', '3');
					$query->where('customer_id',$user->id);
				})->update(['status_order'=>0,'transaction_id'=>0,'customer_id'=>0]);
            
            
				if (checkRole(['Super Admin','Client','Super Client'],$user->id)) {
					return view('custom.asian-games-2018.event-view',compact('event','venues','agent','seat','custom'));
				}
			}
			if ($event->status == 1005 && $event->trash == false && $event->live_date <= Carbon::now()) {
				$event->increment('view', 1);
				return view('custom.asian-games-2018.event-view',compact('event','venues','agent','seat','custom'));
			}
		}
		return redirect('/');
	}
	public function setTransaction(OrderRequest $request)
	{
		$transaction = $this->createTransaction($request);
		return redirect(URL::to('/').'/id/asian-games-2018/seat/'.$request->tiket.'/'.encrypt($transaction->order_no));
	}
	/**
	 *  get Seat
	 */
	public function getSeat($locale,$ticket,$key)
	{
		
		$custom='asian-games-2018';
		$customer = Auth::user()->id ?? null;;
		$key = decrypt($key);
		// $key ='18062400007';
		$transaction  = Transaction::where('order_no',$key)->InCartStatus()->first();
		$tickets=EventTicket::where('id',$ticket)->first();
		$seat =  Seat::where('tiket_id',$ticket)->get();
		$id=$transaction->event_id;
		$event = Cache::remember('event-'.$id,1440,function()  use ($id) {
			return Event::withTranslation()->with('venues')->with('categories')->with('schedules')->with('venues')->find($id);
		});
		
		$schedules =  Schedule::where('id',$tickets->schedule_id)->first();
		// dd($schedules);
        if(isset($seat[0]->id)){
                $seat = $seat->toJson();
                return view('custom.asian-games-2018.seating',compact('transaction','schedules','custom','tickets','seat','customer','event'));
        }
        return redirect('/');
	}
	  /**
	 *  check Seat
	 */
	public function checkSeat(Request $request)
	{
       
        $id = $_POST['id'];
        $customer = Auth::user()->id ?? null;;
		$transactionId=  $_POST['transaction'];
        $seat = Seat::where('id',$id)->first();
        
        $result['status'] ='0';
        $result['msg'] = 'proses gagal';
        if($seat){
            if($seat->customer_id== $customer && $seat->status_order==3 && $seat->transaction_id==$transactionId){
                $seat->status_order=0;
				$seat->customer_id=0;
				$seat->transaction_id=0;
                $seat->update();
				$result['status'] ='4';
				$result['seat'] =$seat;
				
                $result['msg'] = 'proses berhasil order di batalkan';
            }
            else if($seat->customer_id!= $customer && $seat->status_order==2){
                $result['status'] ='2';
				$result['seat'] =$seat;
				$result['msg'] = 'seat telah di booking';
            }
            else if( $seat->status_order==1){
                $result['status'] ='1';
				$result['seat'] =$seat;
				$result['transaction_id'] =1;
                $result['msg'] = 'seat telah di beli';
            }else{
                $seat->status_order=3;
				$seat->customer_id=$customer;
				$seat->date_order=date('Y-m-d H:i:s');
				$seat->transaction_id=$transactionId;
                $seat->update();
                $result['status'] ='3';
				$result['seat'] =$seat;
				
                $result['msg'] = 'proses berhasil seat telah di simpan';
            }

        }
        return response()->json($result);
	}
	/**
	 *  order
	 */
	public function order(OrderRequest $request,$locale)
	{
		
		$transaction = Transaction::find($request->transaction);
		$transaction->quantity =array_sum($request->qty);
		$transaction->update();
		$orders = $this->createOrder($request, $transaction);
		return redirect(URL::to('/').'/booking/'.encrypt($transaction->order_no));

	}
	/**
	 * Create Transaction
	 */
	private function  createTransaction($request)
	{
		$transaction = new Transaction;
		$transaction->order_no = orderNo();
		$transaction->status = 1011;
		$transaction->event_id = $request->event;
		$transaction->venue_id = $request->venue;
		$transaction->quantity = 0;
		$transaction->value_sub_total = 0;
		$transaction->value_grand_total = 0;
		$transaction->sales_channel_id = 1;
		$transaction->ref_type = 'web';
		$transaction->customer_id  = Auth::user()->id ?? null;
		$transaction->save();
	 	return $transaction;
	}
	/**
	 * Create Order
	 */
	private function  createOrder($request,$transaction)
	{
		$tickets = $this->mapping($request->ticket, 'ticket');
		$quantities = $this->mapping($request->qty, 'quantity');
		$i=0;
		$orders = array();
		foreach($tickets as $value) {
			  $orders[] = array_merge($value,$quantities[$i]);
		      $i++;
		}
		$orders = collect($orders);
		$orders = $orders->filter(function($item) {
			    return $item['quantity'] != 0;
			});
		$subtotal = 0;
		foreach ($orders as $order) {
			$ticket = getTicket($order['ticket']);
			if ($order['quantity'] > 1) {
				for ($x = 0; $x <= $order['quantity']-1; $x++) {
					$seat = Seat::where('transaction_id',$transaction->id)->where('customer_id',$transaction->customer_id)->where('status_order',2)->skip($x)->take(1)->first();
					$this->newOrder($request,$transaction,$ticket,$order,$seat);
				} 
			}else{
				$seat = Seat::where('transaction_id',$transaction->id)->where('customer_id',$transaction->customer_id)->where('status_order',2)->skip(0)->take(1)->first();
				$this->newOrder($request,$transaction,$ticket,$order,$seat );
			}
		}
		$transaction->value_sub_total = $transaction->orders()->sum('order_value');
		$transaction->value_grand_total = $transaction->orders()->sum('order_value');
		$transaction->save();

	}
	/**
	 *  Mapping Array Request
	 */
	private function  mapping($arrays, $key)
	{
	        $arrays = array_map(function($array) use ($key) {
	                return array(
	                    $key =>  $array,
	                );
	            }, $arrays);
	 	return $arrays;
	}
	/**
	 * New Order
	 */
	private function  newOrder($request,$transaction,$ticket,$order,$seat)
	{
		
		$newOrder = new Order;
		$newOrder->transaction_id = $transaction->id;
		$newOrder->barcode = generateBarcode($transaction->event_id);
		$newOrder->event_ticket_id = $order['ticket'];
		$newOrder->order_value = $ticket->value;
		$newOrder->periode = $ticket->name;
		$newOrder->visit_date = Carbon::createFromFormat('d M Y',date('d M Y',strtotime($request->visit_date))); 
		$newOrder->visit_time = $request->visit_time;
		if($seat){
			$newOrder->seat_mapping_id = $seat['id'];
			$newOrder->seat_row = $seat['seat_row'];
			$newOrder->seat_number = $seat['seat_number'];
		}
		$newOrder->save();
        $barcode= $newOrder->barcode;
        $config['format']='png'; $config['size']=200;
		$path = env('STORAGE_QRCODE_PATH').$transaction->event_id.'/';
		if(!File::exists($path)) {
		    File::makeDirectory($path, $mode = 0777, true, true);
		}
        generateQrcode($barcode,$config,$path,$barcode);
	}
}
