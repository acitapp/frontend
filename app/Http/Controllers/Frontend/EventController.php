<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Auth;

use DB;
use App\Models\Event;
use App\Models\User;
use App\Models\Transaction;
use App\Models\Schedule;
use App\Models\Rating;
use App\Models\Venue;
use App\Models\Seat;
use App\Models\Priceable;
use App\Models\Periodable;
use App\Models\Sectionable;
use App\Models\ApprovalLog;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;
use Request;
use Response;
use Mail;
use Cache;
use QrCode;

class EventController extends Controller {
	public function __construct() {
		$this->middleware('email');
	}

	/**
	 * Index
	 */
	public function index() {
		$events  = Event::latest('created_at')->live()->undeleted()->get();
		return view('app.event.index',compact('events'));
	}
	/**
	 * Rekommended
	 */
	public function recommended() {
		$events  = Event::latest('created_at')->live()->recommended()->get();
		return view('app.event.recommended',compact('events'));
	}
	/**
	 * View
	 */
	public function view($locale,$id,$slug) {
	    $event = Cache::remember('event-'.$id,1440,function()  use ($id) {
			return Event::withTranslation()->with('venues')->with('categories')->with('schedules')->find($id);
	    });
		$agent = new Agent();
		$seat = new Seat();
		if($event){
			$venues = $event->venues;
			//$comments = $event->comments()->simplePaginate(5);
			if ($event->template) {
				return view('app.event.view-custom',compact('event','venues','agent','seat'));
			}
			if (Auth::check()) {
				$user = Auth::user();
				if (checkRole(['Super Admin','Client','Super Client'],$user->id)) {
					return view('app.event.view',compact('event','venues','agent','seat'));
				}
			}
			if ($event->status == 1005 && $event->trash == false && $event->live_date <= Carbon::now()) {
				$event->increment('view', 1);
				return view('app.event.view',compact('event','venues','agent','seat'));
			}
		}
		return redirect('/');
	}

	/**
	 * Approve Event
	 **/
	public function approve() {
		if (Request::ajax()) {
			if (Auth::check()) {
				$user = Auth::user();
				if (checkRole(['Super Admin','Client','Super Client'],$user->id)) {
						$event_id = $_POST['event_id'];
						$published = $_POST['published'];
						$message = $_POST['message'];
						$event = Event::find($event_id);
						$event->status = $published;
						$event->update();

						$approve = new ApprovalLog;
						$approve->event_id = $event->id;
						$approve->user_id = Auth::user()->id;
						$approve->message = $message;
						$approve->save();

						$data = array('published' => $event->status, 'event' => $event->id);
						return Response::json($data);
				}
			}
		}
	}
	/**
	 * Venue Schedule
	 */
	public function venue($locale,$id,$slug,$venue) {
		$event  = Event::find($id);
		$venue = Venue::where('slug',$venue)->first();
		if($venue){
			$comments = $event->comments()->simplePaginate(5);
			return view('app.event.view-venue',compact('event','cities','comments','venue'));
		}
		return redirect('/');
	}
	/**
	 * Search Venue 
	 */
	public function rating() {
		if (Request::ajax()) {
			$event = $_POST['event'];
			$value = $_POST['value'];
			if (Auth::check()) {
				$user   = Auth::user();
				$event  = Event::find($event);
				$rating = new Rating;
				$rating->user_id     = $user->id;
				$rating->value    	 = $value;
				$event->ratings()->save($rating);
				$total = count($event->ratings);
				$sum = $event->ratings->sum('value');
				$event->rate_average = $sum/$total;
				$event->update();
				$data = array(
					'login' => true,
					'rating' => $value,
				);
			}else{
				$data = array(
					'login' => false,
					'rating' => $value,
				);
			}
			return Response::json($data);
		}
	}
	/**
	 * Search Venue 
	 */
	public function searchVenue() {
		if (Request::ajax()) {
			$id = $_POST['id'];
			$event = $_POST['event'];
			$event  = Event::find($event);
			$venues = $event->schedules->first()->venue->where('location_id', $id)->pluck('id','name');
			$data = array(
				'search' => 'venue',
				'data' =>  $venues,
			);
			return Response::json($data);
		}
	}
	/**
	 * Search Date 
	 */
	public function searchDate() {
		if (Request::ajax()) {
			$id = $_POST['id'];
			$event = $_POST['event'];
			$event_venues = DB::table('event_venue')->where('event_id',$event)->where('venue_id',$id)->pluck('id')->toArray();
			$sections = EventSection::whereIn('event_venue_id',$event_venues)->pluck('id')->toArray();
			$dates = DB::table('event_section_dates')->whereIn('event_section_id',$sections)->pluck('event_date_id')->toArray();
			$EventDates = EventDate::whereIn('id',$dates)->select('id', 'start_date')->get()->toArray();

	        $EventDates = array_map(function($date) {
	                return array(
	                    'id' =>  $date['id'],
	                    'startdate' =>  Carbon::parse($date['start_date'])->format('l, jS F Y, h:i:s T')
	                );
	            }, $EventDates);

			$data = array(
				'dates' =>  $dates,
				'event_venues' =>  $event_venues,
				'data' =>  $EventDates,
				'sections' =>  $sections,
				'search' => 'date',
			);
			return Response::json($data);
		}
	}
	/**
	 * Search Section 
	 */
	public function searchSection() {
		if (Request::ajax()) {
			$event = $_POST['event'];
			$schedule = $_POST['schedule'];
			$session = $_POST['session'];
			$visitdate = $_POST['visitdate']; 
	        $schedule = Schedule::find($schedule);
		    $tickets = $schedule->tickets()->get()->toArray();
			$data = array(
				'data' =>  $tickets,
				'schedule' => $schedule->id,
				'session' => $session,
				'visitdate' => $visitdate,
			);
			return Response::json($data);
		}
	}
}
