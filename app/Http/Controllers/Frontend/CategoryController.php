<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Auth;

use DB;
use App\Models\Category;
use App\Models\Country;
use App\Models\State;
use Carbon\Carbon;
use Request;
use Response;
use Cache;
class CategoryController extends Controller {
	public function __construct() {
		$this->middleware('email');
	}
	/**
	 * View
	 */
	public function view($locale,$id,$slug) {
		$country     = \Request::get('country');
		$state     = \Request::get('state');
		$filter     = \Request::get('filter');
	    $category = Cache::remember('view-category-'.$slug,1440,function()  use ($slug) {
				 return  Category::whereTranslation('slug', $slug)->first();
	    });
		if($category){
		    $events = Cache::remember('events-cat-'.$id,1440,function()  use ($id,$category) {
				return $category->events()->withTranslation()->with('media')->with('schedule')->with('schedules')->live()->latest('created_at')->get();
		    });
			if ($filter) {
				switch ($filter) {
				    case 'terpopuler':
						$events = $category->events()->withTranslation()->with('media')->with('schedule')->with('schedules')->live()->orderBy('view', 'desc')->latest('created_at')->get();
						return view('app.event.index',compact('category','events'));
				        break;
				    case 'termurah':
						$events = $category->events()->withTranslation()->with('media')->with('schedule')->with('schedules')
						->join('schedules', 'schedules.event_id', '=', 'events.id')
						->join('event_tickets', 'event_tickets.schedule_id', '=', 'schedules.id')
						->orderBy('event_tickets.value','asc')
						->where('events.status',1005)
						->where('events.trash', false)
						->where('events.live_date', '<=', Carbon::now())
						->groupBy('events.id')
						->get();
						return view('app.event.index',compact('category','events'));
				        break;
				    case 'terdekat':
						$events = $category->events()->withTranslation()->with('media')->with('schedule')->with('schedules')
						->join('schedules', 'schedules.event_id', '=', 'events.id')
						->where('events.status',1005)
						->where('events.trash', false)
						->where('events.live_date', '<=', Carbon::now())
						->orderBy(DB::raw('ABS(DATEDIFF(schedules.started_at, NOW()))'))
						->groupBy('events.id')
						->get();
						return view('app.event.index',compact('category','events'));
				        break;
				    case 'termahal':
						$events = $category->events()->withTranslation()->with('media')->with('schedule')->with('schedules')
						->join('schedules', 'schedules.event_id', '=', 'events.id')
						->join('event_tickets', 'event_tickets.schedule_id', '=', 'schedules.id')
						->orderBy('event_tickets.value','desc')
						->where('events.status',1005)
						->where('events.trash', false)
						->where('events.live_date', '<=', Carbon::now())
						->groupBy('events.id')
						->get();
						return view('app.event.index',compact('category','events'));
				        break;
				    default:
						return view('app.event.index',compact('category','events'));
				}
			}
			if ($category->id == 2) {
				if ($country == null || $country == 'indonesia') {
					return view('app.event.indonesia',compact('category','events','country','states'));
				}elseif ($country == 'international') {
					return view('app.event.attactions',compact('category','events','countries'));
				}else{
					return view('app.event.indonesia',compact('category','events','country','states'));
				}
			}
			return view('app.event.index',compact('category','events'));
		}
		return redirect('/');
	}
}
