<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use Srmklive\PayPal\Services\ExpressCheckout;

use App\Models\Voucher;
use App\Models\Order;
use App\Models\VoucherUser;
use App\Models\Transaction;
use App\Models\PaymentMethod;
use App\Models\PaypalPaymentResponse;
use Carbon\Carbon;
use Mail;
use DB;
use Storage;
use File;
use PDF;
use Illuminate\Http\Request;

class PaypalController extends Controller
{
	protected $provider;
	public function __construct() {
	    $this->provider = new ExpressCheckout();
	}

	public function expressCheckout(Request $request) {
		$key = decrypt($request->key);
	    $transactions = DB::table('orders')
	                        ->leftJoin('transactions','orders.transaction_id','transactions.id')
	                        ->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
	                        ->leftJoin('event_translations','event_tickets.event_id','event_translations.event_id')
	                        ->where('event_translations.locale','id')
	                        ->where('transactions.order_no',$key)
	                        ->groupBy('event_tickets.name')
	                        ->select(DB::raw('count(*) as qty'),DB::raw('sum(orders.order_value) as total_value'),'event_tickets.name as name','event_translations.title as title','orders.visit_date as visit_date','orders.visit_time as visit_time','orders.event_ticket_id as event_ticket_id','orders.order_value as order_value','orders.periode as periode')
	                        ->get();
	                        // dd($transactions);
	    $transaction  = Transaction::where('order_no',$key)->first();
	    
	    if($transaction) {
	    	$event = $transaction->event;
	    	$payment = PaymentMethod::find($request->payment_method_id);
	    	$voucher = Voucher::where('code',$request->voucher_code)->first();
	    	$fee_fixed = $payment->fees()->where('unit','fixed')->first();
	    	$percent = $payment->fees()->where('unit','percent')->first();
	    	$mdr_fixed = $payment->fees()->where('unit','mdr_fixed')->first();
	    	$mdr_percent = $payment->fees()->where('unit','mdr_percent')->first();

	    	$subtotal = $transaction->value_sub_total;
	    	//Get Fee Percent
	    	$fee_percent = ($subtotal/100)*$percent->value;

	    	$transaction->payment_method_id = $payment->id;
	    	$transaction->fee_convenience_percent = $fee_percent;
	    	$transaction->fee_convenience_fixed = $fee_fixed->value;
	    	//Total Convenience Fee
	    	$transaction->fee_convenience_total = $fee_fixed->value+$fee_percent;

	    	//Get MDR Percent
	    	$mdrPercent = ($subtotal/100)*$mdr_percent->value;
	    	$transaction->mdr_percent = $mdrPercent;
	    	$transaction->mdr_fixed = $mdr_fixed->value;
	    	//Total MDR 
	    	$totalMdr = $mdrPercent+$mdr_fixed->value;
	    	//Grand Total
	    	$grand_total = $subtotal+$fee_fixed->value+$fee_percent;
	    	$transaction->value_grand_total = $grand_total;
	    	if ($voucher) {
	    	    $transaction->voucher_code = $request->voucher_code;
	    	    if ($voucher->promotion->unit == 'fixed') {
	    	         $transaction->value_promo = $voucher->promotion->value;
	    	    }elseif($voucher->promotion->unit == 'percent'){
	    	         $transaction->value_promo = ($transaction->value_grand_total/100)*$voucher->promotion->value;
	    	    }
	    	    $grand_total = $subtotal+$fee_fixed->value+$fee_percent-$transaction->value_promo;
	    	    $transaction->value_grand_total = $grand_total;
	    	    $voucher->status = 1000;
	    	    $voucher->update();
	    	    $voucherUser = new VoucherUser;
	    	    $voucherUser->user_id = $transaction->customer_id;
	    	    $voucherUser->voucher_id = $voucher->id;
	    	    $voucherUser->save();
	    	}
	    	//Paid Value
	    	$transaction->paid_value = $grand_total-$totalMdr;
	    	$transaction->update();

		  	// check if payment is recurring
		  	$recurring = $request->input('recurring', false) ? true : false;

		  	// get new invoice id
		  	$invoice_id = $transaction->order_no;
		      
		  	// Get the cart data
		  	$cart = $this->getCart($recurring, $invoice_id, $transaction);
		  	// dd($cart);
		  	//Save Response
		  	$invoice = new PaypalPaymentResponse();
		  	$invoice->invoice_id = $cart['invoice_id'];
		  	$invoice->invoice_description = $cart['invoice_description'];
		  	$invoice->amount = $transaction->value_grand_total;
		  	$invoice->amount_usd = $cart['total'];
		  	$invoice->save();

		  	// send a request to paypal 
		  	// paypal should respond with an array of data
		  	// the array should contain a link to paypal's payment system
		  	$response = $this->provider->setExpressCheckout($cart, $recurring);
		  	// dd($response);
		  	// if there is no link redirect back with error message
		  	if (!$response['paypal_link']) {
		  		$transaction->status = 1016;
        		$transaction->update();
		  		flash()->success('Something went wrong with PayPal');
		    	return redirect('/');
		    	// For the actual error message dump out $response and see what's in there
		  	}
	    }

	  	// redirect to paypal
	  	// after payment is done paypal
	  	// will redirect us back to $this->expressCheckoutSuccess
	  	return redirect($response['paypal_link']);
	}

	private function getCart($recurring, $invoice_id, $transaction)
	{
		$event_name = $transaction->event->getTranslation('id')->title;
		$kurs = $this->kurs();
	    $quotes = $kurs->quotes;
	    $idr = $quotes->USDIDR;
		// dd($event_name);
	    if ($recurring) {
	        return [
	            // if payment is recurring cart needs only one item
	            // with name, price and quantity
	            'items' => $this->productItems($transaction,$idr),

	            // return url is the url where PayPal returns after user confirmed the payment
	            'return_url' => url('/payment/paypal/direct?recurring=1'),
	            'subscription_desc' => 'Monthly Subscription ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
	            // every invoice id must be unique, else you'll get an error from paypal
	            'invoice_id' => $invoice_id,
	            'invoice_description' => $event_name,
	            'cancel_url' => url('/'),
	            // total is calculated by multiplying price with quantity of all cart items and then adding them up
	            // in this case total is 20 because price is 20 and quantity is 1
	            'total' => $totalAmount, // Total price of the cart
	        ];
	    }
	    $order_no = encrypt($invoice_id);
	    $url = url('/payment/paypal/direct/'.$order_no);
	    $items = $this->productItems($transaction,$idr);
	    $totalAmount = 0; 
	    for($i=0; $i<count($items); $i++){
	    	$totalAmount = $totalAmount + $items[$i]["price"];
	    }
	    return [
	        // if payment is not recurring cart can have many items
	        // with name, price and quantity
	        'items' => $this->productItems($transaction,$idr),
	        // return url is the url where PayPal returns after user confirmed the payment
	        'return_url' => $url,
	        // every invoice id must be unique, else you'll get an error from paypal
	        'invoice_id' => $invoice_id,
	        'invoice_description' => $event_name,
	        'cancel_url' => url('/'),
	        // total is calculated by multiplying price with quantity of all cart items and then adding them up
	        // in this case total is 20 because Product 1 costs 10 (price 10 * quantity 1) and Product 2 costs 10 (price 5 * quantity 2)
	        'total' => $totalAmount,
	    ];
	}

	private function kurs() {
		$url = "http://apilayer.net/api/live?access_key=76b2b827fb9e7b324915e1ae12c337ae&currencies=IDR";
		$data = curl_init();
		curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($data, CURLOPT_URL, $url);
		$hasil = curl_exec($data);
		curl_close($data);
		return json_decode($hasil);
	}

	private function productItems($transaction,$idr)
	{
	    $productItems = $transaction->orders->toArray();
	    $fee = array(array(
	                'name'          => 'Convenience Fee',
	                'price'         => round($transaction->fee_convenience_total / $idr,2),
	                'qty'      		=> 1
	            ));
	    $voucher = array(array(
	                'name'          => 'Voucher',
	                'price'         => round(-$transaction->value_promo / $idr,2),
	                'qty'      		=> 1
	            ));
	    $productItems = array_map(function($item) use ($idr) {
	            $transaction  = Transaction::find($item['transaction_id']);
	            return array(
	                'name'          => $transaction->event->getTranslation('id')->title.' ('.$item['periode'].')',
	                'price'         => round($item['order_value'] / $idr,2),
	                'qty'      		=> 1
	            );
	        }, $productItems);
	    $productItems = array_merge($productItems,$fee);
	    $productItems = array_merge($productItems,$voucher);
	    return $productItems;
	}

	public function expressCheckoutSuccess(Request $request,$order_no) {
		$key = decrypt($order_no);
	    $transactions = DB::table('orders')
	                        ->leftJoin('transactions','orders.transaction_id','transactions.id')
	                        ->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
	                        ->leftJoin('event_translations','event_tickets.event_id','event_translations.event_id')
	                        ->where('event_translations.locale','id')
	                        ->where('transactions.order_no',$key)
	                        ->groupBy('event_tickets.name')
	                        ->select(DB::raw('count(*) as qty'),DB::raw('sum(orders.order_value) as total_value'),'event_tickets.name as name','event_translations.title as title','orders.visit_date as visit_date','orders.visit_time as visit_time','orders.event_ticket_id as event_ticket_id','orders.order_value as order_value','orders.periode as periode')
	                        ->get();
	    $transaction  = Transaction::where('order_no',$key)->first();
	    $event = $transaction->event;
	    $paypal_response = PaypalPaymentResponse::where('invoice_id',$key)->first();
	    // check if payment is recurring
	    $recurring = $request->input('recurring', false) ? true : false;

	    $token = $request->get('token');
	    $PayerID = $request->get('PayerID');

	    // initaly we paypal redirects us back with a token
	    // but doesn't provice us any additional data
	    // so we use getExpressCheckoutDetails($token)
	    // to get the payment details
	    $response = $this->provider->getExpressCheckoutDetails($token);
	    // dd($response);

	    // if response ACK value is not SUCCESS or SUCCESSWITHWARNING
	    // we return back with error
	    if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
	    	$transaction->status = 1016;
        	$transaction->update();
	    	flash()->success('Error processing PayPal payment');
	        return redirect('/');
	    }

	    // invoice id is stored in INVNUM
	    // because we set our invoice to be xxxx_id
	    // we need to explode the string and get the second element of array
	    // witch will be the id of the invoice
	    $invoice_id = $response['INVNUM'];

	    // get cart data
	    $cart = $this->getCart($recurring, $invoice_id, $transaction);
	    // dd($cart);
	    // check if our payment is recurring
	    if ($recurring === true) {
	        
	        // if recurring then we need to create the subscription
	        // you can create monthly or yearly subscriptions
	        $response = $this->provider->createMonthlySubscription($response['TOKEN'], $response['AMT'], $cart['subscription_desc']);
	        
	        $status = 'Invalid';
	        // if after creating the subscription paypal responds with activeprofile or pendingprofile
	        // we are good to go and we can set the status to Processed, else status stays Invalid
	        if (!empty($response['PROFILESTATUS']) && in_array($response['PROFILESTATUS'], ['ActiveProfile', 'PendingProfile'])) {
	            $status = 'Processed';
	        }

	    } else {

	        // if payment is not recurring just perform transaction on PayPal
	        // and get the payment status
	        $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);
	        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];

	    }

	    // find invoice by id
	    $invoice = PaypalPaymentResponse::where('invoice_id',$key)->first();
	    // dd($invoice);
	    // set invoice status
	    $invoice->payment_status = $status;

	    // if payment is recurring lets set a recurring id for latter use
	    if ($recurring === true) {
	        $invoice->recurring_id = $response['PROFILEID'];
	    }

	    // save the invoice
	    $invoice->save();

	    // App\Invoice has a paid attribute that returns true or false based on payment status
	    // so if paid is false return with error, else return with success message
	    if ($invoice->paid) {
	    	$transaction->status = 1012;
            $transaction->update();
	    	$subject = "[kiosTix] Transaksi anda berhasil untuk nomor pesanan ".$key;
	    	$this->generatePdf($transaction);
	    	$this->sendMail($transactions,$transaction,$subject);
	    	flash()->success('Transaksi berhasil');

	    	$event = $transaction->event;
	    	$date = Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
	    	$currentDate = strtotime($date);
	    	$futureDate = $currentDate+(60*180);
	    	$expTime = date("Y-m-d H:i:s", $futureDate);
	    	$expTimeView = date("Y/m/d H:i:s", $futureDate);
	    	$expTimeViewDate = date("Y/m/d", $futureDate);
	    	$expTimeViewTime = date("H:i:s", $futureDate);
	    	$expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
	    	return view('app.order.thanks',compact('transaction','event','expired_date','expTime','expTimeViewDate','expTimeViewTime','expTimeView','paypal_response'));
	        // return redirect('/')->with(['code' => 'success', 'message' => 'Order ' . $invoice_id . ' has been paid successfully!']);
	    }
	    $response;
	    $transaction->status = 1016;
        $transaction->update();
	    flash()->success('Error processing PayPal payment for Order ' . $invoice_id . '!');
	    return view('app.order.ovo_message',compact('response','event'));
	}

	private function generatePdf($transaction)
	{
	    $ticketData=Order::join('event_tickets','orders.event_ticket_id','event_tickets.id')
	    ->join('events','event_tickets.event_id','events.id')
	    ->where('transaction_id',$transaction->id)
	    ->groupBy('event_tickets.schedule_id')
	    ->select(
	        'orders.transaction_id',
	        'event_tickets.event_id',
	        'events.type as ticket_type',
	        'event_tickets.name as ticket_name',
	        'event_tickets.schedule_id as schedule_id',
	        'orders.event_ticket_id'
	    )
	    ->get();
	        // dd($ticketData);
	    foreach ($ticketData as $key => $values) {
	        $transaction_id = $values->transaction_id;
	        $schedule_id = $values->schedule_id;
	        if($values->ticket_type=='["E-Voucher"]'){
	            $name_type = 'E-Voucher';
	        }else{
	            $name_type = 'E-Ticket';
	        }
	        if($name_type=='E-Voucher'){
	            $ticketFolder=config('storage.transaction.ticket.evoucher').$transaction->event->id;
	            $ticketPath=env('PDF_PATH').'evoucher/'.$transaction->order_no.'.pdf';
	            $view='app.order.evoucher';
	        }else{
	            $ticketFolder=config('storage.transaction.ticket.eticket').$transaction->event->id;
	            $ticketPath=env('PDF_PATH').'eticket/'.$transaction->order_no.'.pdf';
	            $view='app.order.eticket';
	        }
	        if(!File::exists($ticketFolder)) {
	            File::makeDirectory($ticketFolder, $mode = 0777, true, true);
	        }
	        $mpdf= new \Mpdf\Mpdf([
	            'margin-left'=>10,
	            'margin-right'=>5,
	            'margin-top'=>2,
	            'margin-bottom'=>2,
	            'margin-header'=>0,
	            'margin-footer'=>0,
	        ]);
	        $mpdf->showImageErrors = false;
	        $mpdf->SetDisplayMode('fullpage');
	        $mpdf->list_indent_first_level = 0;
	        $mpdf->WriteHTML(view($view,compact('values')));
	        $mpdf->Output($ticketPath,'F');
	        if ($mpdf) {
	            return true;
	        }
	        return false;
	    }
	}

	private function sendMail($loopDetailTransaction,$getTransaction, $subject)
	{
		// dd($getTransaction);
	    $email = $getTransaction->customer->email;
	    $event = $getTransaction->event;
	    // $response = PaypalPaymentResponse::where('merchant_invoice',$getTransaction->order_no)->first();
	    $expired_date = Carbon::parse($getTransaction->created_at)->addDays(1)->format('d/m/Y h:i A');
	    if ($getTransaction->event->type == '["E-Voucher"]') {
	           $ticket_type = 'evoucher';
	    }else{
	           $ticket_type = 'eticket';
	    }
	    $attach = env('PDF_PATH').$ticket_type.'/'.$getTransaction->order_no.'.pdf';
	    Mail::send('email.complete_order_'.$getTransaction->payment->type, compact('loopDetailTransaction','getTransaction','event','expired_date'), function ($message) use ($email, $subject, $attach) {
	         $message->from('booking@kiostix.com', 'kiosTix');
	         $message->attach($attach);
	         $message->to($email)->subject($subject);
	    });
	}


    // public function checkout_process(Request $request)
    // {
    // 	$key = decrypt($request->key);
    //     $transactions = DB::table('orders')
    //                         ->leftJoin('transactions','orders.transaction_id','transactions.id')
    //                         ->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
    //                         ->leftJoin('event_translations','event_tickets.event_id','event_translations.event_id')
    //                         ->where('event_translations.locale','id')
    //                         ->where('transactions.order_no',$key)
    //                         ->groupBy('event_tickets.name')
    //                         ->select(DB::raw('count(*) as qty'),DB::raw('sum(orders.order_value) as total_value'),'event_tickets.name as name','event_translations.title as title','orders.visit_date as visit_date','orders.visit_time as visit_time','orders.event_ticket_id as event_ticket_id','orders.order_value as order_value','orders.periode as periode')
    //                         ->get();
    //     $transaction  = Transaction::where('order_no',$key)->first();
    //     if($transaction){
    //         $event = $transaction->event;
    //         $payment = PaymentMethod::find($request->payment_method_id);
    //         $voucher = Voucher::where('code',$request->voucher_code)->first();
    //         $fee_fixed = $payment->fees()->where('unit','fixed')->first();
    //         $percent = $payment->fees()->where('unit','percent')->first();
    //         $mdr_fixed = $payment->fees()->where('unit','mdr_fixed')->first();
    //         $mdr_percent = $payment->fees()->where('unit','mdr_percent')->first();

    //         $subtotal = $transaction->value_sub_total;
    //         //Get Fee Percent
    //         $fee_percent = ($subtotal/100)*$percent->value;

    //         $transaction->payment_method_id = $payment->id;
    //         $transaction->fee_convenience_percent = $fee_percent;
    //         $transaction->fee_convenience_fixed = $fee_fixed->value;
    //         //Total Convenience Fee
    //         $transaction->fee_convenience_total = $fee_fixed->value+$fee_percent;

    //         //Get MDR Percent
    //         $mdrPercent = ($subtotal/100)*$mdr_percent->value;
    //         $transaction->mdr_percent = $mdrPercent;
    //         $transaction->mdr_fixed = $mdr_fixed->value;
    //         //Total MDR 
    //         $totalMdr = $mdrPercent+$mdr_fixed->value;
    //         //Grand Total
    //         $grand_total = $subtotal+$fee_fixed->value+$fee_percent;
    //         $transaction->value_grand_total = $grand_total;
    //         if ($voucher) {
    //             $transaction->voucher_code = $request->voucher_code;
    //             if ($voucher->promotion->unit == 'fixed') {
    //                  $transaction->value_promo = $voucher->promotion->value;
    //             }elseif($voucher->promotion->unit == 'percent'){
    //                  $transaction->value_promo = ($transaction->value_grand_total/100)*$voucher->promotion->value;
    //             }
    //             $grand_total = $subtotal+$fee_fixed->value+$fee_percent-$transaction->value_promo;
    //             $transaction->value_grand_total = $grand_total;
    //             $voucher->status = 1000;
    //             $voucher->update();
    //             $voucherUser = new VoucherUser;
    //             $voucherUser->user_id = $transaction->customer_id;
    //             $voucherUser->voucher_id = $voucher->id;
    //             $voucherUser->save();
    //         }
    //         //Paid Value
    //         $transaction->paid_value = $grand_total-$totalMdr;
    //         $transaction->update();

    //         //paypal
    //         $paymentResponse = $this->sendPayment($request,$key,$transaction,$transactions);
    //         $url = $paymentResponse->links[1];
    //         // dd($url->href);
    //         return redirect($url->href);
    //         // dd($paymentResponse);
    //         // if ($paymentResponse == "") {
    //         //     $response = 404;
    //         //     $transaction->status = 1016;
    //         //     $transaction->update();
    //         //     return view('app.order.ovo_message',compact('response','event'));
    //         // }
    //         // else {
    //         //     $transactionRequestData = $paymentResponse->transactionRequestData;
    //         //     switch ($paymentResponse->responseCode) {
    //         //         case '00':
    //         //             $responseStatus = "Success";
    //         //             break;
    //         //         case '40':
    //         //             $responseStatus = "Transaction Failed";
    //         //             break;
    //         //         case '17':
    //         //             $responseStatus = "Transaction Decline";
    //         //             break;
    //         //         case '14':
    //         //             $responseStatus = "Invalid Mobile Number/OVO ID";
    //         //             break;
    //         //         default:
    //         //             $responseStatus = "Undeclared";
    //         //             break;
    //         //     }
    //         //     $response = new OvoPaymentResponse;
    //         //     $response->reference_number = $paymentResponse->referenceNumber ?? null;
    //         //     $response->approval_code = $paymentResponse->approvalCode ?? null;
    //         //     $response->response_code = $paymentResponse->responseCode ?? null;
    //         //     $response->response_status = $responseStatus;
    //         //     $transactionRequestData = $paymentResponse->transactionRequestData ?? null;
    //         //     $response->merchant_invoice = $transactionRequestData->merchantInvoice ?? $transaction->order_no;
    //         //     $response->batch_no = $transactionRequestData->batchNo ?? null;
    //         //     $response->phone_cust = $transactionRequestData->phone ?? null;
    //         //     $response->amount = $paymentResponse->amount ?? null;
    //         //     $transactionResponseData = $paymentResponse->transactionResponseData ?? null;
    //         //     $response->payment_type = $transactionResponseData->paymentType ?? null;
    //         //     $response->save();
    //         //     if ($response) {
    //         //         if($response->response_code == "00") {
    //         //             $transaction->status = 1012;
    //         //             $transaction->update();
    //         //             $this->generatePdf($transaction);
    //         //             $this->sendMail($transactions,$transaction,'Transaksi Berhasil');
    //         //             flash()->success('Transaksi berhasil');
    //         //         }
    //         //         // elseif ($response->response_code == "40" || $response->response_code == "14") {
    //         //         //     $transaction->status = 1016;
    //         //         //     $transaction->update();
    //         //         //     return view('app.order.ovo_message',compact('response','event'));
    //         //         // }
    //         //         else{
    //         //             $transaction->status = 1016;
    //         //             $phone = $request->phone;
    //         //             $transaction->update();
    //         //             return view('app.order.ovo_message',compact('response','event','phone'));
    //         //         }

    //         //     }
    //         // }
    //     }

    //         $date = Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
    //         $currentDate = strtotime($date);
    //         $futureDate = $currentDate+(60*180);
    //         $expTime = date("Y-m-d H:i:s", $futureDate);
    //         $expTimeView = date("Y/m/d H:i:s", $futureDate);
    //         $expTimeViewDate = date("Y/m/d", $futureDate);
    //         $expTimeViewTime = date("H:i:s", $futureDate);
    //         $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
    //         return view('app.order.thanks',compact('transaction','event','paymentResponse','expired_date','expTime','expTimeViewDate','expTimeViewTime','expTimeView'));
    // }

    // public function sendPayment(Request $request,$key,$transaction,$loopDetailTransaction)
    // {
    //     $url = urlStaging();
    //     $ch = curl_init($url);
    //     $random = date_timestamp_get($transaction->created_at);
    //     $getAuth = $this->getAuth();
    //     $authorization = "Authorization: Bearer ".$getAuth->access_token;
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
    //     $payer["payment_method"] = "paypal";
    //     $amount["total"] = "30.11";
    //     $amount["currency"] = "USD";
    //     $details["subtotal"] = "30.00";
    //     $details["tax"] = "0.07";
    //     $details["shipping"] = "0.03";
    //     $details["handling_fee"] = "1.00";
    //     $details["shipping_discount"] = "-1.00";
    //     $details["insurance"] = "0.01";
    //     $amount["details"] = $details;
    //     $redirect_urls["return_url"] = "https://www.google.com";
    //     $redirect_urls["cancel_url"] = "https://www.kiostix.com";
    //     $transactions = array();
    //     $transactions_amount['amount'] = (object) $amount;
       
    //     $transactions_amount["description"] = "This is the payment transaction description.";
    //     $transactions_amount["custom"] = "EBAY_EMS_90048630024435";
    //     $transactions_amount["invoice_number"] = "48787589673";
    //     $payment_options["allowed_payment_method"] = "INSTANT_FUNDING_SOURCE";
    //     $transactions_amount["payment_options"] = $payment_options;
    //     $transactions_amount["soft_descriptor"] = "ECHI5786786";

    //     $items = array();
    //     foreach ($loopDetailTransaction as $data) {
    //     	$item["name"] = $data->title;
    //     	$item["quantity"] = $data->qty;
    //     	$item["price"] = "30.00";
    //     	$item["currency"] = "USD";
    //     	$items[] = (object) $item;
    //     }


    //     $item_list['items'] = $items;
    //     $transactions_amount["item_list"] = (object) $item_list;

    //     $transactions[] = (object) $transactions_amount;

    //     // $transactions["item_list"] = $items;
    //     // $transactions[] = $transactions;

        
    //     $jsonData = array(
    //     		"intent"=> "sale",
    //     		"payer"=> $payer,
    //     		"transactions"=>$transactions,
    //     		"note_to_payer"=>"Contact us for any questions on your order.",
    //     		"redirect_urls"=> (object) $redirect_urls 
    //     );

    //    $object = (object) $jsonData;
    //    $jsonDataEncoded = json_encode($object,TRUE);
    //    // dd($jsonDataEncoded);

    //     //Tell cURL that we want to send a POST request.
    //     curl_setopt($ch, CURLOPT_POST, true);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    //     //Attach our encoded JSON string to the POST fields.
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);


    //     //Execute the request
    //     $result = curl_exec($ch);
    //     return json_decode($result);
    // }

    // private function getAuth()
    // {
    // 	$service_url = 'https://api.sandbox.paypal.com/v1/oauth2/token';
    // 	$curl = curl_init($service_url);
    // 	$username = username();
    // 	$password = password();
    // 	$headers  = [
    //         'Content-Type: application/x-www-form-urlencoded'
    //     ];
    //     $body = "grant_type=client_credentials";
    // 	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    // 	curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password); //Your credentials goes here
    // 	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    // 	curl_setopt($curl, CURLOPT_POST, true);
    // 	curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
    // 	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    // 	$curl_response = curl_exec($curl);
    // 	return $response = json_decode($curl_response);
    // }

    // private function sendMail($loopDetailTransaction,$getTransaction, $subject)
    // {
    //     $email = $getTransaction->customer->email;
    //     $event = $getTransaction->event;
    //     $response = OvoPaymentResponse::where('merchant_invoice',$getTransaction->order_no)->first();
    //     $expired_date = Carbon::parse($getTransaction->created_at)->addDays(1)->format('d/m/Y h:i A');
    //     if ($getTransaction->event->type == '["E-Voucher"]') {
    //            $ticket_type = 'evoucher';
    //     }else{
    //            $ticket_type = 'eticket';
    //     }
    //     $attach = env('PDF_PATH').$ticket_type.'/'.$getTransaction->order_no.'.pdf';
    //     Mail::send('email.complete_order_'.$getTransaction->payment->type, compact('loopDetailTransaction','getTransaction','event','response','expired_date'), function ($message) use ($email, $subject, $attach) {
    //          $message->from('booking@kiostix.com', 'kiosTix');
    //          $message->attach($attach);
    //          $message->to($email)->subject($subject);
    //     });
    // }

    // private function generatePdf($transaction)
    // {
    //     $ticketData=Order::join('event_tickets','orders.event_ticket_id','event_tickets.id')
    //     ->join('events','event_tickets.event_id','events.id')
    //     ->where('transaction_id',$transaction->id)
    //     ->groupBy('event_tickets.schedule_id')
    //     ->select(
    //         'orders.transaction_id',
    //         'event_tickets.event_id',
    //         'events.type as ticket_type',
    //         'event_tickets.name as ticket_name',
    //         'event_tickets.schedule_id as schedule_id',
    //         'orders.event_ticket_id'
    //     )
    //     ->get();
    //         // dd($ticketData);
    //     foreach ($ticketData as $key => $values) {
    //         $transaction_id = $values->transaction_id;
    //         $schedule_id = $values->schedule_id;
    //         if($values->ticket_type=='["E-Voucher"]'){
    //             $name_type = 'E-Voucher';
    //         }else{
    //             $name_type = 'E-Ticket';
    //         }
    //         if($name_type=='E-Voucher'){
    //             $ticketFolder=config('storage.transaction.ticket.evoucher').$transaction->event->id;
    //             $ticketPath=env('PDF_PATH').'evoucher/'.$transaction->order_no.'.pdf';
    //             $view='app.order.evoucher';
    //         }else{
    //             $ticketFolder=config('storage.transaction.ticket.eticket').$transaction->event->id;
    //             $ticketPath=env('PDF_PATH').'eticket/'.$transaction->order_no.'.pdf';
    //             $view='app.order.eticket';
    //         }
    //         if(!File::exists($ticketFolder)) {
    //             File::makeDirectory($ticketFolder, $mode = 0777, true, true);
    //         }
    //         $mpdf= new \Mpdf\Mpdf([
    //             'margin-left'=>10,
    //             'margin-right'=>5,
    //             'margin-top'=>2,
    //             'margin-bottom'=>2,
    //             'margin-header'=>0,
    //             'margin-footer'=>0,
    //         ]);
    //         $mpdf->showImageErrors = false;
    //         $mpdf->SetDisplayMode('fullpage');
    //         $mpdf->list_indent_first_level = 0;
    //         $mpdf->WriteHTML(view($view,compact('values')));
    //         $mpdf->Output($ticketPath,'F');
    //         if ($mpdf) {
    //             return true;
    //         }
    //         return false;
    //     }
    // }
}
