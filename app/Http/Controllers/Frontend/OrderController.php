<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use App\Http\Requests\PaymentRequest;
use App\Http\Requests\BookingInfoRequest;
use App\Http\Requests\OrderRequest;
use App\Models\UserInformation;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\PaymentResponse;
use App\Models\PaymentMethod;
use App\Models\Event;
use App\Models\Promotion;
use App\Models\Voucher;
use App\Models\Schedule;
use App\Models\Seat;
use App\Models\Sectionable;
use App\Models\Sessionable;
use App\Models\Priceable;
use App\Models\Periodable;
use App\Models\AdditionalFee;
use App\Models\EventSalesChannel;
use App\Models\EventSalesChannelTicket;
use Carbon\Carbon;
use Auth;
use URL;
use DB;
use Request;
use Response;
use Session;
use Cookie;
use Storage;
use File;
use PDF;

class OrderController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 *  Order Event
	 */
	public function orderEvent(OrderRequest $request)
	{
		$transaction = $this->createTransaction($request);
		$orders = $this->createOrder($request, $transaction);
		return redirect(URL::to('/').'/booking/'.encrypt($transaction->order_no));
	}
	/**
	 *  View Booking Event
	 */
	public function orderReview($key) {
		$key = decrypt($key);
		$transaction  = Transaction::where('order_no',$key)->InCartStatus()->first();
		if($transaction){
			if (Auth::check()) {
				if (Auth::user()->status == 1001) {
					$decrementTicket  = $this->decrementTicket($transaction, Auth::user()->id);
					// dd($decrementTicket);
					if ($decrementTicket["message"] == false) {
						if($decrementTicket["qty"] == 0){
							flash()->warning('Maaf, Tiket Habis');
							$seat = Seat::where('transaction_id', $transaction->id)->first();
							if($seat){
								Seat::Where('transaction_id',$transaction->id)->update(['status_order'=>0,'transaction_id'=>0,'customer_id'=>0]);;
							}
							return redirect()->back();
						}else{
							flash()->warning('Maaf, Tiket '.$decrementTicket["periode"].' hanya tersedia '.$decrementTicket["qty"]);
							$seat = Seat::where('transaction_id', $transaction->id)->first();
							if($seat){
								Seat::Where('transaction_id',$transaction->id)->update(['status_order'=>0,'transaction_id'=>0,'customer_id'=>0]);;
							}
							return redirect()->back();
						}
					}else{
						return redirect(URL::to('/').'/payment/'.encrypt($transaction->order_no));
					}
				}
			}
			return redirect('/');
			// $event = Event::find($transaction->event_id);
			// return view('app.order.booking',compact('transaction','event'));
		}
	}
	/**
	 *  View Payment Order 
	 */
	public function bookingReview($key) {
		$key = decrypt($key);
		$transactions = DB::table('orders')
							->leftJoin('transactions','orders.transaction_id','transactions.id')
							->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
							->where('transactions.order_no',$key)
							->groupBy('event_tickets.name')
							->select(DB::raw('count(*) as qty'),DB::raw('sum(orders.order_value) as total_value'),'event_tickets.name as name','orders.visit_date as visit_date','orders.visit_time as visit_time','orders.event_ticket_id as event_ticket_id','orders.order_value as order_value')
							->get();

		// dd($transaction);
		$transaction  = Transaction::where('order_no',$key)->InCartStatus()->first();
		if($transaction){
			$event = $transaction->event;
			$date = Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
			$currentDate = strtotime($date);
			$futureDate = $currentDate+(60*10);
			//$futureDate = $currentDate+(90000*33335);
			$expTime = date("Y-m-d H:i:s", $futureDate);
			$installment3 = $transaction->value_grand_total / 3;
			$installment6 = $transaction->value_grand_total / 6;
			$installment12 = $transaction->value_grand_total / 12;
			$installment24 = $transaction->value_grand_total / 24;
			return view('app.order.payment',compact('transaction','event','expTime','transactions','installment3','installment6','installment12','installment24'));
		}
		return redirect('/');
	}
	/**
	 *  Expired Order
	 */
	public function expired() {
		if (Request::ajax()) {
			$eventID = $_POST['eventID'];
			$trxId = $_POST['trxId'];
			$transaction  = Transaction::find($trxId);
			// $transaction->status = 1009;
			// $transaction->update();
			// foreach ($transaction->orders as $order) {
			// 	foreach ($order->ticket->quantities as $quantity) {
			// 			$quantity->increment('available_quantity', 1);
			// 	}
			// }
			$data = array(
				'url' => '/expired/'.encrypt($transaction->id)
			);
			return Response::json($data);
		}
	}
	/**
	 *  View Expired Order
	 */
	public function expiredView($key) {
		$key = decrypt($key);
		$transaction  = Transaction::find($key);
		if($transaction){
			$event = $transaction->event;
			return view('app.order.expired',compact('event'));
		}
		return redirect('/');
	}
	public function paymentFee() {
		if (Request::ajax()) {
			$paymentId = $_POST['id'];
			$payment = PaymentMethod::find($paymentId);
			$fee_fixed = $payment->fees()->where('unit','fixed')->first();
			$fee_percent = $payment->fees()->where('unit','percent')->first();
			$data = array(
				'fee_fixed' => $fee_fixed->value,
				'fee_percent' => $fee_percent->value,
			);
			return Response::json($data);
		}
	}
	public function getVoucher() {
		if (Request::ajax()) {
			$event = $_POST['event'];
			$payment = (int)$_POST['payment'];
			$voucher = $_POST['voucher'];
			$voucher = Voucher::where('code',$voucher)->first();
			$data = array(
				'status' => 5,
				'voucher' => $voucher,
				'message' => 'Maaf, Voucher tidak ditemukan',
			);
			// check voucher available
			if ($voucher) {
				// check voucher status
				if ($voucher->status == 1001) {
					$checkPayment = DB::table('promotion_payment_methods')
							->where('promotion_id',$voucher->promotion_id)
							->where('payment_method_id',$payment)->first();

					$checkEvent = DB::table('promotion_events')
							->where('promotion_id',$voucher->promotion_id)
							->where('event_id',$event)->first();

					if (!$checkPayment) {
						$data = array(
							'status' => 5,
							'checkPayment' => $checkPayment,
							'message' => 'Maaf, Voucher tidak berlaku untuk jenis pembayaran yang anda pilih',
						);
						return Response::json($data);
					}
					if (!$checkEvent) {
						$data = array(
							'status' => 5,
							'event' => $event,
							'promotion_id' => $voucher->promotion_id,
							'voucher' => $voucher,
							'checkEvent' => $checkEvent,
							'message' => 'Maaf, Voucher tidak berlaku untuk event ini',
						);
						return Response::json($data);
					}
					if ($voucher->promotion->start_date <= Carbon::now() && $voucher->promotion->end_date >= Carbon::now()) {
						$data = array(
							'promotion' => $voucher->promotion,
							'voucher' => $voucher,
							'status' => 1,
							'message' => '',
						);
						return Response::json($data);
					}else{
						$data = array(
							'status' => 5,
							'voucher-status' => $voucher->status,
							'message' => 'Maaf, Voucher sudah tidak berlaku nih',
						);
						return Response::json($data);
					}
				}else{
					$data = array(
						'status' => 5,
						'voucher-status' => $voucher->status,
						'message' => 'Maaf, Voucher sudah tidak berlaku',
					);
					return Response::json($data);
				}
			}
			return Response::json($data);
		}
	}
/**
	 *  Decrement Section Quantity
	 */
	private function decrementTicket($transaction,$user_id)
	{
		if($transaction->status == 1011){

			foreach ($transaction->orders as $order) {
				if (GetQuantity($transaction->event_id,$order->event_ticket_id) >= count($transaction->orders->where('event_ticket_id',$order->event_ticket_id))) {
					// dd("yesy");
					
					$saleschannel = EventSalesChannel::where('event_id',$transaction->event_id)->where('sales_channel_id',1)->first();
					$saleschannelTicket = EventSalesChannelTicket::where('event_sales_channel_id',$saleschannel->id)->where('event_ticket_id', $order->event_ticket_id)->first();
					$saleschannelTicket->decrement('available_quantity', 1);
				}else{
					// dd($order->periode);
					$seat = Seat::where('transaction_id', $transaction->id)->first();
					if($seat){
						Seat::Where('transaction_id',$transaction->id)->update(['status_order'=>0,'transaction_id'=>0,'customer_id'=>0]);;
					}
					$message["message"] = false;
					$message["periode"] = $order->periode;
					$message["qty"] = GetQuantity($transaction->event_id,$order->event_ticket_id);
					return $message;
				}
			}
		}
		$transaction->customer_id = $user_id;
		$transaction->update();
		$message["message"] = true;
		return $message;
	}
	/**
	 *  Mapping Array Request
	 */
	private function  mapping($arrays, $key)
	{
	        $arrays = array_map(function($array) use ($key) {
	                return array(
	                    $key =>  $array,
	                );
	            }, $arrays);
	 	return $arrays;
	}
	/**
	 * Create User Information
	 */
	private function  createUserInfo($request)
	{
		$userInformation = new UserInformation;
		$userInformation->firstname = $request->firstname;
		$userInformation->lastname = $request->lastname;
		$userInformation->phone = $request->phone;
		$userInformation->gender = $request->gender;
		$userInformation->dob = Carbon::createFromFormat('d/m/Y',$request->dob);
		// $userInformation->email = $request->email;
		$userInformation->save();

	 	return $userInformation;
	}
	/**
	 * Create Order
	 */
	private function  createOrder($request,$transaction)
	{
		$tickets = $this->mapping($request->ticket, 'ticket');
		$quantities = $this->mapping($request->qty, 'quantity');
		$i=0;
		$orders = array();
		foreach($tickets as $value) {
			  $orders[] = array_merge($value,$quantities[$i]);
		      $i++;
		}
		$orders = collect($orders);
		$orders = $orders->filter(function($item) {
			    return $item['quantity'] != 0;
			});
		$subtotal = 0;
		foreach ($orders as $order) {
			$ticket = getTicket($order['ticket']);
			if ($order['quantity'] > 1) {
				for ($x = 0; $x <= $order['quantity']-1; $x++) {
					$seat = Seat::where('tiket_id',$ticket->id)->where('customer_id',$transaction->customer_id)->where('status_order',2)->skip($x)->take(1)->first();
					$this->newOrder($request,$transaction,$ticket,$order,$seat);
				} 
			}else{
				$seat = Seat::where('tiket_id',$ticket->id)->where('customer_id',$transaction->customer_id)->where('status_order',2)->skip(0)->take(1)->first();
				$this->newOrder($request,$transaction,$ticket,$order,$seat );
			}
		}
		$transaction->value_sub_total = $transaction->orders()->sum('order_value');
		$transaction->value_grand_total = $transaction->orders()->sum('order_value');
		$transaction->save();

	}
	/**
	 * New Order
	 */
	private function  newOrder($request,$transaction,$ticket,$order,$seat)
	{
		$newOrder = new Order;
		$newOrder->transaction_id = $transaction->id;
		$newOrder->barcode = generateBarcode($transaction->event_id);
		$newOrder->event_ticket_id = $order['ticket'];
		$newOrder->order_value = $ticket->value;
		$newOrder->periode = $ticket->name;
		$newOrder->visit_date = Carbon::createFromFormat('d M Y',$request->visit_date); 
		$newOrder->visit_time = $request->visit_time;
		if($seat){
			$newOrder->seat_mapping_id = $seat['id'];
			$newOrder->seat_row = $seat['seat_row'];
			$newOrder->seat_number = $seat['seat_number'];
		}
		$newOrder->save();
        $barcode= $newOrder->barcode;
        $config['format']='png'; $config['size']=200;
		$path = env('STORAGE_QRCODE_PATH').$transaction->event_id.'/';
		if(!File::exists($path)) {
		    File::makeDirectory($path, $mode = 0777, true, true);
		}
        generateQrcode($barcode,$config,$path,$barcode);
	}
	/**
	 * Create Transaction
	 */
	private function  createTransaction($request)
	{
		$transaction = new Transaction;
		$transaction->order_no = orderNo();
		$transaction->status = 1011;
		$transaction->event_id = $request->event;
		$transaction->venue_id = $request->venue;
		$transaction->quantity = array_sum($request->qty);
		$transaction->value_sub_total = 0;
		$transaction->value_grand_total = 0;
		$transaction->sales_channel_id = 1;
		$transaction->ref_type = 'web';
		$transaction->customer_id  = Auth::user()->id ?? null;
		$transaction->save();
	 	return $transaction;
	}

}
