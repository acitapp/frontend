<?php
namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use App\Http\Requests\PaymentRequest;
use App\Http\Requests\BookingInfoRequest;
use App\Http\Requests\OrderRequest;
use App\Models\UserInformation;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\PaymentResponse;
use App\Models\PaymentMethod;
use App\Models\Event;
use App\Models\Seat;
use App\Models\SeatLayout;
use App\Models\Promotion;
use App\Models\Voucher;
use App\Models\Schedule;
use App\Models\Sectionable;
use App\Models\Sessionable;
use App\Models\Priceable;
use App\Models\Periodable;
use App\Models\AdditionalFee;
use App\Models\EventSalesChannel;
use App\Models\EventSalesChannelTicket;
use Carbon\Carbon;
use Auth;
use URL;
use DB;
use Request;
use Response;
use Session;
use Cookie;
use Storage;
use File;
use Cache;
use PDF;

class SeatController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

    /**
	 *  get Seat
	 */
	public function getSeat(OrderRequest $request)
	{
        $id=$request->event;
        $event = Cache::remember('event-'.$id,1440,function()  use ($id) {
			return Event::withTranslation()->with('venues')->with('categories')->with('schedules')->with('venues')->find($id);
		});
        $customer = Auth::user()->id ?? null;;
        $tickets = $this->mapping($request->ticket, 'ticket');
        $schedule = $request->schedule;
        $visitdate = $request->visit_date;
        $visitTime = $request->visit_time;
        $arraytiket = array();
        for($i=0;$i<count($request->qty);$i++){
            $arraytiket[$request->ticket[$i]] = $request->qty[$i];
                    
        }
        $arraytiket = json_encode($arraytiket);
        $eventkey=$request->event;
        $venue=$request->venue;
      
		$tiket = array_flatten($tickets);
		$seat =  DB::table('seats')
						->whereIn('seats.tiket_id',$tiket)
                        ->get();
        if(isset($seat[0]->id)){
                $seat = $seat->toJson();
                return view('app.event.seat',compact('seat','customer','event','schedule','visitdate','visitTime','arraytiket','eventkey','venue'));
        }
        return redirect('/');
    }
   /**
	 *  Mapping Array Request
	 */
	private function  mapping($arrays, $key)
	{
	        $arrays = array_map(function($array) use ($key) {
	                return array(
	                    $key =>  $array,
	                );
	            }, $arrays);
	 	return $arrays;
    }
     /**
	 *  check Seat
	 */
	public function checkSeat(Request $request)
	{
       
        $id = $_POST['id'];
        $customer = Auth::user()->id ?? null;;
      
        $seat = Seat::where('id',$id)->first();
        
        $result['status'] ='0';
        $result['msg'] = 'proses gagal';
        if($seat){
            if($seat->customer_id== $customer && $seat->status_order==2){
                $seat->status_order=0;
                $seat->customer_id=0;
                $seat->update();
                $result['status'] ='3';
                $result['seat'] =$seat;
                $result['msg'] = 'proses berhasil order di batalkan';
            }
            else if($seat->customer_id!= $customer && $seat->status_order==2){
                $result['status'] ='2';
                $result['seat'] =$seat;
                $result['msg'] = 'seat telah di booking';
            }
            else if( $seat->status_order==1){
                $result['status'] ='1';
                $result['seat'] =$seat;
                $result['msg'] = 'seat telah di beli';
            }else{
                $seat->status_order=2;
                $seat->customer_id=$customer;
                $seat->update();
                $result['status'] ='1';
                $result['seat'] =$seat;
                $result['msg'] = 'proses berhasil seat telah di simpan';
            }

        }
        return response()->json($result);
    }
}