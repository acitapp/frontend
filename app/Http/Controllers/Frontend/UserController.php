<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;
use App\Http\Requests\UserInformationRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserInformation;
use App\Models\UserAddress;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Carbon\Carbon;
use Request;
use Response;
use Auth;

class UserController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}
	/**
	 * My Account
	 */
	public function view($locale) {
		$user  = Auth::user();
		$countries   = Country::undeleted()->pluck('name','id');
		$states   	 = (isset($user->information->address->state))? $user->information->address->state->pluck('name','id') : [];
	    $cities   	 = (isset($user->information->address->city))? $user->information->address->city->pluck('name','id') : [];
		return view('app.user.view',compact('user','countries','states','cities'));
	}
	public function updateProfile() {
		$user  = Auth::user();
		if ($user->status == 1001) {
			return redirect('/');
		}
	
		return view('app.user.update-profile',compact('user'));
	}
	public function updateEmail() {
		$user  = Auth::user();
		return view('app.user.update-profile',compact('user'));
	}
	/**
	 * Transaction
	 */
	public function transaction($locale) {
		$user  = Auth::user();
		$transactions = $user->transactions()->latest('created_at')->paginate(10);
		return view('app.user.transaction',compact('user','transactions'));
	}
	/**
	 * Order Detail
	 */
	public function orderDetail($trxId) {
		$user  = Auth::user();
		$transaction = $user->transactions->where('order_no',$trxId)->first();
		return view('app.user.order-detail',compact('transaction'));
	}
	/**
	 * Change Password
	 */
	public function changePassword($locale) {
		$user  = Auth::user();
		return view('app.user.change-password',compact('user','countries','states','cities'));
	}
	public function updatePassword(PasswordRequest $request) {
		$user  = Auth::user();
		$user->password = bcrypt($request->password);
		$user->update();
		
		flash()->success('Your profile has been updated!');
		return redirect()->back();
	}
	/**
	 * Update Account
	 */
	public function update(UserInformationRequest $request) {
		$user           		= Auth::user();
		$this->insertProfile($request, $user->id);
		$image = $request->avatar;
		if (!is_null($image)) {
			if($user->getMedia('avatar')->first()){
				$user->getMedia('avatar')->first()->delete();
			}
			if (!empty($image)) {
				$user->addMedia($image)->toMediaCollection('avatar');
			}
		}
		$user->status = 1001;
		$user->email = $request->email;
		$user->name = $request->fullname;
		$user->update();
		flash()->success('Your profile has been updated!');
		return redirect()->back();
	}
	/**
	 * Update PROFILE
	 */
	private function  insertProfile($request,$user_id)
	{
			$profile 				= UserInformation::where('user_id',$user_id)->first();
			if ($profile) {
				$profile->dob    		= Carbon::createFromFormat('d/m/Y',$request->dob);
				$profile->pob    		= $request->pob;
				$profile->gender   		= $request->gender;
				$profile->phone   		= $request->phone;
				$profile->id_type 		= $request->id_type;
				$profile->id_number 	= $request->id_number;
				$profile->biography 	= $request->biography;
				$profile->update();
				$address 	= UserAddress::where('user_information_id',$profile->id)->first();
				if ($address) {
					$address->address = $request->address;
					$address->country_id = $request->country_id;
					$address->city_id = $request->city_id;
					$address->state_id = $request->state_id;
					$address->zip_code = $request->zip_code;
					$address->update();
				}else{
					$address = new UserAddress;
					$address->user_information_id = $profile->id;
					$address->address = $request->address;
					$address->country_id = $request->country_id;
					$address->city_id = $request->city_id;
					$address->state_id = $request->state_id;
					$address->zip_code = $request->zip_code;
					$address->save();
				}
			}else{
				$profile = new UserInformation;
				$profile->dob    		= Carbon::createFromFormat('d/m/Y',$request->dob);
				$profile->pob    		= $request->pob;
				$profile->gender   		= $request->gender;
				$profile->phone   		= $request->phone;
				$profile->id_type 		= $request->id_type;
				$profile->id_number 	= $request->id_number;
				$profile->biography 	= $request->biography;
				$profile->save();
			}
	 
	}
	/**
	 * Create Event
	 */
	public function createEvent($locale) {
		$user  = Auth::user();
		return view('app.free-event.create',compact('user'));
	}
}
