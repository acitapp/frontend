<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Frontend\Controller;

use App\Models\Voucher;
use App\Models\Order;
use App\Models\VoucherUser;
use App\Models\Transaction;
use App\Models\PaymentMethod;
use App\Models\OvoPaymentResponse;
use App\Models\Seat;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;
use DB;
use Storage;
use File;
use PDF;

class OvoController extends Controller
{
    public function checkout_process(Request $request)
    {
    	$key = decrypt($request->key);
        $transactions = DB::table('orders')
                            ->leftJoin('transactions','orders.transaction_id','transactions.id')
                            ->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
                            ->where('transactions.order_no',$key)
                            ->groupBy('event_tickets.name')
                            ->select(DB::raw('count(*) as qty'),DB::raw('sum(orders.order_value) as total_value'),'event_tickets.name as name','orders.visit_date as visit_date','orders.visit_time as visit_time','orders.event_ticket_id as event_ticket_id','orders.order_value as order_value','orders.periode as periode')
                            ->get();
        $transaction  = Transaction::where('order_no',$key)->first();
        if($transaction){
            $event = $transaction->event;
            $payment = PaymentMethod::find($request->payment_method_id);
            $voucher = Voucher::where('code',$request->voucher_code)->first();
            $fee_fixed = $payment->fees()->where('unit','fixed')->first();
            $percent = $payment->fees()->where('unit','percent')->first();
            $mdr_fixed = $payment->fees()->where('unit','mdr_fixed')->first();
            $mdr_percent = $payment->fees()->where('unit','mdr_percent')->first();

            $subtotal = $transaction->value_sub_total;
            //Get Fee Percent
            $fee_percent = ($subtotal/100)*$percent->value;

            $transaction->payment_method_id = $payment->id;
            $transaction->fee_convenience_percent = $fee_percent;
            $transaction->fee_convenience_fixed = $fee_fixed->value;
            //Total Convenience Fee
            $transaction->fee_convenience_total = $fee_fixed->value+$fee_percent;

            //Get MDR Percent
            $mdrPercent = ($subtotal/100)*$mdr_percent->value;
            $transaction->mdr_percent = $mdrPercent;
            $transaction->mdr_fixed = $mdr_fixed->value;
            //Total MDR 
            $totalMdr = $mdrPercent+$mdr_fixed->value;
            //Grand Total
            $grand_total = $subtotal+$fee_fixed->value+$fee_percent;
            $transaction->value_grand_total = $grand_total;
            if ($voucher) {
                $transaction->voucher_code = $request->voucher_code;
                if ($voucher->promotion->unit == 'fixed') {
                     $transaction->value_promo = $voucher->promotion->value;
                }elseif($voucher->promotion->unit == 'percent'){
                     $transaction->value_promo = ($transaction->value_grand_total/100)*$voucher->promotion->value;
                }
                $grand_total = $subtotal+$fee_fixed->value+$fee_percent-$transaction->value_promo;
                $transaction->value_grand_total = $grand_total;
                $voucher->status = 1000;
                $voucher->update();
                $voucherUser = new VoucherUser;
                $voucherUser->user_id = $transaction->customer_id;
                $voucherUser->voucher_id = $voucher->id;
                $voucherUser->save();
            }
            //Paid Value
            $transaction->paid_value = $grand_total-$totalMdr;
            $transaction->update();

            //ovo
            $paymentResponse = $this->sendPayment($request,$key,$transaction);
            // dd($paymentResponse);
            if ($paymentResponse == "") {
                $response = 404;
                $transaction->status = 1016;
                $transaction->update();
                return view('app.order.ovo_message',compact('response','event'));
            }
            else {
                $transactionRequestData = $paymentResponse->transactionRequestData;
                switch ($paymentResponse->responseCode) {
                    case '00':
                        $responseStatus = "Success";
                        break;
                    case '40':
                        $responseStatus = "Transaction Failed";
                        break;
                    case '17':
                        $responseStatus = "Transaction Decline";
                        break;
                    case '14':
                        $responseStatus = "Invalid Mobile Number/OVO ID";
                        break;
                    default:
                        $responseStatus = "Undeclared";
                        break;
                }
                $response = new OvoPaymentResponse;
                $response->reference_number = $paymentResponse->referenceNumber ?? null;
                $response->approval_code = $paymentResponse->approvalCode ?? null;
                $response->response_code = $paymentResponse->responseCode ?? null;
                $response->response_status = $responseStatus;
                $transactionRequestData = $paymentResponse->transactionRequestData ?? null;
                $response->merchant_invoice = $transactionRequestData->merchantInvoice ?? $transaction->order_no;
                $response->batch_no = $transactionRequestData->batchNo ?? null;
                $response->phone_cust = $transactionRequestData->phone ?? null;
                $response->amount = $paymentResponse->amount ?? null;
                $transactionResponseData = $paymentResponse->transactionResponseData ?? null;
                $response->payment_type = $transactionResponseData->paymentType ?? null;
                $response->save();
                if ($response) {
                    if($response->response_code == "00") {
                        $transaction->status = 1012;
                        $transaction->update();
                        $seat = Seat::where('transaction_id', $transaction->id)->first();
                        if($seat){
                            Seat::Where('transaction_id',$transaction->id)->update(['status_order'=>1]);
                        }
                        $this->generatePdf($transaction);
                        $this->sendMail($transactions,$transaction,'Transaksi Berhasil');
                        flash()->success('Transaksi berhasil');
                    }
                    // elseif ($response->response_code == "40" || $response->response_code == "14") {
                    //     $transaction->status = 1016;
                    //     $transaction->update();
                    //     return view('app.order.ovo_message',compact('response','event'));
                    // }
                    else{
                        $transaction->status = 1016;
                        $phone = $request->phone;
                        $transaction->update();
                        $seat = Seat::where('transaction_id', $transaction->id)->first();
                        if($seat){
                            Seat::Where('transaction_id',$transaction->id)->update(['status_order'=>0,'transaction_id'=>0,'customer_id'=>0]);
                        }
                        return view('app.order.ovo_message',compact('response','event','phone'));
                    }

                }
            }
        }

            $date = Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
            $currentDate = strtotime($date);
            $futureDate = $currentDate+(60*180);
            $expTime = date("Y-m-d H:i:s", $futureDate);
            $expTimeView = date("Y/m/d H:i:s", $futureDate);
            $expTimeViewDate = date("Y/m/d", $futureDate);
            $expTimeViewTime = date("H:i:s", $futureDate);
            $expired_date = Carbon::parse($transaction->created_at)->addDays(1)->format('d/m/Y h:i A');
            return view('app.order.thanks',compact('transaction','event','paymentResponse','expired_date','expTime','expTimeViewDate','expTimeViewTime','expTimeView'));
    }

    public function sendPayment(Request $request,$key,$transaction)
    {
        $url = "https://api.byte-stack.net/pos";
        $ch = curl_init($url);

        $transactionRequestData['merchantInvoice'] = $transaction->order_no;
        $transactionRequestData['batchNo'] = $transaction->id;
        $transactionRequestData['phone'] = $request->phone ?? "";
        $random = date_timestamp_get($transaction->created_at);
        $headers  = [
            'Content-Type: application/json'
        ];
        $header = array(
        			"Content-Type" => "application/json",
        			"app-id" => getAppId(),
        			"random" => $random,
        			"hmac" => generateHmac($random)
        		);
        $jsonData = array(
        		"type"=> "0200", 
        		"processingCode"=> "040000",  
        		"amount"=> $transaction->value_grand_total, 
        		"date"=> $transaction->created_at->format('Y-m-d h:m:s.ms'), 
        		"referenceNumber"=> $transaction->id,
        		"tid"=> getTid(),
        		"mid"=> getMid(),
        		"merchantId"=> getMerchantId(),
        		"storeCode"=> getStoreCode(),
        		"appSource"=> "POS", 
        		"transactionRequestData"=>$transactionRequestData
        );
        $jsonDataEncoded = json_encode($jsonData);

        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); 

        //Execute the request
        $result = curl_exec($ch);
        $header_size = curl_getinfo($ch);
        // return $header_size;
        if ($header_size["http_code"] == 0) {
            $result = "";
            return $result;
        }else{
            return json_decode($result);
        }
    }

    private function sendMail($loopDetailTransaction,$getTransaction, $subject)
    {
        $email = $getTransaction->customer->email;
        $event = $getTransaction->event;
        $response = OvoPaymentResponse::where('merchant_invoice',$getTransaction->order_no)->first();
        $expired_date = Carbon::parse($getTransaction->created_at)->addDays(1)->format('d/m/Y h:i A');
        if ($getTransaction->event->type == '["E-Voucher"]') {
               $ticket_type = 'evoucher';
        }else{
               $ticket_type = 'eticket';
        }
        $attach = env('PDF_PATH').$ticket_type.'/'.$getTransaction->order_no.'.pdf';
        Mail::send('email.complete_order_'.$getTransaction->payment->type, compact('loopDetailTransaction','getTransaction','event','response','expired_date'), function ($message) use ($email, $subject, $attach) {
             $message->from('booking@kiostix.com', 'kiosTix');
             $message->attach($attach);
             $message->to($email)->subject($subject);
        });
    }

    private function generatePdf($transaction)
    {
        $ticketData=Order::join('event_tickets','orders.event_ticket_id','event_tickets.id')
        ->join('events','event_tickets.event_id','events.id')
        ->where('transaction_id',$transaction->id)
        ->groupBy('event_tickets.schedule_id')
        ->select(
            'orders.transaction_id',
            'event_tickets.event_id',
            'events.type as ticket_type',
            'event_tickets.name as ticket_name',
            'event_tickets.schedule_id as schedule_id',
            'orders.event_ticket_id'
        )
        ->get();
            // dd($ticketData);
        foreach ($ticketData as $key => $values) {
            $transaction_id = $values->transaction_id;
            $schedule_id = $values->schedule_id;
            if($values->ticket_type=='["E-Voucher"]'){
                $name_type = 'E-Voucher';
            }else{
                $name_type = 'E-Ticket';
            }
            if($name_type=='E-Voucher'){
                $ticketFolder=config('storage.transaction.ticket.evoucher').$transaction->event->id;
                $ticketPath=env('PDF_PATH').'evoucher/'.$transaction->order_no.'.pdf';
                $view='app.order.evoucher';
            }else{
                $ticketFolder=config('storage.transaction.ticket.eticket').$transaction->event->id;
                $ticketPath=env('PDF_PATH').'eticket/'.$transaction->order_no.'.pdf';
                $view='app.order.eticket';
            }
            if(!File::exists($ticketFolder)) {
                File::makeDirectory($ticketFolder, $mode = 0777, true, true);
            }
            $mpdf= new \Mpdf\Mpdf([
                'margin-left'=>10,
                'margin-right'=>5,
                'margin-top'=>2,
                'margin-bottom'=>2,
                'margin-header'=>0,
                'margin-footer'=>0,
            ]);
            $mpdf->showImageErrors = false;
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->list_indent_first_level = 0;
            $mpdf->WriteHTML(view($view,compact('values')));
            $mpdf->Output($ticketPath,'F');
            if ($mpdf) {
                return true;
            }
            return false;
        }
    }
}
