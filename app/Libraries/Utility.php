<?php

namespace App\Libraries;

use Config;
use Storage;
use Request;
use App\Models\Module;
use App\Models\AdminRoleModule;
use App\Models\Order;
use QrCode;
use Mail;


class Utility
{
    public static function hash($password)
    {
        return password_hash($password, PASSWORD_BCRYPT, array('cost'=>'10'));
    }

    public static function verify($password, $hash)
    {
        return password_verify($password, $hash);
    }

    public static function convertBase($numberInput, $fromBaseInput, $toBaseInput)
    {
        if($fromBaseInput == $toBaseInput) {
            return $numberInput;
        }

        $fromBase  = str_split($fromBaseInput, 1);
        $toBase    = str_split($toBaseInput, 1);
        $number    = str_split($numberInput, 1);
        $fromLen   = strlen($fromBaseInput);
        $toLen     = strlen($toBaseInput);
        $numberLen = strlen($numberInput);
        $retval    = '';

        if($toBaseInput == '0123456789') {
            $retval = 0;
            for($i = 1; $i <= $numberLen; $i++) {
                $retval = bcadd($retval, bcmul(array_search($number[$i-1], $fromBase), bcpow($fromLen, $numberLen-$i)));
            }
            return $retval;
        }
        if($fromBaseInput != '0123456789') {
            $base10 = self::convertBase($numberInput, $fromBaseInput, '0123456789');
        } else {
            $base10 = $numberInput;
        }
        if($base10 < strlen($toBaseInput)) {
            return $toBase[$base10];
        }
        while($base10 != '0') {
            $retval = $toBase[bcmod($base10, $toLen)] . $retval;
            $base10 = bcdiv($base10, $toLen, 0);
        }
        return $retval;
    }

    public static function generateSequenceId()
    {
        $startupMillis = strtotime("2017-10-01T00:00:01.1+07:00") * 1000;
        $currentMillis = floor(microtime(true) * 1000);

        $diffMillis    = $currentMillis - $startupMillis;
        $diffBin       = self::convertBase($diffMillis, '0123456789', '01');
        $diffBinPadded = '1' . str_pad($diffBin, 37, '0', STR_PAD_LEFT);  // 37+1 bits valid for 4 years since $startupMillis (+1 bit after 4 year, 38+1)

        $paddedHex = self::convertBase($diffBinPadded, '01', '0123456789abcdef');
        $randomHex = bin2hex(openssl_random_pseudo_bytes(3));
        $generated = $paddedHex . $randomHex;

        return $generated;
    }

    public static function upload($file,$path,$current='')
    {
        $fileName = $file->getClientOriginalName();
        if (Storage::exists($path.$fileName)) {

            $pathInfo = pathinfo($fileName);
            $extension = isset($pathInfo['extension']) ? ('.' . $pathInfo['extension']) : '';

            if (preg_match('/(.*?)(\d+)$/', $pathInfo['filename'], $match)) {
                $base = $match[1];
                $number = intVal($match[2]);
            } else {
                $base = $pathInfo['filename'];
                $number = 0;
            }

            do {
                $fileName = $base . ++$number . $extension;
            } while (Storage::exists($path.$fileName));
        }
        if (!empty($current) && Storage::exists($path.$current)) Storage::delete($path.$current);
        Storage::putFileAs($path,$file,$fileName);
        return $fileName;
    }

    public static $modules;

    public static function has_modules($role_id=0, $id=0)
    {
        if(!isset(static::$modules)) static::$modules = Module::where('status',1001)->orderBy('position','asc')->get();
        $nested = '';
        if(!static::$modules->isEmpty()) foreach(static::$modules as $module){
            if($module->module_id==$id){
                $childs = static::has_modules($role_id,$module->id);

                $openable = (!empty($childs)) ? ' xn-openable' : '';
                $text = (empty($module->module_id)) ? ' xn-text' : '';

                $active = (Request::is($module->slug) || (!empty(config('module')) && config('module')->module_id==$module->id)) ? ' active' : '';
                $url = (!strpos($module->slug,'*')) ? '/'.$module->slug : 'javascript:void(0)';
                $icon = (!empty($module->icon)) ? '<span class="fa fa-'.$module->icon.'"></span>&nbsp;' : '';

                $checkModule = in_array($module->name,config('permissions'));
                if(!empty($checkModule) || $role_id==1){
                    $nested .= "<li class='".$active.$openable."'>";
                    $nested .= "<a href='".$url."' title='".$module->name."'>".$icon.'<span class="'.$text.'">'.$module->name."</span></a>";
                    if(!empty($childs)) $nested .= "<ul>";
                    $nested .= $childs;
                    if(!empty($childs)) $nested .= "</ul>";
                    $nested .= "</li>";
                }
            }
        }
        return $nested;
    }

    public static function permission($id=0,$keys=[])
    {
        $response = new \stdClass();
        if(!empty($keys)) foreach($keys as $key)
            $response->$key = in_array(str_replace('_', ' ', $key),config('permissions'));

        if(config('app.debug') || config('role')->id==1)
            if(!empty($keys)) foreach($keys as $key) $response->$key = true;

        return $response;
    }

    public static function generateQrcode($string,$config,$path,$name)
    {
        return QrCode::format($config['format'])
        ->size($config['size'])
        ->margin((!empty($config['margin']))?$config['margin']:0)
        ->generate($string,$path.$name.'.png');
    }

    public static function mail($data,$attach=[])
    {

        Mail::send($data['view'],$data['data'],function ($mail) use($data,$attach)
        {
          $mail->to($data['recMail'], $data['recName']);
          if (!empty($attach)) foreach ($attach as $value) {
              $mail->attach($value['path']);
          }
          $mail->subject($data['subject']);
        });
    }

    public static function generateBarcode($eventId)
    {
        $number = mt_rand(1000000, 9999999);
        if (static::checkBarcode($number)) return static::generateBarcode($eventId);
        return date('y').$eventId.$number;
    }

    public static function checkBarcode($number)
    {
        return Order::where('orders.barcode',$number)->exists();
    }

    public static function action($actions,$data)
    {
        $result='';
        if(!empty($actions)) foreach($actions as $action){
            $tags = (isset($action['tags']))?$action['tags']:'';
            $btn = (isset($action['btn']))?$action['btn']:'default';
            $icon = (isset($action['icon']))?'<i class="fa fa-right fa-'.$action['icon'].'"></i>':'';
            $name = (isset($action['name']))?' <b>'.$action['name'].'</b>':'';
            $conditions = (isset($action['conditions']))?$action['conditions']:'';

            $html = '';
            if(!empty($tags)) foreach($tags as $k=>$v){
                $vars = static::compileBracket($v);
                if(!empty($vars)) foreach($vars as $var){
                    $explodes = explode(".",$var); $obj=$data;
                    foreach($explodes as $explode){
                        $obj = $obj->$explode;
                    }
                    $v = str_replace('['.$var.']',$obj,$v);
                }
                if(strpos($v, '"')) $html.=" ".$k."='".$v."'";
                else $html.=' '.$k.'="'.$v.'"';
            }

            $show=true;
            if(!empty($conditions)) foreach($conditions as $k=>$v) if($data->$k!=$v) $show=false;

            if($show){
                if(!empty($result)) $result.='&nbsp;';
                $result .= '<a class="btn btn-xs btn-'.$btn.'" '.$html.'>'.$icon.$name.'</a>';
            }
        }
        if(empty($result)) $result='-';
        return $result;
    }

    public static function compileBracket($str)
    {
        $result = [];
        preg_match_all("/\[([^\]]+)\]/", $str, $result);
        if(!empty($result)) foreach($result as $k=>$v)
        return (!empty($result))?$result[1]:'';
    }
}
