# Introduction

- PHP Framework : Laravel 5.4 
- [See Documentation](https://laravel.com/docs/5.4)

# Requirement

- PHP requirement :  PHP 7.0+
- exif extension
- GD Library >=2.0 or Imagick PHP extension >=6.5.7
- MySQL 5.7 or higher

# Installation

- Import Sql Database For Dummy Data or Use Artisan Migrate
- Config .env file

# Change Permission Folder

- change public/media to 777
- change public/upload to 777





