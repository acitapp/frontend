$('.sortByFilter').on('change', function(event) {
	var value = $(this).val();
	var lang = $(this).attr("data-lang");
	var category = $(this).attr("data-category");
	var title = $(this).attr("data-title");
	window.location.href = value;
})