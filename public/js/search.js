jQuery(document).ready(function($) {
    var engine = new Bloodhound({
        remote: {
            url: '/find?q=%QUERY%',
            wildcard: '%QUERY%'
        },
        datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });
    $(".search-input").typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        source: engine.ttAdapter(),
        name: 'eventsList',
        display: 'title',
        templates: {
            empty: [
                '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
            ],
            header: [
                '<div class="list-group search-results-dropdown">'
            ],
            suggestion: function (data) {
              var result = '<a href="'+base_url+lang+'event/'+data.id +'/'+data.slug +'"class="list-group-item">'+ 
                            '<span class="event-img"><img src="'+img_url+'/media/'+data.media_id+'/conversions/small.jpg"/></span>'+
                            '<div class="event-meta"><span class="eventTitle">'+data.title +'</span><br>'+
                            '<span class="venueName">'+data.venue+'</span>'+
                            '</div></a>';
              return result;
          }
        }
    });
});