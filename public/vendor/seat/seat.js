seat = {
  grid:function(grid){
      // create grid
      for (var i = 0; i < (4000 / grid); i++) {
        canvas.add(new fabric.Line([ i * grid, 0, i * grid, 4000], { 
          stroke: '#f2f2f2', 
          selectable: false 
        }));
        canvas.add(new fabric.Line([ 0, i * grid, 4000, i * grid], { 
          stroke: '#f2f2f2', 
          selectable: false }))
      }
      // snap to grid
      canvas.on('object:moving', function(options) { 
        options.target.set({
          left: Math.round(options.target.left / grid) * grid,
          top: Math.round(options.target.top / grid) * grid
        });
      });
  },
  create:function(i,name,start,color,text_color){
      x = i+parseInt(start);
      i = i+1;
      var rect = new fabric.Rect({ 
            width: 40, 
            height: 40, 
            fill: '#'+color, 
            strokeWidth:1 ,
            stroke: '#fff',
      });
       var text = new fabric.Text(name+ x, { 
          fontSize: 8, 
          textAlign: 'left',
          fill: '#'+text_color, 
          originX: 'center',
          fontFamily: 'arial', 
          strokeWidth:0,
          left:20, 
          top: 12,
      });
      var group = new fabric.Group([ rect, text ], { 
        id : 'A'+i,
        name : 'A'+i,
        left: 40, 
        top: 40*i,
        hasControls:false,
      });
      canvas.add(group).calcOffset();
  },
  createRow:function(i,name,start,color,text_color){
      x = i+parseInt(start);
      i = i+1;
      var rect = new fabric.Rect({ 
            width: 20, 
            height: 20, 
            fill: '#'+color, 
            strokeWidth:1 ,
            stroke: '#fff',
      });
       var text = new fabric.Text(name+ x, { 
          fontSize: 8, 
          textAlign: 'left',
          fill: '#'+text_color, 
          originX: 'center',
          fontFamily: 'arial', 
          strokeWidth:0,
          left:10, 
          top: 6,
      });
      var group = new fabric.Group([ rect, text ], { 
        id : 'A'+i,
        name : 'A'+i,
        left: 20, 
        top: 20*i,
        hasControls:false,
      });
      canvas.add(group).calcOffset();
  },
  createText:function(text){
       var NewText = new fabric.Text(text, { 
          fontSize: 30, 
          textAlign: 'left',
          fill: '#000', 
          fontFamily: 'arial', 
          strokeWidth:0,
          left:30, 
          top: 30,
      });
      canvas.add(NewText).calcOffset();
      canvas.renderAll();
  },
  createRectangle:function(text){
      var newRect = new fabric.Rect({ 
            width: 100, 
            height: 50, 
            left:30, 
            top: 30,
            fill: '#f2a23a', 
            strokeWidth:0 ,
      });
      canvas.add(newRect).calcOffset();
      canvas.renderAll();
  },
  createCircle:function(text){
      var newCircle = new fabric.Circle({ 
          left:30, 
          top: 30,
          radius: 50,
          strokeWidth: 0,
          stroke: 'black',
          fill: '#f2a23a', 
      });
      canvas.add(newCircle).calcOffset();
      canvas.renderAll();
  },
  group:function(selector){
      var group = $(selector);
       group.click(function() {
        if (!canvas.getActiveObject()) {
          return;
        }
        if (canvas.getActiveObject().type !== 'activeSelection') {
          return;
        }
        canvas.getActiveObject().toGroup();
        canvas.requestRenderAll();
      });
  },
  ungroup:function(selector){
      var ungroup = $(selector);
      ungroup.click(function() {
        if (!canvas.getActiveObject()) {
          return;
        }
        if (canvas.getActiveObject().type !== 'group') {
          return;
        }
        canvas.getActiveObject().toActiveSelection();
        canvas.requestRenderAll();
      });
  },
  destroy:function(selector){
      var destroy = $(selector);
      destroy.click(function() {
      var selected = canvas.getActiveObjects(),
        selGroup = new fabric.ActiveSelection(selected, {
          canvas: canvas
        });
        if (selGroup) {
          if (confirm('Deleted selected?')) {
            selGroup.forEachObject(function(obj) {
              canvas.remove(obj);
            });
          }
        } else {
          return false;
        }
       canvas.discardActiveObject().renderAll();
      });
  },
  copy:function(selector){
      var copy = $(selector);
      copy.click(function() {
        canvas.getActiveObject().clone(function(cloned) {
          _clipboard = cloned;
        });
      });
  },
  paste:function(selector){
      var paste = $(selector);
      paste.click(function() {
        _clipboard.clone(function(clonedObj) {
          canvas.discardActiveObject();
          clonedObj.set({
            left: clonedObj.left + 10,
            top: clonedObj.top + 10,
            evented: true,
              hasControls:false,
          });
          if (clonedObj.type === 'activeSelection') {
            // active selection needs a reference to the canvas.
            clonedObj.canvas = canvas;
            clonedObj.forEachObject(function(obj) {
              canvas.add(obj);
            });
            // this should solve the unselectability
            clonedObj.setCoords();
          } else {
            canvas.add(clonedObj);
          }
          _clipboard.top += 10;
          _clipboard.left += 10;
          canvas.setActiveObject(clonedObj);
          canvas.requestRenderAll();
        });
      });
  },
  color:function(color){
      var objs = canvas.getActiveObject();
      if(!objs) return;
      objs.forEachObject(function(obj) {
      obj._objects[0].set("fill", color);
      obj._objects[1].set("fill", '#fff');
          canvas.renderAll();
      });
  },
  zoom:function(){
        canvas.on('mouse:wheel', function(opt) {
        var delta = opt.e.deltaY;
        var zoom = canvas.getZoom();
        zoom = zoom + delta/100;
        if (zoom > 10) zoom = 10;
        if (zoom < 0.01) zoom = 0.01;
        canvas.setZoom(zoom);
        opt.e.preventDefault();
        opt.e.stopPropagation();
      });
  },
}
$(seat.init);