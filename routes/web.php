<?php
//FRONTEND
Route::group(['middleware' => 'web'], function () {
		Auth::routes();

		//Route::get('/generatepdf/{transaction_id}', 'Frontend\StateController@generator');
	
		Route::get('/AsianGames2018', 'Frontend\AsiangamesController@master');
		Route::get('/maintenance', 'Frontend\FrontendController@maintenance');
		Route::get('logout', 'Auth\LoginController@logout');
		Route::get('/vtweb', 'Midtrans\VtwebController@vtweb');
		Route::get('/vtdirect', 'Midtrans\VtdirectController@vtdirect');
		Route::post('/payment/direct', 'Midtrans\VtdirectController@checkout_process');
		Route::post('/payment/ovo/direct', 'Frontend\OvoController@checkout_process');
		// Route::post('/payment/ovo/void', 'Frontend\OvoController@voidPayment');
		Route::post('/payment/paypal/direct', 'Frontend\PaypalController@checkout_process');
		Route::get('/vt_transaction', 'Midtrans\TransactionController@transaction');
		Route::post('/vt_transaction', 'Midtrans\TransactionController@transaction_process');
		Route::post('/order/notification', 'Midtrans\SnapController@notification');
		Route::get('/snap', 'Midtrans\SnapController@snap');
		Route::get('/snaptoken', 'Midtrans\SnapController@token');
		Route::post('/snapfinish', 'Midtrans\SnapController@finish');

		//PAYPAL
		Route::post('/paypal/express-checkout', 'Frontend\PaypalController@expressCheckout');
		Route::get('/payment/paypal/direct/{order_no}', 'Frontend\PaypalController@expressCheckoutSuccess');
		// Route::post('/paypal/notify', 'PaypalController@notify');

		//SOCIAL LOGIN
		Route::get('/callback/{provider}', 'Frontend\SocialAuthController@callback');
		Route::get('/redirect/{provider}', 'Frontend\SocialAuthController@redirect');

		//FRONT END
		Route::get('/', 'Frontend\FrontendController@index');



		//Route::get('/loginwith/{id}', 'Frontend\FrontendController@login');
		Route::get('find', 'Frontend\FrontendController@find');
		Route::get('/update-profile', 'Frontend\UserController@updateProfile');
		Route::get('/update-email', 'Frontend\UserController@updateEmail');
		Route::get('/{locale}/search', 'Frontend\FrontendController@search');
		Route::get('/{locale}/filter', 'Frontend\FrontendController@filter');
		Route::get('/sort', 'Frontend\FrontendController@sortBy');
		Route::get('/{locale}', 'Frontend\FrontendController@home');

		//ACCOUNT
		Route::post('/update-account', 'Frontend\UserController@update');
		Route::post('/search-country', 'Frontend\CountryController@search');
		Route::post('/search-state', 'Frontend\StateController@search');
		Route::get('/order-detail/{trxId}', 'Frontend\UserController@orderDetail');
		Route::get('/{locale}/my-account', 'Frontend\UserController@view');
		Route::get('/{locale}/create-free-event', 'Frontend\UserController@createEvent');
		Route::get('/{locale}/transaction', 'Frontend\UserController@transaction');
		Route::get('/{locale}/password/change', 'Frontend\UserController@changePassword');
		Route::post('/password/update', 'Frontend\UserController@updatePassword');

		//Tag
		Route::get('{locale}/tag/{slug}', 'Frontend\TagController@view');

		//Venue
		Route::get('{locale}/venue/{slug}', 'Frontend\VenueController@view');

		//Country
		Route::get('{locale}/country/{slug}', 'Frontend\CountryController@view');

		//asiangames
		Route::get('/{locale}/AsianGames2018', 'Frontend\AsiangamesController@index');
		Route::get('/{locale}/asian-games-2018', 'Frontend\AsiangamesController@index');
		Route::get('/{locale}/asian-games-2018/schedules', 'Frontend\AsiangamesController@schedules');
		Route::get('/{locale}/asian-games-2018/location', 'Frontend\AsiangamesController@location');
		Route::get('/{locale}/asian-games-2018/location/{slug}', 'Frontend\AsiangamesController@viewVenue');
		Route::get('/{locale}/asian-games-2018/{id}/{slug}', 'Frontend\AsiangamesController@viewEvent');
		Route::post('/{locale}/asian-games-2018/transaction', 'Frontend\AsiangamesController@setTransaction');
		Route::get('/{locale}/asian-games-2018/seat/{ticket}/{key}', 'Frontend\AsiangamesController@getSeat');
		Route::post('/{locale}/asian-games-2018/seat/check/', 'Frontend\AsiangamesController@checkSeat');
		Route::post('/{locale}/asian-games-2018/seat/order', 'Frontend\AsiangamesController@order');
				
	
		//Category
		Route::get('{locale}/{id}/{slug}', 'Frontend\CategoryController@view');


		//Event
		Route::get('{locale}/recommended', 'Frontend\EventController@recommended');
		Route::get('{locale}/event/{id}/{slug}', 'Frontend\EventController@view');
		//Route::get('{locale}/event/{id}/{slug}/{venue}', 'Frontend\EventController@venue');
		Route::post('/rating', 'Frontend\EventController@rating');
		Route::post('/searchticket', 'Frontend\EventController@searchSection');
		Route::post('/approve', 'Frontend\EventController@approve');

		//Order
		//Route::get('/order-generate/{transID}', 'Frontend\OrderController@orderTest');
		Route::post('/searchdate', 'Frontend\OrderController@searchdate');
		Route::post('/order', 'Frontend\OrderController@order');
		Route::post('/order-event', 'Frontend\OrderController@orderEvent');
		Route::post('/expired', 'Frontend\OrderController@expired');
		Route::get('expired/{key}', 'Frontend\OrderController@expiredView');
		Route::get('booking/{key}', 'Frontend\OrderController@orderReview');
		Route::post('/booking', 'Frontend\OrderController@booking');
		Route::get('payment/notification', 'Frontend\OrderController@notification');
		Route::post('payment/complete', 'Midtrans\SnapController@complete');
		Route::post('payment/fail', 'Midtrans\SnapController@fail');
		Route::post('payment/error', 'Midtrans\SnapController@error');
		Route::get('/order/complete', 'Midtrans\SnapController@orderComplete');
		Route::get('/order/fail', 'Midtrans\SnapController@orderFail');
		Route::get('/order/error', 'Midtrans\SnapController@orderError');
		Route::get('payment/{key}', 'Frontend\OrderController@bookingReview');
		Route::get('payments/{key}', 'Frontend\OrderController@bookingEmail');
		Route::post('/payment', 'Frontend\OrderController@payment');
		Route::post('/payment-fee', 'Frontend\OrderController@paymentFee');
		Route::post('/get-voucher', 'Frontend\OrderController@getVoucher');

		//Search
		Route::get('{locale}/search', 'Frontend\FrontendController@search');

		//Page
		Route::post('career/submission', 'Frontend\PageController@careerSubmission');
		Route::get('{locale}/page/career/apply', 'Frontend\PageController@careerApply');
		Route::get('{locale}/page/{id}/{slug}', 'Frontend\PageController@view');
		Route::post('/send', 'Frontend\PageController@send');

		//Article
		Route::get('{locale}/article', 'Frontend\PostController@index');
		Route::get('{locale}/article/{id}/{slug}', 'Frontend\PostController@view');

		//Subscribed
		Route::post('/subscribe', 'Frontend\SubscribeController@subscribe');
		Route::post('/subscribed', 'Frontend\SubscribeController@subscribed');
		Route::post('/unsubscribe', 'Frontend\SubscribeController@unsubscribe');

		//Comments
		Route::post('/comment', 'Frontend\CommentController@comment');
		Route::post('/comment/like-comment', 'Frontend\CommentController@likeComment');
		Route::post('/comment/dislike-comment', 'Frontend\CommentController@dislikeComment');
		Route::post('/comment/report-comment', 'Frontend\CommentController@reportComment');
		
		


		//Custom Layout
		Route::get('/{locale}/{custom}', 'Frontend\CustomController@index');

		
	});

