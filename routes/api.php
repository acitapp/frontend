<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware' => 'api'], function () {
    Auth::routes();
    Route::group(
        [
            'prefix' => 'alfamart'
        ],
        function () {
            Route::get('/inquery', 'Api\AlfamartController@inquery');
            Route::get('/payment', 'Api\AlfamartController@payment');
            Route::get('/commit', 'Api\AlfamartController@commit');
        
    });
    // Route::group(
    //     [
    //         'prefix' => 'seat'
    //     ],
    //     function () {
    //         Route::get('/getseat', 'Api\SeattController@getSeat');
    //         Route::get('/setseat', 'Api\SeattController@payment');
           
    // });
    Route::post('/order/notification', 'Midtrans\SnapController@notification');
    Route::post('/payment/notification', 'Midtrans\SnapController@notification');
}); 