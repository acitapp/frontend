 <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}
    <div class="form-group{{ $errors->has('error_email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('error_email'))
                <span class="help-block">
                   {{ $errors->first('error_email') }}
                </span>
                <script>
                    $.magnificPopup.open({
                      items: {
                        src: '#popup-login'
                      },
                      type: 'inline'
                    });
                </script>
            @endif
    </div>
    <div class="form-group{{ $errors->has('error_password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control" placeholder="@lang('content.password')" name="password" autocomplete="off" required>
            @if ($errors->has('error_password'))
                <span class="help-block">
                   {{ $errors->first('error_password') }}
                </span>
                <script>
                    $.magnificPopup.open({
                      items: {
                        src: '#popup-login'
                      },
                      type: 'inline'
                    });
                </script>
            @endif
    </div>
    <div class="form-group">
        <div class="remember-me">
             <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} class="checkbox">
            <label>
                @lang('content.remember_me')
            </label>
        </div>
    </div>
    <div class="form-group center">
        <div class="row">
            <div class="col-md-6">
                    <button type="submit" class="button">
                        @lang('content.login')
                    </button>
            </div>
            <div class="col-md-6">
                    <a class="button showPopup" href="#popup-register">
                        @lang('content.signup')
                    </a>
            </div>
        </div>
    </div>
    @php
        $template = $template ?? null;
    @endphp
    @if ($template != 'soundrenaline')
    <div class="form-group center">
        <span>@lang('content.or')</span>
    </div>
    <div class="form-group center">
        <div class="socialLogin">
           <a href="{{ url('/redirect/facebook') }}" class="button loginFacebook setCookie" data-url="{{URL::current()}}"> <i class="ion-social-facebook"></i> Login</a>
           <a href="{{ url('/redirect/google') }}" class="button loginGoogle setCookie"  data-url="{{URL::current()}}"> <i class="ion-social-googleplus"></i> Login</a>
        </div>
    </div>
    @endif
    <div class="form-group center">
        <a class="btn btn-link" href="{{ url('/password/reset') }}">
            @lang('content.forgot_password')
        </a>
    </div>
</form>