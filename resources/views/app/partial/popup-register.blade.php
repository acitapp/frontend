    <div class="popup">
      <div class="popup-container" id="popup-register">
        <div class="popup-header">
            <h3>@lang('content.signup')</h3>
        </div><!-- /.popup-header -->
        <div class="popup-body">
           <form class="login-form" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                <input id="fullname" type="text" class="form-control" name="fullname" placeholder="@lang('content.help_fullname')" value="{{ old('fullname') }}" required autofocus>
                @if ($errors->has('fullname'))
                    <span class="help-block">
                        {{ $errors->first('fullname') }}
                    </span>
                    <script>
                        $.magnificPopup.open({
                          items: {
                            src: '#popup-register'
                          },
                          type: 'inline'
                        });
                    </script>
                @endif
            </div>
            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="phoneCodeBox">
                        <select name="phone_code" id="phone_code" class="form-control select" data-live-search="true">
                          @foreach (getCountryLists() as $phoneCode)
                          @if (!is_null($phoneCode->phone_code))
                           <option value="{{$phoneCode->phone_code}}"  @if ($phoneCode->code == 'ID') selected @endif>+{{$phoneCode->phone_code}} - {{$phoneCode->name}}</option>
                          @endif
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-md-9">
                     {!! Form::text('phone',Auth::user()->information->phone ?? null, ['class'=>'form-control','placeholder'=>  Lang::get('content.phone')]) !!}
                    @if ($errors->has('phone'))
                        <span class="help-block">
                           {{ $errors->first('phone') }}
                        </span>
                        <script>
                            $.magnificPopup.open({
                              items: {
                                src: '#popup-register'
                              },
                              type: 'inline'
                            });
                        </script>
                    @endif
                    </div>
                  </div>
            </div>
            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                 {!! Form::text('dob',null, ['class'=>'form-control dob','placeholder'=>   Lang::get('content.dob')]) !!}
                @if ($errors->has('dob'))
                    <span class="help-block">
                       {{ $errors->first('dob') }}
                    </span>
                    <script>
                        $.magnificPopup.open({
                          items: {
                            src: '#popup-register'
                          },
                          type: 'inline'
                        });
                    </script>
                @endif
            </div>
            <div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                  <select name="country_id" id="country_id" class="form-control select">
                    @foreach (getCountryLists() as $country)
                     <option value="{{$country->id}}" @if ($country->code == 'ID') selected="selected" @endif>{{$country->name}}</option>
                    @endforeach
                  </select>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              
                <input type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        {{ $errors->first('email') }}
                    </span>
                    <script>
                        $.magnificPopup.open({
                          items: {
                            src: '#popup-register'
                          },
                          type: 'inline'
                        });
                    </script>
                @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              
                <input type="email" class="form-control" name="email_confirmation" placeholder="Ulangi Email" value="{{ old('email') }}" required>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
           
                <input type="password" class="form-control" placeholder="@lang('content.password')" name="password" required>

                @if ($errors->has('password'))
                    <script>
                        $.magnificPopup.open({
                          items: {
                            src: '#popup-register'
                          },
                          type: 'inline'
                        });
                    </script>
                @endif
            </div>

            <div class="form-group">
              <input id="password-confirm" type="password" class="form-control" placeholder="@lang('content.confirm_password')" name="password_confirmation" required>
            </div>

            <div class="form-group">
                <input id="checkbox" type="checkbox" required>
                <span>@lang('content.iagree')<a href="{{url('/')}}/page/terms-conditions">"@lang('content.tnc')"</a></span>
            </div>

            <div class="form-group">
                        <button type="submit" class="button btn-primary">
                           @lang('content.signup')
                        </button>
            </div>
        </form>
        </div><!-- /.popup-body -->
      </div><!-- /.popup-container -->
    </div><!-- /.popup -->