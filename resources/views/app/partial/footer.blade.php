{{-- <section class="section section-ads">
	<div class="ads leaderboard">
		<img src="{{url('/images/ads_728x90.gif')}}" alt="">
	</div>
</section> --}}
<footer>
<section class="footer-top">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="widget">
					<div class="widget-title">
						<h3>@lang('content.help_support')</h3>
					</div> {{-- .widget-title --}}
					<div class="widget-content">
						<ul>
							@foreach (getPageType('support') as $support)
							<li><a href="{{url('/')}}/{{lang()}}/page/{{$support->id}}/{{$support->getTranslation(lang())->slug}}">{{$support->getTranslation(lang())->title}}</a></li>
							@endforeach
						</ul>
					</div> {{-- .widget-content --}}
				</div> {{-- .widget --}}
			</div> {{-- .col --}}
			<div class="col-md-3">
				<div class="widget">
					<div class="widget-title">
						<h3>@lang('content.kiostix')</h3>
					</div> {{-- .widget-title --}}
					<div class="widget-content">
						<ul>
							@foreach (getPageType('kiostix') as $kiostix)
							<li><a href="{{url('/')}}/{{lang()}}/page/{{$kiostix->id}}/{{$kiostix->getTranslation(lang())->slug}}">{{$kiostix->getTranslation(lang())->title}}</a></li>
							@endforeach
							<li><a href="{{url('/')}}/{{lang()}}/article">@lang('content.magazine')</a></li>
						</ul>
					</div> {{-- .widget-content --}}
				</div> {{-- .widget --}}
			</div> {{-- .col --}}
			{{-- <div class="col-md-3">
				<div class="widget">
					<div class="widget-title">
						<h3><a href="{{url('/')}}/{{lang()}}/article">@lang('content.magazine')</a></h3>
					</div> 
					<div class="widget-content">
						<ul>
							@foreach (getPageType('service') as $service)
							<li><a href="{{url('/')}}/{{lang()}}/page/{{$service->id}}/{{$service->getTranslation(lang())->slug}}">{{$service->getTranslation(lang())->title}}</a></li>
							@endforeach
						</ul>
					</div>
				</div> 
			</div> --}}
			<div class="col-md-3">
				<div class="widget">
					<div class="widget-title">
						<h3>@lang('content.follow_us')</h3>
					</div> {{-- .widget-title --}}
					<div class="widget-content">
							<div class="socmed">
		       				  <a href="<?=getOption('social_instagram');?>" target="_blank"><i class="icon-brand-instagram"></i></a>
					          <a href="<?=getOption('social_facebook');?>" target="_blank"><i class="ion-social-facebook"></i></a>
					          <a href="<?=getOption('social_twitter');?>" target="_blank"><i class="ion-social-twitter"></i></a>
					          <a href="<?=getOption('social_youtube');?>" target="_blank"><i class="ion-social-youtube"></i></a>
							</div>{{-- .socmed --}}
					</div> {{-- .widget-content --}}
				</div> {{-- .widget --}}
			</div> {{-- .col --}}
		</div> {{-- .row --}}
	</div> {{-- .container --}}
</section>
<section class="footer-bottom">
	<div class="container flex-center">
			<p class="copy">@lang('content.copyright')</p>
	</div>{{-- .container --}}
</section>
</footer>