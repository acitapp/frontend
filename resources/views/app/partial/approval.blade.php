@if ($event->status != 1005)
<div class="widget-approval">
    <div class="panel panel-default" id="approveBox">
    <div class="panel-heading">
      <h3 class="panel-title">Approve This Event</h3>
    </div>
    <div class="panel-body">
       <form id="approveForm" class="form" role="form" method="POST" >
          {{ csrf_field() }}
          <input type="hidden" name="event_id" value="{{$event->id}}">
          <div class="form-group {{ $errors->has('published') ? ' has-error' : '' }}">
            {!! Form::label('published','Published Status:') !!}
            {!! Form::select('published',[1005 => 'Make Event Live',1004 => 'Pending',1000 => 'Reject'],null, ['class'=>'form-control']) !!}
          </div> <!-- /.form group -->
          <div class="form-group">
            <textarea name="message" id="approvemessage" class="form-control" placeholder="Message"></textarea>
          </div> <!-- /.form group -->
          <div class="form-group">
            <div class="loading loading-double hide" id="loaderapprove"></div>
            <input type="submit" id="approvesubmit" value="SUBMIT" class="button">
          </div> <!-- /.form group -->
      </form>
    </div>
  </div>
</div>
<style>
  .widget-approval{
    position: fixed;
    bottom: 20px;
    right: 20px;
    width: 280px;
    z-index: 1000;
  }
</style>
@endif