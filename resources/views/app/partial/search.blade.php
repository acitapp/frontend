<section id="search" class="page-{{currentUrl(1)}}">
    <div class="header-search bgcover">
        <div class="container">
          <div id="searchbox">
              <h2>@lang('content.action_text')</h2>
              <div>
              {!! Form::open(['method'=>'GET','url'=> lang().'/search/','class'=>'searchform typeahead','role'=>'search'])  !!}
                  <input type="text" name="q" value="" class="input-search search-input" placeholder="@lang('content.search_here')"  autocomplete="off">
                  <button type="submit" class="btn-search" name="submit"><i class="ion-ios-search-strong"></i></button>
              {!! Form::close() !!} 
              </div>
          </div>{{-- #searchbox --}}
        </div>{{-- .container --}}
    </div>{{-- .heading-search --}}
  </section>