<header>
<div id="top">
	<div id="sticky-anchor"></div>
	<div  id="sticky" class="header-box sticky">
		<div class="container">
			<div class="flex-center">
				<a href="{{url('/')}}" class="logo">
					<img src="{{url('/')}}/images/logo.png" alt="kiosTix">
				</a>
				<div id="navigation">
					<div class="navigation">
						<nav>
							<ul id="menu">
							
								@foreach (getCategory(3) as $key=>$menu)
									<li>
										<a href="{{url('/')}}/{{lang()}}/{{$menu->id}}/{{$menu->getTranslation(lang())->slug}}/" class="@if(currentUrl(2) == $menu->id) current @endif ">{{$menu->getTranslation(lang())->title}}</a>
									</li>
								@endforeach
								<li>
									<a href="{{url('/')}}/AsianGames2018" class="@if(currentUrl(2) == 'AsianGames2018') current @endif ">Asian Games 2018</a>
								</li>
							</ul>
						</nav>
					</div> {{-- .navigation --}}
				</div>{{-- #navigation --}}
				<div id="mobilenav">
					<ul id="footerNav">
						@auth
							<li>
								<a href="{{url('/')}}/{{lang()}}/my-account" class="">
									<i class="ion-person"></i> Hi, {{limitWord(Auth::user()->name,1)}}
								</a>
							</li>
							<li class="logout-btns">
								<a href="{{url('/')}}/logout">
									<i class="ion-ios-locked"></i> Keluar
								</a>
							</li>
						@else
							<li>
								<a href="#popup-login" class="showPopup">
									<i class="ion-person"></i> Masuk
								</a>
							</li>
						@endauth
					</ul>
					<div id="search-mobile">@include('app.widget.search')</div>
				</div>
				<div class="top-search">
					@include('app.widget.search')
				</div>
				<div class="socmed">
   				  <a href="<?=getOption('social_instagram');?>" target="_blank"><i class="icon-brand-instagram"></i></a>
		          <a href="<?=getOption('social_facebook');?>" target="_blank"><i class="ion-social-facebook"></i></a>
		          <a href="<?=getOption('social_twitter');?>" target="_blank"><i class="ion-social-twitter"></i></a>
		          <a href="<?=getOption('social_youtube');?>" target="_blank"><i class="ion-social-youtube"></i></a>
				</div>{{-- .socmed --}}
				<div class="box-nav">
					<ul id="topnav">
						@auth
							<li>
								<a href="{{url('/')}}/{{lang()}}/my-account" class="">
									<i class="ion-person"></i> <span>Hi, {{limitWord(Auth::user()->name,1)}}</span>
								</a>
							</li>
							<li class="logout-btns">
								<a href="{{url('/')}}/logout">
									<i class="ion-ios-locked"></i> <span>@lang('content.logout')</span>
								</a>
							</li>
						@else
							<li>
								<a href="#popup-login" class="showPopup button btn-outline btn-s  login">
									<span>@lang('content.login')</span>
								</a>
							</li>
						@endauth
					</ul>
				</div>{{-- .box-nav --}}
			</div>
		</div>{{-- .container --}}
	</div>{{-- .header-box --}}
</div>{{-- #top --}}


</header>