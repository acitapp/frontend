@component('layouts.app')
  @slot('webtitle')
    {{$page->title}}
  @endslot
  <section id="page-header" class="bgcover page-{{currentUrl(2)}}">
    <div class="header-page bgcover" style="background-image: url({{getThumbnail($page,'pages','large')}})">
        <div class="container">
          <div class="w800">
              <h2>{{$page->getTranslation(lang())->title}}</h2>
              <p>{!!$page->getTranslation(lang())->summary!!}</p>
          </div>{{-- #searchbox --}}
        </div>{{-- .container --}}
    </div>{{-- .heading-search --}}
  </section>
<section class="section flex-center">
  <div class="container">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      @foreach ($careers as $career)
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading-{{$career->getTranslation(lang())->slug}}">
          <h4 class="panel-title">
            <a role="button"  data-toggle="collapse"  data-parent="#accordion" href="#{{$career->getTranslation(lang())->slug}}" 
            @if($loop->first) aria-expanded="true" @else class="collapsed" data-toggle="collapse" aria-expanded="false" @endif aria-controls="{{$career->getTranslation(lang())->slug}}">
            {{$career->getTranslation(lang())->position}} - <span class="date">Available Until {{formatdate($career->expired_at)}}</span>
            </a>
          </h4>
        </div>{{-- .panel-heading --}}
        <div id="{{$career->getTranslation(lang())->slug}}" class="panel-collapse collapse @if($loop->first) in @endif" role="tabpanel" aria-labelledby="heading-{{$career->getTranslation(lang())->slug}}">
          <div class="panel-body"> 
          {!!$career->getTranslation(lang())->description!!}
          </div>
        </div>{{-- .panel-collapse --}}
      </div>{{-- .panel --}}
        @endforeach
    </div>{{-- .panel-group --}}
    <br>
    <div class="flex-center">
      <a href="{{url('/')}}/{{lang()}}/page/career/apply" class="button">APPLY NOW</a>
    </div>
  </div> {{-- .container --}}
</section>
@endcomponent
