@component('layouts.app')
 @include('app.partial.search')
 <div class="section">
  <section id="section-featured" class="sections desktop">
    <div class="container">
      <div class="titleBox no-border center">
        <h2 class="title">@lang('content.recommended')</h2>
      </div>
      <div class="rows">
        <div class="homeContent">
            <div class="singlecarousel owl-carousel owl-theme">
              @foreach (getFeaturedEvent(16)->chunk(8) as $events)
                <div class="row">
                    @foreach ($events as $event)
                    @if (notExpired($event->id))
                      <div class="col-md-3">
                          <div class="items">
                                  <div class="event-box box">
                                    <div class="event-image thumb">
                                      <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}">
                                         <img src="{{getThumbnail($event, 'events', 'small')}}">
                                      </a>
                                    </div>{{-- .event-image --}}
                                    <div class="event-entry">
                                       <h3>{{$event->getTranslation(lang())->title}}</h3>
                                    </div>
                                  </div>{{-- .box --}}
                          </div>
                      </div>
                    @endif
                    @endforeach
                </div>
              @endforeach
            </div>{{-- .carousel --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
  <section id="section-newest" class="sections">
    <div class="container">
      <div class="titleBox no-border center">
        <h2 class="title">@lang('content.newest')</h2>
        <a href="{{url('/')}}/id/1/event" class="btn-line seeAll">@lang('content.see_all') <i class="ion-chevron-right"></i></a>
      </div>
      <div class="rows">
        <div class="homeContent">
            <div class="carousel owl-carousel owl-theme">
              @foreach (getCategoryLimit(1, 16) as $event)
               @if (notExpired($event->id))
                <div class="items">
                        <div class="event-box box">
                          <div class="event-image thumb">
                            <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}">
                               <img src="{{getThumbnail($event, 'events', 'small')}}">
                            </a>
                          </div>{{-- .event-image --}}
                          <div class="event-entry">
                              <h3>{{$event->getTranslation(lang())->title}}</h3>
                          </div>
                        </div>{{-- .box --}}
                </div>
              @endif
              @endforeach
            </div>{{-- .carousel --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
  <section id="section-family" class="sections">
    <div class="container">
      <div class="titleBox no-border center">
        <h2 class="title">@lang('content.family_attraction')</h2>
        <a href="{{url('/')}}/id/2/aktivitas-dan-hiburan" class="btn-line seeAll">@lang('content.see_all') <i class="ion-chevron-right"></i></a>
      </div>
      <div class="rows">
        <div class="homeContent">
            <div class="carousel owl-carousel owl-theme">
              @foreach (getCategoryLimit(2, 8) as $event)
               @if (notExpired($event->id))
                  <div class="items">
                      <div class="event-box box">
                        <div class="event-image thumb">
                          <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}">
                             <img src="{{getThumbnail($event, 'events', 'small')}}">
                          </a>
                         {{--  <div class="event-rates">
                            <span class="total-rating">Ratings : {{$event->rate_average}}</span>
                            <span class="total-review">Review : {{$event->comment}} </span>
                          </div> --}}
                        </div>{{-- .event-image --}}
                        <div class="event-entry">
                            <div class="event-entry">
                               <h3>{{$event->getTranslation(lang())->title}}</h3>
                                <div class="event-venue">
                                  {{$event->schedule->venue->info->city->name ?? ''}},
                                  {{$event->schedule->venue->info->country->name ?? ''}}
                                </div>
                            </div>
                        </div>
                      </div>{{-- .box --}}
                  </div>
                  @endif
              @endforeach
            </div>{{-- .carousel --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
</div>
@endcomponent