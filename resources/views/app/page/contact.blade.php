@component('layouts.app')
  @slot('webtitle')
    {{getOption('web_title')}}
  @endslot
  <section id="page-header" class="bgcover page-{{currentUrl(2)}}">
    <div class="header-page bgcover" style="background-image: url({{getThumbnail($page,'pages','large')}})">
        <div class="container">
          <div class="w800">
              <h2>{{$page->getTranslation(lang())->title}}</h2>
              <p>{!!$page->getTranslation(lang())->summary!!}</p>
          </div>{{-- #searchbox --}}
        </div>{{-- .container --}}
    </div>{{-- .heading-search --}}
  </section>
<section class="section flex-center mt20">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <form class="" action="{{url('/payment/complete')}}" method="POST">
          <input type="submit" value="SUBMIT">
        </form>
        <form class="form contact-form mt20" id="contactform">
          {{csrf_field()}}
          <div class="rows">
            <input type="text" name="name" placeholder="Full Name">
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="rows">
                <input type="text" name="email" placeholder="Email Address">
              </div>
            </div>
            <div class="col-md-6">
              <div class="rows">
                <input type="text" name="phone" placeholder="Phone Number">
              </div>
            </div>
          </div>
          <div class="rows">
            <input type="text" name="subject" placeholder="Subject">
          </div>
          <div class="rows">
            <textarea name="message" placeholder="Message"></textarea>
          </div>
          <div class="rows">
            <div class="loading loading-double hide" id="loaderContact"></div>
            <input type="submit" class="button btn-blue" value="SUBMIT">
          </div>
        </form>
      </div>{{-- .col --}}
      <div class="col-md-5">
          <div class="map mt20">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.9085506419715!2d106.81926231420506!3d-6.275753663179077!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fb2f7f8d7873%3A0x981157b80b82125f!2sKiosTiX!5e0!3m2!1sen!2sid!4v1504766991555" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <div class="address">
              {!!$page->description!!}
          </div>
      </div>{{-- .col --}}
    </div>{{-- .row --}}
  </div> {{-- .container --}}
</section>
@endcomponent