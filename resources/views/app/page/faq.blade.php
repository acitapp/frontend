@component('layouts.app')
  @slot('webtitle')
    {{getOption('web_title')}}
  @endslot
  <section id="page-header" class="bgcover page-{{currentUrl(2)}}">
    <div class="header-page bgcover" style="background-image: url({{getThumbnail($page,'pages','large')}})">
        <div class="container">
          <div class="w800">
              <h2>{{$page->getTranslation(lang())->title}}</h2>
              <p>{!!$page->getTranslation(lang())->summary!!}</p>
          </div>{{-- #searchbox --}}
        </div>{{-- .container --}}
    </div>{{-- .heading-search --}}
  </section>
  <section id="section-home" class="section">
    <div class="container">
      <div class="theContent w800">
        {!!$page->getTranslation(lang())->description!!}
      </div>{{-- .col --}}
    </div>{{-- .container --}}
  </section>
@endcomponent