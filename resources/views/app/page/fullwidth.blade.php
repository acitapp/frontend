@component('layouts.app')
  @slot('webtitle')
    {{getOption('web_title')}} - {{$page->title}}
  @endslot
  @if (count($page->medias))
    @php
      $media = $page->medias()->first();
    @endphp
  @endif
<section id="section" class="section flex-center single mt20">
  <div class="container">
    <div class="row">
      <div class="col-md-12 theContent">
        <div class="box">
          <div class="box-contents">
          <div class="titlebox center-title">
            <h2 class="title"><span>{{$page->title}}</span></h2>
          </div>
           <div class="entry-content mt20">
              @if (count($page->getMedia()))
              <div class="box-meta">
                    <a class="thumb">
                        @php
                          $media = $page->getMedia()->first();
                        @endphp
                        <img src="{{ url('/') }}/media/{{$media->id}}/conversions/maxres.jpg" >
                    </a>
              </div>{{-- .box-meta --}}
             @endif
              <div class="page-content">
                <div class="page-desc" id="page">
                    {!! $page->description !!}
                </div>
              </div>{{-- .box-footer --}}
            </div>{{-- .entry-content --}}
          </div>{{-- .box-content --}}
        </div>{{-- .box --}}
      </div>{{-- .col --}}
    </div>{{-- .row --}}
  </div> {{-- .container --}}
</section>
@endcomponent
