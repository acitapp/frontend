@component('layouts.app')
  @slot('webtitle')
    kiosTix -  Asian Games 2018
  @endslot
  <section id="section-home" class="section">
    <div class="container">
      <div class="theContent w800">
          <a href="#popup-register" class="showPopup thumb">
            <img src="{{url('/images/comingsoon.jpg')}}" alt="">
          </a>
      </div>{{-- .col --}}
    </div>{{-- .container --}}
  </section>
@endcomponent