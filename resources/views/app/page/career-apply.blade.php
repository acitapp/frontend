@component('layouts.app')
  @slot('webtitle')
    Career
  @endslot
  <section id="page-header" class="bgcover page-{{currentUrl(2)}}">
    <div class="header-page bgcover">
        <div class="container">
          <div class="w800">
              <h2>Work at <strong>KiosTix.com</strong></h2>
          </div>{{-- #searchbox --}}
        </div>{{-- .container --}}
    </div>{{-- .heading-search --}}
  </section>
<section class="section flex-center">
  <div class="container">
        {!! Form::model($CareerSubmission = new\App\Models\CareerSubmission,['url' => 'career/submission', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
          <div class="row flex-center">
            <div class="col-md-6">
                <div class="widget">
                    <div class="widget-form">
                        <div class="row rows flex-center center">
                          <p>We invite you to fill them in immediately!<br>
Make sure to show us your area of expertise and why you think that you would make a great fit.</p>
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                              {!! Form::select('career_id',$careers ,null, ['class'=>'form-control select','data-live-search' => 'true','id'=>'career_id','placeholder'=> Lang::get('content.select_position')]) !!}
                                @if ($errors->has('career_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('career_id') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                               {!! Form::text('name',null, ['class'=>'form-control','placeholder'=> Lang::get('content.fullname')]) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                               {!! Form::text('email',null, ['class'=>'form-control','placeholder'=>'Email']) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                               {!! Form::text('phone',null, ['class'=>'form-control','placeholder'=> Lang::get('content.phone')]) !!}
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-6">
                                      <input type="file"  id="cv" class="button"  name="cv" title="Upload CV (PDF)">
                                      @if ($errors->has('cv'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('cv') }}</strong>
                                          </span>
                                      @endif
                            </div>{{-- .col --}}
                            <div class="col-md-6">
                                {!! Form::submit('Apply Now!', ['class'=>'button btn-blue fr']) !!} 
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                    </div>{{-- .widget-form --}}
                </div>{{-- .widget --}}
            </div>{{-- .col --}}
          </div>{{-- .row --}}
            {!! Form::close() !!}
  </div> {{-- .container --}}
</section>
@endcomponent
