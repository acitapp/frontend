@component('layouts.app')
  @slot('webtitle')
  Tiket {{$event->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$event->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($event->getTranslation(lang())->summary)}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($event, 'events', 'medium')}}
  @endslot
  @slot('template')
   {{$event->template}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
      <div class="canvas">
	  		<div class="canvas-entry">
	  			<h3>kiosTix Seat Layout - {{$event->getTranslation(lang())->title}}</h3>
	  			<div class="nav-boxs">
			  		<div class="navSeat btn-group list-ticket">
				  		
			  		</div>
	  				
	  			</div>
		 		<canvas id="createSeat" width="1000" height="600"></canvas>
                <form action="{{url('/order-event')}}" method="post" accept-charset="utf-8" id="form-data" role="form" enctype="multipart/form-data">
		             {!! csrf_field() !!}
                     <div class="row">
                        <div class="form-group">
                            <div class="hide-input">
                            </div>
                            <div class="navSeat">
                                <input type="hidden"  id="seats" placeholder="name" name="seats" class="form-control">
                                
                                <button class="btn btn-primary btn-submit" name="button"  type="button" style="display:none">Submit</button>
                            </div>
                        </div>
			        </div>
                </form>
	  		</div>

	  	</div>
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
  <script type="text/javascript" src="{{url('vendor/jscolor/jscolor.min.js')}}"></script>
<script type="text/javascript" src="{{url('vendor/fabric/dist/fabric.min.js')}}"></script>
<script type="text/javascript" src="{{url('vendor/seat/seat.js')}}"></script>
<script>
	  var canvas = new fabric.Canvas('createSeat');
    
    var jsonSeat ={!!$seat!!};	
    var jsontiket ={!!$arraytiket!!};
    var arraytiket = new Array();	
    var jumlahtiketskrg = new Array();
    var customer = {!!$customer!!}
    var total = 0;
    //for input 
    var event ="{!!$eventkey!!}";
    var schedule ="{!!$schedule!!}";
    var visit_date ="{!!$visitdate!!}";
    var visit_time ="{!!$visitTime!!}";
    var venue ="{!!$venue!!}";
    var dragging = false;
		var lastX = 0;
	    var marginLeft = 0;
		var marginTop = 0;

    canvas.on('mouse:dblclick', function (option) {   
			
			if(dragging==true)
			{
				dragging=false;
				
			}
			else{
				dragging=true;
				option.e.target.style.marginLeft = "0px";
				$('#createSeat').css("margin-left", "0px");
			}
		});
		canvas.on('mouse:move:before', function (option) {  
			if(dragging==true)
			{
				lastX='-200';
	    		marginLeft = 0;
				marginTop = 0;
				option.e.target.style.marginLeft = "0px";
				$('#createSeat').css("margin-left", "0px");
			}
		});
		canvas.on('mouse:move', function (option) {  
			if(dragging==true)
			{
				var delta =  parseInt(lastX) - parseInt(option.e.clientX);
			
				lastX = option.e.clientX;
				marginLeft = parseInt(marginLeft)+parseInt(delta);
				
				
				// option.e.target.style.marginLeft = marginLeft + "px";
				$('#createSeat').css("margin-left",marginLeft + "px");
				
			
			}
			option.e.preventDefault();
		});
		canvas.on('mouse:out', function (option) {
			$('#createSeat').css("margin-left", "0px");
		});



    $.each(jsontiket, function(row, value) {
        arraytiket[row]=value;
        jumlahtiketskrg[row]=0;
        total = (parseInt(total) + parseInt(value));
        $('.hide-input').append('<input type="hidden" name="ticket[]"  value="'+row+'">');
        $('.hide-input').append('<input type="hidden" name="qty[]"  value="'+value+'">');
    });
    showseat();	
    checkQty();
    function showseat(){
        for (i = 0; i < jsonSeat.length; i++) {
           selectable=true;
           if(jsonSeat[i].status_order==1)
           {
                jsonSeat[i].color_seat='#cecece';
                selectable=false;

           }
           else if(jsonSeat[i].status_order==2)
           {
               if(customer==jsonSeat[i].customer_id)
               {
                    jumlahtiketskrg[jsonSeat[i].tiket_id]=(parseInt(jumlahtiketskrg[jsonSeat[i].tiket_id])+1);
               }
                jsonSeat[i].color_seat='#edfc01';
                
           }
           
            var rect = new fabric.Rect({ 
                    width: 40, 
                    height: 40, 
                    fill: jsonSeat[i].color_seat, 
                    strokeWidth:1 ,
                    stroke: '#fff',
            });
            var text = new fabric.Text(jsonSeat[i].title, { 
                fontSize: 8, 
                textAlign: 'left',
                fill: jsonSeat[i].color_text, 
                originX: 'center',
                fontFamily: 'arial', 
                strokeWidth:0,
                left:20, 
                top: 12,
            });
            var group = new fabric.Group([ rect, text ], { 
                id : parseInt(jsonSeat[i].id),
                name : jsonSeat[i].title,
                left: parseInt(jsonSeat[i].left), 
                top: parseInt(jsonSeat[i].top),
                lockMovementY :true,
                lockMovementX:true,
                selectable:selectable,
                hasControls:false,
                tiket:jsonSeat[i].tiket_id,
                customer:jsonSeat[i].customer_id,
            });
            canvas.add(group).calcOffset();
           
        }

      
    }
    function checkQty(){
        sisa =0;
        $.each(jsontiket, function(row, value) {
             sisa = sisa + jumlahtiketskrg[row];
        });
        
        if(sisa>=total){
            $('.btn-submit').show();

        }
    }
   
    canvas.on('selection:created', function(options) { 
      
       id=options.selected[0].id;
       if(options.selected[0].customer!=customer){
           console.log(jumlahtiketskrg[options.selected[0].tiket]+'>=' + arraytiket[options.selected[0].tiket])
            if(jumlahtiketskrg[options.selected[0].tiket] >= arraytiket[options.selected[0].tiket])
                {
                    $("#popup-title").text("Ooops!");
                    $("#popup-text").text('kuota tiket '+options.selected[0].name+' sudah habis');
                    common.popup();
                    canvas.requestRenderAll();
                
                    canvas.discardActiveObject();
                    checkQty();
                   
                    return false;
                }
           
            
       }
        

       
            $.ajax({
                    method: "POST",
                    url: base_url + "/seat/check/",
                    data: {id:id,_token:token }
            }).done(function (data) {
                
                if(data.status==1){
                    jumlahtiketskrg[options.selected[0].tiket] = (parseInt(jumlahtiketskrg[options.selected[0].tiket])+1);
            
                    options.selected[0]._objects[0].set("fill", '#edfc01');
                }else if(data.status==3){
                    options.selected[0]._objects[0].set("fill", data.seat.color_seat);
                    if(jumlahtiketskrg[options.selected[0].tiket]>0){
                        jumlahtiketskrg[options.selected[0].tiket] = (parseInt(jumlahtiketskrg[options.selected[0].tiket])-1);
            
                    }
                    
                }
                canvas.requestRenderAll();
                
                canvas.discardActiveObject();
                checkQty();
            });
            
      
        
      });
      $('.btn-submit').on('click',function(){
         $('.hide-input').append('<input type="hidden" name="schedule"  value="'+schedule+'">');
         $('.hide-input').append('<input type="hidden" name="visit_date"  value="'+visit_date+'">');
         $('.hide-input').append('<input type="hidden" name=""visit_time"  value="'+visit_time+'">');
         $('.hide-input').append('<input type="hidden" name="event"  value="'+event+'">');
         $('.hide-input').append('<input type="hidden" name=""venue"  value="'+venue+'">');
         $('#form-data').submit();
      })
</script>
  @auth
  @role('Super Admin')
     @include('app.partial.approval')
  @endrole
  @endauth
@endcomponent