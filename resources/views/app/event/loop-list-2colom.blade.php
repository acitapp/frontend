<div id="data" class="event-list">
@foreach ($events->chunk(2) as $events)
    <div class="row">
      @foreach ($events as $event)
      @if ($event->trash == false)
        @if ($event->status == 1005)  
        @if (notExpired($event->id))
        <div class="col-md-6">
        <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}" class="eventBox">
        <div class="event-box box flex-center">
          <div class="event-image thumb thumb-small">
               <img src="{{getThumbnail($event, 'events', 'small')}}">
          </div>{{-- .event-image --}}
          <div class="event-information">
            <div class="event-entry">
                <h3>{{$event->getTranslation(lang())->title}}</h3>
                @php
                 $schedule = $event->schedules->first();
                 $countSechedule = $event->schedules->count();
                 $venue = $event->schedules()->with('venue')->first();
                @endphp
                {!! getRangeDate($event->id) !!}
                <div class="event-venue">
                  @if ($countSechedule > 1)
                    Ada di beberapa tempat
                  @else
                   {{$schedule->venue->title ?? ''}}
                  @endif
                </span>
                </div>
            </div>
            <div class="btnAction">
              <h3 class="lowerPrice">
                <span>{{getRangePrice($schedule)}}</span>
              </h3>
            </div>{{-- .btnAction --}}
          </div>
        </div>{{-- .box --}}
    </a>
        </div>
        @endif
        @endif
      @endif
      @endforeach
      
    </div>
@endforeach
</div>{{-- .row --}}
{{-- <br>
<div class="flex-center">{{ $events -> links()}}</div> --}}