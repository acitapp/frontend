<div id="data" class="row">
@foreach ($events as $event)
<div class="col-md-3">
    <div class="event-box box">
      <div class="event-image thumb">
        @if (is_null($event->url))
        <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}">
        @else
        <a href="{{$event->url}}" target="_blank">
        @endif
           <img src="{{getThumbnail($event, 'events', 'small')}}">
        </a>
      </div>{{-- .event-image --}}
      <div class="event-entry">
          <h3>{{$event->getTranslation(lang())->title}}</h3>
      </div>
    </div>{{-- .box --}}
</div>{{-- .col --}}
@endforeach
</div>{{-- .row --}}
{{-- <div class="flex-center">{{ $events -> links()}}</div> --}}