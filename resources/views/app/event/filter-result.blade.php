@component('layouts.app')
 @include('app.partial.search')
  <section id="section-home" class="section">
    <div class="container">
      <div class="titleBox flex flex-center">
        <h2 class="title" id="titleFilters">Event {{$filter}}</h2>
        <div class="pull-right">
         @include('app.widget.filter')
        </div>
      </div>
      <div class="theContents Events">
          @if (count($events))
            <div class="scroll">
              @include('app.event.loop-list-filter')
            </div>
          @else
            <h3>Data Not Available</h3>
          @endif
      </div>{{-- .col --}}
    </div>{{-- .container --}}
  </section>
@endcomponent