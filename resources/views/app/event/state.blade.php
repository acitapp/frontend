@component('layouts.app')
  @slot('webtitle')
    {{getOption('web_title')}}
  @endslot
  <section id="section-home" class="section">
    <div class="container">
      <div class="titleBox flex flex-center">
        <h2 class="title">{{$state->name}}</h2>
        @include('app.widget.filter')
      </div>
      <div class="theContents">
          @if (count($events))
            <div class="scroll">
              @include('app.event.loop')
            </div>
          @else
            <h3>Data Not Available</h3>
          @endif
      </div>{{-- .col --}}
    </div>{{-- .container --}}
  </section>
@endcomponent