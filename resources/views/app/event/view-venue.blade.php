@component('layouts.app')
  @slot('webtitle')
  {{$event->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$event->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($event->getTranslation(lang())->summary)}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($event, 'events', 'medium')}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="event-meta">
            <h1>{{$event->getTranslation(lang())->title}}</h1>
          </div>{{-- .event-meta --}}
        </div>
        <div class="col-md-6">
            <div class="event-entry">
                <div class="event-image thumb">
                  <img src="{{getThumbnail($event, 'events', 'medium')}}">
                </div>{{-- .event-image --}}
                <div class="event-detail">
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">DETAILS</a></li>
                        <li role="presentation"><a href="#tnc" aria-controls="tnc" role="tab" data-toggle="tab">TERMS & CONDITIONS</a></li>
                        <li class="shareTab">
                          @include('app.widget.sharebox')
                        </li>
                      </ul>
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="details">
                          <div class="event-info event-artist flex">
                              <label>Tag :</label>
                              <div class="artists">
                                @foreach($event->tags as $tag)
                                 <a class="venue-name" href="{{url('/')}}/{{lang()}}/tag/{{$tag->slug}}">{{$tag->name}}</a> 
                                @endforeach
                              </div>
                          </div>
                          <div class="event-info event-venue flex">
                              <label>Venue :</label>
                              <div class="venues">
                                @foreach($event->venues as $venue)
                                 <a class="venue-name" href="{{url('/')}}/{{lang()}}/venue/{{$venue->slug}}">{{$venue->title}}</a> 
                                @endforeach
                              </div>
                          </div>
                          <div class="event-description">
                          {!! $event->getTranslation(lang())->description !!}
                          </div>
                        </div>{{-- .tab-pane --}}
                        <div role="tabpanel" class="tab-pane" id="tnc">
                          {!! $event->getTranslation(lang())->terms !!}
                        </div>{{-- .tab-pane --}}
                      </div>{{-- .tab-content --}}
                </div>{{-- .event-detail --}}
               @include('app.widget.comment')
            </div>{{-- .event-entry --}}
        </div>{{-- .col --}}
        <div class="col-md-6">
            <div class="widget-options">
                  @include('app.event.venue')
                  @include('app.event.schedule')
                  @include('app.event.booking-detail')
            </div>
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
@endcomponent