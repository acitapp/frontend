<div class="widget widget-box">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          @foreach ($venues as $venue)
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="ticket-{{$venue->id}}">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" class="collapsed sectionBtn section-disable" data-parent="#accordion" href="#ticket-venue-{{$venue->id}}" aria-expanded="true" aria-controls="ticket-venue-{{$venue->id}}">
                      {{$venue->title}}, <span>{{$venue->info->state->name}}, {{$venue->info->country->name}}</span>
                    </a>
                  </h4>
                </div>{{-- .panel-heading --}}
                <div id="ticket-venue-{{$venue->id}}" class="panel-collapse collapse @if ($loop->first) in  @endif" role="tabpanel" aria-labelledby="{{$venue->id}}">
                  <div class="panel-body">
                      <p><strong>Capacity : {{number_format($venue->info->capacity)}} Peoples</strong></p>
                      {{$venue->description}}

                      <div class="map">
                        {!!$venue->info->iframe!!}
                      </div>
                      <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}/{{$venue->slug}}" class="button">FIND SCHEDULES</a>
                  </div>{{-- .panel-body --}}
                </div>{{-- .panel-collapse --}}
              </div>{{-- .panel --}}
          @endforeach
      </div>
</div>