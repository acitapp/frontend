<div class="widget widget-box">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="ticket-{{$venue->id}}">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" class="collapsed sectionBtn section-disable" data-parent="#accordion" href="#ticket-venue-{{$venue->id}}" aria-expanded="true" aria-controls="ticket-venue-{{$venue->id}}">
                      {{$venue->title}}, <span>{{$venue->info->state->name ?? ''}}, {{$venue->info->country->name ?? ''}}</span>
                    </a>
                  </h4>
                </div>{{-- .panel-heading --}}
                <div id="ticket-venue-{{$venue->id}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="{{$venue->id}}">
                  <div class="panel-body">
                      <p><strong>Capacity : {{number_format($venue->info->capacity ?? 0)}} Peoples</strong></p>
                      {{$venue->description}}

                      <div class="map">
                        {!!$venue->info->iframe ?? 0!!}
                      </div>
                  </div>{{-- .panel-body --}}
                </div>{{-- .panel-collapse --}}
              </div>{{-- .panel --}}
      </div>
</div>