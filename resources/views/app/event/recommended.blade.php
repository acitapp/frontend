@component('layouts.app')
  @slot('webtitle')
    {{getOption('web_title')}}
  @endslot
 @include('app.partial.search')
  <section id="section-home" class="section">
    <div class="container">
      <div class="titleBox flex flex-center">
        <h2 class="title">Rekomendasi</h2>
         @include('app.widget.filter')
      </div>{{-- .titleBox --}}
      <div class="theContents">
          @if (count($events))
            <div class="scroll">
                  @include('app.event.loop-list')
            </div>
          @else
            <h3>Data Not Available</h3>
          @endif
      </div>{{-- .col --}}
    </div>{{-- .container --}}
  </section>
@endcomponent