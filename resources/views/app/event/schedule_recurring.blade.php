<form id="order-form-{{$schedule->id}}" schedule="{{$schedule->id}}" class="orderForm" method="POST" action="{{url('/order-event')}}">
  {{csrf_field()}}
 <div class="widget-list">
    <div class="colom-row">
        <div class="flex-center">
          <div class="colom-left">
           <h4 class="schedule-name">{{$schedule->name}}</h4>
       {{--    <span class="date start-date">{{eventDateFormat($schedule->started_at)}}</span>
          -&nbsp;&nbsp;&nbsp;<span class="date end-date">{{eventDateFormat($schedule->ended_at)}}</span> --}}
          </div>{{-- .colom-right --}}
          <div class="colom-right">
            <h5 class="priceBox">
              <span>{{getRangePrice($schedule)}}</span>
            </h5> 
          </div>{{-- .colom-right --}}
        </div>
        <div class="visitDate">
          <input type="text" id="visitdate-{{$schedule->id}}" schedule="{{$schedule->id}}" name="visit_date" class="visitdate form-control" placeholder="Pilih tanggal berkunjung" readonly>
          <input type="hidden" name="schedule" value="{{$schedule->id}}">
        </div>
        <div id="time-schedule-{{$schedule->id}}" class="times session hide">
          @foreach ($schedule->sessions as $session)
            <input class="radio-session" type="radio" session="{{$session->id}}" schedule="{{$schedule->id}}"  value="{{$session->time}}" name="visit_time" id="session-{{$session->id}}">
            <label  for="session-{{$session->id}}" class="label label-session" schedule="{{$schedule->id}}">{{date('g:ia', strtotime($session->time))}}</label>
          @endforeach
        </div>
        <script>
            var start = "{{formatdateday($schedule->started_at)}}";
            var end = "{{formatdateday($schedule->ended_at)}}";
            var scheduleID = "{{$schedule->id}}";
            var dayDisabled = "{{everyDay($schedule->every_day)}}";
        </script>
          @if ($schedule->recurring_type == 'everyday')
           <script>  
                 $("#visitdate-{{$schedule->id}}").datepicker({
                    format: "dd M yyyy",
                    startDate: start,
                    endDate: end,
                    daysOfWeekDisabled: dayDisabled,
                   // daysOfWeekHighlighted: "0,6",
                     autoclose: true
                  }).on('changeDate', function(e) {
                      console.log(e);
                      console.log(e.currentTarget.value);
                       common.hideOtherSchedule({{$schedule->id}});
                      $('#time-schedule-{{$schedule->id}}').removeClass('hide');
                      $('#view-schedule-{{$schedule->id}}').removeClass('hide');
                      $('#data-visit-{{$schedule->id}}').val(e.currentTarget.value);
                      var text  = $('#alert-{{$schedule->id}}').attr('alert-session');
                      $('#alert-{{$schedule->id}}').text(text).show().delay(3000).fadeOut(1000);
                  });;    
           </script>
          @elseif($schedule->recurring_type == 'every_date')
           <script>  
                 $("#visitdate-{{$schedule->id}}").datepicker({
                    format: "dd M yyyy",
                    startDate: start,
                    endDate: end,
                    daysOfWeekDisabled: "1,2,3,4,5",
                   // daysOfWeekHighlighted: "0,6",
                     autoclose: true
                  }).on('changeDate', function(e) {
                      console.log(e);
                      console.log(e.currentTarget.value);
                       common.hideOtherSchedule({{$schedule->id}});
                      $('#time-schedule-{{$schedule->id}}').removeClass('hide');
                      $('#view-schedule-{{$schedule->id}}').removeClass('hide');
                      $('#data-visit-{{$schedule->id}}').val(e.currentTarget.value);
                      var text  = $('#alert-{{$schedule->id}}').attr('alert-session');
                      $('#alert-{{$schedule->id}}').text(text).show().delay(3000).fadeOut(1000);
                  });;    
           </script>
          @elseif($schedule->recurring_type == 'daily')
           <script>  
                 $("#visitdate-{{$schedule->id}}").datepicker({
                    format: "dd M yyyy",
                    startDate: start,
                    endDate: end,
                    autoclose: true
                  }).on('changeDate', function(e) {
                      console.log(e);
                      console.log(e.currentTarget.value);
                       common.hideOtherSchedule({{$schedule->id}});
                      $('#time-schedule-{{$schedule->id}}').removeClass('hide');
                      $('#view-schedule-{{$schedule->id}}').removeClass('hide');
                      $('#data-visit-{{$schedule->id}}').val(e.currentTarget.value);
                      var text  = $('#alert-{{$schedule->id}}').attr('alert-session');
                      $('#alert-{{$schedule->id}}').text(text).show().delay(3000).fadeOut(1000);
                  });;    
           </script>
          @endif
        <a id="find-ticket-{{$schedule->id}}" href="#" class="hide button btn-m find-ticket" schedule="{{$schedule->id}}" event="{{$event->id}}" venue="{{$schedule->venue->id}}" type="{{$schedule->type}}">Cari Tiket</a>
        <input type="hidden" id="data-visit-{{$schedule->id}}" value="@if($schedule->type == 'spesific'){{formatdateday($schedule->started_at)}}@endif">
    </div>{{-- .colom-left --}}
 </div>{{-- .widget-list --}}
 <div class="loading loading-double hide" id="loader-schedule-{{$schedule->id}}"></div>
<div class="alert alert-warning alert-small" id="alert-{{$schedule->id}}" alert-session="Mohon pilih jam berkunjung terlebih dahulu." style="display: none;"></div>
<div class="booking-container" style="display: none;">
  <div class="booking-body" id="booking-body-{{$schedule->id}}">
      <div class="booking-header flex-center">
          <div class="colom-info col3">
            <span>Harga/Section</span>
          </div>{{-- .colom-left --}}
          <div class="colom-left col3 center">
            <span>Kuantitas</span>
          </div>{{-- .colom-left --}}
          <div class="colom-right col3 text-right">
            <span>Subtotal</span>
          </div>{{-- .colom-right --}}
      </div>
     <div class="section-box" id="section-schedule-{{$schedule->id}}">
     </div>
   </div>
   <div id="booking-detail-{{$schedule->id}}" class="booking-detail hide">
      <div class="booking-footer">
          <div class="item-row booking-section">
            <span class="product-item">Total</span>
            <span class="product-price total-price">Rp. <span id="total-price-{{$schedule->id}}" data-price="0">0</span></span>
          </div>
            <input id="input-event-{{$schedule->id}}" type="hidden" name="event" value="{{$event->id}}">
            <input id="input-venue-{{$schedule->id}}" type="hidden" name="venue" value="{{$schedule->venue->id}}">
            @auth
            <button type="submit" class="button btn-block">@lang('content.order_now')</button>
            @else
            <a href="#popup-login" class="button btn-block submitOrder showPopup disabled">@lang('content.order_now')</a>
            @endauth
            <div class="loading loading-double hide" id="loader-order-{{$schedule->id}}"></div>
      </div>
  </div>{{-- .booking-detail --}}
  <a id="view-schedule-{{$schedule->id}}" href="#" class="view-schedule hide">Lihat Jadwal Lainnya</a>
</div>{{-- .booking-container --}}
</form>