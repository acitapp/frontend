<form action="#">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  @foreach ($event->sections as $section)
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="ticket-{{$section->id}}">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" class="collapsed sectionBtn section-disable" data-parent="#accordion" href="#ticket-section-{{$section->id}}" aria-expanded="true" aria-controls="ticket-section-{{$section->id}}">
              {{$section->name}}
            </a>
          </h4>
        </div>{{-- .panel-heading --}}
        <div id="ticket-section-{{$section->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{$section->id}}">
          <div class="panel-body">
              <div class="row rows" id="row-date">
                <div class="col-md-4">
                  <span for="">Visit Date</span>
                   <div class="loading loading-double hide loaderSelectGo" id="loader-section"></div>
                </div>{{-- .col --}}
                <div class="col-md-8">
                  <input type="text" class="datepickerEvent" start-date="{{$scheduleEvent->started_at}}" end-date="{{$scheduleEvent->ended_at}}" event-id="{{$event->id}}" section-id="{{$section->id}}" placeholder="Select Date" readonly>
                </div>{{-- .col --}}
              </div>{{-- .row --}}
              @foreach ($section->prices as $price)
                @if ($price->adult_value != 0)
                  <div class="row rows">
                    <div class="col-md-8">
                      <span  class="labels"><strong>Adults</strong>  ({{$price->period_name}})</span>
                      <div class="priceBox">
                        <span class="price price-discount">Rp. {{number_format($price->disc_adult_value)}}</span>
                        <span class="price @if ($price->disc_adult_value != 0) has-discount @endif">Rp. {{number_format($price->adult_value)}}</span>
                      </div>
                    </div>{{-- .col --}}
                    <div class="col-md-4">
                      <div class="input-group order-qty">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="qty">
                                    <span class="ion-minus"></span>
                                </button>
                            </span>
                            <input type="text" name="qty" class="form-control input-number" value="0" min="0" max="10">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="qty">
                                    <span class="ion-plus"></span>
                                </button>
                            </span>
                        </div>{{-- .input-group --}}
                    </div>{{-- .col --}}
                  </div>{{-- .row --}}
                @endif
                @if ($price->child_value != 0)       
                <div class="row rows">
                    <div class="col-md-8">
                      <span  class="labels"><strong>Children</strong> ({{$price->period_name}})</span>
                      <div class="priceBox">
                        <span class="price price-discount">Rp. {{number_format($price->disc_child_value)}}</span>
                        <span class="price @if ($price->disc_child_value != 0) has-discount @endif">Rp. {{number_format($price->child_value)}}</span>
                      </div>
                    </div>{{-- .col --}}
                    <div class="col-md-4">
                      <div class="input-group order-qty">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="qty-{{$section->id}}">
                                    <span class="ion-minus"></span>
                                </button>
                            </span>
                            <input type="text" name="qty-{{$section->id}}" class="form-control input-number" value="0" min="0" max="10">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="qty-{{$section->id}}">
                                    <span class="ion-plus"></span>
                                </button>
                            </span>
                        </div>{{-- .input-group --}}
                    </div>{{-- .col --}}
                  </div>{{-- .row --}}
                @endif
              @endforeach
              <div class="row">
                <div class="col-md-6">
                    <a class="btn-line" role="button" data-toggle="collapse" href="#term-{{$section->id}}" aria-expanded="false" aria-controls="term-{{$section->id}}">
                      Description
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="#" class="button btn-block btn-add">ADD TICKET</a>
                </div>
              </div>
              <div class="collapse" id="term-{{$section->id}}">
                <div class="well">
                  {!!$section->tnc!!}
                </div>
              </div>
          </div>{{-- .panel-body --}}
        </div>{{-- .panel-collapse --}}
      </div>{{-- .panel --}}
  @endforeach
    </div>
</form>