@if (empty($event->template) || is_null($event->template))
@component('layouts.app')
@else
@component('custom.'.$event->template.'.'.$event->template)
@endif
  @slot('webtitle')
  {{$event->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$event->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($event->getTranslation(lang())->summary)}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($event, 'events', 'medium')}}
  @endslot
  @slot('template')
   {{$event->template}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="event-meta">
            <h1>{{$event->getTranslation(lang())->title}}</h1>
          </div>{{-- .event-meta --}}
        </div>
        <div class="col-md-6">
            <div class="event-entry">
                <div class="event-image thumb">
                  <img src="{{getThumbnail($event, 'events', 'medium')}}">
                </div>{{-- .event-image --}}
                <div class="collapse in" id="eventview">
                <div class="event-detail">
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">@lang('content.detail')</a></li>
                        <li role="presentation"><a href="#tnc" aria-controls="tnc" role="tab" data-toggle="tab">@lang('content.tnc')</a></li>
                      </ul>
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="details">
                          
                            <div class="row">
                              <div class="col-md-12">
                                  <div class="event-info event-venue flex">
                                     @if (count($event->schedules) > 1)
                                      <label>Lokasi :</label>
                                       <div class="venues">
                                           <a class="venue-name" data-toggle="collapse" href="#venueList" aria-expanded="false" aria-controls="venueList">Lihat semua lokasi</a>
                                           <div class="collapse" id="venueList">
                                              <ul>
                                            @foreach($event->schedules as $schedule)
                                                <li> <a class="venue-name" href="{{url('/')}}/{{lang()}}/venue/{{$schedule->venue->slug}}">{{$schedule->venue->title}}</a>
                                               </li>
                                            @endforeach
                                              </ul>
                                           </div>
                                        </div>
                                          @else
                                       <label>Lokasi :</label>
                                       <div class="venues">
                                            @foreach($event->schedules as $schedule)
                                                 <div class="the-schedule-list">
                                                   <a class="venue-name" href="{{url('/')}}/{{lang()}}/venue/{{$schedule->venue->slug}}">{{$schedule->venue->title}}</a> 
                                                 </div>
                                            @endforeach
                                        </div>
                                     @endif
                                  </div>
                              </div>{{-- .col --}}
                            </div>{{-- .row --}}
                          @if (!is_null($event->getTranslation(lang())->description))
                            <div class="event-description">
                                  {!! $event->getTranslation(lang())->description !!}
                            </div>
                          @endif
                        </div>{{-- .tab-pane --}}
                        <div role="tabpanel" class="tab-pane" id="tnc">
                          {!! $event->getTranslation(lang())->terms !!}
                        </div>{{-- .tab-pane --}}
                      </div>{{-- .tab-content --}}
                </div>{{-- .event-detail --}}
             {{--   @include('app.widget.comment') --}}
               </div>{{-- #eventview --}}
            </div>{{-- .event-entry --}}
        </div>{{-- .col --}}
        <div class="col-md-6">
            <div class="widget-options">
                  @include('app.event.schedule')
            </div>
        </div>{{-- .col --}}
      </div>{{-- .row --}}
      <div class="soundre-fixleft-bot"></div>
      <div class="soundre-fixright-bot"></div>
    </div>{{-- .container --}}
  </section>
  
   <script type='text/javascript'>
  // Conversion Name: Amild GAC18 - Buy Now Button
  // INSTRUCTIONS 
  // The Conversion Tags should be placed at the top of the <BODY> section of the HTML page. 
  // In case you want to ensure that the full page loads as a prerequisite for a conversion 
  // being recorded, place the tag at the bottom of the page. Note, however, that this may 
  // skew the data in the case of slow-loading pages and in general not recommended. 
  //
  // NOTE: It is possible to test if the tags are working correctly before campaign launch 
  // as follows:  Browse to http://bs.serving-sys.com/Serving/adServer.bs?cn=at, which is 
  // a page that lets you set your local machine to 'testing' mode.  In this mode, when 
  // visiting a page that includes an conversion tag, a new window will open, showing you 
  // the data sent by the conversion tag to the Sizmek servers. 
  // 
  // END of instructions (These instruction lines can be deleted from the actual HTML)
  var ebRand = Math.random()+'';
  ebRand = ebRand * 1000000;
  //<![CDATA[ 
  document.write('<scr'+'ipt src="HTTPS://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&ActivityID=1207784&rnd=' + ebRand + '"></scr' + 'ipt>');
  //]]>
  function ebConversionTracker(conv)
  {
   var ebConversionImg = new Image();
   var ebConversionURL = "HTTP://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?"
   ebConversionURL += "cn=as&ActivityID="+conv+"&ns=1";
   ebConversionImg.src = ebConversionURL;
  }
  </script>
  <noscript>
  <img width="1" height="1" style="border:0" src="HTTPS://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&ActivityID=1207784&ns=1"/>
  </noscript>
@endcomponent