@component('layouts.app')
  @slot('webtitle')
   kiosTix - Solusi Tepat Tiket Hiburan Keluarga Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket aktivitas dan hiburan keluarga terpercaya di Indonesia. Jual beli tiket aktivitas dan hiburan keluarga dengan mudah, aman dan terpercaya.
  @endslot
  <section id="section-attraction" class="section">
    <div class="container">
      <div class="navTab">
          <div class="btn-group flex-center">
            <a href="{{url('/')}}/{{lang()}}/{{$category->id}}/{{$category->getTranslation(lang())->slug}}/?country=indonesia" class="button btn-grey active">INDONESIA</a>
            <a href="{{url('/')}}/{{lang()}}/{{$category->id}}/{{$category->getTranslation(lang())->slug}}/?country=international" class="button btn-grey">INTERNASIONAL</a>
          </div>
          <p>@lang('content.attraction_message')</p>
      </div>
      <div class="sections">
          <div class="titleBox flex flex-center">
             <h2 class="title">{{$category->getTranslation(lang())->title}} - Indonesia</h2>
            <div class="pull-right flex-center attraction-filter">
            {!! Form::open(['method'=>'GET','url'=> lang().'/search/','class'=>'flex-center search-form typeahead','role'=>'search'])  !!}
                <input type="text" name="q" value="" class="input-search search-input" placeholder="Cari Nama Provinsi/Kota"  autocomplete="off">
               <button type="submit" class="searchGo"><i class="ion-ios-search-strong"></i></button>
            {!! Form::close() !!} 
            </div>{{-- .pull-right --}}
          </div>
          @if (count($events))
            <div class="scroll">
              @include('app.event.loop-family')
            </div>
          @else
            <h3>Data Not Available</h3>
          @endif
      </div>{{-- .section --}}
    </div>{{-- .container --}}
  </section>
@endcomponent