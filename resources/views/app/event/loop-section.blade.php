<div class="flex-center">
    <div class="colom-info col3">
      <span class="price ticket-price" id="section-price-{{$ticket->id}}" >
        @if ($ticket->value == 0)
         Free
        @else
          Rp. {{number_format($ticket->value)}}
        @endif
      </span>
    </div>{{-- .colom-left --}}
    @if (GetQuantity($event->id, $ticket->id))
   <div class="colom-left col3 flex-center">
      <div class="input-group order-qty">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number btn-calculate" data-type="minus" data-field="qty-{{$ticket->id}}" data-schedule="{{$schedule->id}}" data-section="{{$ticket->id}}" data-count-session="{{count($schedule->sessions)}}">
                  <span class="ion-minus"></span>
              </button>
          </span>
          <input type="text" id="qty-{{$ticket->id}}" name="qty[]" readonly="readonly" class="form-control input-number" value="0" min="0" max="{{$ticket->max_purchase}}" data-price="{{$ticket->value}}" data-schedule="{{$schedule->id}}" data-section="{{$ticket->id}}">
          <input type="hidden" name="ticket[]" value="{{$ticket->id}}">
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number btn-calculate" data-type="plus" data-field="qty-{{$ticket->id}}" data-schedule="{{$schedule->id}}" data-section="{{$ticket->id}}" data-count-session="{{count($schedule->sessions)}}">
                  <span class="ion-plus"></span>
              </button>
          </span>
      </div>
    </div>{{-- .colom-left --}}
    <div class="colom-right" id="subtotal-{{$ticket->id}}">
           @if ($ticket->value == 0)
            <span id="subtotal-value-{{$ticket->id}}" class="product-price subtotal-price" value="0"></span>
           @else
          Rp. 
            <span id="subtotal-value-{{$ticket->id}}" class="product-price subtotal-price" value="0">0</span>
           @endif
    </div>
   @else
   <div class="colom-left col3 flex-center">
      <span>Tiket sudah habis</span>
   </div>
   @endif
</div>