<div class="widget widget-box widget-schedule">
  @foreach ($event->schedules->where('status',1001) as $schedule)
   @if($schedule->type == 'recurring')
     @include('app.event.schedule_recurring_new')
   @else
     @include('app.event.schedule_default')
   @endif
  @endforeach
</div>