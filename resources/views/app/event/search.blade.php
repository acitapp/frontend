@component('layouts.app')
  @slot('webtitle')
    {{getOption('web_title')}}
  @endslot
 @include('app.partial.search')
  <section id="section-home" class="section">
    <div class="container">
      <div class="theContents">
          <div class="titleBox flex flex-center">
            <h2 class="title">{{$title}}</h2>
            <div class="pull-right">
             @include('app.widget.filter')
            </div>
          </div>
          <div class="row">
            <div class="col-md-8   to-right">
              @if (count($events))
                <div class="scroll">
                  @include('app.event.loop-list')
                </div>
              @else
                <h3>Data Not Available</h3>
              @endif
            </div>
            <div class="col-md-4">
              <div class="widget-container">
                <h4>Rekomendasi</h4>
                <div class="singlecarousel owl-carousel owl-theme">
                  @foreach (getFeaturedEvent(5) as $featured)
                        <div class="items">
                                <div class="event-box box">
                                  <div class="event-image thumb">
                                    <a href="{{ url('/') }}/{{lang()}}/event/{{$featured->id}}/{{$featured->getTranslation(lang())->slug}}">
                                       <img src="{{getThumbnail($featured, 'events', 'small')}}">
                                    </a>
                                  </div>{{-- .event-image --}}
                                  <div class="event-entry">
                                     <h3>{{$featured->getTranslation(lang())->title}}</h3>
                                  </div>
                                </div>{{-- .box --}}
                        </div>
                  @endforeach
                </div>{{-- .carousel --}}
              </div>
              <div class="widget-container">
                    <h4>Tag Terpopuler</h4>
                   <div class="widget widget-box widget-tag">
                      @foreach ($tags as $tag)
                      <a href="{{url('/')}}/{{lang()}}/tag/{{$tag->slug}}" class="label label-tag">#{{$tag->name}}</a>
                      @endforeach
                  </div>{{-- .widget --}}
              </div>{{-- .widget-container --}}
            </div>{{-- .col --}}
          </div>
      </div>{{-- .col --}}
    </div>{{-- .container --}}
  </section>
@endcomponent