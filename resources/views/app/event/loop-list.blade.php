<div id="data" class="event-list">
@foreach ($events as $event)
  @if ($event->trash == false)
    @if ($event->status == 1005)
        @if (notExpired($event->id))
          <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}" class="eventBox">
          <div class="event-box box flex-center">
            <div class="event-image thumb thumb-small">
                 <img src="{{getThumbnail($event, 'events', 'small')}}">
            </div>{{-- .event-image --}}
            <div class="event-information">
              <div class="event-entry">
                  <h3>{{$event->getTranslation(lang())->title}}</h3>
                  @php
                   $countSechedule = $event->schedules->count();
                  @endphp
                  {!! getRangeDate($event->schedules) !!}
                  <div class="event-venue">
                    @if ($countSechedule > 1)
                      Beberapa lokasi acara
                    @else
                     {{$event->schedule->venue->title ?? ''}}
                    @endif
                  </span>
                  </div>
              </div>
              <div class="btnAction">
                <h3 class="lowerPrice">
                  <span>{{getRangePrice($event->schedule)}}</span>
                </h3>
              </div>{{-- .btnAction --}}
            </div>
          </div>{{-- .box --}}
      </a>
        @endif
    @endif
  @endif
@endforeach
</div>{{-- .row --}}
{{-- <br>
<div class="flex-center">{{ $events -> links()}}</div> --}}