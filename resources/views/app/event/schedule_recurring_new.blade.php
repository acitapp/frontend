
<form id="order-form-{{$schedule->id}}" class="orderForm" method="POST" action="{{url('/order-event')}}">
  {{csrf_field()}}
<a class="chooseTicket widget-list" @if (count($event->schedules->where('status',1001)) > 1) data-toggle="collapse" href="#section-schedule-{{$schedule->id}}" aria-expanded="false" aria-controls="section-schedule-{{$schedule->id}}"  @endif>
    <div class="flex-center">
      <div class="colom-left">
          <h4 class="schedule-name">{{$schedule->name}}</h4>
        <span>
            {{$schedule->venue->title ?? ''}}</span>
      </div>{{-- .colom-left --}}
      <div class="colom-right">
          <span class="lowerPrice">
            <span>{{getRangePrice($schedule)}}</span>
          </span>
          <input type="hidden" name="schedule" value="{{$schedule->id}}">
          <input type="hidden" name="visit_date" value="{{formatdateday($schedule->started_at)}}">
      </div>{{-- .colom-right --}}
    </div>
    <div class="visitDate">
      <input type="text" id="visitdate-{{$schedule->id}}" schedule="{{$schedule->id}}" name="visit_date" class="visitdate form-control" placeholder="Pilih tanggal berkunjung" readonly>
      <input type="hidden" name="schedule" value="{{$schedule->id}}">
      @if ($errors->has('visit_date'))
        <div class="alert alert-warning alert-small" alert-session="Mohon pilih jam berkunjung terlebih dahulu.">
             {{ $errors->first('visit_date') }}
        </div>
      @endif
    </div>
      @if (count($schedule->sessions) == 1)
        <div id="time-schedule-{{$schedule->id}}" class="times session" data-toggle="tooltip" data-placement="top"  title="Please select the event time, before find ticket.">
             <input class="radio-session hide" checked type="radio" session="{{$schedule->sessions->first()->id}}" value="{{$schedule->sessions->first()->time}}" name="visit_time" id="session-{{$schedule->sessions->first()->id}}">
        </div>
      @else
        <div id="time-schedule-{{$schedule->id}}" class="times session hide">
          @foreach ($schedule->sessions as $session)
            <input class="radio-session" type="radio" session="{{$session->id}}" schedule="{{$schedule->id}}"  value="{{$session->time}}" name="visit_time" id="session-{{$session->id}}">
            <label  for="session-{{$session->id}}" class="label label-session" schedule="{{$schedule->id}}">{{date('g:ia', strtotime($session->time))}}</label>
          @endforeach
        </div>
      @endif
    <script>
        var start = "{{formatdateday($schedule->started_at)}}";
        var end = "{{formatdateday($schedule->ended_at)}}";
        var scheduleID = "{{$schedule->id}}";
        var dayDisabled = "{{everyDay($schedule->every_day)}}";
    </script>
      @if ($schedule->recurring_type == 'everyday')
       <script>  
             $("#visitdate-{{$schedule->id}}").datepicker({
                format: "dd M yyyy",
                startDate: start,
                endDate: end,
                daysOfWeekDisabled: dayDisabled,
               // daysOfWeekHighlighted: "0,6",
                 autoclose: true
              }).on('changeDate', function(e) {
                  console.log(e);
                  console.log(e.currentTarget.value);
                  $('#data-visit-{{$schedule->id}}').val(e.currentTarget.value);
                  var text  = $('#alert-{{$schedule->id}}').attr('alert-session');
                  $('#alert-{{$schedule->id}}').text(text).show().delay(3000).fadeOut(1000);
              });;    
       </script>
      @elseif($schedule->recurring_type == 'every_date')
       <script>  
             $("#visitdate-{{$schedule->id}}").datepicker({
                format: "dd M yyyy",
                startDate: start,
                endDate: end,
                beforeShowDay : function(date) {
                  var dates_avail = [{{$schedule->every_date}}];
                  var dt = date.getDate();
                  if (dates_avail.indexOf(dt) != -1) {
                      return {
                          enabled: true
                      };
                  } else {
                      return {
                          enabled: false
                      };
                  }
                },
               // daysOfWeekHighlighted: "0,6",
                 autoclose: true,
              }).on('changeDate', function(e) {
                  console.log(e.currentTarget.value);
                  $('#data-visit-{{$schedule->id}}').val(e.currentTarget.value);
                  var text  = $('#alert-{{$schedule->id}}').attr('alert-session');
                  $('#alert-{{$schedule->id}}').text(text).show().delay(3000).fadeOut(1000);
              });;    
       </script>
      @elseif($schedule->recurring_type == 'daily')
       <script>  
             $("#visitdate-{{$schedule->id}}").datepicker({
                format: "dd M yyyy",
                startDate: start,
                endDate: end,
                autoclose: true
              }).on('changeDate', function(e) {
                  console.log(e);
                  console.log(e.currentTarget.value);
                  $('#data-visit-{{$schedule->id}}').val(e.currentTarget.value);
                  var text  = $('#alert-{{$schedule->id}}').attr('alert-session');
                  $('#alert-{{$schedule->id}}').text(text).show().delay(3000).fadeOut(1000);
              });;    
       </script>
      @endif
 </a>{{-- .widget-list --}}
 @if ($event->sales_date >= Carbon\Carbon::now())
   <div class="center message-box">
     <span>Akan segera tersedia di kioTix.com</span>
   </div>
 @else
 <div class="section-box collapse @if (count($event->schedules->where('status',1001)) <= 1) in single-schedule @endif" id="section-schedule-{{$schedule->id}}">
    <div class="booking-header flex-center">
        <div class="colom-info col3">
          <span>Harga/Section</span>
        </div>{{-- .colom-left --}}
        <div class="colom-left col3 center">
          <span>Kuantitas</span>
        </div>{{-- .colom-left --}}
        <div class="colom-right col3 text-right">
          <span>Subtotal</span>
        </div>{{-- .colom-right --}}
    </div>
    <div class="booking-body" id="booking-body-{{$schedule->id}}">
    @php
      $statusSeats = 0;
    @endphp
    @foreach ($schedule->tickets->where('status',1001) as $ticket)
        <div class="row-section-ticket" id="section-schedule-ticket-{{$ticket->id}}">
            <div class="section-name col3">
                  <span  class="labels ticket-name">
                  {{$ticket->name}}
                 </span>
            </div>
            @php
                $seats =$seat->where('tiket_id',$ticket->id)->first();
            @endphp
            @if ($seats)
              @php
                $statusSeats=1;
              @endphp
            @endif
            @if ($ticket->period == true && $ticket->available_at_venue == false)
                @php
                  $now = Carbon\Carbon::now();
                  $start_date = Carbon\Carbon::createFromFormat('d/m/Y h:i A',$ticket->started_at);
                  $end_date = Carbon\Carbon::createFromFormat('d/m/Y h:i A',$ticket->ended_at);
                @endphp
                @if ($now < $start_date)
                  <div class="flex-center">
                      <div class="colom-info col3">
                        <span class="price ticket-price" id="section-price-{{$ticket->id}}" >
                          @if ($ticket->value == 0)
                           Free
                          @else
                            Rp. {{number_format($ticket->value)}}
                          @endif
                        </span>
                      </div>{{-- .colom-left --}}
                      <div class="colom-left col3 flex-center">
                         Segera di kiosTix
                      </div>
                      <div class="section-name col3">
                      </div>
                    </div>
                @elseif($now > $end_date)
                  <div class="flex-center">
                      <div class="colom-info col3">
                        <span class="price ticket-price" id="section-price-{{$ticket->id}}" >
                          @if ($ticket->value == 0)
                           Free
                          @else
                            Rp. {{number_format($ticket->value)}}
                          @endif
                        </span>
                      </div>{{-- .colom-left --}}
                      <div class="colom-left col3 flex-center">
                         Tutup
                      </div>
                      <div class="section-name col3">
                      </div>
                    </div>
                @else
                  @include('app.event.loop-section')
                @endif
            @elseif($ticket->available_at_venue == true)
                  <div class="flex-center">
                      <div class="colom-info col3">
                        <span class="price ticket-price" id="section-price-{{$ticket->id}}" >
                          @if ($ticket->value == 0)
                           Free
                          @else
                            Rp. {{number_format($ticket->value)}}
                          @endif
                        </span>
                      </div>{{-- .colom-left --}}
                      <div class="colom-left col3 flex-center">
                         Tersedia di lokasi acara
                      </div>
                      <div class="section-name col3">
                      </div>
                    </div>
            @else
                  @include('app.event.loop-section')
            @endif
            @if ($ticket->show_description == true)
             <div class="ticket-terms">
               {!!$ticket->description!!}
             </div>
            @endif
        </div>{{-- .row-section-ticket --}}
    @endforeach
    </div>{{-- .booking-body --}}
   <div id="booking-detail-{{$schedule->id}}" class="booking-detail">
      <div class="booking-footer">
          <div class="item-row booking-section">
            <span class="product-item">Total</span>
            <span class="product-price total-price">Rp. <span id="total-price-{{$schedule->id}}" data-price="0">0</span></span>
          </div>
            <input id="input-event-{{$schedule->id}}" type="hidden" name="event" value="{{$event->id}}">
            <input id="input-venue-{{$schedule->id}}" type="hidden" name="venue" value="{{$schedule->venue->id}}">
            <input id="input-seat-{{$schedule->id}}" type="hidden" name="seat" value="{{$statusSeats}}">
            @auth
            <button type="submit" class="button btn-block submitOrder disabled" disabled="disabled">@lang('content.order_now')</button>
            @else
            <a href="#popup-login" class="button btn-block submitOrder showPopup disabled">@lang('content.order_now')</a>
            @endauth
            <div class="loading loading-double hide" id="loader-order-{{$schedule->id}}"></div>
      </div>
  </div>{{-- .booking-detail --}}
 </div>
 @endif
</form>