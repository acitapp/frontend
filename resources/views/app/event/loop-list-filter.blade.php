<div id="data" class="event-list">
@foreach ($events as $event)
    <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->slug}}" class="eventBox">
    <div class="event-box box flex-center">
      <div class="event-image thumb thumb-small">
           <img src="{{getThumbnail($event, 'events', 'small')}}">
      </div>{{-- .event-image --}}
      <div class="event-information">
        <div class="event-entry">
            <h3>{{$event->title}}</h3>
            {!! getRangeDate($event->schedules) !!}
            <div class="event-venue">
              @if (count($event->schedules) > 1)
                Beberapa lokasi acara
              @else
               {{$event->schedule->venue->title ?? ''}}
              @endif
            </div>
        </div>
        <div class="btnAction">
          <h3 class="lowerPrice">
             <span>{{getRangePrice($event->schedule)}}</span>
          </h3>
        </div>{{-- .btnAction --}}
      </div>
    </div>{{-- .box --}}
  </a>
@endforeach
</div>{{-- .row --}}
{{-- <br>
<div class="flex-center">{{ $events -> links()}}</div> --}}