<div id="booking-detail-{{$schedule->id}}" class="booking-detail hide">
    <div class="booking-head">
      <h3 style="margin:0;">Order Summary</h3>
    </div>{{-- .booking-head --}}
    <div class="booking-body" id="booking-body-{{$schedule->id}}">
    </div>{{-- .booking-body --}}
    <div class="booking-footer">
        <div class="item-row booking-section">
          <span class="product-item">TOTAL</span>
          <span class="product-price total-price">Rp. <span id="total-price-{{$schedule->id}}" data-price="0">0</span></span>
        </div>
        <form id="order-form-{{$schedule->id}}">
          {{csrf_field()}}
          <input id="input-event-{{$schedule->id}}" type="hidden" name="event" value="{{$event->id}}">
          <input id="input-total-{{$schedule->id}}" type="hidden" name="total">
          <input id="input-venue-{{$schedule->id}}" type="hidden" name="venue">
          <input id="input-totalqty-{{$schedule->id}}" type="hidden" name="totalqty">
          <textarea name="order" id="order-cart-{{$schedule->id}}" class="hide order-cart"></textarea>
          <button type="submit" class="button btn-block">ORDER NOW</button>
          <div class="loading loading-double hide" id="loader-order-{{$schedule->id}}"></div>
        </form>
    </div>
</div>{{-- .booking-detail --}}