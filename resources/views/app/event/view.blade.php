@component('layouts.app')
  @slot('webtitle')
  Tiket {{$event->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$event->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($event->getTranslation(lang())->summary)}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($event, 'events', 'medium')}}
  @endslot
  @slot('template')
   {{$event->template}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="event-meta">
            <h1>{{$event->getTranslation(lang())->title}}</h1>
          </div>{{-- .event-meta --}}
        </div>
        <div class="col-md-6">
            <div class="event-entry">
                <div class="event-image thumb">
                  <img src="{{getThumbnail($event, 'events', 'medium')}}">
                </div>{{-- .event-image --}}
                <div class="collapse in" id="eventview">
                <div class="event-detail">
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">@lang('content.detail')</a></li>
                        <li role="presentation"><a href="#tnc" aria-controls="tnc" role="tab" data-toggle="tab">@lang('content.tnc')</a></li>
                        <li class="shareTab">
                          @include('app.widget.sharebox')
                        </li>
                      </ul>
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="details">
                          <div class="row">
                            <div class="col-md-12">
                                <div class="event-info event-artist flex">
                                    <label>Tag :</label>
                                    <div class="artists">
                                      @foreach($event->tags as $tag)
                                       <a class="venue-name" href="{{url('/')}}/{{lang()}}/tag/{{$tag->slug}}">{{$tag->name}}</a> 
                                      @endforeach
                                    </div>
                                </div>
                                <div class="event-info event-venue flex">
                                   @if (count($event->schedules) > 1)
                                    <label>Lokasi :</label>
                                     <div class="venues">
                                          @php
                                            $scheduled = $event->schedules->first();
                                          @endphp
                                               <div class="the-schedule-list">
                                                 <a class="venue-name" href="{{url('/')}}/{{lang()}}/venue/{{$scheduled->venue->slug}}">{{$scheduled->venue->title}}</a> 
                                               </div>
                                      </div>
                                        @else
                                     <label>Lokasi :</label>
                                     <div class="venues">
                                          @foreach($event->schedules as $schedule)
                                               <div class="the-schedule-list">
                                                 <a class="venue-name" href="{{url('/')}}/{{lang()}}/venue/{{$schedule->venue->slug}}">{{$schedule->venue->title}}</a> 
                                               </div>
                                          @endforeach
                                      </div>
                                   @endif
                                </div>
                                 @if (!thisAttraction($event))
                                <div class="event-info event-venue flex">
                                     <label>Tanggal :</label>
                                     <div class="venues">
                                         <a class="venue-name">{!! getRangeDate($event->schedules) !!}</a>
                                      </div>
                                </div>
                                @endif
                            </div>{{-- .col --}}
                          {{--   <div class="col-md-6">
                              @if (thisAttraction($event))
                                <div class="event-info event-rating flex">
                                    <label>Ratings :</label>
                                     <span>{{$event->rate_average}}</span>
                                </div>
                                <div class="event-info event-review flex">
                                    <label>Reviews :</label>
                                     <span>{{$event->comment}}</span>
                                </div>
                               @endif
                            </div> --}}
                          </div>{{-- .row --}}
                          @if (!is_null($event->getTranslation(lang())->description))
                            <div class="event-description">
                                @if (!$agent->isDesktop())
                                <div class="moreContent">
                                  {!! $event->getTranslation(lang())->description !!}
                                </div>
                                @else
                                  {!! $event->getTranslation(lang())->description !!}
                                @endif
                            </div>
                          @endif
                        </div>{{-- .tab-pane --}}
                        <div role="tabpanel" class="tab-pane" id="tnc">
                          {!! $event->getTranslation(lang())->terms !!}
                        </div>{{-- .tab-pane --}}
                      </div>{{-- .tab-content --}}
                </div>{{-- .event-detail --}}
             {{--   @include('app.widget.comment') --}}
               </div>{{-- #eventview --}}
            </div>{{-- .event-entry --}}
        </div>{{-- .col --}}
        <div class="col-md-6">
            <div class="widget-options">
                  @include('app.event.schedule')
            </div>
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
  @auth
  @role('Super Admin')
     @include('app.partial.approval')
  @endrole
  @endauth
@endcomponent