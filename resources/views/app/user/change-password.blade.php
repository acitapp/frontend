@component('layouts.app')
  <section id="section-user" class="section">
    <div class="container">
      <div class="titleBox no-border no-margin flex-column">
        <h2 class="title">Welcome, {{$user->name}}</h2>
        @include('app.user.menu')
      </div>
      <div class="theContent">
        {!! Form::model($user,['url' => 'password/update', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
          <div class="row flex-center">
            <div class="col-md-6">
                <div class="widget">
                    <div class="widget-form">
                        <div class="row rows flex-center">
                          <h2>Ubah Kata Sandi</h2>
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                               <input id="oldpassword" type="password" class="form-control" placeholder="Kata Sandi Sebelumnya" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                               <input id="password" type="password" class="form-control" placeholder="Kata Sandi" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                               <input id="password-confirm" type="password" class="form-control" placeholder="Ulangi Kata Sandi" name="password_confirmation" required>
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                {!! Form::submit('Update Password', ['class'=>'button btn-block']) !!} 
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                    </div>{{-- .widget-form --}}
                </div>{{-- .widget --}}
            </div>{{-- .col --}}
          </div>{{-- .row --}}
            {!! Form::close() !!}
      </div>{{-- .theContent --}}
    </div>{{-- .container --}}
  </section>
@endcomponent