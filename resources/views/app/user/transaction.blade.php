@component('layouts.app')
  <section id="section-user" class="section">
    <div class="container">
      <div class="titleBox no-border no-margin flex-column">
        <h2 class="title">@lang('content.welcome'), {{$user->name}}</h2>
        @include('app.user.menu')
      </div>
      <div class="theContent">
        <div class="subTitleBox">
          <h2 class="subtitle">@lang('content.transaction_history')</h2>
        {{--   <div class="btn-group" style="margin-left: auto;">
            <a href="#" class="btn btn-success btn-xs">Paid</a>
            <a href="#" class="btn btn-warning btn-xs">Unpaid</a>
            <a href="#" class="btn btn-danger btn-xs">Expired</a>
          </div> --}}
        </div>
        <div class="trx-table">
        @if ($transactions)
          {{-- expr --}}
        <table class="table table-bordered table-hover center">
          <thead>
            <tr>
              <th>No</th>
              <th>Event</th>
              <th>Trx ID</th>
              <th>Trx Date</th>
              <th>Qty</th>
              <th>Total</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($transactions as $transaction)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$transaction->event->title}}</td>
                <td>
                   {{$transaction->order_no}}
                </td>
                <td>{{formatdate($transaction->created_at)}}</td>
                <td>{{$transaction->quantity}}</td>
                <td>Rp. {{number_format($transaction->value_grand_total)}}</td>
                <td>
                  {{checkStatus($transaction->paymentResponse->transaction_status ?? 'expired')}}
                </td>
              </tr>
            @endforeach
          </tbody>
          @if ($transactions->total() > $transactions->perPage())
            <tfoot>
              <tr>
                <td colspan="20">
                  {{ $transactions-> links()}}
                </td>
              </tr>
            </tfoot>
          @endif
        </table>
        @else
          <div class="center">
            <h3>@lang('content.dont_have_transaction')</h3>
          </div>
        @endif
        </div>
      </div>{{-- .theContent --}}
    </div>{{-- .container --}}
  </section>
  @include('app.user.popup-order')
@endcomponent