<div class="box-nav">
  <ul id="user-nav">
      <li>
        <a href="{{url('/')}}/{{lang()}}/my-account" class="@if(currentUrl(2) == 'my-account') current @endif">
          <i class="ion-person"></i> <span>@lang('content.my_account')</span>
        </a>
      </li>
      <li>
        <a href="{{url('/')}}/{{lang()}}/transaction" class="@if(currentUrl(2) == 'transaction') current @endif">
          <i class="ion-bag"></i> <span>@lang('content.transaction')</span>
        </a>
      </li>
      <li>
        <a href="{{url('/')}}/{{lang()}}/password/change" class="@if(currentUrl(2) == 'password') current @endif">
          <i class="ion-key"></i> <span>@lang('content.change_password')</span>
        </a>
      </li>
{{--       <li>
        <a href="{{url('/')}}/{{lang()}}/create-free-event" class="@if(currentUrl(2) == 'create-free-event') current @endif">
          <i class="ion-edit"></i> <span>Create Free Event</span>
        </a>
      </li> --}}
  </ul>
</div>{{-- .box-nav --}}