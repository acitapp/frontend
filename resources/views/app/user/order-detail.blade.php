<table class="table table-bordered table-hover center">
  <thead>
    <tr>
      <th>No</th>
      <th>Event</th>
      <th>Visit Date</th>
      <th>Section</th>
      <th>Qty</th>
      <th>Amount</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($transaction->orders as $order)
      <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{$transaction->event->title}}</td>
        <td>
           {{$order->visit_date}}, at {{$order->visit_time}}
        </td>
        <td>{{$order->sectionable->name}}</td>
        <td>{{$order->quantity}}</td>
        <td>Rp. {{number_format($order->order_value)}}</td>
      </tr>
    @endforeach
  </tbody>
</table>