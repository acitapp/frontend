@component('layouts.master')
  <section id="section-user" class="section">
    <div class="w400">
      <div class="titleBox no-border no-margin flex-column">
        <h2 class="title">@lang('content.welcome'), {{$user->name}} 
        <p>@lang('content.input_info_message')</p></h2>
      </div>
      <div class="theContents">
        {!! Form::model($user,['url' => 'update-account', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
          <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-form">
                        <div class="row">
                            <div class="col-md-12">
                                 {!! Form::text('fullname',$user->name ?? null, ['class'=>'form-control','placeholder'=>Lang::get('content.fullname')]) !!}
                                @if ($errors->has('fullname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fullname') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row --}}
                        <div class="row">
                            <div class="col-md-12"><span class="help-block help-info">@lang('content.help_fullname')</span></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                 {!! Form::text('email',$user->email ?? null, ['class'=>'form-control','placeholder'=>'Email']) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            <span class="help-block help-info">@lang('content.help_email')</span>
                            </div>{{-- .col --}}
                        </div>{{-- .row --}}
                        <div class="row">
                            <div class="col-md-12">
                                 {!! Form::text('phone',$user->information->phone ?? null, ['class'=>'form-control','placeholder'=> Lang::get('content.phone')]) !!}
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('help_phone') }}</strong>
                                    </span>
                                @endif
                                 <span class="help-block help-info">@lang('content.phone')</span>
                            </div>{{-- .col --}}
                        </div>{{-- .row --}}
                        <div class="row">
                            <div class="col-md-12">
                             {!! Form::select('gender',['Male' => Lang::get('content.male'),'Female' => Lang::get('content.female')],$user->information->gender ?? null, ['class'=>'form-control select','placeholder'=> Lang::get('content.gender')]) !!}       
                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('help_gender') }}</strong>
                                    </span>
                                @endif
                                 <span class="help-block help-info">@lang('content.gender')</span>
                            </div>{{-- .col --}}
                        </div>{{-- .row --}}
                        <div class="row">
                            <div class="col-md-12">
                                 {!! Form::text('dob',$user->information->dob ?? null, ['class'=>'form-control dob','placeholder'=> Lang::get('content.dob')]) !!}
                                @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                                 <span class="help-block help-info">@lang('content.dob')</span>
                            </div>{{-- .col --}}
                        </div>{{-- .row --}}
                        <div class="row">
                            <div class="col-md-12">
                                 {!! Form::select('country_id',getCountries(),null, ['class'=>'form-control select ','data-live-search' => 'true','id'=>'country_id','type'=>'country','target'=>'state','placeholder' => Lang::get('content.choose_country'), 'required' => 'required']) !!}
                                @if ($errors->has('country_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country_id') }}</strong>
                                    </span>
                                @endif
                                 <span class="help-block help-info">@lang('content.choose_country')</span>
                            </div>{{-- .col --}}
                        </div>{{-- .row --}}
                    </div>{{-- .widget-form --}}
                     {!! Form::submit(Lang::get('content.save'), ['class'=>'button btn-block']) !!} 
                </div>{{-- .widget --}}
            </div>{{-- .col --}}
          </div>{{-- .row --}}
            {!! Form::close() !!}
      </div>{{-- .theContent --}}
    </div>{{-- .container --}}
  </section>
@endcomponent