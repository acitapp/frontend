@component('layouts.app')
  <section id="section-user" class="section">
    <div class="container">
      <div class="titleBox no-border no-margin flex-column">
        <h2 class="title">@lang('content.welcome'), {{$user->name}} </h2>
        @include('app.user.menu')
      </div>
      <div class="theContent">
        {!! Form::model($user,['url' => 'update-account', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
          <div class="row">
            <div class="col-md-6">
                <div class="widget">
                    <div class="widget-form">
                        <div class="row rows">
                            <div class="col-md-3">
                                  <div class="boxPhoto">
                                    <div class="thumb thumb-user is-circle">
                                        @if (count($user->getMedia('avatar')))
                                          <img src="{{ url('/') }}/media/{{$user->getMedia('avatar')->first()->id}}/conversions/square.jpg" class="is-circle">
                                        @else
                                           <img src="{{ url('/') }}/images/default_square.jpg" class="is-circle">
                                        @endif
                                    </div>
                                  </div>{{-- .box-photo --}}
                            </div>{{-- .col --}}
                            <div class="col-md-9">
                                      <input type="file"  id="image" class="button"  name="avatar" title="@lang('content.browse_img')">
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                 {!! Form::text('fullname',$user->name ?? null, ['class'=>'form-control','placeholder'=>Lang::get('content.fullname')]) !!}
                                @if ($errors->has('fullname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fullname') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                 {!! Form::text('email',$user->information->email ?? null, ['class'=>'form-control','placeholder'=>Lang::get('content.email')]) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                 {!! Form::text('phone',$user->information->phone ?? null, ['class'=>'form-control','placeholder'=> Lang::get('content.phone')]) !!}
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                             {!! Form::select('gender',['Male' => Lang::get('content.male'),'Female' => Lang::get('content.female')],$user->information->gender ?? null, ['class'=>'form-control select','placeholder'=> Lang::get('content.gender')]) !!}       
                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                 {!! Form::text('pob',$user->information->pob ?? null, ['class'=>'form-control','placeholder'=> Lang::get('content.pob')]) !!}
                                @if ($errors->has('pob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pob') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                 {!! Form::text('dob',$user->information->dob ?? null, ['class'=>'form-control dob','placeholder'=> Lang::get('content.dob')]) !!}
                                @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                    </div>{{-- .widget-form --}}
                </div>{{-- .widget --}}
            </div>{{-- .col --}}
            <div class="col-md-6">
                <div class="widget">
                    <div class="widget-form">
                        <div class="row rows">
                            <div class="col-md-12">
                             {!! Form::select('id_type',['KTP' => 'KTP','SIM' => 'SIM','PASSPORT' => 'PASSPORT'],$user->information->id_type, ['class'=>'form-control select','placeholder' => Lang::get('content.choose_idcard')]) !!}       
                                @if ($errors->has('id_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_type') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                 {!! Form::text('id_number',$user->information->id_number, ['class'=>'form-control','placeholder'=> Lang::get('content.id_no')]) !!}
                                @if ($errors->has('id_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_number') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                 {!! Form::select('country_id',$countries ,$user->information->address->country_id ?? null, ['class'=>'form-control select findPlace','data-live-search' => 'true','id'=>'country_id','type'=>'country','target'=>'state','placeholder' => Lang::get('content.choose_country')]) !!}
                                @if ($errors->has('country_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country_id') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                               {!! Form::select('state_id',$states ,$user->information->address->state_id ?? null, ['class'=>'form-control select findPlace','data-live-search' => 'true','id'=>'state_id','type'=>'state','target'=>'city','placeholder'=>Lang::get('content.choose_state')]) !!}
                                @if ($errors->has('city_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                               {!! Form::select('city_id',$cities ,$user->information->address->city_id ?? null, ['class'=>'form-control select','data-live-search' => 'true','id'=>'city_id','placeholder'=> Lang::get('content.choose_city')]) !!}
                                @if ($errors->has('city_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                 {!! Form::textarea('address',$user->information->address->address ?? null, ['class'=>'form-control','placeholder'=>Lang::get('content.address')]) !!}
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                        <div class="row rows">
                            <div class="col-md-12">
                                 {!! Form::text('zip_code',$user->information->address->zip_code ?? null, ['class'=>'form-control','placeholder'=> Lang::get('content.postcode')]) !!}
                                @if ($errors->has('zip_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                        </div>{{-- .row rows --}}
                    </div>{{-- .widget-form --}}
                </div>{{-- .widget --}}
            </div>{{-- .col --}}
          </div>{{-- .row --}}
              {!! Form::submit(Lang::get('content.save'), ['class'=>'button btn-block']) !!} 
            {!! Form::close() !!}
      </div>{{-- .theContent --}}
    </div>{{-- .container --}}
  </section>
@endcomponent