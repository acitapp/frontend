  <div class="meta-title">
    <h3 class="s-title"><a title="{{$post->title}}" href="{{ url('/') }}/read/{{$post->slug}}">
        {{limitWord($post->title,7)}}
    </a></h3>
    <span class="date"><i class="ion-android-calendar"></i> {{ formatdate($post->published_at) }}</span>
     <a href="#" id="favorite-{{$post->id}}" class="favorite setLike {{haslike($post->id)}}" post-id="{{$post->id}}" token="{{csrf_token()}}" data-toggle='tooltip' data-placement='top' @if (haslike($post->id)) title='Remove as favorite' @else title='Set as favorite' @endif>
     @if (haslike($post->id))
     <i class="ion-android-favorite"></i>
     @else
     <i class="ion-android-favorite-outline"></i>
     @endif
     <span class="countLike">{{$post->like}}</span>
     </a>
  </div>