<a href="{{url('/')}}/read/{{$post->slug}}"  title="{{$post->title}}" class="thumb thumb-medium">
   @if (count($post->getMedia('thumbnail')))
      <img src="{{ url('/') }}/media/{{$post->getMedia('thumbnail')->first()->id}}/conversions/large.jpg">
    @else
       <img src="{{ url('/') }}/images/default.jpg">
    @endif
    @if ($post->type == 'video')
       <i class="iconpost ion-ios-videocam"></i>
    @elseif($post->type == 'photo')
       <i class="iconpost ion-images"></i>
    @endif
  </a>
@if ($post->published == 1)
  @if (count($post->categories()->first()))
    @php
    $cat = $post->categories()->first();
    @endphp
    <a class="label label-category {{$cat->color}}" href="{{ url('/') }}/{{$cat->slug}}">{{$cat->title}}</a>
 @endif
@endif