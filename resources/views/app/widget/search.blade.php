<div class="search-widget">
{!! Form::open(['method'=>'GET','url'=> lang().'/search/','class'=>'flex-center search-form typeahead','role'=>'search'])  !!}
    <input type="text" name="q" value="" class="input-search search-input" placeholder="@lang('content.search_here')"  autocomplete="off">
   <button type="submit" class="searchGo"><i class="ion-ios-search-strong"></i></button>
{!! Form::close() !!} 
</div>{{-- .pull-right --}}