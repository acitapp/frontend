  <div class="sharebox flex-center">
        <ul>
          <li>
          <a href="#" class="shareFacebook" 
            data-name="{{$event->getTranslation(lang())->title}}" 
            data-link="{{ url('/') }}/event/{{$event->getTranslation(lang())->slug}}" 
            data-picture="{{ url('/') }}/media/{{$event->cover}}" 
            data-caption="{{$event->getTranslation(lang())->summary}}"
            data-post="{{$event->id}}"
            data-user=""><i class="icon-brand-facebook"></i></a>
          </li>
          <li >
          <a href="#" class="shareTwitter" 
            data-text="{{$event->getTranslation(lang())->title}}" 
            data-shareurl="{{ url('/') }}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}" 
            data-via="kiostix" 
            data-hastag="kiostix"
            data-user=""><i class="icon-brand-twitter"></i></a>
          </li>
          <li >
            <a class="shareWhatsapp" href="https://api.whatsapp.com/send?text={{ url('/') }}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}" target="_blank">
              <i class="icon-brand-whatsapp"></i>
            </a>
          </li>
          <li>
            <a class="shareLine" href="line://msg/text/{{ url('/') }}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}" target="_blank">
              <i class="icon-brand-line"></i>
            </a>
          </li>
        </ul>
        </div>{{-- .sharebox --}}