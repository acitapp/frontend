  <div class="sharebox flex-center">
        <ul>
          <li><span>SHARE:</span></li>
          <li>
          <a href="#" class="shareFacebook" 
            data-name="{{$post->getTranslation(lang())->title}}" 
            data-link="{{ url('/') }}/article/{{$post->id}}/{{$post->getTranslation(lang())->slug}}" 
            data-picture="{{ url('/') }}/media/{{$post->cover}}" 
            data-caption="{{$post->getTranslation(lang())->summary}}"
            data-post="{{$post->id}}"
            data-user=""><i class="icon-brand-facebook"></i></a>
          </li>
          <li >
          <a href="#" class="shareTwitter" 
            data-text="{{$post->getTranslation(lang())->title}}" 
            data-shareurl="{{ url('/') }}/article/{{$post->id}}/{{$post->getTranslation(lang())->slug}}" 
            data-via="kiostix" 
            data-hastag="kiostix"
            data-user=""><i class="icon-brand-twitter"></i></a>
          </li>
          <li >
            <a class="shareWhatsapp" href="https://api.whatsapp.com/send?text={{ url('/') }}/article/{{$post->id}}/{{$post->getTranslation(lang())->slug}}" target="_blank">
              <i class="icon-brand-whatsapp"></i>
            </a>
          </li>
          <li>
            <a class="shareLine" href="line://msg/text/{{ url('/') }}/article/{{$post->id}}/{{$post->getTranslation(lang())->slug}}" target="_blank">
              <i class="icon-brand-line"></i>
            </a>
          </li>
        </ul>
        </div>{{-- .sharebox --}}