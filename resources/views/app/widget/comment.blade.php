@if (thisAttraction($event))
<div class="box">
  @if ($agent->isDesktop())
    <div class="the-title flex-center">
      <h2 class="title"><span>Review</span></h2>
    </div>
    <div class="box-content" id="commentList">
  @else
    <div class="the-title flex-center">
      <h2 class="title"><a class="button btn-outline btn-s" data-toggle="collapse" href="#commentList" aria-expanded="false" aria-controls="commentList">Lihat Review</a></h2>
    </div>
    <div class="box-content collapse" id="commentList">
  @endif
    <div class="box-comment">
      <div id="commentTab">
           <div class="flex-center hide" id="commentLoader"><div class="loading loading-double"></div></div>
           <div id="newComment"></div>
           <div class="scroll">
           <div class="comment-box">
              @foreach ($comments as $comment)
                <div class="comment-list flex-box">
                  <a href="{{ url('/') }}/user/{{$comment->user->slug}}" class="thumb thumb-author is-circle">
                    @if (count($comment->user->getMedia('avatar')))
                      <img src="{{ url('/') }}/media/{{$comment->user->getMedia('avatar')->first()->id}}/conversions/square.jpg" class="is-circle">
                    @else
                       <img src="{{ url('/') }}/images/default_square.jpg" class="is-circle">
                    @endif
                  </a>
                  <div class="comment-container">
                    <div class="comment-head flex-box">
                      <h3 class="s-title">
                       <a href="{{ url('/') }}/user/{{$comment->user->slug}}">{{$comment->user->name}}</a>
                      </h3>
                      <span class="date">
                          {{humandate($comment->created_at)}}
                      </span>
                    </div>{{-- .comment-head  --}}
                    <div class="coment-body">
                          {{$comment->content}}
                    </div>{{-- .comment-body  --}}
                    <div class="coment-footer flex-box">
                        <a href="#" id="like-{{$comment->id}}" class="like-comment" post-id="{{$event->id}}" data-token="{{csrf_token()}}" comment-id="{{$comment->id}}"><i class="ion-thumbsup"></i> <span class="countLikeComment">{{$comment->like}}</span></a>
                        <a href="#" id="dislike-{{$comment->id}}" class="dislike-comment" post-id="{{$event->id}}" data-token="{{csrf_token()}}" comment-id="{{$comment->id}}"><i class="ion-thumbsdown"></i> <span class="countDisLikeComment">{{$comment->dislike}}</span></a>
                    </div>{{-- .comment-footer  --}}
                    <div class="replybox hide" id="replybox-{{$comment->id}}">
                        <div class="comment-form">
                             <textarea placeholder="Tulis Komentar" id="commentMessage-{{$comment->id}}"></textarea>
                            <small><strong>KiosTix.com</strong> tidak bertanggung jawab atas isi komentar yang ditulis. Komentar sepenuhnya menjadi tanggung jawab komentator seperti diatur dalam UU ITE</small>
                             @if (Auth::check())
                               <a href="#" class="button addComment" post-id="{{$event->id}}" comment-id="{{$comment->id}}" data-token="{{csrf_token()}}">Comment</a>
                             @else
                               <a href="#popup-login" class="button showPopup">Comment</a>
                             @endif
                        </div>{{-- .comment-form--}}
                    </div>
                  </div>{{-- .comment-container  --}}
                </div>{{-- .comment-list  --}}
                <div class="replyComment" id="replyboxList-{{$comment->id}}">
                   @if (count($comment->parents))
                    @foreach ($comment->parents as $comment)
                    <div class="comment-list flex-box">
                      <a href="{{ url('/') }}/user/{{$comment->user->slug}}" class="thumb thumb-author is-circle">
                        @if (count($comment->user->getMedia('avatar')))
                          <img src="{{ url('/') }}/media/{{$comment->user->getMedia('avatar')->first()->id}}/conversions/square.jpg" class="is-circle">
                        @else
                           <img src="{{ url('/') }}/images/default_square.jpg" class="is-circle">
                        @endif
                      </a>
                      <div class="comment-container">
                        <div class="comment-head flex-box">
                          <h3 class="s-title">
                            <a href="{{ url('/') }}/user/{{$comment->user->slug}}">{{$comment->user->name}}</a>
                          </h3>
                          <span class="date">
                              {{humandate($comment->created_at)}}
                          </span>
                        </div>{{-- .comment-head  --}}
                        <div class="coment-body">
                              {{$comment->content}}
                        </div>{{-- .comment-body  --}}
                        <div class="coment-footer flex-box">
                            <a href="#" id="like-{{$comment->id}}" class="like-comment" post-id="{{$event->id}}" data-token="{{csrf_token()}}" comment-id="{{$comment->id}}"><i class="ion-thumbsup"></i> <span class="countLikeComment">{{$comment->like}}</span></a>
                            <a href="#" id="dislike-{{$comment->id}}" class="dislike-comment" post-id="{{$event->id}}" data-token="{{csrf_token()}}" comment-id="{{$comment->id}}"><i class="ion-thumbsdown"></i> <span class="countDisLikeComment">{{$comment->dislike}}</span></a>
                            <a href="#" id="report-{{$comment->id}}" class="report-comment" post-id="{{$event->id}}" data-token="{{csrf_token()}}" comment-id="{{$comment->id}}"><i class="ion-android-warning"></i>Report</a>
                        </div>{{-- .comment-footer  --}}
                      </div>{{-- .comment-container  --}}
                    </div>{{-- .comment-list  --}}
                    @endforeach
                   @endif
                </div>
             @endforeach
          </div>{{-- .comment-box --}}
          <div class="flex-center">
            {{ $comments -> links()}}
          </div>
          </div>{{-- .scroll --}}
      </div>{{-- #commentTab --}}
    </div>{{-- .box-comment --}}
  </div>{{-- .box-content --}}
</div>{{-- .box --}}
@endif