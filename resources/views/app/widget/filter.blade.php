<div class="flex-center pull-right">
  <div class="loading loading-double hide" ></div>
    {!! Form::open(['method'=>'GET','url'=> lang().'/filter/','class'=>'date-filter','role'=>'filter'])  !!}
      <div class="selectstyle">
        <select name="place" class="sortByFilter" data-lang="{{lang()}}" @if(!empty($category)) data-category="{{$category->getTranslation(lang())->id}}" data-title="{{$category->getTranslation(lang())->title}}" @endif>
          <option value="" disabled="true" selected="true">Urutkan</option>
          <option value="?filter=terdekat">Terdekat</option>
          <option value="?filter=terbaru">Terbaru</option>
          <option value="?filter=termurah">Termurah</option>
          <option value="?filter=termahal">Termahal</option>
          <option value="?filter=terpopuler">Terpopuler</option>
        </select>
      </div>{{-- .selecstyle --}}
    {!! Form::close() !!} 
</div>{{-- .pull-right --}}