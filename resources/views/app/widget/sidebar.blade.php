<div class="col-md-4 sidebar">
  @if (count(advertise('sidebar1')))
    @foreach (advertise('sidebar1') as $ads)
          <div class="widget">
            <div class="ads"> {!!$ads->script!!}</div>
          </div>
    @endforeach
  @endif
  @component('app.widget.latest')
      @slot('title')
        LATEST NEWS
      @endslot
  @endcomponent
  @component('app.widget.popular-sidebar')
  @endcomponent
  @if (count(advertise('sidebar2')))
    @foreach (advertise('sidebar2') as $ads)
          <div class="widget">
            <div class="ads"> {!!$ads->script!!}</div>
          </div>
    @endforeach
  @endif
  @component('app.widget.tagcloud')
  @endcomponent
  @if (count(advertise('sidebar3')))
    @foreach (advertise('sidebar3') as $ads)
          <div class="widget">
            <div class="ads"> {!!$ads->script!!}</div>
          </div>
    @endforeach
  @endif
</div>{{-- .col --}}