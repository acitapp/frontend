@component('layouts.app')
  @slot('webtitle')
   CREATE FREE EVENT
  @endslot
<section class="section flex-center mt20">
  <div class="container">
    <div class="titlebox title-arrow-right">
      <h2 class="title"><span>CREATE EVENT</span></h2>
    </div>
    <div class="row">
      <div class="col-md-7">
        <form class="form contact-form mt20" id="contactform">
          {{csrf_field()}}
          <div class="rows">
            <input type="text" name="name" placeholder="Full Name">
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="rows">
                <input type="text" name="email" placeholder="Email Address">
              </div>
            </div>
            <div class="col-md-6">
              <div class="rows">
                <input type="text" name="phone" placeholder="Phone Number">
              </div>
            </div>
          </div>
          <div class="rows">
            <input type="text" name="subject" placeholder="Subject">
          </div>
          <div class="rows">
            <textarea name="message" placeholder="Message"></textarea>
          </div>
          <div class="rows">
            <div class="loading loading-double hide" id="loaderContact"></div>
            <input type="submit" class="button btn-blue" value="SUBMIT">
          </div>
        </form>
      </div>{{-- .col --}}
    </div>{{-- .row --}}
  </div> {{-- .container --}}
</section>
@endcomponent