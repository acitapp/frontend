@component('layouts.app')
  @slot('webtitle')
    {{getOption('web_title')}}
  @endslot
  <section id="section-home" class="section">
    <div class="container">
      <div class="titleBox flex flex-center">
        <h2 class="title">Article</h2>
      </div>
      <div class="theContent">
          @if (count($posts))
            <div class="scroll">
              @include('app.article.loop')
            </div>
          @else
            <h3>Data Not Available</h3>
          @endif
      </div>{{-- .col --}}
    </div>{{-- .container --}}
  </section>
@endcomponent