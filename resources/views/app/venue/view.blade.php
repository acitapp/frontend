@component('layouts.app')
  @slot('webtitle')
   {{$venue->title}}
  @endslot
  @slot('keyword')
   {{$venue->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($venue->description)}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
            <div class="event-entry">
                <div class="iframe">
                  {!!$venue->info->iframe!!}
                </div>{{-- .event-image --}}
                <div class="event-meta">
                  <h1>{{$venue->title}}</h1>
                  <span class="date">{{$venue->info->address ?? ''}}</span>
                </div>{{-- .event-meta --}}
            </div>{{-- .event-entry --}}
        </div>{{-- .col --}}
        <div class="col-md-4">
            <div class="widget-options">
                <h3>Daftar Venue Lainnya</h3>
                <div class="widget">
                    <div class="LatestList">
                        @foreach ($latests as $venue)
                          <div class="list-body flex-center has-border">
                            <div class="post-meta">
                              <h3><a href="{{url('/')}}/{{lang()}}/venue/{{$venue->slug}}">{{$venue->title}}</a></h3>
                              <span class="date">{{$venue->info->state->name ?? ''}}</span>
                            </div>{{-- .post-meta --}}
                          </div>{{-- .flex-box --}}
                        @endforeach
                    </div>{{-- .LatestList --}}
                </div>{{-- .widget --}}
            </div>{{-- .widget-options --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
@endcomponent