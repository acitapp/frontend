<div id="data" class="row">
@foreach ($posts as $post)
<div class="col-md-3">
    <div class="event-box box">
      <div class="event-image thumb">
        <a href="{{ url('/') }}/{{lang()}}/article/{{$post->id}}/{{$post->getTranslation(lang())->slug}}">
           <img src="{{getThumbnail($post, 'posts', 'small')}}">
        </a>
      </div>{{-- .post-image --}}
      <div class="event-entry">
          <span class="date">{{formatdate($post->created_at)}}</span>
          <h3><a href="{{ url('/') }}/{{lang()}}/article/{{$post->id}}/{{$post->getTranslation(lang())->slug}}">{{limitWord($post->getTranslation(lang())->title,10)}} </a></h3>
      </div>
    </div>{{-- .box --}}
</div>{{-- .col --}}
@endforeach
</div>{{-- .row --}}
<div class="flex-center">{{ $posts -> links()}}</div>