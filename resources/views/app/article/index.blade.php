@component('layouts.app')
  @slot('webtitle')
  kiosTix - Solusi Tepat Tiket Event Dan Informasi Hiburan Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket event dan media informasi hiburan terpercaya di Indonesia. Jual beli tiket event musik, seni pertunjukan, seminar, olahraga, hiburan untuk keluarga dalam dan luar negeri . Mudah, aman dan terpercaya.
  @endslot
  <section id="section-home" class="section">
    <div class="container">
      <div class="titleBox flex flex-center">
        <h2 class="title">Article</h2>
      </div>
      <div class="theContents">
          @if (count($posts))
            <div class="scroll">
              @include('app.article.loop')
            </div>
          @else
            <h3>Data Not Available</h3>
          @endif
      </div>{{-- .col --}}
    </div>{{-- .container --}}
  </section>
@endcomponent