@component('layouts.app')
  @slot('webtitle')
   {{$post->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$post->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($post->description)}}
  @endslot
  @slot('shareimage')
   {{url('/')}}/media/{{$post->cover}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
            <div class="event-entry">
                <div class="event-image thumb">
                  <img src="{{getThumbnail($post, 'posts', 'medium')}}">
                </div>{{-- .event-image --}}
                <div class="event-meta">
                  <h1>{{$post->getTranslation(lang())->title}}</h1>
                  <span class="date">{{formatdate($post->created_at)}}</span>
                </div>{{-- .event-meta --}}
                <div class="event-detail">
                    @if (count($post->tags))
                      <div class="event-info event-artist flex">
                          <label>Tag :</label>
                          <div class="artists">
                            @foreach($post->tags as $tag)
                             <a class="venue-name" href="{{url('/')}}/{{lang()}}/tag/{{$tag->slug}}">{{$tag->name}}</a> 
                            @endforeach
                          </div>
                      </div>
                    @endif
                    <div class="post-description">
                    {!! $post->getTranslation(lang())->description !!}
                    </div>
                </div>{{-- .event-detail --}}
            </div>{{-- .event-entry --}}
        </div>{{-- .col --}}
        <div class="col-md-4">
            <div class="widget-options">
                <h3>Artikel Terkini</h3>
                <div class="widget">
                    <div class="LatestList LatestBLog">
                        @foreach ($latests as $post)
                          <div class="list-body flex-center">
                            <div class="thumb-small thumb">
                                <a href="{{ url('/') }}/{{lang()}}/article/{{$post->id}}/{{$post->getTranslation(lang())->slug ?? ''}}">
                                   <img src="{{getThumbnail($post, 'posts', 'small')}}">
                                </a>
                            </div>{{-- .event-image --}}
                            <div class="post-meta">
                              <h3>{{$post->getTranslation(lang())->title ?? ''}}</h3>
                              <span class="date">{{formatdate($post->created_at)}}</span>
                            </div>{{-- .post-meta --}}
                          </div>{{-- .flex-box --}}
                        @endforeach
                    </div>{{-- .LatestList --}}
                </div>{{-- .widget --}}
            </div>{{-- .widget-options --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
@endcomponent