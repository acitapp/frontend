@if (empty($event->template) || is_null($event->template))
@component('layouts.master')
@else
@component('custom.'.$event->template.'.'.$event->template)
@endif
  @slot('webtitle')
  {{$transaction->event->getTranslation(lang())->title ?? ''}}
  @endslot
  @slot('keyword')
   {{$transaction->event->keyword ?? ''}}
  @endslot
  @slot('description')
  {{strip_tags($transaction->event->getTranslation(lang())->summary ?? '')}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($transaction->event, 'events', 'medium')}}
  @endslot
  @slot('template')
  {{$event->template}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="thanksPage">
      <div class="row">
        <div class="col-md-12">
          <div class="event-meta center">
            <h1>@lang('content.thanks')</h1>
          </div>{{-- .event-meta --}}
        </div>
        <div class="col-md-12"> 
            <div class="order-detail widget-options center">
              @if($transaction->payment->type == 'ovo')
                @include('app.order.ovo_thanks')
              @elseif($transaction->payment->type == 'paypal')
                @include('app.order.paypal_thanks')
              @else
                <div class="widget-head">
                  @if ($transaction->payment->type == 'bank_transfer' || $transaction->payment->type == 'cstore' || $transaction->payment->type == 'echannel')
                  <h3>Pesanan Tiket <br><span class="orange">{{$transaction->event->getTranslation(lang())->title ?? ''}}<br></span> Kamu Menunggu Pembayaran </h3>
                  @else
                    @if ($paymentResponse->transaction_status == 'capture' || $paymentResponse->transaction_status == 'settlement')
                  <h3>Pesanan Tiket <br><span class="orange">{{$transaction->event->getTranslation(lang())->title ?? ''}}<br></span> Kamu Telah Berhasil Diproses   </h3>
                    @else
                  <h3>Pesanan Tiket <br><span class="orange">{{$transaction->event->getTranslation(lang())->title ?? ''}}<br></span> Kamu Belum Berhasil Diproses   </h3>
                    @endif
                  @endif
                </div>{{-- .widget-head --}}
                <div class="event-image thumb">
                  <img src="{{getThumbnail($transaction->event, 'events', 'medium')}}">
                </div>{{-- .event-image --}}
                @if (thisAttraction($event))
                <div class="comment-box">
                 @if (Auth::check())
                    <div class="box-author flex-box">
                      <a class="thumb thumb-author is-circle" href="{{ url('/') }}/user/{{Auth::user()->slug}}">
                        @if (count(Auth::user()->getMedia('avatar')))
                          <img src="{{ url('/') }}/media/{{Auth::user()->getMedia('avatar')->first()->id}}/conversions/square.jpg" class="is-circle">
                        @else
                           <img src="{{ url('/') }}/images/default_square.jpg" class="is-circle">
                        @endif
                      </a>
                      <div class="meta-title">
                        <h3 class="s-title">
                          <a href="{{ url('/') }}/user/{{Auth::user()->slug}}">{{Auth::user()->name}}</a>
                        </h3>
                      </div>
                      <div class="event-rate flex-center">
                          <div id="rate-{{$event->id}}" event-id="{{$event->id}}" class="rating-star" average="{{$event->rate_average}}"></div>
                      </div>
                    </div>{{-- .box-meta --}}
                  @endif
                  <div class="comment-form">
                       <textarea placeholder="Tulis Komentar" id="commentMessage-0"></textarea>
                       @if (Auth::check())
                         <a href="#" class="button addComment" post-id="{{$event->id}}" data-token="{{csrf_token()}}" comment-id="0">Simpan</a>
                       @else
                         <a href="#popup-login" class="button showPopup">Comment</a>
                       @endif
                  </div>{{-- .comment-form--}}
                </div>{{-- .comment-box --}}
                @endif
                <div class="widget-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="greybox">
                          <span>Total Pembayaran: </span>
                           <h3 style="margin:0;"><strong>Rp {{number_format($transaction->value_grand_total)}}</strong></h3>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="greybox">
                          <span>Metode Pembayaran: </span>
                           <h3 style="margin:0;"><strong>{{$transaction->payment->name}}</strong></h3>
                        </div>
                      </div>
                    </div>
                      @if ($transaction->payment->type == 'bank_transfer' || $transaction->payment->type == 'cstore')
                    <div class="row">
                      <div class="col-md-6">
                          @if (!is_null($paymentResponse->permata_va_number))
                            <div class="greybox">
                              <span>Virtual Account no: </span>
                               <h3 style="margin:0;"><strong>{{$paymentResponse->permata_va_number}}</strong></h3>
                            </div>
                          @endif
                          @if (!is_null($paymentResponse->payment_code))
                            <div class="greybox">
                              <span>Permata Virtual Account: </span>
                               <h3 style="margin:0;"><strong>{{$paymentResponse->payment_code}}</strong></h3>
                            </div>
                          @endif
                      </div>
                      <div class="col-md-6">
                        <div class="greybox">
                          <span>Order ID: </span>
                           <h3 style="margin:0;"><strong>{{$transaction->order_no}}</strong></h3>
                        </div>
                      </div>
                    </div>
                      <div class="center">
                            <br>
                            <span>Sisa Waktu Pembayaran </span>
                            <h3 style="margin:0;"><div id="countdownExpPay" event-id="{{$event->id}}" transaction-id="{{$transaction->id}}"></div></h3>
                             <h4 style="margin:0;"><strong>{{$expTimeViewDate}}<br>{{$expTimeViewTime}}</strong></h4>
                      </div>
                      @else
                        <div class="row">
                          <div class="col-md-12">
                            <div class="greybox">
                              <span>Order ID: </span>
                               <h3 style="margin:0;"><strong>{{$transaction->order_no}}</strong></h3>
                            </div>
                          </div>
                        </div>
                      @endif
                      <br>
                      @if ($paymentResponse->transaction_status == 'capture')
                        @if($transaction->event->type == '["E-Voucher"]')
                        <h3>e-voucher akan dikirim ke email <br>{{$transaction->customer->email ?? ''}}</h3>
                        @else
                        <h3>e-ticket akan dikirim ke email <br>{{$transaction->customer->email ?? ''}}</h3>
                        @endif
                      @endif
                      <br>
                      @if (empty($event->template) || is_null($event->template))
                        <div class="share-box">
                          <h4>Bagikan event ini dan ajak temanmu</h4>
                           @include('app.widget.sharebox')
                        </div>
                      @endif
                  </div>{{-- .event-detail --}}
                  <div class="howTo-pay">
                  @if ($transaction->payment->type == 'bank_transfer' || $transaction->payment->type == 'cstore')
                    {!!$transaction->payment->description ?? '' !!}
                  @endif
                  </div>
                @endif
              </div>{{-- .widget-body --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
      </div>
    </div>{{-- .container --}}
    <div class="thxleft"></div>
    <div class="thxright"></div>
  </section>
  <script>
    var expTime = '{{$expTime}}';
    $("#countdownExpPay").countdown(expTime, function(event) {
      $(this).html(
        event.strftime('<strong>%H</strong> <span>Jam</span>  <strong>%M</strong> <span>Menit</span> <strong>%S</strong> <span>Detik</span>')
      )}).on('finish.countdown', function() {
          var eventID = $(this).attr('event-id');
          var trxId = $(this).attr('transaction-id')
            var data = {
                _token: token,
                trxId: trxId,
                eventID: eventID,
          }
          $.ajax({
            method: "POST",
            url: base_url+"/expired",
            data : data
          }).done(function(data) {
              $("#popup-title").text("Maaf,");
              $("#popup-text").html("Pesanan anda sudah melewati batas waktu pembayaran.");
              common.popup()
               setTimeout(function () {
                     var url = base_url+data.url;
                     window.location = url;
                }, 4000);
          }).fail(function (jqXHR, textStatus, errorThrown) {
                $("#popup-title").text("Ooops!");
                $("#popup-text").text("Please try again.");
                common.popup()
            });
          return false;
      });
  </script>
@endcomponent