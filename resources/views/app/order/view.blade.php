@component('layouts.app')
  @slot('webtitle')
  {{$event->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$event->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($event->getTranslation(lang())->summary)}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($event, 'events', 'medium')}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="event-meta">
            <h1>ORDER SUMMARY</h1>
          </div>{{-- .event-meta --}}
        </div>
        <div class="col-md-6">
            <div class="order-detail widget-options">
              <div class="widget-head">
                <h3>{{$event->getTranslation(lang())->title}}</h3>
              </div>{{-- .widget-head --}}
              <div class="widget-body">
                  <p>
                    <span>Selected Dates</span><br>
                    <strong>Saturday, 24 May 2017</strong>
                  </p>
                  <p>
                    <span>Venue Location</span><br>
                    <strong>Istora Senayan, Jakarta Indonesia</strong>
                  </p>
                  <div id="booking-detail" class="booking-detail">
                      <div class="booking-head">
                        <h3>Order Details</h3>
                      </div>{{-- .booking-head --}}
                      <div class="booking-body">
                          <div class="flex-row booking-section">
                            <p>
                              <strong>GOLD</strong>
                            </p>
                            <div class="item-row">
                              <span class="product-item">Adults</span>
                              <span class="product-qty">1</span>
                              <span class="product-price">Rp 868,400</span>
                            </div>{{-- .flex-row --}}
                            <div class="item-row">
                              <span class="product-item">Children</span>
                              <span class="product-qty">1</span>
                              <span class="product-price">Rp 573,018</span>
                            </div>{{-- .flex-row --}}
                          </div>{{-- .flex-row --}}
                          <div class="flex-row booking-section">
                            <p>
                            <strong>PLATINUM</strong>
                            </p>
                            <div class="item-row">
                              <span class="product-item">Adults</span>
                              <span class="product-qty">1</span>
                              <span class="product-price">Rp 868,400</span>
                            </div>{{-- .flex-row --}}
                            <div class="item-row">
                              <span class="product-item">Children</span>
                              <span class="product-qty">1</span>
                              <span class="product-price">Rp 573,018</span>
                            </div>{{-- .flex-row --}}
                          </div>{{-- .flex-row --}}
                      </div>{{-- .booking-body --}}
                      <div class="booking-footer">
                          <div class="booking-section">
                            <div class="item-row f16">
                              <span class="product-item"><strong>SUBTOTAL</strong></span>
                              <span class="product-price"><strong>Rp 1,441,418</strong></span>
                            </div>{{-- .flex-row --}}
                            <div class="item-row f14">
                              <span class="product-item">Fee</span>
                              <span class="product-price">Rp 500,000</span>
                            </div>{{-- .flex-row --}}
                            <div class="item-row">
                              <span class="product-item">TOTAL</span>
                              <span class="product-price total-price">Rp 1,941,418</span>
                            </div>{{-- .flex-row --}}
                          </div>{{-- .booking-section--}}
                      </div>
                  </div>{{-- .booking-detail --}}
                </div>{{-- .event-detail --}}
              </div>{{-- .widget-body --}}
             <a href="{{url('/')}}/{{lang()}}/order/{{$event->id}}/{{$event->getTranslation(lang())->slug}}" class="button btn-block">ORDER NOW</a>
        </div>{{-- .col --}}
        <div class="col-md-6">
            <div class="order-detail widget-options payment-method">
                <div class="widget-head">
                 <h3>CHOOSE YOUR PAYMENT METHOD</h3>
                </div>
                  <div class="panel-group accordion-border" id="accordion-payment" role="tablist" aria-multiselectable="true">
                  @foreach ($event->payments as $payment)
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="payment-{{$payment->id}}">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" class="collapsed sectionBtn section-disable" data-parent="#accordion-payment" href="#payment-section-{{$payment->id}}" aria-expanded="true" aria-controls="ticket-section-{{$payment->id}}">
                              {{$payment->name}}
                            </a>
                          </h4>
                        </div>{{-- .panel-heading --}}
                        <div id="payment-section-{{$payment->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{$payment->id}}">
                          <div class="panel-body">
                            <h4>{{$payment->name}}</h4>
                            {!!$payment->description!!}
                            <a href="#" class="button btn-block btn-add">Pay With {{$payment->name}}</a>
                          </div>{{-- .panel-body --}}
                        </div>{{-- .panel-collapse --}}
                      </div>{{-- .panel --}}
                  @endforeach
                    </div>
            </div>
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
@endcomponent