@component('layouts.app')
  @slot('webtitle')
  {{$event->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$event->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($event->getTranslation(lang())->summary)}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($event, 'events', 'medium')}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="event-meta center">
          </div>{{-- .event-meta --}}
        </div>
        <div class="col-md-12"> 
            <div class="order-detail widget-options center">
              <div class="widget-head">
                  @if ($response->status_code == 406)
                  <h3>Maaf, Anda sudah melakukan transaksi ini.</h3><br><br>
                  <a href="{{url('/')}}/{{lang()}}" class="button">Beli Event Lainnya</a>
                  @else
                  <h3>Maaf, Transaksi anda belum berhasil.</h3><br><br>
                  <a href="{{url('/')}}/{{lang()}}/event/{{$event->id}}/{{$event->slug}}" class="button">Ulangi Pembelian</a>
                  @endif
              </div>{{-- .widget-head --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
@endcomponent