@component('layouts.app')
  @slot('webtitle')
    kiosTix.com - Temukan Event-mu di sini
  @endslot
  @slot('keyword')
    Tiket, Event
  @endslot
  @slot('description')
    Tiket, Event
  @endslot
  @slot('shareimage')
    https://www.kiostix.com/images/default_medium.jpg
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="event-meta center">
          </div>{{-- .event-meta --}}
        </div>
        <div class="col-md-12"> 
            <div class="order-detail widget-options center">
              <div class="widget-head">
                <h1>Maaf,</h1>
                <h3>Pesanan anda gagal</h3><br><br>
                @if (!is_null($event))
                   <a href="{{url('/')}}/{{lang()}}/event/{{$event->id}}/{{$event->slug}}" class="button">Pesan Ulang</a>
                @endif
              </div>{{-- .widget-head --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
@endcomponent