@if (empty($event->template) || is_null($event->template))
@component('layouts.master')
@else
@component('custom.'.$event->template.'.'.$event->template)
@endif
  @slot('webtitle')
  {{$event->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$event->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($event->getTranslation(lang())->summary)}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($event, 'events', 'medium')}}
  @endslot
  @slot('template')
   {{$event->template}}
  @endslot
  <section id="section-details" class="section payment-detail">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
            <div class="payMentBox">
              <div class="order-detail widget-options">
                <div class="widget-head">
                  <h3>Rincian Pemesanan</h3>
                </div>{{-- .widget-head --}}
                <div class="widget-body">
                    <div class="flex-center">
                      <div class="colom-left col-md-6">
                        <p>
                          <strong class="full_name">{{$transaction->customer->name ?? ''}}</strong><br>
                          <span class="full_name">{{$transaction->customer->email ?? ''}}</span><br>
                          <span class="full_name">{{$transaction->customer->information->phone ?? ''}}</span><br>
                        </p>
                      </div>
                      <div class="colom-right">
                        <p>
                       <strong id="event-title" data-id="{{$event->id}}">{{$event->getTranslation(lang())->title}}</strong><br>
                          <span class="venue_name">{{$transaction->venue->title ?? ''}}, {{$transaction->venue->info->state->name ?? ''}}</span><br>
                          <span>&nbsp;</span>
                        </p>
                      </div>
                    </div>
                    <br>
                    <div id="booking-detail" class="booking-details">
                        <div class="booking-header flex-center">
                            <div class="colom-info col3">
                              <span>Harga/Section</span>
                            </div>{{-- .colom-left --}}
                            <div class="colom-left col3 center">
                              <span>Kuantitas</span>
                            </div>{{-- .colom-left --}}
                            <div class="colom-right col3 text-right">
                              <span>Subtotal</span>
                            </div>{{-- .colom-right --}}
                        </div>{{-- .booking-header --}}
                        <div class="booking-body">
                              @foreach ($transactions as $order)
                                  <div class="row-section-ticket">
                                    <div class="section-name">
                                          <span  class="labels ticket-name">
                                         {{$order->name ?? ''}}
                                         </span><br>
                                         @if (thisAttraction($event))
                                       <span class="date">{{eventDateFormat($order->visit_date)}}
                                      {{--     @if (!is_null($order->visit_time))
                                          <br>at {{date('g:ia', strtotime($order->visit_time))}}
                                          @endif --}}
                                        </span> 
                                        @endif
                                    </div>
                                    <div class="flex-center">
                                        <div class="colom-info col3">
                                          @if ($order->order_value == 0)
                                          <span class="product-price">Free</span>
                                          @else
                                          <span class="product-price">Rp {{number_format($order->order_value)}}</span>
                                          @endif
                                        </div>{{-- .col3 --}}
                                        <div class="colom-info col3 center">
                                          <span class="product-qty">x {{$order->qty}}</span>
                                        </div>{{-- .col3 --}}
                                        <div class="colom-info col3 text-right">
                                          @if ($order->total_value== 0)
                                          <span class="product-price">Free</span>
                                          @else
                                          <span class="product-price">Rp {{number_format($order->total_value)}}</span>
                                          @endif
                                        </div>{{-- .col3 --}}
                                      </div>
                                  </div>{{-- .row-section-ticket--}}
                              @endforeach
                        </div>{{-- .booking-body --}}
                       <div class="booking-detail">
                          <div class="booking-footer">
                              <div class="item-row booking-section">
                                <span class="product-item">Jumlah</span>
                                <span class="product-price total-price">Rp. <strong>{{number_format($transaction->value_sub_total)}}</strong></span>
                              </div>
                              <div class="item-row f14 booking-section">
                                <span class="product-item">Fee</span>
                                <span class="product-price">Rp. <span id="feePayment">0</span></span>
                              </div>{{-- .flex-row --}}
                              <div id="voucherBox">
                                <div class="item-row f14 booking-section" id="voucherGroup">
                                  <span class="product-item">Kode Voucher (Jika Ada)</span>
                                  <div class="inputgroup">
                                    <div class="loading loading-double hide" id="loader-voucher"></div>
                                    <input type="text" name="voucher" id="voucher">
                                    <a href="#" class="button btn-outline btn-s submitVoucher btn-grey" data-event="{{$event->id}}">Submit</a>
                                  </div>
                                </div>{{-- .flex-row --}}
                              </div>
                              <div class="item-row booking-section" id="totalPriceBox">
                                <span class="product-item">Total</span>
                                <span class="product-price total-price" id="totalPrice" data-value="{{$transaction->value_grand_total}}">Rp. <span id="totalPriceValue">{{number_format($transaction->value_grand_total)}}</span></span>
                              </div>
                          </div>
                      </div>{{-- .booking-detail --}}
                      <div class="agreement">
                        @lang('content.agreement') <a href="{{url('/download/kiosTix_AG2018-Terms-and-Conditions-Ticket-Purchase-and-Spectator Policy.pdf')}}">@lang('content.tnc')</a>
                      </div>
                    </div>{{-- .booking-detail --}}
                  </div>{{-- .event-detail --}}
                </div>{{-- .widget-body --}}
                <div id="countdown-block">
                    <h3>@lang('content.time_left')</h3>
                   <div id="countdown" event-id="{{$event->id}}" transaction-id="{{$transaction->id}}"></div>
                </div>
            </div>
        </div>{{-- .col --}}
        <div class="col-md-6">
            <div class="payMentBox">
            <div class="order-detail widget-options payment-method">
                <div class="widget-head">
                 <h3>@lang('content.payment_method')</h3>
                </div>
                  <div class="panel-group accordion-border" id="accordion-payment" role="tablist" aria-multiselectable="true">
                  @foreach ($event->payments as $payment)
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="payment-{{$payment->id}}" data-typepyment="{{$payment->type}}">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" class="flex-center collapsed sectionBtn section-disable" data-parent="#accordion-payment" title="{{$payment->name}}" href="#payment-section-{{$payment->id}}" aria-expanded="true" aria-controls="ticket-section-{{$payment->id}}"  title="{{$payment->name}}">
                              {{$payment->name}}
                              <img src="{{url('/')}}/images/{{$payment->type}}.png" alt="">
                            </a>
                          </h4>
                        </div>{{-- .panel-heading --}}
                        <div id="payment-section-{{$payment->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{$payment->id}}" title="{{$payment->name}}" data-typepyment="{{$payment->type}}" >
                          <div class="panel-body">
                            @if ($payment->type == 'credit_card')
                              <form action="{{url('/payment/direct')}}" method="POST" id="payment-form-{{$payment->id}}">
                                {{ csrf_field() }}
                                  <input type="hidden" value="{{encrypt($transaction->order_no)}}" name="key">
                                  <input type="hidden" value="{{$payment->id}}" name="payment_method_id">
                                  <input type="hidden" value="{{$payment->type}}" name="payment_type">
                                  <input type="hidden" class="voucher_code"  name="voucher_code">
                                  <input type="hidden" class="voucher_id"  name="voucher_id">
                                  <input type="hidden" class="voucher_value"  name="voucher_value">
                                <div class="booking-section row">
                                    <div class="col-md-12">
                                        <label for="">Nomor Kartu</label>
                                          <input class="card-number form-control card-no" value="" size="20" type="text" autocomplete="off"/>
                                    </div>{{-- .col --}}
                                </div>{{-- .booking-section --}}
                                <div class="booking-section row">
                                    <div class="col-md-12">
                                          <label for="">Nama Pemegang Kartu</label>
                                          <input class="card-number card-name form-control" value="" size="20" type="text" autocomplete="off"/>
                                    </div>{{-- .col --}}
                                </div>{{-- .booking-section --}}
                                <div class="booking-section booking-cc">
                                    <div class="row">
                                      <div class="col-md-8">
                                        <label for="">Tanggal Masa Berlaku</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                 <input class="card-expiry-month" value="" placeholder="MM" size="2" type="text" />
                                            </div>{{-- .col --}}
                                            <div class="col-md-6">
                                                 <input class="card-expiry-year" value="" placeholder="YYYY" size="4" type="text" />
                                            </div>{{-- .col --}}
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                          <label for="">CV/CVV</label>
                                          <input class="card-cvv" value="" size="4" type="password" autocomplete="off"/>
                                      </div>
                                    </div>
                                </div>{{-- .booking-section --}}
                                <input id="token_id" name="token_id" type="hidden" />
                                <input type="hidden" name="gross_amount" value="" id="gross_amount">
                              </form>
                              @elseif($payment->type == 'cicilan')
                                <form action="{{url('/payment/direct')}}" method="POST" id="payment-form-{{$payment->id}}">
                                {{ csrf_field() }}
                                  <input type="hidden" value="{{encrypt($transaction->order_no)}}" name="key">
                                  <input type="hidden" value="{{$payment->id}}" name="payment_method_id">
                                  <input type="hidden" value="{{$payment->type}}" name="payment_type">
                                  <input type="hidden" class="voucher_code"  name="voucher_code">
                                  <input type="hidden" class="voucher_id"  name="voucher_id">
                                  <input type="hidden" class="voucher_value"  name="voucher_value">
                                <div class="booking-section row">
                                    <div class="col-md-12">
                                        <label for="">MANDIRI</label>
                                          <div class="cc-installment">
                                            <input class="" type="radio" value="3" name="month_installment" />
                                            <span>3x, Bunga 0% Rp. {{number_format($installment3)}}</span>
                                          </div>
                                          <div class="cc-installment">
                                            <input class="" type="radio" value="6" name="month_installment" />
                                            <span>6x, Bunga 0% Rp. {{number_format($installment6)}}</span>
                                          <div class="cc-installment">
                                            <input class="" type="radio" value="12" name="month_installment" />
                                            <span>12x, Bunga 0% Rp. {{number_format($installment12)}}</span>
                                          </div>
                                          <div class="cc-installment">
                                            <input class="" type="radio" value="24" name="month_installment" />
                                            <span>24x, Bunga 0% Rp. {{number_format($installment24)}}</span>
                                          </div>
                                    </div>{{-- .col --}}
                                </div>{{-- .booking-section --}}
                                <div class="booking-section row">
                                    <div class="col-md-12">
                                        <label for="">Nomor Kartu</label>
                                          <input class="card-number-installment form-control card-no" value="" size="20" type="text" autocomplete="off"/>
                                    </div>{{-- .col --}}
                                </div>{{-- .booking-section --}}
                                <div class="booking-section row">
                                    <div class="col-md-12">
                                          <label for="">Nama Pemegang Kartu</label>
                                          <input class="card-number card-name form-control" value="" size="20" type="text" autocomplete="off"/>
                                    </div>{{-- .col --}}
                                </div>{{-- .booking-section --}}
                                <div class="booking-section booking-cc">
                                    <div class="row">
                                      <div class="col-md-8">
                                        <label for="">Tanggal Masa Berlaku</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                 <input class="card-installment-expiry-month" value="" placeholder="MM" size="2" type="text" />
                                            </div>{{-- .col --}}
                                            <div class="col-md-6">
                                                 <input class="card-installment-expiry-year" value="" placeholder="YYYY" size="4" type="text" />
                                            </div>{{-- .col --}}
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                          <label for="">CV/CVV</label>
                                          <input class="card-installment-cvv" value="" size="4" type="password" autocomplete="off"/>
                                      </div>
                                    </div>
                                </div>{{-- .booking-section --}}
                                <input id="token_id" name="token_id" type="hidden" />
                                <input type="hidden" name="gross_amount" value="" id="gross_amount">
                              </form>
                              @elseif($payment->type == 'mandiri-credit-card')
                              <form action="{{url('/payment/direct')}}" method="POST" id="payment-form-{{$payment->id}}">
                                {{ csrf_field() }}
                                  <input type="hidden" value="{{encrypt($transaction->order_no)}}" name="key">
                                  <input type="hidden" value="{{$payment->id}}" name="payment_method_id">
                                  <input type="hidden" value="{{$payment->type}}" name="payment_type">
                                  <input type="hidden" class="voucher_code"  name="voucher_code">
                                  <input type="hidden" class="voucher_id"  name="voucher_id">
                                  <input type="hidden" class="voucher_value"  name="voucher_value">
                                <div class="booking-section row">
                                    <div class="col-md-12">
                                        <label for="">Nomor Kartu</label>
                                          <input class="card-number-mandiri form-control card-no" value="" size="20" type="text" autocomplete="off"/>
                                    </div>{{-- .col --}}
                                </div>{{-- .booking-section --}}
                                <div class="booking-section row">
                                    <div class="col-md-12">
                                          <label for="">Nama Pemegang Kartu</label>
                                          <input class="card-number card-name form-control" value="" size="20" type="text" autocomplete="off"/>
                                    </div>{{-- .col --}}
                                </div>{{-- .booking-section --}}
                                <div class="booking-section booking-cc">
                                    <div class="row">
                                      <div class="col-md-8">
                                        <label for="">Tanggal Masa Berlaku</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                 <input class="card-mandiri-expiry-month" value="" placeholder="MM" size="2" type="text" />
                                            </div>{{-- .col --}}
                                            <div class="col-md-6">
                                                 <input class="card-mandiri-expiry-year" value="" placeholder="YYYY" size="4" type="text" />
                                            </div>{{-- .col --}}
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                          <label for="">CV/CVV</label>
                                          <input class="card-mandiri-cvv" value="" size="4" type="password" autocomplete="off"/>
                                      </div>
                                    </div>
                                </div>{{-- .booking-section --}}
                                <input id="token_id_mandiri" name="token_id" type="hidden" />
                                <input type="hidden" name="gross_amount" value="" id="gross_amount_mandiri">
                              </form>
                              @elseif($payment->type == 'mandiri_clickpay')
                              <form action="{{url('/payment/direct')}}" method="POST" id="payment-form-{{$payment->id}}">
                                {{ csrf_field() }}
                                  <input type="hidden" value="{{encrypt($transaction->order_no)}}" name="key">
                                  <input type="hidden" value="{{$payment->id}}" name="payment_method_id">
                                  <input type="hidden" value="{{$payment->type}}" name="payment_type">
                                  <input type="hidden" class="voucher_code"  name="voucher_code">
                                  <input type="hidden" class="voucher_id"  name="voucher_id">
                                  <input type="hidden" class="voucher_value"  name="voucher_value">
                                  <input type="hidden" name="gross_amount" value="{{$transaction->fee_tax+$transaction->value_sub_total}}">
                                  {!!$payment->description!!}
                                 <br>
                                 <div class="from-token-mandiri" style="display:none">
                                    <div class="booking-section row">
                                        <div class="col-md-12">
                                            <label for="">Nomor Kartu Debet</label>
                                              <input class="card-numberdebet form-control card-no" value="" size="20" type="text" autocomplete="off"/>
                                        </div>{{-- .col --}}
                                    </div>{{-- .booking-section --}}
                                    <div class="booking-section row">
                                      <div class="col-md-12">
                                          <div style="display: none">
                                          <div class="col-md-12 " >
                                                <label for="">APPLI</label>
                                                <span>3</span>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="">Input1</label>
                                                <span class="input1"></span>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="">Input2</label>
                                                <span class="input2">{{$transaction->fee_tax+$transaction->value_sub_total}}</span>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="">Input3</label>
                                                <span class="input3"></span>
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                              <label for="">Token</label>
                                                <input class="token form-control" name="token" value="" size="20" type="text" autocomplete="off"/>
                                          </div>
                                        {{-- .col --}}
                                      </div>{{-- .booking-section --}}
                                    </div>
                                  </div>
                                <input id="token_idmandiriclickpay" name="token_id" type="hidden" />
                                <input id="input3" name="input3" type="hidden" />
                              
                              </form>
                              @elseif($payment->type == 'ovo')
                              <form action="{{url('/payment/ovo/direct')}}" method="POST" id="payment-form-{{$payment->id}}">
                                {{ csrf_field() }}
                                  <input type="hidden" value="{{encrypt($transaction->order_no)}}" name="key">
                                  <input type="hidden" value="{{$payment->id}}" name="payment_method_id">
                                  <input type="hidden" value="{{$payment->type}}" name="payment_type">
                                  <input type="hidden" class="voucher_code"  name="voucher_code">
                                  <input type="hidden" class="voucher_id"  name="voucher_id">
                                  <input type="hidden" class="voucher_value"  name="voucher_value">
                                  <input type="hidden" name="gross_amount" value="{{$transaction->fee_tax+$transaction->value_sub_total}}">
                                  <div class="booking-section row">
                                      <div class="col-md-12">
                                          <label for="">Masukan No HP yang Terdaftar di OVO</label>
                                            <input class="form-control" size="20" name="phone" maxlength="16"  type="text" autocomplete="off" required="true"/>
                                      </div>{{-- .col --}}
                                  </div>{{-- .booking-section --}}
                              </form>
                              {!!$payment->description!!}
                            @elseif($payment->type == 'paypal')
                              {!!$payment->description!!}
                              <br>
                              <form action="{{url('/paypal/express-checkout')}}" method="POST" id="payment-form-{{$payment->id}}">
                                {{ csrf_field() }}
                                  <input type="hidden" value="{{encrypt($transaction->order_no)}}" name="key">
                                  <input type="hidden" value="{{$payment->id}}" name="payment_method_id">
                                  <input type="hidden" value="{{$payment->type}}" name="payment_type">
                                  <input type="hidden" class="voucher_code"  name="voucher_code">
                                  <input type="hidden" class="voucher_id"  name="voucher_id">
                                  <input type="hidden" class="voucher_value"  name="voucher_value">
                                  <input type="hidden" name="gross_amount" value="{{$transaction->fee_tax+$transaction->value_sub_total}}">
                              </form> 
                            @else
                              {!!$payment->description!!}
                              <br>
                              <form action="{{url('/payment/direct')}}" method="POST" id="payment-form-{{$payment->id}}">
                                {{ csrf_field() }}
                                  <input type="hidden" value="{{encrypt($transaction->order_no)}}" name="key">
                                  <input type="hidden" value="{{$payment->id}}" name="payment_method_id">
                                  <input type="hidden" value="{{$payment->type}}" name="payment_type">
                                  <input type="hidden" class="voucher_code"  name="voucher_code">
                                  <input type="hidden" class="voucher_id"  name="voucher_id">
                                  <input type="hidden" class="voucher_value"  name="voucher_value">
                                  <input type="hidden" name="gross_amount" value="{{$transaction->fee_tax+$transaction->value_sub_total}}">
                              </form> 
                            @endif     
                          </div>{{-- .panel-body --}}
                        </div>{{-- .panel-collapse --}}
                      </div>{{-- .panel --}}
                  @endforeach
                    </div>
                    <div class="loading loading-double hide" id="loader-payment"></div>
                    <div id="btnPayBox">
                     <button id="btnPay" class="button btn-block btn-add btnPay" type="submit" disabled="disabled">Bayar Sekarang</button>
                    </div>
            </div>
            </div>
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
  <script>
    var expTime = '{{$expTime}}';
  </script>
@endcomponent