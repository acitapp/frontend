@component('layouts.master')
  @slot('webtitle')
  {{$event->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$event->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($event->getTranslation(lang())->summary)}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($event, 'events', 'medium')}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="event-meta">
            <h1>Booking Summary</h1>
          </div>{{-- .event-meta --}}
        </div>
        <div class="col-md-6">
            <div class="order-detail widget-options">
              <div class="widget-head">
                <h3>{{$event->getTranslation(lang())->title}}</h3>
              </div>{{-- .widget-head --}}
              <div class="widget-body">
                  <p>
                    <span>Venue Location</span><br>
                    <strong>{{$transaction->venue->title ?? ''}}</strong>, <br>
                    <span>{{$transaction->venue->info->state->name ?? ''}},{{$transaction->venue->info->country->name ?? ''}}</span>
                  </p>
                  <div id="booking-detail" class="booking-details">
                      <div class="booking-head">
                      </div>{{-- .booking-head --}}
                      <div class="booking-body">
                          <div class="flex-row booking-section">
                            @foreach ($transaction->orders as $order)
                              <div class="orders-items">
                                <div class="item-row">
                                  <strong class="product-item">{{$order->sectionable->name}}  <span class="date">({{$order->periode}})</span></strong>
                                  <span class="product-qty">x {{$order->quantity}}</span>
                                  @if ($order->order_value*$order->quantity == 0)
                                  <span class="product-price">Free</span>
                                  @else
                                  <span class="product-price">Rp {{number_format($order->order_value*$order->quantity)}}</span>
                                  @endif
                                </div>{{-- .flex-row --}}
                                <div class="visit-schedules date">
                                  <div class="visit-date">Visit date:<br> <span class="v-date">{{$order->visit_date}}, <br>{{date('g:ia', strtotime($order->visit_time))}}</span></div>
                                </div>{{-- .visit-schedule --}}
                              </div>
                            @endforeach
                          </div>{{-- .flex-row --}}
                      </div>{{-- .booking-body --}}
                      <div class="booking-footer">
                          <div class="booking-section">
                            <div class="item-row">
                              <span class="product-item"><strong>Subtotal</strong></span>
                              <span class="product-price"><strong>Rp {{number_format($transaction->value_sub_total)}}</strong></span>
                            </div>{{-- .flex-row --}}
                            <div class="item-row">
                              <span class="product-item">Total</span>
                              <span class="product-price total-price">Rp {{number_format($transaction->value_grand_total)}}</span>
                            </div>{{-- .flex-row --}}
                          </div>{{-- .booking-section--}}
                      </div>
                  </div>{{-- .booking-detail --}}
                </div>{{-- .event-detail --}}
              </div>{{-- .widget-body --}}
        </div>{{-- .col --}}
        <div class="col-md-6">
            <div class="order-detail widget-options">
                <div class="widget-head">
                 <h3>@lang('content.customer_info')</h3>
                 <p>@lang('content.customer_info_message')</p>
                </div>
                <div class="widget-body">
                  @if (Auth::check())
                  @else
                  <p>@lang('content.please') <a href="#popup-login" class="showPopup">@lang('content.login')</a> @lang('content.login_suggest')</p>
                  @endif
                   {!! Form::model($userInfo = new\App\Models\UserInformation,['url' => 'booking', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                    <input type="hidden" name="key" value="{{encrypt($transaction->order_no)}}">
                    <div class="booking-section">
                          <div class="row">
                            <div class="col-md-6">
                                 {!! Form::text('firstname',Auth::user()->information->firstname ?? null, ['class'=>'form-control','placeholder'=>Lang::get('content.firstname')]) !!}
                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        {{ $errors->first('firstname') }}
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                            <div class="col-md-6">
                                 {!! Form::text('lastname',Auth::user()->information->lastname ?? null, ['class'=>'form-control','placeholder'=>Lang::get('content.lastname')]) !!}
                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        {{ $errors->first('lastname') }}
                                    </span>
                                @endif
                            </div>{{-- .col --}}
                          </div>
                          <span class="help-block help-info">@lang('content.help_fullname')</span> 
                    </div>{{-- .booking-section --}}
                    <div class="booking-section row">
                        <div class="col-md-12">
                         {!! Form::select('gender',['Male' => 'Male','Female' => 'Female'],Auth::user()->information->gender ?? null, ['class'=>'form-control select', 'placeholder' =>   Lang::get('content.gender')]) !!}       
                            @if ($errors->has('gender'))
                                <span class="help-block">
                                   {{ $errors->first('gender') }}
                                </span>
                            @endif
                        </div>{{-- .col --}}
                    </div>{{-- .row rows --}}
                    <div class="booking-section row">
                        <div class="col-md-12">
                             {!! Form::text('dob',null, ['class'=>'form-control dob','placeholder'=>   Lang::get('content.dob')]) !!}
                            @if ($errors->has('dob'))
                                <span class="help-block">
                                   {{ $errors->first('dob') }}
                                </span>
                            @endif
                            <span class="help-block help-info">@lang('content.dob')</span>
                        </div>{{-- .col --}}
                    </div>{{-- .row rows --}}
                    <div class="booking-section row">
                        <div class="col-md-12">
                             {!! Form::text('phone',Auth::user()->information->phone ?? null, ['class'=>'form-control','placeholder'=>  Lang::get('content.phone')]) !!}
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                   {{ $errors->first('phone') }}
                                </span>
                            @endif
                            <span class="help-block help-info">@lang('content.help_phone')</span>
                        </div>{{-- .col --}}
                    </div>{{-- .booking-section --}}
                    <div class="booking-section row">
                        <div class="col-md-12">
                             {!! Form::text('email',Auth::user()->information->email ?? null, ['class'=>'form-control','placeholder'=>'Email Address']) !!}
                            @if ($errors->has('email'))
                                <span class="help-block">
                                   {{ $errors->first('email') }}
                                </span>
                            @endif
                            <span class="help-block help-info">@lang('content.help_email')</span>
                        </div>{{-- .col --}}
                    </div>{{-- .booking-section --}}
                    <button type="submit" class="button btn-block">@lang('content.btntopayment')</button>
                  {!! Form::close() !!}
                </div>{{-- .widget-body --}}
            </div>{{-- .widget --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
@endcomponent