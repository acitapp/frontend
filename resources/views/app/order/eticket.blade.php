<style type="text/css">
	@page {margin: 20px 20px 0px 25px; }


	body{
		font-family: 'montserrat', sans-serif;
	}

	strong{
		font-weight: bold;
	}
	.light{
		font-weight: 300;
	}
	table td {
		border: 1px solid #000;
		padding: 0px 0px;
		vertical-align: middle;
	}

	table .text-left {
		text-align: left !important;
	}

	table .pd5 {
		padding: 5px 5px;
	}

	table .pd10 {
		padding: 9px 9px;
	}

	table.table_small {
		text-align: center;
		font-weight: bold;
		border-collapse: collapse;
	}

	.table_small th {
		border: 1px solid #000;
		font-size:4pt;
		padding: 2px 2px;
		vertical-align: middle;
	}

	.table_small td {
		border: 1px solid #000;
		font-size: 5.5pt;
		padding: 4px 4px;
		vertical-align: middle;
	}

	.btm0 {
		margin: 0px auto;
	}

	.font6 {
		font-size: 6pt;
	}

	.font7 {
		font-size: 7pt;
	}

	.font8 {
		font-size: 8px;
	}

	.font9 {
		font-size: 9px;
	}

	.font5 {
		font-size: 5pt;
	}

	.font11 {
		font-size: 11pt;
	}

	.padding{
		padding:12px 8px 7px 8px;
	}
	.potong{
		margin-top: 15px;
	}
</style>
<body>
<?php
	$transactions=App\Models\Order::join('transactions','orders.transaction_id','transactions.id')
                ->leftJoin('event_translations','transactions.event_id','event_translations.event_id')
                ->leftJoin('users','transactions.customer_id','users.id')
                ->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
                ->leftJoin('schedules','event_tickets.schedule_id','schedules.id')
                ->leftJoin('venues','schedules.scheduleable_id','venues.id')
                ->leftJoin('venue_informations','venues.id','venue_informations.id')
                ->leftJoin('cities','venue_informations.city_id','cities.id')
                ->where('event_translations.locale','id')
                ->where('orders.transaction_id',$values->transaction_id)
                ->where('event_tickets.schedule_id',$values->schedule_id)
                ->where('schedules.scheduleable_type','like','%Venue')
                ->select(
                    'event_translations.title AS eventName',
                    'orders.visit_date AS visitDate',
                    'orders.visit_time AS visitTime',
                    'venues.title AS venueName',
                    'cities.name AS city',
                    'users.name AS fullName',
                    'transactions.order_no AS orderNo',
                    'orders.order_value AS orderValue',
                    'event_tickets.name AS ticket_name',
                    'orders.seat_row AS seatRow',
                    'orders.seat_number AS seatNumber',
                    'orders.barcode AS barcode',
                    'event_translations.event_id AS eventId',
                    'schedules.id AS schedule_id'
                )->get();
	$event=App\Models\Event::find($values->event_id);
	$image1= getThumbnail($event, 'events', 'medium');
	foreach ($transactions as $key => $value) {
	$schedule =App\Models\Schedule::find($value->schedule_id);
	$image2= getThumbnail($schedule, 'tickets', 'medium');
?>
<div>
	<div style="float:left; width:27%; border-top:#bdc3c7 1px solid; border-left:#bdc3c7 1px solid; border-right:#bdc3c7 1px solid; height: 330px;">
		<div>
			<img src="{{env('ASSETS_URL').'eticket2/barkuning.jpg'}}" style="width:100%;">
		</div>
		<div style="border-top:#bdc3c7 1px solid; border-bottom:#bdc3c7 1px solid; text-align: center; height: 112px;">
			<p style="line-height: 95%" class="font11"><strong>{{$value->eventName}}</strong></p>
			<p class="font7">
				<strong>{{date('l, d F Y', strtotime($value->visitDate))}} {{date('h:i', strtotime($value->visitTime))}}</strong><br>
					{{$value->venueName}},<br>
					{{$value->city}}
			</p>
		</div>
		<div class="padding" style="border-bottom: #bdc3c7 1px solid; text-align: center;">
			<table class="table_small btm0" style="width:180px;">
				<tr>
					<th style="background-color: #bdc3c7;" width="100%">CUSTOMER NAME</th>
				</tr>
				<tr>
					<td>{{$value->fullName}}</td>
				</tr>
				<tr>
					<th style="background-color: #bdc3c7;" width="100%">TRANSACTION ID</th>
				</tr>
				<tr>
					<td>{{$value->orderNo}}</td>
				</tr>
				<tr>
					<th style="background-color: #bdc3c7;" width="100%">TICKET PRICE</th>
				</tr>
				<tr>
					<td>Rp.{{number_format($value->orderValue, 0,',','.')}}</td>
				</tr>
			</table>
			<br>
			<table class="table_small btm0" style="width:180px;">
				<tr>
					<th style="background-color: #bdc3c7;" width="50%">SECTION</th>
					<th style="background-color: #bdc3c7;" width="50%">GATE</th>
				</tr>
				<tr>
					<td>{{$value->ticket_name}}</td>
					<td>-</td>
				</tr>
				<tr>
					<th style="background-color: #bdc3c7;" width="50%">ROW</th>
					<th style="background-color: #bdc3c7;" width="50%">SEAT</th>
				</tr>
				<tr>
					<td>{{(!empty($value->seatRow))?$value->seatRow:'-'}}</td>
					<td>{{(!empty($value->seatNumber))?$value->seatNumber:'-'}}</td>
				</tr>
			</table>
		</div>
	</div>
	<div style="float:left; width:44.45%; border-top: #bdc3c7 1px solid;border-bottom: #bdc3c7 1px solid; ">
		<img src="{{$image1}}" style="width: 100%">
	</div>
	<div style="float:left; width:27%;  border-left:1px solid #bdc3c7;">
		<div style="position:relative; margin-top:-30%;padding: 1px; margin-left: 50%; text-align: right; margin-right: -13%;">
				<div style="position: absolute; margin-top: -20px; margin-right: -5px;">
					<img src="{{env('ASSETS_URL').'eticket2/cropatas.png'}}">
				</div>
				<div style="position: absolute;">
					<p class="font5" style="margin-top: -110px; margin-bottom: 0px; line-height: 95%;">
						<strong>
							{{$value->orderNo}}<br>
							Rp.{{number_format($value->orderValue, 0,',','.')}}<br>
							{{$value->eventName}}<br>
							{{$value->startDate}}<br>
							{{$value->city}}
						</strong>
					</p>
				</div>
		</div>
		<div style="border-bottom:#bdc3c7 1px solid; margin-top:-47px; padding-top: 25px; padding-left: 25px">
			<p class="font7" style="margin-left:7px; margin-top:10px; margin-bottom: 0px;"><strong>{{$value->barcode}}</strong></p>
			<img src="{{env('QRCODE_URL').$value->eventId.'/'.$value->barcode.'.png'}}" style="width:50%;">
			<p class="font7" style="margin-left:7px; margin-top: 0px; margin-bottom: 5px;"><strong>{{$value->barcode}}</strong></p>
		</div>
		<div>
			<img src="{{env('ASSETS_URL').'eticket2/barmerah.jpg'}}" style="width:100%;">
		</div>
		<div class="padding" style="border-top:1px solid #bdc3c7; border-right: 1px solid #bdc3c7;border-bottom:1px solid #bdc3c7; height: 160px">
			<p class='font7'>Lembar Ini Merupakan <strong>E-Ticket / Tiket Elektronik</strong> Anda</p>
			<p class="font7">Lembar ini <strong> wajib dicetak</strong> di kertas berukuran A4 dan <strong>wajib dibawa</strong> dan <strong>wajib diperlihatkan</strong> saat akan memasuki tempat acara
			</p>
		</div>
	</div>
	<br style="clear:both;" />
</div>

<div style="margin-top: -15px; margin-bottom: 10px">
	<img src="{{env('ASSETS_URL').'eticket2/lipat1.jpg'}}">
</div>

<div>
	<div style="float:left; width:27%; border:1px solid #bdc3c7; height: 329.5px;">
		<div style="border-bottom:1px solid #bdc3c7;">
			<img src="{{env('ASSETS_URL').'eticket2/barmerah.jpg'}}" style="width:100%;">
		</div>
		<div class="padding" style="text-align: left;">
			<p class='font7'>Lembar Ini Merupakan <strong>E-Ticket / Tiket Elektronik</strong> Anda</p>
			<p class="font7">Lembar ini <strong> wajib dicetak</strong> di kertas berukuran A4 dan <strong>wajib dibawa</strong> dan <strong>wajib diperlihatkan</strong> saat akan memasuki tempat acara
			</p>
		</div>
		<div style="border-top:1px solid #bdc3c7; text-align: center; padding-top: 8px; padding-bottom: 5px">
			<p class="font7" style="margin-top:5px; margin-bottom: 0px;"><strong>{{$value->barcode}}</strong></p>
			<img src="{{env('QRCODE_URL').$value->eventId.'/'.$value->barcode.'.png'}}" style="width:60%;">
			<p class="font7" style="margin-top: 0px; margin-bottom: 5px;"><strong>{{$value->barcode}}</strong></p>
		</div>
	</div>
	<div style="float:left; width:44.45%; border-top:1px solid #bdc3c7; border-bottom:1px solid #bdc3c7; ">
		<img src="{{$image2}}" style="width: 100%">
	</div>
	<div style="float:left; width:27%; border-top:1px solid #bdc3c7; border-left:1px solid #bdc3c7; border-right: 1px solid #bdc3c7;">
		<div style="border-bottom:1px solid #bdc3c7; text-align: center; padding: 27px 0px;height:280px;">
			<p style="line-height: 95%" class="font11"><strong>{{$value->eventName}}</p>
			<p class="font7">
				<strong>{{date('l, d F Y', strtotime($value->visitDate))}} {{date('h:i', strtotime($value->visitTime))}}</strong><br>
					{{$value->venueName}},<br>
					{{$value->city}}
			</p>

			<table class="table_small btm0" style="width:180px;">
				<tr>
					<th style="background-color: #bdc3c7;" width="100%">CUSTOMER NAME</th>
				</tr>
				<tr>
					<td>{{$value->fullName}}</td>
				</tr>
				<tr>
					<th style="background-color: #bdc3c7;" width="100%">TRANSACTION ID</th>
				</tr>
				<tr>
					<td>{{$value->orderNo}}</td>
				</tr>
				<tr>
					<th style="background-color: #bdc3c7;" width="100%">TICKET PRICE</th>
				</tr>
				<tr>
					<td>Rp.{{number_format($value->orderValue, 0,',','.')}}</td>
				</tr>
			</table>
			<br>
			<table class="table_small btm0" style="width:180px;">
				<tr>
					<th style="background-color: #bdc3c7;" width="50%">SECTION</th>
					<th style="background-color: #bdc3c7;" width="50%">GATE</th>
				</tr>
				<tr>
					<td>{{$value->ticket_name}}</td>
					<td>-</td>
				</tr>
				<tr>
					<th style="background-color: #bdc3c7;" width="50%">ROW</th>
					<th style="background-color: #bdc3c7;" width="50%">SEAT</th>
				</tr>
				<tr>
					<td>{{(!empty($value->seatRow))?$value->seatRow:'-'}}</td>
					<td>{{(!empty($value->seatNumber))?$value->seatNumber:'-'}}</td>
				</tr>
			</table>
		</div>
	</div>
	<br style="clear:both;" />
</div><br>
<div style="margin-top: -30px; margin-bottom: 5px">
	<img src="{{env('ASSETS_URL').'eticket2/lipat2.jpg'}}">
</div>

<div style="margin-right:5px;">
	<div style="float:left; width:99.45%;  border-top:#bdc3c7 1px  solid; border-left:1px solid #bdc3c7; border-right: 1px solid #bdc3c7;">
		<div style="border-bottom:1px solid #bdc3c7;">
			<img src="{{env('ASSETS_URL').'eticket2/syarat.jpg'}}" style="width:100%;">
		</div>
	</div>
	<br style="clear:both;" />
</div>

<div style="margin-right:5px; padding-top: 10px;">
	<div style="float:left; width:30% ;">
		<div>
			<img src="{{env('ASSETS_URL').'eticket2/alamat.jpg'}}" style="width:100%;">
		</div>
	</div>
	<div style="float:right; width:15%; margin-right:5px;">
		<div>
			<img src="{{env('ASSETS_URL').'eticket2/logo.jpg'}}" style="width:100%;">
		</div>
	</div>
	<br style="clear:both;" />
</div>
<?php
}
?>
</body>