<div class="widget-head">
  @if ($paymentResponse->responseCode == '00')
  <h3>Pesanan Tiket <br><span class="orange">{{$transaction->event->getTranslation(lang())->title ?? ''}}<br></span> Kamu Telah Berhasil Diproses </h3>
  @else
  <h3>Pesanan Tiket <br><span class="orange">{{$transaction->event->getTranslation(lang())->title ?? ''}}<br></span> Kamu Gagal Diproses   </h3>
  @endif
</div>{{-- .widget-head --}}
<div class="event-image thumb">
  <img src="{{getThumbnail($transaction->event, 'events', 'medium')}}">
</div>{{-- .event-image --}}
@if($paymentResponse->responseCode == '00')
  @if (thisAttraction($event))
  <div class="comment-box">
   @if (Auth::check())
      <div class="box-author flex-box">
        <a class="thumb thumb-author is-circle" href="{{ url('/') }}/user/{{Auth::user()->slug}}">
          @if (count(Auth::user()->getMedia('avatar')))
            <img src="{{ url('/') }}/media/{{Auth::user()->getMedia('avatar')->first()->id}}/conversions/square.jpg" class="is-circle">
          @else
             <img src="{{ url('/') }}/images/default_square.jpg" class="is-circle">
          @endif
        </a>
        <div class="meta-title">
          <h3 class="s-title">
            <a href="{{ url('/') }}/user/{{Auth::user()->slug}}">{{Auth::user()->name}}</a>
          </h3>
        </div>
        <div class="event-rate flex-center">
            <div id="rate-{{$event->id}}" event-id="{{$event->id}}" class="rating-star" average="{{$event->rate_average}}"></div>
        </div>
      </div>{{-- .box-meta --}}
    @endif
    <div class="comment-form">
         <textarea placeholder="Tulis Komentar" id="commentMessage-0"></textarea>
         @if (Auth::check())
           <a href="#" class="button addComment" post-id="{{$event->id}}" data-token="{{csrf_token()}}" comment-id="0">Simpan</a>
         @else
           <a href="#popup-login" class="button showPopup">Comment</a>
         @endif
    </div>{{-- .comment-form--}}
  </div>{{-- .comment-box --}}
  @endif
<div class="widget-body">
    <div class="row">
      <div class="col-md-6">
        <div class="greybox">
          <span>Total Pembayaran: </span>
           <h3 style="margin:0;"><strong>Rp {{number_format($transaction->value_grand_total)}}</strong></h3>
        </div>
      </div>
      <div class="col-md-6">
        <div class="greybox">
          <span>Metode Pembayaran: </span>
           <h3 style="margin:0;"><strong>{{$transaction->payment->name}}</strong></h3>
        </div>
      </div>
    </div>
        <div class="row">
          <div class="col-md-12">
            <div class="greybox">
              <span>Order ID: </span>
               <h3 style="margin:0;"><strong>{{$transaction->order_no}}</strong></h3>
            </div>
          </div>
        </div>
      <br>
        @if($transaction->event->type == '["E-Voucher"]')
        <h3>e-voucher akan dikirim ke email <br>{{$transaction->customer->email ?? ''}}</h3>
        @else
        <h3>e-ticket akan dikirim ke email <br>{{$transaction->customer->email ?? ''}}</h3>
        @endif
      <br>
      @if (empty($event->template) || is_null($event->template))
        <div class="share-box">
          <h4>Bagikan event ini dan ajak temanmu</h4>
           @include('app.widget.sharebox')
        </div>
      @endif
  </div>{{-- .event-detail --}}
  <div class="howTo-pay">
  @if ($transaction->payment->type == 'bank_transfer' || $transaction->payment->type == 'cstore')
    {!!$transaction->payment->description ?? '' !!}
  @endif
  </div>
@endif