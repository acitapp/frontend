<style type="text/css">
	@page {margin: 20px 15px 5px 15px; }
	/*@import url('https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');*/


	body{
		font-family: 'lato', sans-serif;
	}

	strong{
		font-weight: bold;
	}
	.light{
		font-weight: 100;
	}
	table td {
		border: 1px solid #000;
		padding: 0px 0px;
		vertical-align: middle;
	}

	table .text-left {
		text-align: left !important;
	}

	table .pd5 {
		padding: 5px 5px;
	}

	table .pd10 {
		padding: 9px 9px;
	}

	table.table_small {
		text-align: left;
		border-collapse:collapse;
	}

	.table_small th {
		font-size:4pt;
		vertical-align: middle;
	}

	.table_small td {
		padding: 7px;
		border:0px;
		font-size: 7pt;
		vertical-align: middle;
	}

	.btm0 {
		margin: 0px auto;
	}

	.font6 {
		font-size: 6pt;
	}

	.font7 {
		font-size: 7pt;
	}

	.font8 {
		font-size: 8pt;
	}

	.font9 {
		font-size: 9pt;
	}

	.font10 {
		font-size: 10pt;
	}

	.font11 {
		font-size: 11pt;
	}

	.font12 {
		font-size: 12pt;
	}

	.padding{
		padding:8px;
	}
	.potong{
		margin-top: 10px;
	}
</style>
<body>
<?php
	$transactions=App\Models\Order::join('transactions','orders.transaction_id','transactions.id')
                ->leftJoin('event_translations','transactions.event_id','event_translations.event_id')
                ->leftJoin('users','transactions.customer_id','users.id')
                ->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
                ->leftJoin('event_sections','event_tickets.event_section_id','event_sections.id')
                ->leftJoin('schedules','event_tickets.schedule_id','schedules.id')
                ->leftJoin('venues','schedules.scheduleable_id','venues.id')
                ->leftJoin('venue_informations','venues.id','venue_informations.id')
                ->leftJoin('cities','venue_informations.city_id','cities.id')
                ->where('event_translations.locale','id')
                ->where('orders.transaction_id',$values->transaction_id)
                ->where('event_tickets.schedule_id',$values->schedule_id)
                ->where('schedules.scheduleable_type','like','%Venue')
                ->select(
                    'event_translations.title AS eventName',
                    'schedules.started_at AS startDate',
                    'orders.visit_time AS visitTime',
                    'venues.title AS venueName',
                    'cities.name AS city',
                    'users.name AS fullName',
                    'users.customer_id AS customerId',
                    'transactions.order_no AS orderNo',
                    'orders.order_value AS orderValue',
                    'event_tickets.name AS ticket_name',
                    'orders.seat_row AS seatRow',
                    'orders.seat_number AS seatNumber',
                    'orders.barcode AS barcode',
                    'event_translations.event_id AS eventId',
                    'schedules.id AS schedule_id',
                    'schedules.ended_at AS endDate',
                    'transactions.quantity AS quantity',
                    'transactions.value_sub_total AS subTotal',
                    'transactions.value_grand_total AS grandTotal',
                    'transactions.fee_convenience_total as feeTotal'
                )
                ->groupBy('orders.event_ticket_id')
                ->get();
    //dd($transactions);
	$media_url = env('MEDIA_URL');
	$image1=App\Models\Event::find($values->event_id)->getMedia('events')->first();
	$image1=(!empty($image1))? $media_url.'/media/'.$image1->id.'/'.$image1->file_name : env('ASSETS_URL').'eticket2/default_small.jpg' ;
	foreach ($transactions as $key => $value) {

	$image2=App\Models\Schedule::find($value->schedule_id)->getMedia('tickets')->where('name','<>','image_term')->first();
	$image2=(!empty($image2))? $media_url.'/media/'.$image2->id.'/'.$image2->file_name :'';

	$image3=App\Models\Schedule::find($value->schedule_id)->getMedia('tickets')->where('name','image_term')->first();
	$image3=(!empty($image3))? $media_url.'/media/'.$image3->id.'/'.$image3->file_name : env('ASSETS_URL').'eticket2/default_small.jpg';
?>
<div style="width: 100%;background-color: #ef4665;color: #fff;padding: 5px;">
	<div style="float: left;width: 50%">
		<p class="font8" style="margin:5px 0px 5px 10px;"><strong>Perhatian! E-Voucher ini tidak berlaku sebagai tiket & wajib ditukarkan!</strong></p>
	</div>
	<div style="float: right;width: 50%">
		<p class="font8" style="margin:5px 10px 5px 0px;text-align: right;"><strong>Important Notice! This E-Voucher must be exchanged with a valid ticket!</strong></p>
	</div>
</div>
<br>

<div class="potong">
	<div style="float:left; width:31%;">
		<img src="{{$image1}}" style="width: 100%">
	</div>
	<div style="float:left; width:42%;">
		<div style="text-align: left; padding: 0px 5px 5px 15px;">
			<p style="line-height: 95%;margin-top: 0px;" class="font11"><strong>{{$value->eventName}}</strong></p>
			<p class="font7">
				@if(date('d',strtotime($value->startDate))!=date('d',strtotime($value->endDate)) && !empty($value->endDate))
				<strong>
					{{date('d', strtotime($value->startDate)).'-'.date('d F Y', strtotime($value->endDate))}} {{date('H:i', strtotime($value->visitTime))}}
				</strong>
				@else
				<strong>{{date('l, d F Y', strtotime($value->startDate))}} {{date('H:i', strtotime($value->visitTime))}}</strong>
				@endif
				<br>
					{{$value->venueName}},<br>
					{{$value->city}}
			</p>
			<p class="font10">
				<strong>{{$value->fullName}}</strong><br>
				<strong>Transaction ID : {{$value->orderNo}}</strong><br>
				<strong>Customer ID : {{$value->customerId}}</strong><br><br>
			</p>
			 <p class="font6" style="text-align: justify;"><strong>Lembar ini merupakan E-voucher / Voucher Elektronik Anda. Lembar Ini wajib dicetak di kertas berukuran A4 dan wajib dibawa pada hari penukaran tiket dan wajib ditukarkan dengan tiket asli / gelang tanda masuk untuk memasuki tempat acara.</strong><br><br>
			 </p>
			 <p class="font6 light">
			 This is your E-Voucher / Electronic Voucher. This E-Voucher must be printed on A4 paper and is mandatory to be exchanged with a valid ticket / wristband to enter the venue.
			</p>
		</div>
	</div>
	<div style="float:left; width:23%; text-align: center">
		<div>
			<p style="margin-bottom: 0px;"><strong>{{$value->orderNo}}</strong></p>
			<img src="{{env('QRCODE_URL').$value->eventId.'/'.$value->orderNo.'.png'}}" style="width:80%;">
			<p style="margin-top: 0px;"><strong>{{$value->orderNo}}</strong></p>
		</div>
	</div>
	<br style="clear:both;" />
</div>

<div class="potong">
	<div style="float:left; width:25%; border:1px solid #bdc3c7; height: 342px">
		<div style="border-bottom:1px solid #bdc3c7;">
			<img src="{{env('ASSETS_URL').'eticket2/barpink.jpg'}}" style="width:100%;">
		</div>
		<div class="padding" style="text-align: justify;">
			<p class="font6" style="text-align: justify;"><strong>Lembar ini merupakan E-voucher / Voucher Elektronik Anda. Lembar Ini wajib dicetak di kertas berukuran A4 dan wajib dibawa pada hari penukaran tiket dan wajib ditukarkan dengan tiket asli / gelang tanda masuk untuk memasuki tempat acara.</strong><br><br>
			 </p>
			 <p class="font6 light">
			 This is your E-Voucher / Electronic Voucher. This E-Voucher must be printed on A4 paper and is mandatory to be exchanged with a valid ticket / wristband to enter the venue.
			</p>
		</div>
		<div style="border-top:1px solid #bdc3c7; text-align: center; padding:10px;">
			<p class="font7" style="margin-top:5px; margin-bottom: 0px;"><strong>{{$value->orderNo}}</strong></p>
			<img src="{{env('QRCODE_URL').$value->eventId.'/'.$value->orderNo.'.png'}}" style="width:50%;">
			<p class="font7" style="margin-top: 0px; margin-bottom: 5px;"><strong>{{$value->orderNo}}</strong></p>
		</div>
	</div>
	<div style="float:left; width:44.45%; border-top:1px solid #bdc3c7; border-bottom:1px solid #bdc3c7; height: 326.5px">
		<img src="{{$image2}}" style="width: 100%">
	</div>
	<div style="float:left; width:29%; border-top:1px solid #bdc3c7; border-left:1px solid #bdc3c7; border-right: 1px solid #bdc3c7;">
		<div style="border-bottom:1px solid #bdc3c7; text-align: center; padding: 5px 5px 9px 5px;height: 326.5px;">
			<p style="line-height: 95%" class="font12"><strong>{{$value->eventName}}<br>

			<table class="table_small btm0" style="width:180px;">
				<tr bgcolor="#ebebeb">
					<td width="50%"><strong>ID Transaksi</strong><br>Transaction ID</td>
					<td width="50%">{{$value->orderNo}}</td>
				</tr>
				<tr>
					<td width="50%"><strong>Tanggal</strong><br>Date</td>
					<td width="50%">
						@if(date('d',strtotime($value->startDate))!=date('d',strtotime($value->endDate)) && !empty($value->endDate))
						{{date('d', strtotime($value->startDate)).'-'.date('d F Y', strtotime($value->endDate))}} {{date('H:i', strtotime($value->visitTime))}}
						@else
						{{date('l, d F Y', strtotime($value->startDate))}} {{date('H:i', strtotime($value->visitTime))}}
						@endif
					</td>
				</tr>
				<tr bgcolor="#ebebeb">
					<td width="50%"><strong>Tempat</strong><br>Venue</td>
					<td width="50%">{{$value->venueName}}</td>
				</tr>
				<tr>
					<td width="50%"><strong>Bagian</strong><br>Section</td>
					<td width="50%">{{$value->ticket_name}}</td>
				</tr>
				<tr bgcolor="#ebebeb">
					<td width="50%"><strong>Qty</strong></td>
					<td width="50%" style="text-align: right;">{{$value->quantity}}</td>
				</tr>
				<tr bgcolor="#ebebeb">
					<td width="50%"><strong>Order Total</strong></td>
					<td width="50%" style="text-align: right;">Rp. {{number_format($value->subTotal, 0,'.',',')}}</td>
				</tr>
			</table>
		</div>
	</div>
	<br style="clear:both;" />
</div>

<div style="margin-right:5px;">
	<div style="float:left; width:99.45%;  border-top:#bdc3c7 1px  solid; border-left:1px solid #bdc3c7; border-right: 1px solid #bdc3c7;">
		<div style="border-bottom:1px solid #bdc3c7;">
			<img src="{{$image3}}" style="width:100%;">
		</div>
	</div>
	<br style="clear:both;" />
</div>

<div class="potong" style="margin-right:5px;">
	<div style="float:left; width:25% ;">
		<div>
			<img src="{{env('ASSETS_URL').'eticket2/speciallogo.png'}}" style="width:100%;">
		</div>
	</div>
	<div style="float:right; width:40%; margin-right:5px;">
		<div>
			<img src="{{env('ASSETS_URL').'eticket2/specialaddress.png'}}" style="width:100%;">
		</div>
	</div>
	<br style="clear:both;" />
</div>
<?php } ?>
</body>
