@component('layouts.app')
  @slot('webtitle')
  {{$event->getTranslation(lang())->title}}
  @endslot
  @slot('keyword')
   {{$event->keyword}}
  @endslot
  @slot('description')
  {{strip_tags($event->getTranslation(lang())->summary)}}
  @endslot
  @slot('shareimage')
  {{getThumbnail($event, 'events', 'medium')}}
  @endslot
  <section id="section-details" class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="event-meta center">
          </div>{{-- .event-meta --}}
        </div>
        <div class="col-md-12"> 
            <div class="order-detail widget-options center">
              <div class="widget-head">
                <h1>Maaf,</h1>
                <h3>Pesanan anda sudah melewati batas waktu pembayaran</h3><br><br>
                <a href="{{url('/')}}/{{lang()}}/event/{{$event->id}}/{{$event->slug}}" class="button">Pesan Ulang</a>
              </div>{{-- .widget-head --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
@endcomponent