<style type="text/css">
	@page {margin: 20px 20px 50px 25px; }
	/*@import url('https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');*/


	body{
		font-family: 'montserrat', sans-serif;
	}

	strong{
		font-weight: bold;
	}
	.light{
		font-weight: 300;
	}
	table td {
		border: 1px solid #000;
		padding: 0px 0px;
		vertical-align: middle;
	}

	table .text-left {
		text-align: left !important;
	}

	table .pd5 {
		padding: 5px 5px;
	}

	table .pd10 {
		padding: 9px 9px;
	}

	table.table_small {
		text-align: center;
		font-weight: bold;
		border-collapse: collapse;
	}

	.table_small th {
		border: 1px solid #000;
		font-size:4pt;
		padding: 2px 2px;
		vertical-align: middle;
	}

	.table_small td {
		border: 1px solid #000;
		font-size: 5.5pt;
		padding: 8px 8px;
		vertical-align: middle;
	}

	.btm0 {
		margin: 0px auto;
	}

	.font7 {
		font-size: 7pt;
	}

	.font8 {
		font-size: 8px;
	}

	.font9 {
		font-size: 9px;
	}

	.font10 {
		font-size: 10pt;
	}

	.font11 {
		font-size: 11pt;
	}

	.padding{
		padding:8px;
	}
	.potong{
		margin-top: 10px;
	}
</style>
<body>
<?php
	$transactions=App\Models\Order::join('transactions','orders.transaction_id','transactions.id')
                ->join('event_translations','transactions.event_id','event_translations.event_id')
                ->join('users','transactions.customer_id','users.id')
                ->leftJoin('event_tickets','orders.event_ticket_id','event_tickets.id')
                ->leftJoin('schedules','event_tickets.schedule_id','schedules.id')
                ->leftJoin('venues','schedules.scheduleable_id','venues.id')
                ->leftJoin('venue_informations','venues.id','venue_informations.id')
                ->leftJoin('cities','venue_informations.city_id','cities.id')
                ->where('event_translations.locale','id')
                ->where('orders.transaction_id',$values->transaction_id)
                ->where('event_tickets.schedule_id',$values->schedule_id)
                ->where('schedules.scheduleable_type','like','%Venue')
                ->select(
                    'event_translations.title AS eventName',
                    'orders.visit_date AS visitDate',
                    'orders.visit_time AS visitTime',
                    'venues.title AS venueName',
                    'cities.name AS city',
                    'users.name AS fullName',
                    'users.customer_id AS customer_id',
                    'transactions.order_no AS orderNo',
                    'orders.order_value AS orderValue',
                    'event_tickets.name AS ticket_name',
                    'orders.seat_row AS seatRow',
                    'orders.seat_number AS seatNumber',
                    'orders.barcode AS barcode',
                    'event_translations.event_id AS eventId',
                    'schedules.id AS schedule_id'
                )->get();
    //dd($transactions);
	$event=App\Models\Event::find($values->event_id);
	$image1= getThumbnail($event, 'events', 'medium');
	foreach ($transactions as $key => $value) {
	$schedule =App\Models\Schedule::find($value->schedule_id);
	$image2= getThumbnail($schedule, 'tickets', 'medium');
?>
<div style="width: 100%;background-color: #fbaf41; text-align: center;color: #fff;padding-top: 3px">
	<p class="font10" style="margin-top: 5px;  margin-bottom: 7px"><strong>Perhatian! E-Voucher ini tidak berlaku sebagai tiket & wajib ditukarkan!</strong></p>
</div>
<br>

<div class="potong">
	<div style="float:left; width:31%;">
		<img src="{{$image1}}" style="width: 100%">
	</div>
	<div style="float:left; width:44.45%;">
		<div style="text-align: left; padding: 0px 5px 5px 15px;">
			<p style="line-height: 95%;margin-top: 0px;" class="font11"><strong>{{$value->eventName}}</strong></p>
			<p class="font7">
				<strong>{{date('l, d F Y', strtotime($value->visitDate))}} {{date('h:i', strtotime($value->visitTime))}}</strong><br>
					{{$value->venueName}},<br>
					{{$value->city}}
			</p>
			<p class="font7">
				<strong class="font11">{{$value->fullName}}</strong><br>
				<strong class="font11">{{$value->customer_id}}</strong><br>
				<br>
				Lembar ini merupakan <strong>E-voucher / Voucher Elektronik</strong> Anda.
			</p>
			<p class="font7">Lembar Ini <strong>wajib dicetak</strong> di kertas berukuran A4 dan <strong>wajib dibawa</strong> pada hari penukaran tiket dan <strong>wajib ditukarkan dengan tiket asli/gelang tanda masuk</strong> untuk memasuki tempat acara.
			</p>
		</div>
	</div>
	<div style="float:left; width:23%; text-align: center">
		<div>
			<p style="margin-bottom: 0px;"><strong>{{$value->barcode}}</strong></p>
			<img src="{{env('QRCODE_URL').$value->eventId.'/'.$value->barcode.'.png'}}" style="width:80%;">
			<p style="margin-top: 0px;"><strong>{{$value->barcode}}</strong></p>
		</div>
	</div>
	<br style="clear:both;" />
</div>

<div class="potong">
	<div style="float:left; width:27%; border:1px solid #bdc3c7; height: 329.5px">
		<div style="border-bottom:1px solid #bdc3c7;">
			<img src="{{env('ASSETS_URL').'eticket2/barmerah.jpg'}}" style="width:100%;">
		</div>
		<div class="padding" style="text-align: justify;">
			<p class='font7'>Lembar Ini Merupakan <strong>E-Voucher / Voucher Elektronik</strong> Anda</p>
			<p class="font7">Lembar ini <strong> wajib dicetak</strong> di kertas berukuran A4 dan <strong>wajib dibawa</strong> pada hari penukaran tiket dan <strong>wajib ditukarkan dengan ticket asli / gelang tanda masuk</strong> untuk memasuki tempat acara
			</p>
		</div>
		<div style="border-top:1px solid #bdc3c7; text-align: center; padding:10px;">
			<p class="font7" style="margin-top:5px; margin-bottom: 0px;"><strong>{{$value->barcode}}</strong></p>
			<img src="{{env('QRCODE_URL').$value->eventId.'/'.$value->barcode.'.png'}}" style="width:80%;">
			<p class="font7" style="margin-top: 0px; margin-bottom: 5px;"><strong>{{$value->barcode}}</strong></p>
		</div>
	</div>
	<div style="float:left; width:44.45%; border-top:1px solid #bdc3c7; border-bottom:1px solid #bdc3c7; height: 320px	">
		<img src="{{$image2}}" style="width: 100%">
	</div>
	<div style="float:left; width:27%; border-top:1px solid #bdc3c7; border-left:1px solid #bdc3c7; border-right: 1px solid #bdc3c7;">
		<div style="border-bottom:1px solid #bdc3c7; text-align: center; padding: 5px 0px 9px 0px;height: 320px;">
			<p style="line-height: 95%" class="font11"><strong>{{$value->eventName}}<br>
			<!-- NAMA ACARA BARIS 2</strong></p> -->
			<p class="font7">
				<strong>{{date('l, d F Y', strtotime($value->visitDate))}} {{date('h:i', strtotime($value->visitTime))}}</strong><br>
					{{$value->venueName}},<br>
					{{$value->city}}
			</p>

			<table class="table_small btm0" style="width:180px;">
				<tr>
					<th style="background-color: #bdc3c7;" width="100%">CUSTOMER NAME</th>
				</tr>
				<tr>
					<td>{{$value->fullName}}</td>
				</tr>
				<tr>
					<th style="background-color: #bdc3c7;" width="100%">TRANSACTION ID</th>
				</tr>
				<tr>
					<td>{{$value->orderNo}}</td>
				</tr>
				<tr>
					<th style="background-color: #bdc3c7;" width="100%">TICKET PRICE</th>
				</tr>
				<tr>
					<td>Rp.{{number_format($value->orderValue, 0,',','.')}}</td>
				</tr>
			</table>
			<br>
			<table class="table_small btm0" style="width:180px;">
				<tr>
					<th style="background-color: #bdc3c7;" width="50%">SECTION</th>
					<th style="background-color: #bdc3c7;" width="50%">GATE</th>
				</tr>
				<tr>
					<td>{{$value->ticket_name}}</td>
					<td>-</td>
				</tr>
				<tr>
					<th style="background-color: #bdc3c7;" width="50%">ROW</th>
					<th style="background-color: #bdc3c7;" width="50%">SEAT</th>
				</tr>
				<tr>
					<td>{{(!empty($value->seatRow))?$value->seatRow:'-'}}</td>
					<td>{{(!empty($value->seatNumber))?$value->seatNumber:'-'}}</td>
				</tr>
			</table>
		</div>
	</div>
	<br style="clear:both;" />
</div>

<div class="potong" style="margin-right:5px;">
	<div style="float:left; width:99.45%;  border-top:#bdc3c7 1px  solid; border-left:1px solid #bdc3c7; border-right: 1px solid #bdc3c7;">
		<div style="border-bottom:1px solid #bdc3c7;">
			<img src="{{env('ASSETS_URL').'eticket2/syarat.jpg'}}" style="width:100%;">
		</div>
	</div>
	<br style="clear:both;" />
</div>

<div class="potong" style="margin-right:5px;">
	<div style="float:left; width:30% ;">
		<div>
			<img src="{{env('ASSETS_URL').'eticket2/alamat.jpg'}}" style="width:100%;">
		</div>
	</div>
	<div style="float:right; width:15%; margin-right:5px;">
		<div>
			<img src="{{env('ASSETS_URL').'eticket2/logo.jpg'}}" style="width:100%;">
		</div>
	</div>
	<br style="clear:both;" />
</div>
<?php } ?>
</body>
