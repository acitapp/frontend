@component('layouts.app')
 @include('app.partial.search')
 <div id="homepage" class="section section-mobile">
  <section id="section-featuredmobile" class="sections mobile">
    <div class="container">
      <div class="titleBox flex flex-center">
        <h2 class="title">@lang('content.recommended')</h2>
        <a href="{{url('/')}}/{{lang()}}/recommended" class="btn-line seeAll">@lang('content.see_all') <i class="ion-chevron-right"></i></a>
      </div>
      <div class="rows">
        <div class="homeContent">
            <div class="mobileCarousel">
              @foreach (getFeaturedEvent(8) as $event)
                    <div class="items">
                            <div class="event-box box">
                              <div class="event-image thumb">
                                <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}">
                                   <img src="{{getThumbnail($event, 'events', 'small')}}">
                                </a>
                              </div>{{-- .event-image --}}
                              <div class="event-entry">
                                 <h3>{{$event->getTranslation(lang())->title}}</h3>
                              </div>
                            </div>{{-- .box --}}
                    </div>
              @endforeach
            </div>{{-- .carousel --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
  <section id="section-newest" class="sections">
    <div class="container">
      <div class="titleBox flex flex-center">
        <h2 class="title">@lang('content.newest')</h2>
        <a href="{{url('/')}}/id/1/event" class="btn-line seeAll">@lang('content.see_all') <i class="ion-chevron-right"></i></a>
      </div>
      <div class="rows">
        <div class="homeContent">
            <div class="mobileCarousel">
              @foreach (getCategoryLimit(1, 8) as $event)
                <div class="items">
                        <div class="event-box box">
                          <div class="event-image thumb">
                             <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}">
                               <img src="{{getThumbnail($event, 'events', 'small')}}">
                            </a>
                          </div>{{-- .event-image --}}
                          <div class="event-entry">
                              <h3>{{$event->getTranslation(lang())->title}}</h3>
                          </div>
                        </div>{{-- .box --}}
                </div>
              @endforeach
            </div>{{-- .carousel --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
  <section id="section-family" class="sections">
    <div class="container">
      <div class="titleBox flex flex-center">
        <h2 class="title">@lang('content.family_attraction')</h2>
        <a href="{{url('/')}}/id/2/aktivitas-hiburan" class="btn-line seeAll">@lang('content.see_all') <i class="ion-chevron-right"></i></a>
      </div>
      <div class="rows">
        <div class="homeContent">
            <div class="mobileCarousel">
              @foreach (getCategoryLimit(2, 8) as $event)
                  <div class="items">
                      <div class="event-box box">
                        <div class="event-image thumb">
                           <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug}}">
                             <img src="{{getThumbnail($event, 'events', 'small')}}">
                          </a>
                         {{--  <div class="event-rates">
                            <span class="total-rating">Ratings : {{$event->rate_average}}</span>
                            <span class="total-review">Review : {{$event->comment}} </span>
                          </div> --}}
                        </div>{{-- .event-image --}}
                        <div class="event-entry">
                            <div class="event-entry">
                               <h3>{{$event->getTranslation(lang())->title}}</h3>
                                @php
                                 $venue = $event->schedules()->where('status',1001)->with('venue')->first();
                                @endphp
                                <div class="event-venue">
                                  {{$venue->venue->info->city->name ?? ''}},
                                  {{$venue->venue->info->country->name ?? ''}}
                                </div>
                            </div>
                        </div>
                      </div>{{-- .box --}}
                  </div>
              @endforeach
            </div>{{-- .carousel --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
    </div>{{-- .container --}}
  </section>
</div>
@endcomponent