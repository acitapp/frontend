<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>

  <title>{{$webtitle ?? 'Kiostix - Solusi Tepat Tiket Event Anda'}} </title>
  <meta name="description" content="{{ $description ?? getOption('web_description')}}" />
  <meta name="keywords" content="{{ $keyword ?? getOption('web_keyword') }}" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <meta name="csrf_token" content="{{ csrf_token() }}" />
  <meta name="author" content="kiostix.com">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@kiostix">
  <meta name="twitter:title" content="KiosTix">
  <meta name="twitter:description" content="{{$webtitle ?? 'Kiostix - Solusi Tepat Tiket Event Anda'}}">
  <meta name="twitter:creator" content="@kiostix">
  <meta name="twitter:image" content="{{ $shareimage ?? url('/images/share.jpg') }}">
  <meta name="twitter:domain" content="kiostix.com">
  <meta property="og:site_name" content="kiostix.com"/>
  <meta id="shareurl" property="og:url" content="{{ Request::url() }}" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="{{$webtitle ?? 'Kiostix - Solusi Tepat Tiket Event Anda'}}" />
  <meta property="og:description" content="{{ $description ?? getOption('web_description') }}" />
  <meta id="shareimg" property="og:image" content="{{ $shareimage ?? url('/images/share.jpg') }}" />
  <meta property="og:locale" content="en_us" />
  <meta property="og:image:width" content="366">
  <meta property="og:image:height" content="244">
  <meta property="fb:app_id" content="741761969350102"/>
  <link rel="icon" type="image/x-icon" href="{{ url('/') }}/images/favicon.png">
  <link href="{{ mix('/css/kiostix.css') }}" rel="stylesheet">
  <link href="{{ URL::to('css/jquery.fancybox.css') }}" rel="stylesheet"> 
  <script type="text/javascript">
    var base_url = "{{ url('/') }}";
    var img_url = "{{ env('MEDIA_URL') }}";
    var token = "{{csrf_token()}}";
    var lang = '/{{lang()}}/';
    var expTime = '';
    var vCKey =  "{{getOption('midtrans_client_key')}}";
    var vUrl =  "{{getOption('midtrans_url')}}";
    var vSKey =  "{{getOption('midtrans_serverkey')}}";
  </script>
  <script src="{{ mix('/js/vendor.js') }}"></script>
  <script type="text/javascript" src="https://api.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
  <script src="https://api.midtrans.com/v2/assets/js/midtrans.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="{{ URL::to('js/jquery.fancybox.pack.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-32737580-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-32737580-1');
  </script>
</head>
<body>
  <div id="fb-root"></div>
  <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '741761969350102',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.9'
    });
    FB.AppEvents.logPageView();
  };
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s);
 js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
  <div class="animsition flex-container">
    @include('flash::message')
    @include('app.partial.header')
    <main>
      {!! $slot or "<h3 class='center'>NOT FOUND</h3>" !!}
    </main>
    @include('app.partial.footer')
  </div>
  @auth
  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
      {{ csrf_field() }}
  </form>
  @else
  @include('app.partial.popup-login')
  @include('app.partial.popup-register')
  @endauth
  @include('app.partial.popup-message')
  <script src="{{ mix('/js/main.js') }}"></script>
  <script src="{{ url('/js/share.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
  <script src="{{ url('/js/search.js') }}"></script>
</body>
  <script src="{{ url('/js/sort.js') }}"></script>
</html>


