<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>

  <title>{{$webtitle ?? getOption('web_title')}} </title>
  <meta name="description" content="{{ $description ?? getOption('web_description')}}" />
  <meta name="keywords" content="{{ $keyword ?? getOption('web_keyword') }}" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <meta name="csrf_token" content="{{ csrf_token() }}" />
  <meta name="author" content="kiostix.com">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@kiostix">
  <meta name="twitter:title" content="KiosTix">
  <meta name="twitter:description" content="{{$webtitle ?? getOption('web_title')}}">
  <meta name="twitter:creator" content="@kiostix">
  <meta name="twitter:image" content="{{ $shareimage ?? url('/images/share.jpg') }}">
  <meta name="twitter:domain" content="kiostix.com">
  <meta property="og:site_name" content="kiostix.com"/>
  <meta id="shareurl" property="og:url" content="{{ Request::url() }}" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="{{$webtitle ?? getOption('web_title')}}" />
  <meta property="og:description" content="{{ $description ?? getOption('web_description') }}" />
  <meta id="shareimg" property="og:image" content="{{ $shareimage ?? url('/images/share.jpg') }}" />
  <meta property="og:locale" content="en_us" />
  <meta property="og:image:width" content="366">
  <meta property="og:image:height" content="244">
  <meta property="fb:app_id" content="741761969350102"/>
  <link rel="icon" type="image/x-icon" href="{{ url('/') }}/images/favicon.png">
  <link href="{{ mix('/css/kiostix.css') }}" rel="stylesheet">
   <link href="{{ URL::to('css/jquery.fancybox.css') }}" rel="stylesheet"> 
  <script type="text/javascript">
    var base_url = "{{ url('/') }}";
    var token = "{{csrf_token()}}";
    var lang = '/{{lang()}}/';
    var expTime = '';
    var vCKey =  "{{getOption('midtrans_client_key')}}";
    var vUrl =  "{{getOption('midtrans_url')}}";
    var vSKey =  "{{getOption('midtrans_serverkey')}}";
  </script>
  <script src="{{ mix('/js/vendor.js') }}"></script>
  <script type="text/javascript" src="https://api.sandbox.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
  <script src="https://api.midtrans.com/v2/assets/js/midtrans.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="{{ URL::to('js/jquery.fancybox.pack.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-32737580-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-32737580-1');
  </script>
</head>
<body id="{{currentUrl(1)}}">
  <div id="fb-root"></div>
  <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '741761969350102',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.9'
    });
    FB.AppEvents.logPageView();
  };
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s);
 js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
  <div class="animsition flex-container">
    @include('flash::message')
  <header>
    <div id="top">
      <div id="sticky-anchor"></div>
      <div  id="sticky" class="header-box">
        <div class="container">
          <div class="flex">
            <a href="{{url('/')}}" class="logo">
              <img src="{{url('/')}}/images/logo.png" alt="kiosTix">
            </a>
            <div class="socmed">
            <a href="<?=getOption('social_instagram');?>" target="_blank"><i class="icon-brand-instagram"></i></a>
              <a href="<?=getOption('social_facebook');?>" target="_blank"><i class="ion-social-facebook"></i></a>
              <a href="<?=getOption('social_twitter');?>" target="_blank"><i class="ion-social-twitter"></i></a>
              <a href="<?=getOption('social_youtube');?>" target="_blank"><i class="ion-social-youtube"></i></a>
            </div>{{-- .socmed --}}
            <div class="box-nav">
              <ul id="topnav">
                @auth
                  <li>
                    <a href="{{url('/')}}/{{lang()}}/my-account" class="">
                      <i class="ion-person"></i> <span>Hi, {{Auth::user()->name}}</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{url('/')}}/logout">
                      <i class="ion-ios-locked"></i> <span>@lang('content.logout')</span>
                    </a>
                  </li>
                @else
                  <li>
                    <a href="{{url('/')}}/login" class="button btn-outline btn-s  login">
                      <span>@lang('content.login')</span>
                    </a>
                  </li>
                @endauth
              </ul>
            </div>{{-- .box-nav --}}
          </div>
        </div>{{-- .container --}}
      </div>{{-- .header-box --}}
    </div>{{-- #top --}}
    </header>
    <main>
      {!! $slot or "<h3 class='center'>NOT FOUND</h3>" !!}
    </main>
    <footer>
    <section class="footer-bottom">
      <div class="container flex-center">
          <p class="copy">@lang('content.copyright')</p>
      </div>{{-- .container --}}
    </section>
    <div id="mobilenav"></div>
    </footer>
  </div>
  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
      {{ csrf_field() }}
  </form>
  @include('app.partial.popup-login')
  @include('app.partial.popup-message')
  <script src="{{ mix('/js/main.js') }}"></script>
  <script src="{{ url('/js/share.js') }}"></script>
</body>
</html>


