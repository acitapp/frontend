@component('custom.'.$custom.'.master')
  @slot('webtitle')
   kiosTix - Solusi Tepat Tiket Hiburan Keluarga Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket aktivitas dan hiburan keluarga terpercaya di Indonesia. Jual beli tiket aktivitas dan hiburan keluarga dengan mudah, aman dan terpercaya.
  @endslot
  @slot('template')
   {{$custom}}
  @endslot
  @include('custom.asian-games-2018.banner')
  <section id="section-attraction" class="section">
    <div class="container">
      <div class="navTab">
          <div class="btn-group flex-center">
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/" class="button btn-grey active">@lang('asiangames.sports')</a>
      {{--       <a href="{{url('/')}}/{{lang()}}/asian-games-2018/schedules" class="button btn-grey">@lang('asiangames.match_schedule')</a>
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/location" class="button btn-grey">@lang('Lokasi Pertandingan')</a> --}}
          </div>
      </div>
      <div class="sections">
          <div class="titleBox center">
             <h2 class="title">Pilih Event Olahraga</h2>
          </div>
          
          <div class="ag-event-sports">
            <div class="row">
              <div class="col-md-6">
                <div class="box-ag-6 leftBox">
                  <a href="https://www.kiostix.com/id/event/275/asiangames2018">
                    <img src="{{ url('/custom/asian-games-2018/assets/images/opening.jpg') }}">
                    <h3 class="text-lefts ag-absoluteText">
                      <strong>Opening Ceremony</strong><br>
                      Gelora Bung Karno,<br> Jakarta 
                      <strong>18.08.2018</strong>
                    </h3>
                  </a>
                </div>{{-- box-ag-6 --}}
              </div>{{-- col md  6 --}}
              <div class="col-md-6">
                <div class="box-ag-6 rightBox">
                  <a href="https://www.kiostix.com/id/event/274/asiangames2018">
                    <img src="{{ url('/custom/asian-games-2018/assets/images/closing.jpg') }}">
                    <h3 class="text-lefts ag-absoluteText">
                      <strong>Closing Ceremony</strong><br>
                      Gelora Bung Karno, <br> Jakarta
                      <strong>02.09.2018</strong>
                    </h3>
                  </a>
                </div>{{-- box-ag-6 --}}
              </div>{{-- col md  6 --}}
            </div>{{-- row --}}
          </div>{{-- ag-event-sport --}}
          <?php $totalevent= count($events) -1; ?>
          @foreach($events as $key=>$row)
            @if($key==0)
              <div class="row">
            @elseif($key % 4==0)
              </div><div class="row">
            @endif
              
            <div class="col-md-3 pd5">
              <a href="{{ url('/') }}/{{lang()}}/asian-games-2018/{{$row->id}}/{{$row->getTranslation(lang())->slug}}">
                <div class="box-ag-3 redBox">

                  <img class="ag-icon-sports" src="{{getThumbnail($row, 'events', 'small')}}">
                  {{-- <h3 class="ag-title-sports">{{str_replace('Asian Games 2018 - ','',$row->getTranslation('id')->title)}}</h3> --}}
                  <a class="ag-buy" href="{{ url('/') }}/{{lang()}}/asian-games-2018/{{$row->id}}/{{$row->getTranslation(lang())->slug}}">Beli ></a>
                </div>
              </a>
            </div>
            @if($totalevent==$key)
              </div>
            @endif
        	@endforeach
         
      
      </div>{{-- .section --}}
    </div>{{-- .container --}}
  </section>
@endcomponent