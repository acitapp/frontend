
  @component('custom.'.$custom.'.master')
  @slot('webtitle')
   kiosTix - Solusi Tepat Tiket Hiburan Keluarga Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket aktivitas dan hiburan keluarga terpercaya di Indonesia. Jual beli tiket aktivitas dan hiburan keluarga dengan mudah, aman dan terpercaya.
  @endslot
  @slot('template')
   {{$custom}}
  @endslot

  
  <div class="ag-listpng">
    <img src="{{ url('/custom/asian-games-2018/assets/images/list.png') }}">
  </div>
  {{-- end banner --}}

  <section class="ag-seating">
  <form action="{{url('/id/asian-games-2018/seat/order')}}" method="post" accept-charset="utf-8" id="form-data" role="form" enctype="multipart/form-data">
		             {!! csrf_field() !!}
    <div class="container">
      <div class="ag-seating-section">
        <div class="ag-seating-title">
          <h2>{{$event->getTranslation(lang())->title}}</h2>
        </div>
        <div class="ag-seating-header">
          <div class="ag-seat-detail">
            <h3>Rincian Pemesanan</h3>
          </div>
          <div class="ag-seat-date">
            <p> {{$schedules->venue->title ?? ''}}, {{eventDateFormat($schedules->started_at)}}</p>
          </div>
          <div class="ag-seat-class">
            <h3>{{$tickets->name}}</h3>
          </div>
          <div class="ag-seat-qty">
            <p>Jumlah</p>
            <span>
            <div class="input-group order-qty">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default btn-number btn-calculate" data-type="minus" data-field="qty-{{$tickets->id}}" data-schedule="{{$schedules->id}}" data-section="{{$tickets->id}}" data-count-session="{{count($schedules->sessions)}}">
                            <span class="ion-minus"></span>
                        </button>
                    </span>
                    <input type="text" id="qty" name="qty[]" class="form-control input-number" value="0" min="0" max="{{$tickets->max_purchase}}" data-price="{{$tickets->value}}" data-schedule="{{$schedules->id}}" data-section="{{$tickets->id}}">
                    <input type="hidden" name="ticket[]" value="{{$tickets->id}}">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default btn-number btn-calculate" data-type="plus" data-field="qty-{{$tickets->id}}" data-schedule="{{$schedules->id}}" data-section="{{$tickets->id}}" data-count-session="{{count($schedules->sessions)}}">
                            <span class="ion-plus"></span>
                        </button>
                    </span>
                </div>
            </span>
          </div>{{-- ag-seat-quantity --}}
          <div class="ag-seat-no">
            <p>nomor kursi</p> <span class="nomorkursi"></span>
          </div>  
        </div>{{-- ag-seating header --}}
        <div>
           <div class="loading loading-double hide" id="loadersubscribe"></div>
        </div>
        <canvas id="createSeat" width="1000" height="600"></canvas>
        <div class="ag-seat-check">
          <div class="ag-seat-wrap">
            <div class="ag-status-seat">
            
              <ul class="flex-center">
                <li>
                  <span class="checkseat"></span>tersedia
                </li>
                <li>
                  <span class="checkseat greyseat"></span>telah dipesan
                </li>
                <li>
                  <span class="checkseat yellowseat"></span>pilihan anda
                </li>
              </ul>
            </div>{{-- ag-status seat --}}

            <div class="ag-field">
              <h5>lapangan</h5>
            </div>
          </div>{{-- ag-seat wrap --}}
        </div>{{-- ag-seat check --}}
        <div class="ag-seat-order">
          <div class="hide-input">
          </div>
          <input type="hidden" name="visit_date" value="{{formatdateday($schedules->started_at)}}">
          <input type="hidden" name="visit_time" value="{{$schedules->sessions->first()->time}}">
          <button class="seat-submit">pesan sekarang</button>
        </div>
      </div>
    </div>
    </form>
  </section>
 
<script type="text/javascript" src="{{url('vendor/fabric/dist/fabric.min.js')}}"></script>
<script type="text/javascript" src="{{url('vendor/seat/seat.js')}}"></script>
  <script>
    var canvas = new fabric.Canvas('createSeat');
    
    var jsonSeat ={!!$seat!!};	
    
    var jmSeatSelect=0;
    var customer = '{!!$customer!!}';
    //for input 
    var event ="{!!$event->id!!}";
    var transaction ="{!!$transaction->id!!}";
    var schedule ="{!!$schedules->id!!}";
    
    var venue ="{!!$transaction->venue_id!!}";
    var dragging = false;
		var lastX = 0;
	    var marginLeft = 0;
		var marginTop = 0;
   
    var arrynomorkursi = new Array();
    // canvas.on('mouse:dblclick', function (option) {   
			
		// 	if(dragging==true)
		// 	{
		// 		dragging=false;
				
		// 	}
		// 	else{
		// 		dragging=true;
		// 		option.e.target.style.marginLeft = "0px";
		// 		$('#createSeat').css("margin-left", "0px");
		// 	}
		// });
		// canvas.on('mouse:move:before', function (option) {  
		// 	if(dragging==true)
		// 	{
		// 		lastX='-200';
	  //   		marginLeft = 0;
		// 		marginTop = 0;
		// 		option.e.target.style.marginLeft = "0px";
		// 		$('#createSeat').css("margin-left", "0px");
		// 	}
		// });
		// canvas.on('mouse:move', function (option) {  
		// 	if(dragging==true)
		// 	{
		// 		var delta =  parseInt(lastX) - parseInt(option.e.clientX);
			
		// 		lastX = option.e.clientX;
		// 		marginLeft = parseInt(marginLeft)+parseInt(delta);
				
				
		// 		// option.e.target.style.marginLeft = marginLeft + "px";
		// 		$('#createSeat').css("margin-left",marginLeft + "px");
				
			
		// 	}
		// 	option.e.preventDefault();
		// });
		// canvas.on('mouse:out', function (option) {
		// 	$('#createSeat').css("margin-left", "0px");
		// });



    // $.each(jsontiket, function(row, value) {
        
    //     total = (parseInt(total) + parseInt(value));
    //     $('.hide-input').append('<input type="hidden" name="ticket[]"  value="'+row+'">');
    //     $('.hide-input').append('<input type="hidden" name="qty[]"  value="'+value+'">');
    // });
    showseat();	
    checkQty();
    function showseat(){
      nameRow='';
			tempName='';
        for (i = 0; i < jsonSeat.length; i++) {
          console.log(tempName+'!='+jsonSeat[i].seat_row);
            if(tempName!=jsonSeat[i].seat_row){
              nameRow=jsonSeat[i].seat_row;
              tempName=jsonSeat[i].seat_row;
            }
            else{
              nameRow='';
            }
           selectable=true;
           if(jsonSeat[i].status_order==1 || jsonSeat[i].status_order==2)
           {
                jsonSeat[i].color_seat='#cecece';
                selectable=false;

           }else if(jsonSeat[i].status_order==3)
           {
               if(transaction==jsonSeat[i].transaction_id && customer==jsonSeat[i].customer_id)
               {
                  jmSeatSelect =(parseInt(jmSeatSelect)+1);
                  arrynomorkursi[jsonSeat[i].id] = jsonSeat[i].seat_number+''+jsonSeat[i].seat_row+';';
                    // jumlahtiketskrg=(parseInt(jumlahtiketskrg)+1);
               }
                jsonSeat[i].color_seat='#f4ab54';
                
           }
          
            var rect = new fabric.Rect({ 
                    width: 40, 
                    height: 40, 
                    fill: jsonSeat[i].color_seat, 
                    strokeWidth:1 ,
                    stroke: '#fff',
            });
            var text = new fabric.Text(nameRow+""+jsonSeat[i].seat_number, { 
                fontSize: 8, 
                textAlign: 'left',
                fill: jsonSeat[i].color_text, 
                originX: 'center',
                fontFamily: 'arial', 
                strokeWidth:0,
                left:20, 
                top: 12,
            });
            var group = new fabric.Group([ rect, text ], { 
                id : parseInt(jsonSeat[i].id),
                name : jsonSeat[i].title,
                left: parseInt(jsonSeat[i].left), 
                top: parseInt(jsonSeat[i].top),
                lockMovementY :true,
                lockMovementX:true,
                selectable:selectable,
                hasControls:false,
                tiket:jsonSeat[i].tiket_id,
                customer:jsonSeat[i].customer_id,
                transaction:jsonSeat[i].transaction_id
            });
            canvas.add(group).calcOffset();
           
        }

      
    }
    function checkQty(){
      var nomorKursi ='<h3>';
        $.each(arrynomorkursi, function(row, value) {
          if(value!=undefined)
          nomorKursi += value;
        });
        nomorKursi +='</h3>';
        $('.nomorkursi').html(nomorKursi);
       
    }
   
    canvas.on('selection:created', function(options) { 
    
       id=options.selected[0].id;
       
       canvas.requestRenderAll();
       if(options.selected[0].customer!=customer){
         
            if(jmSeatSelect >= $('#qty').val())
                {
                    $("#popup-title").text("Ooops!");
                    $("#popup-text").text('kuota tiket '+options.selected[0].name+' sudah habis');
                    common.popup();
                    canvas.requestRenderAll();
                
                    canvas.discardActiveObject();
                    checkQty();
                   
                    return false;
                }
           
            
       }
        
       $('#loadersubscribe').removeClass('hide');
       
            $.ajax({
                    method: "POST",
                    url: base_url + lang +"asian-games-2018/seat/check/",
                    data: {id:id,_token:token,transaction:transaction }
            }).done(function (data) {
              $('#loadersubscribe').addClass('hide');
                if(data.status==1 || data.status==2){
                  options.selected[0]._objects[0].set("fill", '#cecece');
                  options.selected[0].set("selectable", false);
                  $("#popup-title").text("Ooops!");
                    $("#popup-text").text('mohon maaf seat '+options.selected[0].name+' sudah terbeli');
                    common.popup();
                  // arrynomorkursi[data.seat.id]=data.seat.seat_number+''+data.seat.seat_row+';';
                }else if(data.status==3){
                  jmSeatSelect = (parseInt(jmSeatSelect)+1);
                  options.selected[0]._objects[0].set("fill", '#f4ab54');
                  options.selected[0].set("customer", data.seat.customer_id);
                  arrynomorkursi[data.seat.id]=data.seat.seat_number+''+data.seat.seat_row+';';
                }else if(data.status==4){
                   options.selected[0]._objects[0].set("fill", data.seat.color_seat);
                   
                   options.selected[0].set("customer",'0');
                    if(jmSeatSelect>0){
                      jmSeatSelect = (parseInt(jmSeatSelect)-1);
                      delete arrynomorkursi[id];
                    }
                    
                } 
                canvas.requestRenderAll();
                
                canvas.discardActiveObject();
                checkQty();
            });
            
      
        
      });
      $('.seat-submit').on('click',function(){
        console.log(jmSeatSelect+'>' + $('#qty').val());
        if(jmSeatSelect==0 && $('#qty').val()==0){
          $("#popup-title").text("Ooops!");
            $("#popup-text").text('anda belum pilih kursi');
            common.popup();
            return false;
        }
        else if(jmSeatSelect > $('#qty').val())
          {
            $("#popup-title").text("Ooops!");
            $("#popup-text").text('kuota tiket sudah habis');
            common.popup();
            return false;
          }
          else if(jmSeatSelect < $('#qty').val())
          {
            $("#popup-title").text("Ooops!");
            $("#popup-text").text('kuota tiket kurang '+(parseInt(jmSeatSelect)- parseInt($('#qty').val()))+' seat');
            common.popup();
            return false;
          }
          else
          {
            $('.hide-input').append('<input type="hidden" name="schedule"  value="'+schedule+'">');
            $('.hide-input').append('<input type="hidden" name="event"  value="'+event+'">');
            $('.hide-input').append('<input type="hidden" name="transaction"  value="'+transaction+'">');
            $('.hide-input').append('<input type="hidden" name="venue"  value="'+venue+'">');
            $('#form-data').submit();
          } 
       
      })
  $('.btn-number').click(function (e) {
             
              e.preventDefault();
              type = $(this).attr('data-type');
              var input = $("#qty");
              var currentVal = parseInt(input.val());
              
                   if (!isNaN(currentVal)) {
                       if (type == 'minus') {
                           if (currentVal > input.attr('min')) {
                               input.val(currentVal - 1).change();
                               $('.btn-number').attr('disabled', false);
                           }
                           if (parseInt(input.val()) == input.attr('min')) {
                               $(this).attr('disabled', true);
                           }
                       } else if (type == 'plus') {
                           if (currentVal < input.attr('max')) {
                               input.val(currentVal + 1).change();
                               $('.btn-number').attr('disabled', false);
                           }
                           if (parseInt(input.val()) == input.attr('max')) {
                               $(this).attr('disabled', true);
                           }
                       }
                   } else {
                       input.val(0);
                   }
               console.log($('#qty').val());
           });
  </script>
  
@endcomponent