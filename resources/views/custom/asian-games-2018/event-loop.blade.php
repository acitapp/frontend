<div id="data" class="row attraction-list">
@foreach ($events as $event)
<div class="col-md-3">
    <div class="event-box box">
      <div class="event-image thumb">
        <a href="{{ url('/') }}/{{lang()}}/event/{{$event->id}}/{{$event->getTranslation(lang())->slug  ?? ''}}">
           <img src="{{getThumbnail($event, 'events', 'small')}}">
        </a>
      </div>{{-- .event-image --}}
      <div class="event-entry">
          <div class="event-entry">
             <h3>{{$event->getTranslation(lang())->title  ?? ''}}</h3>
              @php
               $venue = $event->schedules()->where('status',1001)->with('venue')->first();
              @endphp
              <div class="event-venue">
                {{$venue->venue->info->city->name ?? ''}},
                {{$venue->venue->info->country->name ?? ''}}
              </div>
          </div>
      </div>
    </div>{{-- .box --}}
</div>{{-- .col --}}
@endforeach
</div>{{-- .row --}}
{{-- <div class="flex-center">{{ $events -> links()}}</div> --}}