<div id="scheduleBoxEvent" class="widget widget-box widget-schedule">
<form id="order-form-{{$schedule->id}}" class="orderForm" method="POST" action="{{url('/order-event')}}">
  {{csrf_field()}}
<a class="chooseTicket widget-list flex-center" data-toggle="collapse" href="#section-schedule-{{$schedule->id}}" aria-expanded="false" aria-controls="section-schedule-{{$schedule->id}}">
    <div class="colom-left">
        <h4 class="schedule-name">{{$schedule->name}}</h4>
        <span>
            {{$schedule->venue->title ?? ''}}</span>
        <br>
       <span class="date start-date">{{eventDateFormat($schedule->started_at)}}</span>
        @if (!is_null($schedule->ended_at))
         -
        <span class="date end-date">{{eventDateFormat($schedule->ended_at)}}</span>
         @endif 
          <div id="time-schedule-{{$schedule->id}}" class="times session" data-toggle="tooltip" data-placement="top"  title="Please select the event time, before find ticket.">
            @if (count($schedule->sessions))
               <input class="radio-session hide" checked type="radio" session="{{$schedule->sessions->first()->id}}" value="{{$schedule->sessions->first()->time}}" name="visit_time" id="session-{{$schedule->sessions->first()->id}}">
            @endif
          </div>
    </div>{{-- .colom-left --}}
    <div class="colom-right">
        @if ($event->id != 137)
        <span class="lowerPrice">
          <span>{{getRangePrice($schedule)}}</span>
        </span>
        @endif
        <input type="hidden" name="schedule" value="{{$schedule->id}}">
        <input type="hidden" name="visit_date" value="{{formatdMY($schedule->started_at)}}">
    </div>{{-- .colom-right --}}
 </a>{{-- .widget-list --}}
 @if ($event->sales_date >= Carbon\Carbon::now())
   <div class="center message-box">
     <span>Akan segera tersedia di kioTix.com</span>
   </div>
 @else
 <div class="section-box collapse @if (count($event->schedules->where('status',1001)) <= 1) in single-schedule @endif" id="section-schedule-{{$schedule->id}}">
    <div class="booking-header flex-center">
        <div class="colom-info col3">
          <span>Harga/Section</span>
        </div>{{-- .colom-left --}}
        <div class="colom-left col3 center qty-colom">
          <span>Kuantitas</span>
        </div>{{-- .colom-left --}}
        <div class="colom-right col3 text-right total-colom">
          <span>Subtotal</span>
        </div>{{-- .colom-right --}}
    </div>
    <div class="booking-body" id="booking-body-{{$schedule->id}}">
    @php
      
    @endphp
    <?php  
        $tiketseat='0';
        $jumlahscheduletiket = count($schedule->tickets->where('status',1001));
    ?>
    @foreach ($schedule->tickets->where('status',1001) as $ticket)
        @php
          $now = Carbon\Carbon::now();
          $start_date = Carbon\Carbon::createFromFormat('d/m/Y h:i A',$ticket->started_at);
          $end_date = Carbon\Carbon::createFromFormat('d/m/Y h:i A',$ticket->ended_at);
          
         $seats = $seat->where('tiket_id',$ticket->id)->first();
         
         if($seats){
          $tiketseat++;
         }
        @endphp
       
        @if ($ticket->period == true && $ticket->available_at_venue == false)
            @if ($now < $start_date)
              <div class="row-section-ticket" id="section-schedule-ticket-{{$ticket->id}}">
                  <div class="section-name col3">
                        <span  class="labels ticket-name">
                        {{$ticket->name}}
                       </span>
                  </div>
                  <div class="flex-center">
                      <div class="colom-info col3">
                        <span class="price ticket-price" id="section-price-{{$ticket->id}}" >
                          @if ($ticket->value == 0)
                           Free
                          @else
                            Rp. {{number_format($ticket->value)}}
                          @endif
                        </span>
                      </div>{{-- .colom-left --}}
                      <div class="colom-left col3 flex-center">
                         Segera di kiosTix
                      </div>
                      <div class="section-name col3">
                      </div>
                    </div>
                  @if ($ticket->show_description == true)
                   <div class="ticket-terms">
                     {!!$ticket->description!!}
                   </div>
                  @endif
              </div>{{-- .row-section-ticket --}}
            @elseif($now > $end_date)
            @else
                <div class="row-section-ticket" id="section-schedule-ticket-{{$ticket->id}}">
                    <div class="section-name col3">
                          <span  class="labels ticket-name">
                          {{$ticket->name}}
                         </span>
                    </div>
                    @include('custom.asian-games-2018.event-loop-section')
                   
                    @if ($ticket->show_description == true)
                     <div class="ticket-terms">
                       {!!$ticket->description!!}
                     </div>
                    @endif
                </div>{{-- .row-section-ticket --}}
            @endif
        @elseif($ticket->available_at_venue == true)
            <div class="row-section-ticket" id="section-schedule-ticket-{{$ticket->id}}">
                <div class="section-name col3">
                      <span  class="labels ticket-name">
                      {{$ticket->name}}
                     </span>
                </div>
                <div class="flex-center">
                    <div class="colom-info col3">
                      <span class="price ticket-price" id="section-price-{{$ticket->id}}" >
                        @if ($ticket->value == 0)
                         Free
                        @else
                          Rp. {{number_format($ticket->value)}}
                        @endif
                      </span>
                    </div>{{-- .colom-left --}}
                    <div class="colom-left col3 flex-center">
                       Tersedia di lokasi acara
                    </div>
                    <div class="section-name col3">
                    </div>
                  </div>
                @if ($ticket->show_description == true)
                 <div class="ticket-terms">
                   {!!$ticket->description!!}
                 </div>
                @endif
            </div>{{-- .row-section-ticket --}}
        @else
            <div class="row-section-ticket" id="section-schedule-ticket-{{$ticket->id}}">
                <div class="section-name col3">
                      <span  class="labels ticket-name">
                      {{$ticket->name}}
                     </span>
                     
                </div>
                
                @include('custom.asian-games-2018.event-loop-section')

                @if ($ticket->show_description == true)
                 <div class="ticket-terms">
                   {!!$ticket->description!!}
                 </div>
                @endif
            </div>{{-- .row-section-ticket --}}
        @endif
    @endforeach
    </div>{{-- .booking-body --}}
   <div id="booking-detail-{{$schedule->id}}" class="booking-detail">
      <div class="booking-footer">
            <input id="input-event-{{$schedule->id}}" type="hidden" name="event" value="{{$event->id}}">
            <input id="input-venue-{{$schedule->id}}" type="hidden" name="venue" value="{{$schedule->venue->id}}">
            <input id="input-tiket-{{$schedule->id}}" type="hidden" name="tiket" value="">
         
            @if($tiketseat==$jumlahscheduletiket)
            @else
            <div class="item-row booking-section">
              <span class="product-item">Total</span>
              <span class="product-price total-price">Rp. <span id="total-price-{{$schedule->id}}" data-price="0">0</span></span>
            </div>
           
              @auth
              <button type="submit" class="button btn-block submitOrder disabled" disabled="disabled">@lang('content.order_now') </button>
              @else
              <a href="#popup-login" class="button btn-block submitOrder showPopup disabled">@lang('content.order_now') </a>
              @endauth
            @endif
            <div class="loading loading-double hide" id="loader-order-{{$schedule->id}}"></div>
      </div>
  </div>{{-- .booking-detail --}}
 </div>
 @endif
</form>
</div>