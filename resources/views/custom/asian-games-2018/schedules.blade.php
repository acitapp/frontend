@component('custom.'.$custom.'.master')
  @slot('webtitle')
   kiosTix - Solusi Tepat Tiket Hiburan Keluarga Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket aktivitas dan hiburan keluarga terpercaya di Indonesia. Jual beli tiket aktivitas dan hiburan keluarga dengan mudah, aman dan terpercaya.
  @endslot
  @slot('template')
   {{$custom}}
  @endslot
  <section id="section-attraction" class="section">
    <div class="container">
      <div class="navTab">
          <div class="btn-group flex-center">
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018" class="button btn-grey">@lang('asiangames.sports')</a>
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/schedules" class="button btn-grey active">@lang('asiangames.match_schedule')</a>
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/location" class="button btn-grey">@lang('Lokasi Pertandingan')</a>
          </div>
      </div>
      <div class="sections">
          <div class="titleBox center">
             <h2 class="title">@lang('asiangames.choose') Schedules</h2>
          </div>
          
          <div class="ag-schedules">
          	<form>
          		<div class="table-responsive">
          			<table class="table ag-table-schedule">
          				<thead>
          					<tr class="bg-primary">
          						<th>Cabang Olahraga</th>
          						<th>14<br>Aug</th>
	       						<th>15<br>Aug</th>
	       						<th>16<br>Aug</th>
	       						<th>17<br>Aug</th>
	       						<th>18<br>Aug</th>
	       						<th>19<br>Aug</th>
	       						<th>20<br>Aug</th>
	       						<th>21<br>Aug</th>
	       						<th>22<br>Aug</th>
	       						<th>23<br>Aug</th>
	       						<th>24<br>Aug</th>
	       						<th>25<br>Aug</th>
	       						<th>26<br>Aug</th>
	       						<th>27<br>Aug</th>
	       						<th>28<br>Aug</th>
	       						<th>29<br>Aug</th>
	       						<th>30<br>Aug</th>
	       						<th>31<br>Aug</th>
	       						<th>01<br>Sep</th>
	       						<th>02<br>Sep</th>
       						</tr>
          				</thead>
          				<tbody>
          					<tr class="greydark">
	       						<td class="flex-center">Pembukaan <span class="pull-right"><a class="ag-btn danger" href="{{url('/')}}/{{lang()}}/asian-games-2018/?page=seating"">Beli</a></span></td>
	       						<td><ion-icon name="sunny"></ion-icon></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       					</tr>
	       					<tr class="greylight">
	       						<td class="flex-center">Penutupan <span class="pull-right"><a class="ag-btn danger" href="{{url('/')}}/{{lang()}}/asian-games-2018/?page=seating"">Beli</a></span></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td><ion-icon name="sunny"></ion-icon></td>
	       					</tr>
	       					<tr class="greydark">
	       						<td class="flex-center">Anggar <span class="pull-right"><a class="ag-btn primari" href="">Beli</a></span></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td class="icon-schedule"><ion-icon name="medal"></ion-icon></td>
	       						<td class="icon-schedule"><ion-icon name="medal"></ion-icon></td>
	       						<td class="icon-schedule"><ion-icon name="medal"></ion-icon></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td class="icon-schedule"><ion-icon name="medal"></ion-icon></td>
	       						<td class="icon-schedule"><ion-icon name="medal"></ion-icon></td>
	       						<td class="icon-schedule"><ion-icon name="medal"></ion-icon></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       					</tr>
	       					<tr class="greylight"> 
	       						<td class="flex-center">Angkat Besi <span class="pull-right"><a class="ag-btn primari" href="">Beli</a></span></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       					</tr>
	       					<tr class="greydark">
	       						<td class="flex-center">Aquatics <span class="pull-right"><a class="ag-btn primari" href="">Beli</a></span></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       					</tr>
	       					<tr class="greylight">
	       						<td class="flex-center">Atletik <span class="pull-right"><a class="ag-btn primari" href="">Beli</a></span></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td><ion-icon name="megaphone"></ion-icon></td>
	       						<td></td>
	       					</tr>
          				</tbody>
          			</table>
          		</div>
          	</form>
          	<div class="ag-notes flex-center">
          		<div class="ag-notes-opt">
          			<ul>
          				<li><ion-icon name="megaphone"></ion-icon></li>
          				<li>Hari Pertandingan</li>
          			</ul>
          		</div>
          		<div class="ag-notes-opt">
          			<ul>
          				<li><ion-icon name="medal"></ion-icon></li>
          				<li>Pemberian Medali</li>
          			</ul>
          		</div>
          		<div class="ag-notes-opt">
          			<ul>
          				<li><ion-icon name="sunny"></ion-icon></li>
          				<li>Seremoni</li>
          			</ul>
          		</div>
          	</div>
          </div>

      </div>{{-- .section --}}
    </div>{{-- .container --}}
  </section>
@endcomponent