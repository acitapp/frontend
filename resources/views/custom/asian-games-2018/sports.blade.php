@component('custom.'.$custom.'.master')
  @slot('webtitle')
   kiosTix - Solusi Tepat Tiket Hiburan Keluarga Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket aktivitas dan hiburan keluarga terpercaya di Indonesia. Jual beli tiket aktivitas dan hiburan keluarga dengan mudah, aman dan terpercaya.
  @endslot
  @slot('template')
   {{$custom}}
  @endslot
  @include('custom.asian-games-2018.banner')
  <section id="section-attraction" class="section">
    <div class="container">
      <div class="navTab">
          <div class="btn-group flex-center">
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/?page=sports" class="button btn-grey active">@lang('asiangames.sports')</a>
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/?page=schedules" class="button btn-grey">@lang('asiangames.match_schedule')</a>
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/?page=location" class="button btn-grey">@lang('Lokasi Pertandingan')</a>
          </div>
      </div>
      <div class="sections">
          <div class="titleBox center">
             <h2 class="title">Pilih Event Olahraga</h2>
          </div>
          
          <div class="ag-event-sports">
          	<div class="row">
          		<div class="col-md-6">
          			<div class="box-ag-6 leftBox">
          				<a href="">
          					<img src="{{ url('/custom/asian-games-2018/assets/images/box-img-left.jpg') }}">
	          				<img class="ag-corner-logo logoLeft" src="{{ url('/custom/asian-games-2018/assets/images/left-box.png') }}">
	          				<h3 class="text-left ag-absoluteText">
	          					<strong>Opening Ceremony</strong><br>
	          					Gelora Bung Karno <br>
	          					Jakarta, <strong>18 August 2018</strong>
	          				</h3>
          				</a>
          			</div>{{-- box-ag-6 --}}
          		</div>{{-- col md  6 --}}
          		<div class="col-md-6">
          			<div class="box-ag-6 rightBox">
          				<a href="">
          					<img src="{{ url('/custom/asian-games-2018/assets/images/box-img-right.jpg') }}">
	          				<img class="ag-corner-logo logoRight" src="{{ url('/custom/asian-games-2018/assets/images/right-box.png') }}">
	          				<h3 class="text-right ag-absoluteText">
	          					<strong>Closing Ceremony</strong><br>
	          					Jakabaring, Palembang <br>
	          					<strong>2 September 2018</strong>
	          				</h3>
          				</a>
          			</div>{{-- box-ag-6 --}}
          		</div>{{-- col md  6 --}}
          	</div>{{-- row --}}
          </div>{{-- ag-event-sport --}}
		  @php
				$totalevent= count($events) -1;
			@endphp
			@foreach($events as $key=>$row)
				@if($key==0)
					<div class="row">
				@elseif($key % 3==0)
					</div><div class="row">
				@endif
          
				<div class="col-md-3 pd5">
					<a href="{{ url('/') }}/{{lang()}}/event/{{$row->id}}/{{$row->getTranslation(lang())->slug}}">
						<div class="box-ag-3 redBox">
							<img class="ag-icon-sports" src="{{getThumbnail($row, 'events', 'small')}}">
							<h3 class="ag-title-sports">{{$row->getTranslation('id')->title}}</h3>
							<a class="ag-buy" href="">Beli ></a>
						</div>
					</a>
				</div>
				@if($totalevent==$key)
					</div>
				@endif
        	@endforeach
		<?php /*
          <div class="row">
          	<div class="col-md-3 pd5">
          		<a href="{{url('/')}}/{{lang()}}/asian-games-2018/?page=category-sport">
          			<div class="box-ag-3 redBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-badminton.png') }}">
	      				<h3 class="ag-title-sports">Badminton</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 purpleBox">
          				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-judo.png') }}">
          				<h3 class="ag-title-sports">Judo</h3>
          				<a class="ag-buy" href="">Beli ></a>
          			</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 yellowBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-berkuda.png') }}">
	      				<h3 class="ag-title-sports">Berkuda</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 greenBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-basket.png') }}">
	      				<h3 class="ag-title-sports">Basket</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          </div>
          {{-- row 1 --}}

          <div class="row">
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-sepakbola.png') }}">
	      				<h3 class="ag-title-sports">Sepakbola</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-bowling.png') }}">
	      				<h3 class="ag-title-sports">Bowling</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-karate.png') }}">
	      				<h3 class="ag-title-sports">Karate</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-atletik.png') }}">
	      				<h3 class="ag-title-sports">Atletik</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          </div>
          {{-- row2 --}}

          <div class="row">
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-senam.png') }}">
	      				<h3 class="ag-title-sports">Senam</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-gold.png') }}">
	      				<h3 class="ag-title-sports">Golf</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-paragliding.png') }}">
	      				<h3 class="ag-title-sports">Paragliding</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-bisbolsofbol.png') }}">
	      				<h3 class="ag-title-sports">Bisbol dan Sofbol</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          </div>
          {{-- row3 --}}

          <div class="row">
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-dayung.png') }}">
	      				<h3 class="ag-title-sports">Dayung</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-bolavoli.png') }}">
	      				<h3 class="ag-title-sports">Bola Voli</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-tinju.png') }}">
	      				<h3 class="ag-title-sports">Tinju</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-taekwondo.png') }}">
	      				<h3 class="ag-title-sports">Taekwondo</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          </div>
          {{-- row4 --}}

          <div class="row">
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-balpsepeda.png') }}">
	      				<h3 class="ag-title-sports">Balap Sepeda</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-rollersport.png') }}">
	      				<h3 class="ag-title-sports">Roller Sport</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-tenis.png') }}">
	      				<h3 class="ag-title-sports">Tenis</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          	<div class="col-md-3 pd5">
          		<a href="">
          			<div class="box-ag-3 blueBox">
	      				<img class="ag-icon-sports" src="{{ url('/custom/asian-games-2018/assets/images/icon-sepaktakraw.png') }}">
	      				<h3 class="ag-title-sports">Sepak Takraw</h3>
	      				<a class="ag-buy" href="">Beli ></a>
	          		</div>
          		</a>
          	</div>
          </div>
          {{-- row4 --}}
         */ ?>
      </div>{{-- .section --}}
    </div>{{-- .container --}}
  </section>
@endcomponent