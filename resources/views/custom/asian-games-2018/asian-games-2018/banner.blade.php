  <div class="ag-banner">
    <img class="ag-banner-left" src="{{ url('/custom/asian-games-2018/assets/images/banner-left.png') }}">
    <img class="mb-banner" src="{{ url('/custom/asian-games-2018/assets/images/header-banner.jpg') }}">
    <div class="container">
      <div class="ag-img-banner">
        <img class="icon-left" src="{{ url('/custom/asian-games-2018/assets/images/icon-bannerLeft.png') }}">
        <img class="energyasia" src="{{ url('/custom/asian-games-2018/assets/images/energyasia.png') }}">
        <img class="ag-banner-right" src="{{ url('/custom/asian-games-2018/assets/images/icon-bannerRight.png') }}">
        <div class="ag-title-banner">
          <h3>Get your tickets & support your nation’s athletes</h3>
          <h3>at the 18th Asian Games Jakarta Palembang 2018!</h3>
        </div>
      </div>
    </div>{{-- .container --}}
  </div>{{-- .ag-banner --}}
  <div class="ag-listpng">
    <img src="{{ url('/custom/asian-games-2018/assets/images/list.png') }}">
  </div>