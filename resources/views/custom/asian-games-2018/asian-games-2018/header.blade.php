<header>
<div id="top">
  <div id="sticky-anchor"></div>
  <div  id="sticky" class="header-box sticky">
    <div class="container">
      <div class="flex-center">
        <a href="{{url('/')}}" class="logo ag-logo">
          <img src="{{url('/')}}/custom/asian-games-2018/assets/images/logo-header.png" alt="kiosTix">
        </a>
        <a href="{{url('/')}}" class="logo">
          <img src="{{url('/')}}/images/logo.png" alt="kiosTix">
        </a>
        <div id="navigation">
          <div class="navigation">
            <nav>
              <ul id="menu">
                <li>
                  {{-- <a href="{{url('/')}}/{{lang()}}/asian-games-2018">Asian Games 2018</a> --}}
                  <a href="{{url('/')}}/{{lang()}}/asian-games-2018">Cabang Olahraga</a>
            {{--       <a href="{{url('/')}}/{{lang()}}/asian-games-2018/schedules">Jadwal</a>
                  <a href="{{url('/')}}/{{lang()}}/asian-games-2018/location">Lokasi</a> --}}
                </li>
              </ul>
            </nav>
          </div> {{-- .navigation --}}
        </div>{{-- #navigation --}}
        <div id="mobilenav">
          <ul id="footerNav">
            @auth
              <li>
                <a href="{{url('/')}}/{{lang()}}/my-account" class="">
                  <i class="ion-person"></i> Hi, {{limitWord(Auth::user()->name,1)}}
                </a>
              </li>
              <li class="logout-btns">
                <a href="{{url('/')}}/logout">
                  <i class="ion-ios-locked"></i> Keluar
                </a>
              </li>
            @else
              <li>
                <a href="#popup-login" class="showPopup">
                  <i class="ion-person"></i> Masuk
                </a>
              </li>
            @endauth
          </ul>
          <div id="search-mobile">@include('app.widget.search')</div>
        </div>
        <div class="top-search">
          @include('app.widget.search')
        </div>

{{--         <div class="ag-lang">
          <a href="{{url('/')}}/{{lang()}}/asian-games-2018">ID</a>
          <span>|</span>
          <a href="{{url('/')}}/{{lang()}}/asian-games-2018">EN</a>
        </div> --}}
        
        <div class="socmed">
       {{--        <div class="ag-lang">
                <a href="{{url('/')}}/{{lang()}}/asian-games-2018">ID</a>
                <span>|</span>
                <a class="active" href="{{url('/')}}/{{lang()}}/asian-games-2018">EN</a>
              </div> --}}
              <a href="<?=getOption('social_instagram');?>" target="_blank"><i class="icon-brand-instagram"></i></a>
              <a href="<?=getOption('social_facebook');?>" target="_blank"><i class="ion-social-facebook"></i></a>
              <a href="<?=getOption('social_twitter');?>" target="_blank"><i class="ion-social-twitter"></i></a>
              <a href="<?=getOption('social_youtube');?>" target="_blank"><i class="ion-social-youtube"></i></a>
        </div>{{-- .socmed --}}
        <div class="box-nav">
          <ul id="topnav">
            @auth
              <li>
                <a href="{{url('/')}}/{{lang()}}/my-account" class="">
                  <i class="ion-person"></i> <span>Hi, {{limitWord(Auth::user()->name,1)}}</span>
                </a>
              </li>
              <li class="logout-btns">
                <a href="{{url('/')}}/logout">
                  <i class="ion-ios-locked"></i> <span>@lang('content.logout')</span>
                </a>
              </li>
            @else
              <li>
                <a href="#popup-login" class="showPopup button btn-outline btn-s  login">
                  <span>@lang('content.login')</span>
                </a>
              </li>
            @endauth
          </ul>
        </div>{{-- .box-nav --}}
      </div>
    </div>{{-- .container --}}
  </div>{{-- .header-box --}}
</div>{{-- #top --}}


</header>