@component('custom.'.$custom.'.master')
  @slot('webtitle')
   kiosTix - Solusi Tepat Tiket Hiburan Keluarga Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket aktivitas dan hiburan keluarga terpercaya di Indonesia. Jual beli tiket aktivitas dan hiburan keluarga dengan mudah, aman dan terpercaya.
  @endslot
  @slot('template')
   {{$custom}}
  @endslot
  <section id="section-attraction" class="section">
    <div class="container">
      <div class="navTab">
      <div class="btn-group flex-center">
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/" class="button btn-grey ">@lang('asiangames.sports')</a>
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/schedules" class="button btn-grey">@lang('asiangames.match_schedule')</a>
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/location" class="button btn-grey active">@lang('Lokasi Pertandingan')</a>
          </div>
      </div>
      <div class="sections">
      <div class="row">
        <div class="col-md-8">
            <div class="event-entry">
                <div class="iframe">
                  {!!$venue->info->iframe!!}
                </div>{{-- .event-image --}}
                <div class="event-meta">
                  <h1>{{$venue->title}}</h1>
                  <span class="date">{{$venue->info->address ?? ''}}</span>
                </div>{{-- .event-meta --}}
            </div>{{-- .event-entry --}}
        </div>{{-- .col --}}
        <div class="col-md-4">
            <div class="widget-options">
                <h3>Daftar Venue Lainnya</h3>
                <div class="widget">
                    <div class="LatestList">
                        @foreach ($latests as $venue)
                          <div class="list-body flex-center has-border">
                            <div class="post-meta">
                              <h3><a href="{{ url('/') }}/{{lang()}}/asian-games-2018/location/{{$venue->slug}}">{{$venue->title}}</a></h3>
                              <span class="date">{{$venue->info->state->name ?? ''}}</span>
                            </div>{{-- .post-meta --}}
                          </div>{{-- .flex-box --}}
                        @endforeach
                    </div>{{-- .LatestList --}}
                </div>{{-- .widget --}}
            </div>{{-- .widget-options --}}
        </div>{{-- .col --}}
      </div>{{-- .row --}}
      </div>{{-- .section --}}
    </div>{{-- .container --}}
  </section>
@endcomponent