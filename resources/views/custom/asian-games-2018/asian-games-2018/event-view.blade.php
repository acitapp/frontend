@component('custom.'.$custom.'.master')
  @slot('webtitle')
   kiosTix - Solusi Tepat Tiket Hiburan Keluarga Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket aktivitas dan hiburan keluarga terpercaya di Indonesia. Jual beli tiket aktivitas dan hiburan keluarga dengan mudah, aman dan terpercaya.
  @endslot
  @slot('template')
   {{$custom}}
  @endslot
  <section id="section-attraction" class="section">
    <div class="container">
      
      <div class="sections">
          <div class="col-md-6">
            <div class="event-entry">
                <div class="event-image thumb">
                  <img src="{{getThumbnail($event, 'events', 'medium')}}">
                 {{--  <h3 class="title-image"> {!! str_replace(' - ','<br>',$event->getTranslation('id')->title) !!}</h3> --}}
                </div>{{-- .event-image --}}
                <div class="collapse in" id="eventview">
                <div class="event-detail">
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">@lang('content.detail')</a></li>
                        <li role="presentation"><a href="#tnc" aria-controls="tnc" role="tab" data-toggle="tab">@lang('content.tnc')</a></li>
                        <li class="shareTab">
                          @include('app.widget.sharebox')
                        </li>
                      </ul>
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="details">
                          <div class="row">
                            <div class="col-md-12">
                                <div class="event-info event-artist flex">
                                    <label>Tag :</label>
                                    <div class="artists">
                                      @foreach($event->tags as $tag)
                                       <a class="venue-name" href="{{url('/')}}/{{lang()}}/tag/{{$tag->slug}}">{{$tag->name}}</a> 
                                      @endforeach
                                    </div>
                                </div>
                                <div class="event-info event-venue flex">
                                   @if (count($event->schedules) > 1)
                                 
                                    <label>Lokasi :</label>
                                    
                                     <div class="venues">
                                         <a class="venue-name" data-toggle="collapse" href="#venueList" aria-expanded="false" aria-controls="venueList">Lihat semua lokasi</a>
                                         <div class="collapse" id="venueList">
                                            <ul>
                                          @foreach($event->schedules as $schedule)
                                              <li> <a class="venue-name" href="{{url('/')}}/{{lang()}}/venue/{{$schedule->venue->slug}}">{{$schedule->venue->title}}</a>
                                             </li>
                                            
                                          @endforeach
                                            </ul>
                                         </div>
                                      </div>
                                      
                                        @else
                                     <label>Lokasi :</label>
                                     <div class="venues">
                                          @foreach($event->schedules as $schedule)
                                               <div class="the-schedule-list">
                                                 <a class="venue-name" href="{{url('/')}}/{{lang()}}/venue/{{$schedule->venue->slug}}">{{$schedule->venue->title}}</a> 
                                               </div>
                                          @endforeach
                                      </div>
                                   @endif
                                  
                                </div>
                                 @if (!thisAttraction($event))
                                <div class="event-info event-venue flex">
                                     <label>Tanggal :</label>
                                     <div class="venues">
                                         <a class="venue-name">{!! getRangeDate($event->schedules) !!}</a>
                                      </div>
                                </div>
                                @endif
                            </div>{{-- .col --}}
                         
                          </div>{{-- .row --}}
                          @if (!is_null($event->getTranslation(lang())->description))
                            <div class="event-description">
                                @if (!$agent->isDesktop())
                                <div class="moreContent">
                                  {!! $event->getTranslation(lang())->description !!}
                                </div>
                                @else
                                  {!! $event->getTranslation(lang())->description !!}
                                @endif
                            </div>
                          @endif
                        </div>{{-- .tab-pane --}}
                        <div role="tabpanel" class="tab-pane" id="tnc">
                          {!! $event->getTranslation(lang())->terms !!}
                        </div>{{-- .tab-pane --}}
                      </div>{{-- .tab-content --}}
                </div>{{-- .event-detail --}}
               </div>{{-- #eventview --}}
            </div>{{-- .event-entry --}}
        </div>{{-- .col --}}
        <div class="col-md-6">
            <div class="widget-options">
            @foreach ($event->schedules->where('status',1001) as $schedule)
                  @include('custom.asian-games-2018.event-schedule')
            @endforeach
            </div>
        </div>{{-- .col --}}                      
      </div>{{-- .section --}}
    </div>{{-- .container --}}
  </section>
  <script>
 
  $(document).on('click','.submitSeat',function(){
    var id = '#order-form-'+ $(this).data('schedule');
    var tiket = $(this).data('ticket');
    var input = '#input-tiket-'+ $(this).data('schedule');
    $(id).attr('action',base_url+lang+'asian-games-2018/transaction');
    
    $(input).val(tiket);
    $(id).trigger('submit');
    // console.log($(id).attr('action'));
    
  })
  </script>

@endcomponent