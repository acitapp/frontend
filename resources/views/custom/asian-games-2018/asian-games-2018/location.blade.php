@component('custom.'.$custom.'.master')
  @slot('webtitle')
   kiosTix - Solusi Tepat Tiket Hiburan Keluarga Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket aktivitas dan hiburan keluarga terpercaya di Indonesia. Jual beli tiket aktivitas dan hiburan keluarga dengan mudah, aman dan terpercaya.
  @endslot
  @slot('template')
   {{$custom}}
  @endslot
  <section id="section-attraction" class="section">
    <div class="container">
      <div class="navTab">
      <div class="btn-group flex-center">
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/" class="button btn-grey ">@lang('asiangames.sports')</a>
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/schedules" class="button btn-grey">@lang('asiangames.match_schedule')</a>
            <a href="{{url('/')}}/{{lang()}}/asian-games-2018/location" class="button btn-grey active">@lang('Lokasi Pertandingan')</a>
          </div>
      </div>
      <div class="sections">
          <div class="titleBox center">
             <h2 class="title">@lang('asiangames.choose') Location</h2>
          </div>
          
          <div class="ag-event-location">
          @php
            $totalvenue= count($venue) -1;
          @endphp
          @foreach($venue as $key=>$row)
            @if($key==0)
              <div class="row">
            @elseif($key % 4==0)
              </div><div class="row">
            @endif
              <div class="col-md-3">
                <a class="ag-box-location" href="{{ url('/') }}/{{lang()}}/asian-games-2018/location/{{$row->slug}}">
                  <img src="{{getThumbnail($row, 'venue', 'small')}}">
                  <div class="ag-overlay-img"></div>
                  <h2 class="ag-title-loc">{{$row->title}}, <br> {{$row->info->city->name}}</h2>
                  <a class="ag-link-loc" href="{{ url('/') }}/{{lang()}}/asian-games-2018/location/{{$row->slug}}">Lihat <span><ion-icon name="arrow-forward"></ion-icon></span></a>
                </a>{{-- ag-box location --}}
              </div>{{-- col md 3 --}}
              @if($totalvenue==$key)
              </div>
              @endif
        	  @endforeach
              

            

          </div>{{-- ag-event-location --}}

      </div>{{-- .section --}}
    </div>{{-- .container --}}
  </section>
@endcomponent