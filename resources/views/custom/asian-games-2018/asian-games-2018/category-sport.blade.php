
  @component('custom.'.$custom.'.master')
  @slot('webtitle')
   kiosTix - Solusi Tepat Tiket Hiburan Keluarga Anda
  @endslot
  @slot('description')
  kiosTix adalah situs jual beli tiket aktivitas dan hiburan keluarga terpercaya di Indonesia. Jual beli tiket aktivitas dan hiburan keluarga dengan mudah, aman dan terpercaya.
  @endslot
  @slot('template')
   {{$custom}}
  @endslot

  <div class="ag-cat-banner">
      <img src="{{getThumbnail($event, 'events', 'medium')}}">
      <div class="container">
        <div class="ag-content-category">
          <div class="ag-title-cat">
            <h1>{{$event->getTranslation(lang())->title}}</h1>
          </div>
          <div class="ag-desc-cat">
            <p>{{$event->getTranslation(lang())->description}}</p>
          </div>
        </div>
      </div>
  </div>{{-- .container --}}
  <div class="ag-listpng">
    <img src="{{ url('/custom/asian-games-2018/assets/images/list.png') }}">
  </div>
  {{-- end banner --}}

  <section id="section-attraction" class="section">
    <div class="container">
      
      <div class="sections">
          <div class="titleBox center">
             <h2 class="title">@lang('asiangames.choose')</h2>
          </div>
          
          <div class="row">
          @foreach($eventChild as $key=>$row)
            <div class="col-md-3 pd5">
              <a href="{{ url('/') }}/{{lang()}}/asian-games-2018/{{$row->id}}/{{$row->getTranslation(lang())->slug}}">
                <div class="box-ag-3 blueBox">
                <img class="ag-icon-sports" src="{{getThumbnail($row, 'events', 'small')}}">
                <h3 class="ag-title-sports">{{$row->getTranslation('id')->title}}</h3>
                <a class="ag-buy" href="{{ url('/') }}/{{lang()}}/asian-games-2018/{{$row->id}}/{{$row->getTranslation(lang())->slug}}">Beli ></a>
                </div>
              </a>
            </div>
          @endforeach
            
          {{-- row2 --}}

      </div>{{-- .section --}}
    </div>{{-- .container --}}
  </section>
@endcomponent