<div style="background: #f2f2f2; padding: 20px;">
<div style="max-width:700px; margin:0 auto;  border-radius:5px; overflow:hidden;">
      <div style="font-family:arial; font-size:14px; padding:20px 30px; background:#2d3171; text-align:center;">
       <h1 style="color:#fff; font-size: 20px; margin:0;"><img src="https://www.kiostix.com/images/logo.png" alt=""></h1>
      </div>
       @php
        $date = Carbon\Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
        $currentDate = strtotime($date);
        $futureDate = $currentDate+(60*180);
        $expTime = date("Y-m-d H:i:s", $futureDate);
        $expTimeView = date("Y/m/d H:i:s", $futureDate);
        $expTimeViewDate = date("Y/m/d", $futureDate);
        $expTimeViewTime = date("H:i:s", $futureDate);
        $countdown = 'https://www.kiostix.com/emailcountdown/countdown.php?time='.$expTime;
       @endphp
      <div style="font-family:arial; font-size:14px; padding:30px; background:#fff; color:#333; text-align:left;">
            <div class="order-detail widget-options center">
              <div class="widget-head" style="margin-bottom:30px;">
                <h3 style="margin: 0; font-size: 18px;">Halo, {{$transaction->customer->name ?? ''}}</h3>
                <h5 style="margin: 0; font-size: 16px;">Mohon segera selesaikan pembayaran Anda</h5>
                <p>
                  Kami menunggu proses terakhir dari kamu, <br>
                  silahkan lakukan pembayaran sebelum , <strong>{{$expTimeView}}</strong>
                </p>
                <div style="clear: both; overflow: hidden; margin:0 -3px; display: flex; align-items: center; justify-content: center; text-align: center;">
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Total Pembayaran:  <br>
                            <i style="font-size:12px;">Total Payment</i></span>
                         <h3 style="margin:0;"><strong>Rp {{number_format($transaction->value_grand_total)}}</strong></h3>
                    </div>
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Metode Pembayaran:  <br>
                            <i style="font-size:12px;">Payment Method</i></span>
                         <h3 style="margin:0;"><strong>{{$transaction->payment->name}}</strong></h3>
                    </div>
                </div>
                <div style="clear: both; overflow: hidden; margin:0 -3px; display: flex; align-items: center; justify-content: center; text-align: center;">
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Kode Pembayaran: </span>
                         <h3 style="margin:0;"><strong>{{$response->bill_key}}</strong></h3>
                    </div>
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Order No: </span>
                         <h3 style="margin:0;"><strong>{{$transaction->order_no}}</strong></h3>
                    </div>
                </div>
                <div style="clear: both; overflow: hidden; margin:0 -3px; display: flex; align-items: center; justify-content: center; text-align: center;">
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Status Pembayaran: </span>
                         <h3 style="margin:0;"><strong>Pending</strong></h3>
                    </div>
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Batas Waktu Pembayaran   <br>
                            <i style="font-size:12px;">Payment Time Remaining</i></span>
                         <br>
                         <strong>{{$expTimeViewDate}}</strong>
                         <br>
                         <h3 style="margin:0;"><strong style="font-size: 20px;">{{$expTimeViewTime}}</strong></h3>
                    </div>
                </div>
               <h5 style="margin:20px 0 0 0; font-size: 16px;">{{$event->getTranslation(lang())->title}}</h5>
                <p style="margin:0 0 0 0;">
                  <span>{{$transaction->venue->title ?? ''}}</span><br>
                   <span>{{$transaction->venue->info->state->name ?? ''}} {{$transaction->venue->info->country->name ?? ''}}</span>
                </p>
              </div>{{-- .widget-head --}}
              <div class="widget-body">
                  <div id="booking-detail" class="booking-details booking-result">
                      <div class="booking-body">
                          <table border="0" style="width: 100%; border-right: solid 2px #fff; font-size: 12px;" cellpadding="0" cellspacing="0">
                              <tr>
                                <td style="font-weight: bold; background:#eee; color: #000; padding: 10px;">Price/Section</td>
                                <td style="font-weight: bold; background:#eee; color: #000; padding: 10px; text-align: center;">Quantity</td>
                                <td style="font-weight: bold; background:#eee; color: #000; padding: 10px; text-align: right;">Total</td>
                              </tr>
                            @foreach ($transactions as $order)
                                <tr style="background: #f9f9f9;">
                                  <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px;">
                                        <div class="section-name">
                                              <strong  class="labels ticket-name">
                                             {{$order->name ?? ''}}
                                             </strong><br>
                                            <span style="font-weight: normal; color:#666;">{{eventDateFormat($order->visit_date)}}, <br>
                                              at {{date('g:ia', strtotime($order->visit_time))}}</span>
                                        </div>
                                        @if ($order->order_value == 0)
                                        <strong class="product-price">Free</strong>
                                        @else
                                        <strong class="product-price">Rp {{number_format($order->order_value)}}</strong>
                                        @endif
                                  </td>
                                  <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: center;">x {{$order->qty}}</td>
                                  <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Rp {{number_format($order->total_value)}}</td>
                                </tr>{{-- .flex-row --}}
                            @endforeach
                              <tr style="background: #fff8f2">
                                <td colspan="2" style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Sub Total</td>
                                <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->value_sub_total)}}</strong></td>
                              </tr>
                              <tr style="background: #fff8f2">
                                <td colspan="2" style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Fee</td>
                                <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->fee_convenience_total)}}</strong></td>
                              </tr>
                              @if (!is_null($transaction->value_promo))
                                <tr style="background: #fff8f2">
                                  <td colspan="2" style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Voucher</td>
                                  <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->value_promo)}}</strong></td>
                                </tr>
                              @endif
                              <tr style="background: #fff8f2">
                                <td colspan="2" style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Grand Total</td>
                                <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->value_grand_total)}}</strong></td>
                              </tr>
                          </table>
                      </div>{{-- .booking-body --}}
                  </div>{{-- .booking-detail --}}
                </div>{{-- .event-detail --}}
                  {!!$transaction->payment->description ?? '' !!}
              </div>{{-- .widget-body --}}
      </div>
  </div>
</div>
