<div style="background: #f2f2f2; padding: 20px;">
<div style="max-width:700px; margin:0 auto;  border-radius:5px; overflow:hidden;">
      <div style="font-family:arial; font-size:14px; padding:20px 30px; background:#2d3171; text-align:center;">
       <h1 style="color:#fff; font-size: 20px; margin:0;">KIOSTIX</h1>
      </div>
      <div style="font-family:arial; font-size:14px; padding:30px; background:#fff; color:#333; text-align:left;">
            <div class="order-detail widget-options center">
              <div class="widget-head">
                <h3 style="margin:0 0 10px 0; font-size: 18px;">Halo,</h3>
                <p>
                  @if ($response->transaction_status == 'capture')
                   <span style="text-transform: uppercase; color: #2faa06;">Success</span>
                  @else
                   <span style="text-transform: uppercase; color: #ea9a3f;">{{$response->transaction_status}}</span>
                  @endif
                </p>

              <h3>Payment Method : {{$transaction->payment->name}}</h3>
<p>Guide using {{$transaction->payment->name}} payment method click here<br>
For more information or questions please contact our Customer Service at CS@KIOSTIX.COM</p>
                  @if (!is_null($response->permata_va_number))
                   <h3>Your Virtual Account No : {{$response->permata_va_number}}</h3>
                  @endif
                  @if (!is_null($response->payment_code))
                   <h3>Your Payment Code : {{$response->payment_code}}</h3>
                  @endif
                <h3 style="margin:0; font-size: 24px; color: #ea9a3f;"><span style="color: #999;">Trx ID :</span> {{$transaction->order_no}}</h3>
              </div>{{-- .widget-head --}}
              <div class="widget-body">
                 {{--  <p>
                    <strong>Customer Info</strong><br>
                    <span>{{$transaction->userInformation->fullname ?? ''}}</span><br>
                    <span>{{$transaction->userInformation->phone ?? ''}}</span><br>
                    <span>{{$transaction->userInformation->email ?? ''}}</span><br>
                  </p> --}}
                  <h3 style="margin:0">{{$event->getTranslation('id')->title ?? ''}}</h3>
                  <p style="margin:0 0 20px 0;">
                    <strong>Venue Location</strong><br>
                    <span>{{$transaction->venue->title ?? ''}}</span>,<br>
                     <span>{{$transaction->venue->info->state->name ?? ''}},{{$transaction->venue->info->country->name ?? ''}}</span>
                  </p>
                  <div id="booking-detail" class="booking-details booking-result">
                      <div class="booking-body">
                          <table border="0" style="width: 100%; border-right: solid 1px #ddd; font-size: 12px;" cellpadding="0" cellspacing="0">
                            <tr>
                              <td colspan="4" style="font-weight: bold; background:#ea9a3f; color: #fff; padding: 10px; font-size:14px; ">Order Details</td>
                            </tr>
                              <tr>
                                <td style="font-weight: bold; background:#2d3171; color: #fff; padding: 10px;">Description</td>
                                <td style="font-weight: bold; background:#2d3171; color: #fff; padding: 10px; text-align: center;">Quantity</td>
                                <td style="font-weight: bold; background:#2d3171; color: #fff; padding: 10px; text-align: right;">Price/Tix</td>
                                <td style="font-weight: bold; background:#2d3171; color: #fff; padding: 10px; text-align: right;">Sub Total</td>
                              </tr>
                            @foreach ($transaction->orders as $order)
                              <tr>
                                <td style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px;">{{$order->sectionable->name ?? ''}}<br>

                                    <span class="visit-date">Visit Date : <span class="v-date">{{$order->visit_date}}</span></span><br>
                                    <span class="visit-time">Visit Time : <span class="v-time">{{$order->visit_time}}</span></span>

                                </td>
                                <td style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px; text-align: center;">x {{$order->quantity}}</td>
                                <td style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px; text-align: right;">Rp {{number_format($order->order_value/$order->quantity)}}</td>
                                <td style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px; text-align:right;
                                ">Rp {{number_format($order->order_value)}}</td>
                              </tr>{{-- .flex-row --}}
                            @endforeach
                              <tr>
                                <td colspan="3" style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px; text-align: right;">SUBTOTAL</td>
                                <td style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->value_sub_total)}}</strong></td>
                              </tr>
                              <tr>
                                <td colspan="3" style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px; text-align: right;">Tax</td>
                                <td style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->fee_tax)}}</strong></td>
                              </tr>
                              <tr>
                                <td colspan="3" style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px; text-align: right;">Total</td>
                                <td style="border-left:solid 1px #ddd; border-bottom:solid 1px #ddd; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->fee_tax+$transaction->value_sub_total)}}</strong></td>
                              </tr>
                          </table>
                      </div>{{-- .booking-body --}}
                  </div>{{-- .booking-detail --}}
                </div>{{-- .event-detail --}}
              </div>{{-- .widget-body --}}
      </div>
  </div>
</div>
