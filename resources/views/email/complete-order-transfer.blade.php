<div style="background: #f2f2f2; padding: 20px;">
<div style="max-width:700px; margin:0 auto;  border-radius:5px; overflow:hidden;">
      <div style="font-family:arial; font-size:14px; padding:20px 30px; background:#2d3171; text-align:center;">
       <h1 style="color:#fff; font-size: 20px; margin:0;">KIOSTIX</h1>
      </div>
      <div style="font-family:arial; font-size:14px; padding:30px; background:#fff; color:#333; text-align:left;">
            <div class="order-detail widget-options center">
              <div class="widget-head">
                <h3 style="margin:0 0 10px 0; font-size: 18px;">Halo, <br> Mohon segera selesaikan pembayaran Anda</h3>
                <p>
                  Checkout berhasil pada , {{formatdate($transaction->created_at)}}
                </p>

              <h3>Total Pembayaran: <strong>Rp {{number_format($transaction->fee_convenience_total+$transaction->value_sub_total)}}</strong></h3>
              <h3>Metode Pembayaran: {{$transaction->payment->name}}</h3>
              <h3>Virtual Account No : {{$response->permata_va_number}}</h3>
              <h3 style="margin:0; font-size: 24px; color: #ea9a3f;"><span style="color: #999;">Trx ID :</span> {{$transaction->order_no}}</h3>
              </div>{{-- .widget-head --}}
              <div class="widget-body">
                  <h3 style="margin:0">{{$event->getTranslation('id')->title ?? ''}}</h3>
                  <p style="margin:0 0 20px 0;">
                    <strong>Venue Location</strong><br>
                    <span>{{$transaction->venue->title ?? ''}}</span>,<br>
                     <span>{{$transaction->venue->info->state->name ?? ''}},{{$transaction->venue->info->country->name ?? ''}}</span>
                  </p>
                  <div id="booking-detail" class="booking-details booking-result">
                      <div class="booking-body">
                          <table border="0" style="width: 100%; border-right: solid 2px #fff; font-size: 12px;" cellpadding="0" cellspacing="0">
                              <tr>
                                <td style="font-weight: bold; background:#eee; color: #000; padding: 10px;">Price/Section</td>
                                <td style="font-weight: bold; background:#eee; color: #000; padding: 10px; text-align: center;">Quantity</td>
                                <td style="font-weight: bold; background:#eee; color: #000; padding: 10px; text-align: right;">Total</td>
                              </tr>
                            @foreach ($transactions as $order)
                                <tr style="background: #f9f9f9;">
                                  <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px;">
                                        <div class="section-name">
                                              <strong  class="labels ticket-name">
                                             {{$order->name ?? ''}}
                                             </strong><br>
                                            <span style="font-weight: normal; color:#666;">{{eventDateFormat($order->visit_date)}}, <br>
                                              at {{date('g:ia', strtotime($order->visit_time))}}</span>
                                        </div>
                                        @if ($order->order_value == 0)
                                        <strong class="product-price">Free</strong>
                                        @else
                                        <strong class="product-price">Rp {{number_format($order->order_value)}}</strong>
                                        @endif
                                  </td>
                                  <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: center;">x {{$order->qty}}</td>
                                  <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Rp {{number_format($order->total_value)}}</td>
                                </tr>{{-- .flex-row --}}
                            @endforeach
                              <tr style="background: #fff8f2">
                                <td colspan="2" style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Sub Total</td>
                                <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->value_sub_total)}}</strong></td>
                              </tr>
                              <tr style="background: #fff8f2">
                                <td colspan="2" style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Fee</td>
                                <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->fee_convenience_total)}}</strong></td>
                              </tr>
                              @if (!is_null($transaction->value_promo))
                                <tr style="background: #fff8f2">
                                  <td colspan="2" style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Voucher</td>
                                  <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->value_promo)}}</strong></td>
                                </tr>
                              @endif
                              <tr style="background: #fff8f2">
                                <td colspan="2" style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;">Grand Total</td>
                                <td style="border-left:solid 2px #fff; border-bottom:solid 2px #fff; padding: 10px; text-align: right;"><strong>Rp {{number_format($transaction->value_grand_total)}}</strong></td>
                              </tr>
                          </table>
                      </div>{{-- .booking-body --}}
                  </div>{{-- .booking-detail --}}
                </div>{{-- .event-detail --}}
              </div>{{-- .widget-body --}}
      </div>
  </div>
</div>
