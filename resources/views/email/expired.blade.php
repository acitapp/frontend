<div style="background: #f2f2f2; padding: 20px;">
<div style="max-width:700px; margin:0 auto;  border-radius:5px; overflow:hidden;">
      <div style="font-family:arial; font-size:14px; padding:20px 30px; background:#2d3171; text-align:center;">
       <h1 style="color:#fff; font-size: 20px; margin:0;"><img src="https://www.kiostix.com/images/logo.png" alt=""></h1>
      </div>
       @php
        $date = Carbon\Carbon::parse($transaction->created_at)->format('Y/m/d H:i:s');
        $currentDate = strtotime($date);
        $futureDate = $currentDate+(60*180);
        $expTime = date("Y-m-d H:i:s", $futureDate);
        $expTimeView = date("Y/m/d H:i:s", $futureDate);
        $expTimeViewDate = date("Y/m/d", $futureDate);
        $expTimeViewTime = date("H:i:s", $futureDate);
       @endphp
      <div style="font-family:arial; font-size:14px; padding:30px; background:#fff; color:#333; text-align:left;">
            <div class="order-detail widget-options center">
              <div class="widget-head" style="margin-bottom:30px;">
                <h3 style="margin: 0; font-size: 18px;">Halo, {{$transaction->customer->name ?? ''}}</h3></h3>
               <p>Moon maaf, masa berlaku pesanan dengan no. order #{{$transaction->order_no}} untuk {{$transaction->event->getTranslation('id')->title}} telah berakhir. Ayo kembali ke www.kiostix.com untuk melakukan transaksi kembali.</p>

<p style="font-size:12px; font-style: italic;">We’re sorry but your order #{{$transaction->order_no}} for {{$transaction->event->getTranslation('id')->title}} has expired
Let’s go back to www.kiostix.com to place another order.</p>

                </p>
                <div style="clear: both; overflow: hidden; margin:0 -3px; display: flex; align-items: center; justify-content: center; text-align: center;">
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Total Pembayaran: <br>
                            <i style="font-size:12px;">Total Payment</i></span>
                         <h3 style="margin:0;"><strong>Rp {{number_format($transaction->value_grand_total)}}</strong></h3>
                    </div>
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Metode Pembayaran: <br>
                            <i style="font-size:12px;">Payment Method</i></span>
                         <h3 style="margin:0;"><strong>{{$transaction->payment->name}}</strong></h3>
                    </div>
                </div>
                <div style="clear: both; overflow: hidden; margin:0 -3px; display: flex; align-items: center; justify-content: center; text-align: center;">
                    <div style="width: 100%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>No. Order <br>
                            <i style="font-size:12px;">Order No</i></span>
                         <h3 style="margin:0;"><strong>{{$transaction->order_no}}</strong></h3>
                    </div>
                </div>
                <div style="clear: both; overflow: hidden; margin:0 -3px; display: flex; align-items: center; justify-content: center; text-align: center;">
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Status Pembayaran:  <br>
                            <i style="font-size:12px;">Payment Status</i></span>
                         <h3 style="margin:0; color: #c00000"><strong>Expired</strong></h3>
                    </div>
                    <div style="width: 50%; padding: 20px; margin: 3px; border-radius: 10px; background: #eee;">
                        <span>Batas Waktu Pembayaran   <br>
                            <i style="font-size:12px;">Payment Time Remaining</i></span>
                         <br>
                         <strong>{{$expTimeViewDate}}</strong>
                         <br>
                         <h3 style="margin:0;"><strong style="font-size: 20px;">{{$expTimeViewTime}}</strong></h3>
                    </div>
                </div>
              </div>{{-- .widget-head --}}
      </div>
  </div>
</div>
