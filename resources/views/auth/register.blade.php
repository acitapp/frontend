@component('layouts.app')
<section class="section flex-center">
  <div class="boxForm">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
       <div class="big-title center">
        <h2 class="title">@lang('content.signup')</h2>
      </div>
       <form class="login-form" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
            <input id="firstname" type="text" class="form-control" name="firstname" placeholder="@lang('content.firstname')" value="{{ old('firstname') }}" required autofocus>
            @if ($errors->has('firstname'))
                <span class="help-block">
                    {{ $errors->first('firstname') }}
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
             {!! Form::text('lastname', Auth::user()->information->lastname ?? null, ['class'=>'form-control','placeholder'=>   Lang::get('content.lastname')]) !!}
            @if ($errors->has('lastname'))
                <span class="help-block">
                   {{ $errors->first('lastname') }}
                </span>
            @endif
            <span class="help-block help-info">@lang('content.help_fullname')</span>
        </div>
        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
            {!! Form::select('gender',['Male' => Lang::get('content.male'),'Female' => Lang::get('content.female')],Auth::user()->information->gender ?? null, ['class'=>'form-control select', 'placeholder' =>   Lang::get('content.gender')]) !!}       
            @if ($errors->has('gender'))
                <span class="help-block">
                   {{ $errors->first('gender') }}
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
             {!! Form::text('phone',Auth::user()->information->phone ?? null, ['class'=>'form-control','placeholder'=>  Lang::get('content.phone')]) !!}
            @if ($errors->has('phone'))
                <span class="help-block">
                   {{ $errors->first('phone') }}
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
             {!! Form::text('dob',null, ['class'=>'form-control dob','placeholder'=>   Lang::get('content.dob')]) !!}
            @if ($errors->has('dob'))
                <span class="help-block">
                   {{ $errors->first('dob') }}
                </span>
            @endif
            <span class="help-block help-info">@lang('content.dob')</span>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          
            <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <span class="help-block">
                    {{ $errors->first('email') }}
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
       
            <input id="password" type="password" class="form-control" placeholder="@lang('content.password')" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                    {{ $errors->first('password') }}
                </span>
            @endif
        </div>

        <div class="form-group">
          <input id="password-confirm" type="password" class="form-control" placeholder="@lang('content.confirm_password')" name="password_confirmation" required>
        </div>

        <div class="form-group">
            <input id="checkbox" type="checkbox" required>
            <span>@lang('content.iagree')<a href="{{url('/')}}/page/terms-conditions">"@lang('content.tnc')"</a></span>
        </div>

        <div class="form-group">
           
                <button type="submit" class="button btn-primary">
                   @lang('content.signup')
                </button>
        </div>
    </form>
  </div> {{-- .container --}}
</section>
@endcomponent