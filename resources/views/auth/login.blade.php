@component('layouts.app')
<section class="section flex-center">
  <div class="boxForm">
      <div class="big-title">
        <h2 class="title">@lang('content.login')</h2>
      </div>
       @include('app.partial.login-form')
  </div> {{-- .container --}}
</section>
@endcomponent
