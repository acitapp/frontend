require('./snippet/popup.js');
require('./snippet/approve.js');
require('./snippet/common.js');
require('./snippet/subscribe.js');
require('./snippet/comment.js');
require('./snippet/countdown.js');
require('./snippet/contactform.js');
require('./snippet/share.js');
require('./snippet/searchdate.js');
require('./snippet/searchticket.js');
require('./snippet/rating.js');
require('./snippet/snippet.js');
$(document).ready(function() {
   common.inputNumber();
   snippet.getVoucher();
   common.calculatePrice();
   common.findTicketTrigger();
   common.showSchedule();
   snippet.findPaymentFee();
  $('.setCookie').click(function(){
     url = $(this).data('url');
     Cookies.set('redirect', url, { expires: 7 });
  });
  var cookieValue = Cookies.get('redirect');
  console.log(cookieValue);
  if(cookieValue){
     window.location.href = cookieValue;
     Cookies.remove('redirect');
  } 
  $('.orderForm').submit(function(){
      var id = '#'+$(this).attr('id');
      var values = $(id).serializeArray();
      var scheduleId= id.replace('#order-form-', '');
      var arr = [], $select = $(id), name = $select.attr("input-number");
      $select.find(".input-number").each(function() {
          arr[arr.length] = { name: name, value: this.value, id: '#'+$(this).attr('id') };
      });
       Cookies.set('orderValue', arr, { expires: 7 });
      var cookieOrder = Cookies.get('orderValue');
  });
      // var cookieOrder = JSON.parse(Cookies.get('orderValue'));
      // $.each(cookieOrder, function(idx, obj) {
      //   $(obj.id).val(obj.value);
      // });

   $('.moreContent').readmore({
      speed: 75,
      moreLink:'<a href="#" class="readmore-content">Selengkapnya</a>',
      lessLink: '<a href="#" class="readmore-content">Tutup</a>'
    });
  $(".sticky").sticky({
    topSpacing:0,
    bottomSpacing:0,
  });
  $('.alert').fadeOut(5000);
  $('#menu').slicknav({
      prependTo:'#mobilenav',
      label: '',
      beforeOpen: function(){
        $('body').toggleClass('activenav');
      },
      beforeClose: function(){
        $('body').toggleClass('activenav');
      }
  });
    //Find Place
  $('.findPlace').on('change', function () {
      common.findPlace(this);
  });

  $('#voucher').on('focus', function () {
      $('.submitVoucher').removeClass('btn-grey');
  });
   $(".datepicker").datepicker({
      format: 'dd/mm/yyyy',
      startDate: '01/01/1950',
      endDate: '01/01/2000',
  });  
   $(".datepickers").datepicker({
      format: 'dd/mm/yyyy',
  });
  $('.dob').mask("00/00/0000", {
     selectOnFocus: true,
     placeholder: "Tanggal Lahir (dd/mm/yyyy)"
  });    
  $('.card-no').mask("0000-0000-0000-0000", {
     selectOnFocus: true,
     placeholder: "Nomor Kartu"
  });  
  $('.card-expiry-month').mask("00", {
     selectOnFocus: true,
     placeholder: "MM"
  });  
  $('.card-expiry-year').mask("0000", {
     selectOnFocus: true,
     placeholder: "YYYY"
  });     
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
  $(function () {
    $('[data-toggle="popover"]').popover()
  })
  $('.logout').click(function() {
     $('#logout-form').submit(); 
  });
  $('.close-subscribe').click(function() {
     $('#subscribe').fadeOut(400); 
  });
  $('input[type=file]').bootstrapFileInput();
  $(".select").select2();
  $('.scrolldown').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 2000);
    return false;
  });
$('.section-box').on('show.bs.collapse', function () {
    $('.section-box').removeClass('in');
})
  //carousel
  $('.singlecarousel').owlCarousel({
      loop:true,
      margin:10,
      autoplay:true,
      autoplayTimeout:3000,
      autoplayHoverPause:false,
      items:1,
      navText:["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
  });
  $('.headlinecarousel').owlCarousel({
      margin:10,
      dots:false,
      autoplay:true,
      items:1,
      navText:["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
  });
  //carousel
  $('.carousel').owlCarousel({
      loop:true,
      margin:10,
      autoplay:true,
      autoplayTimeout:3000,
      autoplayHoverPause:false,
      dots:false,
      navText:["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
      responsive:{
          0:{
              items:1
          },
          600:{
              items:2
          },
          1000:{
              items:4
          }
      }
  });
});

//preview Image
  function readURL(input) {
     if (input.files && input.files[0]) {
         var reader = new FileReader();
         reader.onload = function (e) {
             $('#image_upload_preview').attr('src', e.target.result);
             $('#image-data').val(e.target.result);
             $('#image_upload_preview').show();

         }
         reader.readAsDataURL(input.files[0]);
     }
 }
 $("#image").change(function () {
     readURL(this);
 });
 $("#file").change(function () {
     readURL(this);
 });
//PAGING SCROLL
$(function() {
  $('.scroll').jscroll({
      loadingHtml: '<div class="loading loading-double"></div>',
      autoTrigger: false,
      nextSelector: 'a.LoadMore',
      contentSelector: 'div.scroll',
      callback: function() {
      }
  });
  $('.mobileCarousel').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      }
    ]
  });
});
