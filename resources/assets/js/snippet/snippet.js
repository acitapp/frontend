snippet = {
  focusVoucher:function(){
    $('#voucher').on('focus', function () {
        $('.submitVoucher').removeClass('btn-grey');
    });
  },
  findPaymentFee:function(){
      $('#accordion-payment').on('show.bs.collapse', function (e) {
          $('.btnPay').remove();
          $('#loader-payment').removeClass('hide');
          var str = e.target.id;
          var id = str.replace("payment-section-", "");
		   var typepayment = $('#'+str).data('typepyment');
          if (id) { // require a ID
              var data = {
                  _token: token,
                  id: id,
            }
            $.ajax({
                method: "POST",
                url: base_url+"/payment-fee",
                data: data
            }).done(function(data) {
                  var buttonData = 
                      '<button id="btnPay-'+id+'" class="button btn-block btn-add btnPay" '
                       +'payment="#payment-form-'+id+'"'
                       +'type="submit">'
                       +'Bayar Menggunakan '+e.target.title
                      +'</button>'
                     $('#btnPayBox').html(buttonData);
                 $('#voucherGroup').remove();
                 var eventId = $('#event-title').data('id');
                 var voucherForm =  
                        '<div class="item-row f14 booking-section" id="voucherGroup">'
                        +'<span class="product-item">Kode Voucher (Jika Ada)</span>'
                        +'<div class="inputgroup">'
                         +'<div class="loading loading-double hide" id="loader-voucher"></div>'
                          +'<input type="text" name="voucher" id="voucher">'
                          +'<a href="#" class="button btn-outline btn-s submitVoucher btn-grey" data-event="'+eventId+'">Submit</a>'
                        +'</div>'
                      +'</div>'
                  $('#voucherBox').html(voucherForm);
                   if (typepayment == 'credit_card' || typepayment == 'mandiri-credit-card' ) {
                           //Pay with Credicard
                           snippet.payCreditCard(id);
                       }else if (typepayment ==  'mandiri_clickpay') {
                        //Pay with Credicard
                        snippet.payMandiriClick(id);
                        } else {
                           snippet.payButton(id);
                       }
                 $('.submitVoucher').attr('payment', id );
                 $('#loader-payment').addClass('hide');
                 
                  var totalPrice = $('#totalPrice');
                  var totalValue = parseInt(totalPrice.data('value'));
                  var fee_fixed   =  parseInt(data.fee_fixed);
                  var percent  = parseInt(data.fee_percent);
                  var fee_percent = (totalValue/100)*percent;
                  var total_fee = fee_fixed+fee_percent;
                  $('#feePayment').text(common.number_format(total_fee));
                  var totalPriceValue = $('#totalPriceValue');
                  var grandTotal = totalValue+total_fee;
                  totalPriceValue.text(common.number_format(grandTotal));
                  $('#feePayment').attr('data-value',total_fee);
                  $('#gross_amount').val(grandTotal);
                  snippet.getVoucher();
                  snippet.focusVoucher();
              });
          }
      });
  },
  payCreditCard:function(payment_id){
      // Live URL
      Veritrans.url = "https://api.veritrans.co.id/v2/token";
      //Sandbox
     //Veritrans.url = "https://api.sandbox.veritrans.co.id/v2/token";
      // Live Key
      Veritrans.client_key = "VT-client-7pIfBbBOQYLbcBmB";
      // Sandbox key.
     // Veritrans.client_key = "VT-client-F1XFrf8QAjBVl9i2";
      //Veritrans.client_key = "d4b273bc-201c-42ae-8a35-c9bf48c1152b";
      var cardNumber = $(".card-number").val();
	   var typepyment = $('#payment-'+payment_id).data('typepyment');
    //   console.log(cardNumber);
      if(typepyment=='mandiri-credit-card'){
                var card = function card() {
                    return { 'card_number': $(".card-number-mandiri").val(),
                        'card_exp_month': $(".card-mandiri-expiry-month").val(),
                        'card_exp_year': $(".card-mandiri-expiry-year").val(),
                        'card_cvv': $(".card-mandiri-cvv").val(),
                        'secure': true,
                        'gross_amount': $('#gross_amount').val()
                    };
                };
           }
           else{
                var card = function card() {
                    return { 'card_number': $(".card-number").val(),
                        'card_exp_month': $(".card-expiry-month").val(),
                        'card_exp_year': $(".card-expiry-year").val(),
                        'card_cvv': $(".card-cvv").val(),
                        'secure': true,
                        'bank' : 'cimb',
                        'gross_amount': $('#gross_amount').val()
                    };
                };
           }
           
    //   console.log(card);
      function callback(response) {
          if (response.redirect_url) {
              // 3dsecure transaction, please open this popup
              openDialog(response.redirect_url);
          } else if (response.status_code == '200') {
              // success 3d secure or success normal
              closeDialog();
              // submit form
              $('#btnPay-'+payment_id).attr("disabled", "disabled"); 
              $("#token_id").val(response.token_id);
              $("#payment-form-"+payment_id).submit();
          } else {
              // failed request token
              console.log(response);
              console.log('Close Dialog - failed');
              //closeDialog();
              //$('#purchase').removeAttr('disabled');
              // $('#message').show(FADE_DELAY);
              // $('#message').text(response.status_message);
              //alert(response.status_message);
          }
      }
      function openDialog(url) {
          $.fancybox.open({
              href: url,
              type: 'iframe',
              autoSize: true,
              height: 450,
              closeBtn: true,
              modal: true
          });
      }
      function closeDialog() {
          $.fancybox.close();
      }
      $('#btnPay-'+payment_id).click(function(event){
          event.preventDefault();
          //$(this).attr("disabled", "disabled"); 
          Veritrans.token(card, callback);
          return false;
      });
      $('body').click(function(){
          $.fancybox.close();
      });
  },
  payMandiriClick: function payMandiriClick(payment_id) {
           // Live URL
           Veritrans.url = "https://api.veritrans.co.id/v2/token";
           //Sandbox
           //Veritrans.url = "https://api.sandbox.veritrans.co.id/v2/token";
           // Live Key
            Veritrans.client_key = "VT-client-7pIfBbBOQYLbcBmB";
           // Sandbox key.
           //Veritrans.client_key = "VT-client-F1XFrf8QAjBVl9i2";
           //Veritrans.client_key = "d4b273bc-201c-42ae-8a35-c9bf48c1152b";
           
           $(".from-token-mandiri").show();
           var cardNumber = $(".card-numberdebet").val();
           var card = function card() {
               return { 'card_number': $(".card-numberdebet").val() };
           };
        //    console.log(cardNumber);
           function callback(response) {
                if (response.status_code == '200') {
                     // submit form
                    $('#btnPay-' + payment_id).attr("disabled", "disabled");
                    var numberrandom =Math.floor(Math.random() * 90000) + 10000;
                    var cardNumber = $(".card-numberdebet").val();
                    // var input1 =cardNumber.replace(/-/g,'');
                    // console.log(input1);
                    $("#token_idmandiriclickpay").val(response.token_id);
                    // $('.input1').html(input1.substring(6));
                    // console.log(input1.substring(6));
                    $('.input3').html(numberrandom);
                    $('#input3').val(numberrandom);

                    $("#payment-form-" + payment_id).submit();
                } else {
                    // failed request token
                    console.log(response);
                    console.log('Close Dialog - failed');
                    //closeDialog();
                    //$('#purchase').removeAttr('disabled');
                    // $('#message').show(FADE_DELAY);
                    // $('#message').text(response.status_message);
                    //alert(response.status_message);
                }
         
           }
           function openDialog(url) {
               $.fancybox.open({
                   href: url,
                   type: 'iframe',
                   autoSize: true,
                   height: 450,
                   closeBtn: true,
                   modal: true
               });
           }
           function closeDialog() {
               $.fancybox.close();
           }
           $(".card-numberdebet").keyup(function(){
                var input1 = $(this).val().replace(/-/g,'');
                $('.input1').html(input1.substring(6));
           });

           $('#btnPay-' + payment_id).click(function (event) {
               event.preventDefault();
              
               //$(this).attr("disabled", "disabled"); 
               Veritrans.token(card, callback);
               return false;
           });
           $('body').click(function () {
               $.fancybox.close();
           });
       }, 
  payButton:function(payment_id){
      $('#btnPay-'+payment_id).click(function(){
              $("#payment-form-"+payment_id).submit();
      });
  },
  getVoucher:function(){
      $('.submitVoucher').click(function(){
            $('#loader-voucher').removeClass('hide');
            var payment = $(this).attr('payment');
            var event = $(this).data('event');
            var voucher = $('#voucher').val();
            if (payment) {
              if (voucher) {
                  var data = {
                        _token: token,
                        event: event,
                        payment: payment,
                        voucher: voucher,
                  }
                  $.ajax({
                      method: "POST",
                      url: base_url+"/get-voucher",
                      data: data
                  }).done(function(data) {
                      if (data.status === 5) {
                        $("#popup-title").text("Ooops!");
                        $("#popup-text").text(data.message);
                        common.popup()
                      }else{
                         var totalPrice = $('#totalPrice');
                         var totalValue = parseInt(totalPrice.data('value'));
                         var dataFee = parseInt($('#feePayment').data('value'));
                         var totalValue = totalValue+dataFee;
                         if (data.promotion.unit == 'fixed') {
                             var promoValue = parseInt(data.promotion.value);
                             var grandTotal = totalValue+promoValue;
                         }else if(data.promotion.unit == 'percent'){
                            var promoValue = parseInt((totalValue/100)*data.promotion.value);
                            var grandTotal = totalValue+promoValue;
                         }
                         $('#voucherGroup').remove();
                         var voucherData = 
                            '<div class="item-row f14 booking-section" id="voucherGroup">'
                              +'<span class="product-item">Nilai Voucher</span>'
                              +'<span id="voucher-'+data.promotion.id+'" class="voucher-value product-price">-'+common.number_format(promoValue)+'</span>'
                             +'</div>'
                           $('#voucherBox').html(voucherData);
                          var totalPriceValue = $('#totalPriceValue');
                        //  console.log('Total Value = '+totalValue);
                          var grandTotal = totalValue-promoValue;
                          totalPriceValue.text(common.number_format(grandTotal));
                          $('#gross_amount').val(grandTotal);
                          $('.voucher_code').val(data.voucher.code);
                          $('.voucher_id').val(data.voucher.id);
                          $('.voucher_value').val(data.voucher.value);
                      }
                     $('#loader-voucher').addClass('hide');
                    //  console.log(data);
                  });
              }
              else{
                  $("#popup-title").text("Ooops!");
                  $("#popup-text").text("Anda belum mengisi kode voucher.");
                  common.popup()
              }
            }else{
                  $("#popup-title").text("Ooops!");
                  $("#popup-text").text("Mohon pilih jenis pembayaran terlebih dahulu.");
                  common.popup()
            }
            return false;
      });
  },
}
$(snippet.init);