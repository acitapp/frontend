// CONTACT FORM
var form = $("#order-form");
form.validate({
  rules: {
  },
  messages: {
  },
  submitHandler: function (form,values) {
      var values = $(form).serializeArray();
      $('#loader-order').removeClass("hide");
      var data = values;
      $.ajax({
        method: "POST",
        url: base_url+"/order",
        timeout: 30000, //Set your timeout value in milliseconds or 0 for unlimited
        data : $.param(data)
      }).done(function(data) {
        console.log(data);
        $('#loader-order').addClass("hide");
        var url = base_url+data.url;
        window.location = url;
      }).fail(function (jqXHR, textStatus, errorThrown) {
            $('#loader-order').hide();
            $("#popup-title").text("Ooops!");
            $("#popup-text").text("Please try again.");
            common.popup()
        });
      return false;
  }
});
