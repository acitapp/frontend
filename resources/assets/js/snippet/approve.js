// APPROVE FORM
var contestform = $("#approveForm");
$("#approveForm").validate({
  rules: {
    mess: {
      required: true,
    },
    email: {
      required: true,
    },
  },
  submitHandler: function (approveForm,values) {
      var values = $(approveForm).serializeArray();
      console.log(values);
      $('#loaderapprove').removeClass('hide');
      var data = values;
      $.ajax({
        method: "POST",
        url: base_url+"/approve",
        data : $.param(data)
      }).done(function(data) {
        $('#loaderapprove').addClass('hide');
        console.log(data);
        if(data.published == 1005){
          $("#approveBox").html("<div class='alert alert-success'><strong>This article has been approved!</strong></div>");
        }
         approveForm.reset();
      }).fail(function (jqXHR, textStatus, errorThrown) {
            $('#loader').hide();
            $("#popup-title").text("Ooops!");
            $("#popup-text").text("Please try again.");
            common.popup()
        });
      return false;
  }
});

  