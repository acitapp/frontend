

// CONTEST FORM

function popup(){
  $.magnificPopup.open({
    items: {
      src: '#popup-message'
    },
    type: 'inline',
  });
}

function popupLogin(){
  $.magnificPopup.open({
    items: {
      src: '#popup-login'
    },
    type: 'inline',
  });
}
function popupVote(){
  $.magnificPopup.open({
    items: {
      src: '#popup-vote'
    },
    type: 'inline',
  });
}

var contestform = $("#contestform");
$("#contestform").validate({
  rules: {
    email: {
      required: true,
    },
  },
  submitHandler: function (contestform,values) {
      var values = $(contestform).serializeArray();
      console.log(values);
      $('#loadercontest').removeClass('hide');
      var data = values;
      $.ajax({
        method: "POST",
        url: base_url+"/contest/submission",
        data : $.param(data)
      }).done(function(data) {
        $('#loadercontest').addClass('hide');
        console.log(data);
        if(data.status == 1){
          $("#popup-title").text("Thanks You!");
          $("#popup-text").text("Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus!");
           popup();
           contestform.reset();
        }else{
           popupLogin();
        }
      }).fail(function (jqXHR, textStatus, errorThrown) {
            $('#loader').hide();
            $("#popup-title").text("Ooops!");
            $("#popup-text").text("Please try again.");
            popup();
        });
      return false;
  }
});

function btnVote(){
$('.btnVote').click(function() {
      $(this).toggleClass('hasVote'); 
        var data_id = $(this).attr("data-id");
        var data = {
            _token: $(this).attr('token'),
            data_id: data_id,
        }
        $.ajax({
          method: "POST",
          url: base_url+"/vote",
          data: data
        }).done(function(data) {
            console.log(data);
            if(data.status == 2){  
                $("#popup-title").text("Oopps");
                $("#popup-text").text("You only can vote one time");
                 popup();
            }else if(data.status == 3){
                popupLogin();
            }else{
                var span = $("#vote-"+data.id+" span.countVote");
                var countVote = $("#vote-"+data.id).find('span.countVote').text();
                var value = parseInt(countVote, 10) + 1;
                span.text(value); 
                $("#popup-title").text("Thanks You!");
                $("#popup-text").text("You have successfully voted!");
                 popup();
            }
        });
        return false;
    });
}
btnVote();


  $('.popupVote').click(function(){
      var id = $(this).attr('data-id');
      $.get( base_url+"/contest/popup/"+id, function( data ) {
          console.log(data);
          var entrypopup = JSON.parse(data.value);
          popupDetail = '<div class="thumb"><img src="'+data.image+'"/>'
                        +'<div class="counterVote">'
                        +'<i class="ion-android-favorite"></i>'
                        +'<span class="icon-countVote">'+data.like+'</span>'
                        +'</div>'
                        +'</div>'
                        +'<div class="popup-entry-meta">'
                        +'<h3 class="popup-entry-title">'+data.title+'</h3>'
                        +'<span class="popup-author">Author : '+data.author+'</span>'
                        +'<span class="popup-date">'+data.date+'</span>'
                        +'</div>'
                        +'<div class="popup-entry"><p>'+data.description+'</p></div>'
                       +'<a href="#" class="btnVote button" data-id="'+data.id+'"  token="'+token+'">VOTE</a>';
          $("#popup-content").html(popupDetail);
          popupVote();
          btnVote();
      }, "json" );
  });

  