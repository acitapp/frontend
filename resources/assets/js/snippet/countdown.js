
$("#countdown").countdown(expTime, function(event) {
  $(this).html(
    event.strftime('<strong>%M</strong> <span>Menit</span> <strong>%S</strong> <span>Detik</span>')
  )}).on('finish.countdown', function() {
  		var eventID = $(this).attr('event-id');
  		var trxId = $(this).attr('transaction-id')
        var data = {
            _token: token,
            trxId: trxId,
            eventID: eventID,
      }
      $.ajax({
        method: "POST",
        url: base_url+"/expired",
        data : data
      }).done(function(data) {
          $("#popup-title").text("Maaf,");
          $("#popup-text").html("Pesanan anda sudah melewati batas waktu pembayaran.");
          common.popup()
           setTimeout(function () {
                 var url = base_url+data.url;
                 window.location = url;
            }, 4000);
      }).fail(function (jqXHR, textStatus, errorThrown) {
            $("#popup-title").text("Ooops!");
            $("#popup-text").text("Please try again.");
            common.popup()
        });
      return false;
  });