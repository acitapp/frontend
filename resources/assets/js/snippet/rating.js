
$('.rating-star').each(function () { 
   var value = $(this).attr('average');
   var event = $(this).attr('event-id');
   var rate = '#'+$(this).attr('id');
   $(rate).rateYo({
    rating : value,
    starWidth: "20px",
    onSet: function (rating, rateYoInstance) {
         var data = {
              _token: token,
              event: event,
              value: rating,
        }
        $.ajax({
            method: "POST",
            url: base_url+"/rating",
            data: data
        }).done(function(data) {
              if (!data.login) {
                common.popupLogin();
              }
              console.log(data);
          });
    }
  });
});

$('.rating-star-readonly').each(function () { 
   var value = $(this).attr('average');
   var event = $(this).attr('event-id');
   var rate = '#'+$(this).attr('id');
   $(rate).rateYo({
    rating : value,
    starWidth: "16px",
    readOnly: true
    });
});