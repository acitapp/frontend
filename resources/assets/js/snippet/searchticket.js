$('.find-ticket').click(function(){
        $('.booking-detail').addClass('hide');
        var type = $(this).attr('type');
        var event = $(this).attr('event');
        var schedule = $(this).attr('schedule');
        var venue = $(this).attr('venue');
        var session = "";
        var selector = "#time-schedule-"+schedule;
        var selector2 = " input[type='radio']:checked"
        var selected = $(selector+selector2);
        var visitDate = $('#data-visit-'+schedule).val();
        $('.order-cart').val();
        if (selected.length > 0) {
            session = selected.val();
            sessionId = selected.attr('session');
            $('#loader-schedule-'+schedule).removeClass('hide');
            $('.row-section-ticket').remove();
            var data = {
                _token: token,
                event: event,
                session: session,
                visitdate : visitDate,
                schedule: schedule,
                venue: venue,
            }
              $.ajax({
                method: "POST",
                url: base_url+"/searchticket",
                data: data
              }).done(function(data) {
                  console.log(data);
                  $('.row-section-ticket').remove();
                  if(data.data.length > 0){
                   $.each(data.data, function (index, value) {
                    var SectionResult = 
                      '<div class="row-section-ticket" id="section-schedule-ticket-'+value.id+'">'+
                        '<div class="section-name">'+
                              '<span  class="labels ticket-name">'+
                              value.name+
                            ' </span>'+
                        '</div>'+
                        '<div class="flex-center">'+
                          '<div class="colom-info col3">'+
                            '<span class="price ticket-price" id="section-ticket-'+value.id+'" >Rp.  '+common.number_format(value.value)+'</span>'+
                          '</div>'+
                            checkQuantity(data,value)
                         '</div>'+
                      +'</div>'
                       $(SectionResult).appendTo('#section-schedule-'+data.schedule);
                   });
                 }else{
                   $('#section-schedule-'+data.schedule).find('.alert').remove();
                    var SectionResult = 
                      '<div class="alert alert-warning alert-small">'+
                          '<span> Maaf, Tiket tidak tersedia </span>'+
                      +'</div>'
                       $(SectionResult).appendTo('#section-schedule-'+data.schedule);
                 }
                  $('#loader-schedule-'+schedule).addClass('hide');
                  $('#v-time-'+schedule).text(session);
                  $('#input-time-'+schedule).val(sessionId);
                  $('#input-venue-'+schedule).val(venue);
                  $("#booking-detail-"+schedule).removeClass('hide');
                  $('.booking-container').show();
                   common.inputNumber();
                   common.calculatePrice();
                   common.addTicket();
              });
        }else{
            var text  = $('#alert-'+schedule).attr('alert-session');
            $('#alert-'+schedule).text(text).show().delay(1000).fadeOut(1000);
        }

    return false;
});
function checkQuantity(data, value){
  if (value.quantity >= 0) {
    return  '<div class="colom-left col3 flex-center">'+
            '<div class="input-group order-qty">'+
                '<span class="input-group-btn">'+
                    '<button type="button" class="btn btn-default btn-number btn-calculate" data-type="minus" data-field="qty-'+value.id+'" data-schedule="'+value.schedule_id+'" data-section="'+value.id+'">'+
                        '<span class="ion-minus"></span>'+
                    '</button>'+
                '</span>'+
                '<input type="text" id="qty-'+value.id+'" name="qty[]" class="form-control input-number" value="0" min="0" max="'+value.max_purchase+'" data-price="'+value.value+'" data-schedule="'+value.schedule_id+'" data-section="'+value.id+'">'+
               
                '<input type="hidden" name="ticket[]" value="'+value.id+'">'+
                '<span class="input-group-btn">'+
                    '<button type="button" class="btn btn-default btn-number btn-calculate" data-type="plus" data-field="qty-'+value.id+'" data-schedule="'+value.schedule_id+'" data-section="'+value.id+'">'+
                        '<span class="ion-plus"></span>'+
                    '</button>'+
                '</span>'+
           '</div>'+
          '</div>'+
           '<div class="colom-right col3" id="subtotal-'+value.id+'">'+
              '<span class="price text-right">Rp. <span id="subtotal-value-'+value.id+'" class="product-price" value="0">0</span></span>'+
          '</div>';
  }else{
    return  '<div class="colom-right col3 text-right">'+
              '<a class="button btn-outline btn-s">SOLD OUT</span>'+
            '</div>';

  }
}