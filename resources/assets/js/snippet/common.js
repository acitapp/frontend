var orderItems = []
var totalQty = 0;
var totalPrice = 0;
common = {
  findPlace:function(e){
    var id = $(e).val(); 
      var type = $(e).attr('type'); 
        var target = $(e).attr('target'); 
        var data = {
            _token: token,
            id: id,
            type: type,
            target: target,
      }
        if (id) { // require a ID
          $.ajax({
              method: "POST",
              url: base_url+"/search-"+type,
              data: data
          }).done(function(data) {
                $('#'+target+'_id').html('<option value="">Select '+target+'</option>');
                $.each(data.data, function (index, value) {
                  var states = '<option value="'+index+'">'+value+'</option>';
                    $(states).appendTo('#'+target+'_id');
                });
                console.log(data.data);
            });
        }
        return false;
  },
  popupLogin:function(){
    $.magnificPopup.open({
      items: {
        src: '#popup-login'
      },
      type: 'inline',
    });
  },
  popup:function(){
    $.magnificPopup.open({
      items: {
        src: '#popup-message'
      },
      type: 'inline',
      callbacks: {
        close: function(){
            $('.loading').addClass('hide');
          }
      }
    });
  },
  inputNumber:function(){
       $('.btn-number').click(function(e){
          $('.booking-detail').addClass('hide'); 
          var schedule      = $(this).data('schedule');
          var countSession      = $(this).data('count-session');
          var selector = "#time-schedule-"+schedule;
          var selector2 = " input[type='radio']:checked"
          var selected = $(selector+selector2);
          $('#booking-detail-'+schedule).removeClass('hide');
          if (selected.length > 0 || countSession == 0)  {
              e.preventDefault();
              fieldName = $(this).attr('data-field');
              type      = $(this).attr('data-type');
              var input = $("#"+fieldName);
              var currentVal = parseInt(input.val());
              if (!isNaN(currentVal)) {
                  if(type == 'minus') {
                      if(currentVal > input.attr('min')) {
                          input.val(currentVal - 1).change();
                          $('.btn-number').attr('disabled', false);
                      } 
                      if(parseInt(input.val()) == input.attr('min')) {
                          $(this).attr('disabled', true);
                      }
                  } else if(type == 'plus') {
                      if(currentVal < input.attr('max')) {
                          input.val(currentVal + 1).change();
                          $('.btn-number').attr('disabled', false);
                      }
                      if(parseInt(input.val()) == input.attr('max')) {
                          $(this).attr('disabled', true);
                      }
                  }
              } else {
                 $('#booking-detail-'+schedule).find("button[type='submit']").attr('disabled', 'disabled').addClass('disabled');
                  input.val(0);
              }
          }else{
              $('#time-schedule-'+schedule).tooltip('show');
          }
    });
    $('.input-number').focusin(function(){
       $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {
        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());
        
        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
    });
    $(".input-number").keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                (e.keyCode == 65 && e.ctrlKey === true) || 
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
    });
  },
  number_format:function(number, decimals, dec_point, thousands_sep){
          number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
          var n = !isFinite(+number) ? 0 : +number,
              prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
              sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
              dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
              s = '',
              toFixedFix = function (n, prec) {
                  var k = Math.pow(10, prec);
                  return '' + Math.round(n * k) / k;
              };
          s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
          if (s[0].length > 3) {
              s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
          }
          if ((s[1] || '').length < prec) {
              s[1] = s[1] || '';
              s[1] += new Array(prec - s[1].length + 1).join('0');
          }
          return s.join(dec);
  },
  removeByKey:function(array, params){
    array.some(function(item, index) {
      if(array[index][params.key] === params.value){
        // found it!
        array.splice(index, 1);
      }
    });
    return array;
  },
  addTicketSection:function(e){
            var obj = {};
            $('#cart-form-'+e+' input')
                .each(function(){obj[this.name] = this.value;});
                 orderItems.push(obj);
  },
  addTicket:function(e){
      $('.addTicket').click(function(){
          var schedule = $(this).attr('schedule');
          var visitdate = $(this).attr('visitdate');
          var visit = $(this).attr('visit');
          var sessionId = $(this).attr('session');
          var ticket = $(this).attr('ticket');
          var sectionTicket = $('#section-schedule-ticket-'+ticket);
          var ticketName = $(sectionTicket).find('.ticket-name').text();
          var ticketPrice = $(sectionTicket).find('.ticket-price').data('price');
          var ticketQty = parseInt($(sectionTicket).find('#qty-'+ticket).val());
          var subTotal = ticketQty*ticketPrice;
          $('#booking-section-'+ticket).remove();
          $('#booking-detail-'+schedule).removeClass('hide');
          var bookingData = 
              '<div class="flex-row booking-section" id="booking-section-'+ticket+'">'
                +'<div class="item-row">'
                  +'<span class="product-item">'+ticketName+'</span>'
                  +'<span class="product-qty"> x<span id="sub-qty-'+schedule+'" class="ticketqty" ticket-qty="'+ticketQty+'">'+ticketQty+'</span></span>'
                  +'<span class="product-price" id="product-price-'+ticket+'" value="'+subTotal+'">Rp '+common.number_format(subTotal)+'</span>'
                +'</div>'
                +'<form id="cart-form-'+ticket+'" class="hide cart-form">'
                  +'<input id="input-schedule-'+ticket+'" type="hidden" name="schedule" value="'+schedule+'">'
                  +'<input id="input-section-'+ticket+'" type="hidden" name="section" value="'+ticket+'">'
                  +'<input id="input-qty-'+ticket+'" type="hidden" name="qty" value="'+ticketQty+'">'
                  +'<input id="input-date-'+ticket+'" type="hidden" name="visitdate"  value="'+visitdate+'">'
                  +'<input id="input-time-'+ticket+'" type="hidden" name="session"  value="'+sessionId+'">'
                  +'<input id="input-subtotal-'+ticket+'" type="hidden" name="subtotal"  value="'+subTotal+'">'
                +'</form>'
              +'</div>'
             $(bookingData).appendTo('#booking-body-'+schedule);
             common.sumQty(ticket,schedule);
             common.sumPrice(ticket,schedule);
             common.removeByKey(orderItems, {
              key: 'section',
              value: ticket
             });
             common.addTicketSection(ticket);
             var orders = JSON.stringify(orderItems);
             console.log(orders);
            if (ticketQty == 0) {
              $('#booking-section-'+ticket).remove();
            }
            var form = $("#order-form-"+schedule);
            form.validate({
              rules: {
              },
              messages: {
              },
              submitHandler: function (form,values) {
                  $('#order-cart-'+schedule).val(JSON.stringify(orderItems));
                    console.log('ORDER ='+ JSON.stringify(orderItems))
                  var values = $(form).serializeArray();
                  $('#loader-order-'+schedule).removeClass("hide");
                  var data = values;
                  $.ajax({
                    method: "POST",
                    url: base_url+"/order",
                    timeout: 30000, //Set your timeout value in milliseconds or 0 for unlimited
                    data : $.param(data)
                  }).done(function(data) {
                    console.log(data);
                    $('#loader-order-'+schedule).addClass("hide");
                    var url = base_url+data.url;
                     window.location = url;
                  }).fail(function (jqXHR, textStatus, errorThrown) {
                        $('#loader-order-'+schedule).hide();
                        $("#popup-title").text("Ooops!");
                        $("#popup-text").text("Please try again.");
                        common.popup()
                    });
                  return false;
              }
            });

      });
  },
  sumQty:function(ticket,schedule){
    var sumQty = 0;
    $("#booking-body-"+schedule+" .ticketqty").each(function() {
      value = $(this).attr('ticket-qty');
      if(!isNaN(value) && value.length!=0) {
        sumQty += parseFloat(value);
      }
    });
   $('#input-totalqty-'+schedule).val(sumQty);
  },
  sumPrice:function(ticket,schedule){
    var sumPrice = 0;
    $("#booking-body-"+schedule+" .product-price").each(function() {
      value = $(this).attr('value');
     console.log('TOtal ='+ value);
      if(!isNaN(value) && value.length!=0) {
        sumPrice += parseFloat(value);
      }
    });
    $('#input-total-'+schedule).val(sumPrice);
    $('#total-price-'+schedule).text(common.number_format(sumPrice));
    $('#total-price-'+schedule).data('price', sumPrice);
  },
  calculatePrice:function(){
    $('.input-number').change(function() {
        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());
        var schedule = $(this).data('schedule');
        var section = $(this).data('section');
        var price = $(this).data('price');
        var subtotal = valueCurrent*price;
        if(valueCurrent == 0){
           $('#booking-detail-'+schedule).find("button[type='submit']").attr('disabled', 'disabled').addClass('disabled');
        }else{
           $('#booking-detail-'+schedule).find("button[type='submit']").removeAttr('disabled').removeClass('disabled');
        }
        $('#booking-detail-'+schedule).removeClass('hide');
        $('#subtotal-'+section).removeClass('hide');
        if (price == 0) {
           $('#subtotal-value-'+section).text('Free');
        }else{
           $('#subtotal-value-'+section).text(common.number_format(subtotal));
        }
        $('#subtotal-value-'+section).attr('value',subtotal);
        common.sumPrice(section,schedule);
    });
  },
  hideOtherForm:function(){
    $('.visitdate').focus(function() {
         var schedule = $( this ).attr('schedule');
         $('.orderForm').hide();
         $('#order-form-'+schedule).show();
      });
  },
  hideOtherSchedule:function(schedule){
     $('.orderForm').addClass('hide');
     $('#order-form-'+schedule).removeClass('hide');
      common.scrollTo('#order-form-'+schedule);
  },
  findTicketTrigger:function(){
     $('.radio-session').click(function() {
         var schedule = $( this ).attr('schedule');
        $( "#find-ticket-"+schedule ).trigger( "click" );
        $( "#total-price-"+schedule ).text(0);
        common.hideOtherSchedule(schedule);
      });
  },
  showSchedule:function(){
     $('.view-schedule').click(function() {
         $('.orderForm').removeClass('hide');
         $('.radio-session').prop('checked', false);
         $('.times').addClass('hide');
         $('.booking-container').hide();
         return false;
      });
  },
  scrollTo:function(divId){ 
      $('html, body').animate({
        scrollTop: $(divId).offset().top
       }, 500);
  }
}
$(common.init);
//Detect Mobile 
 function mobiledevice() {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      return true;
    }else {
      return false;
    }
}