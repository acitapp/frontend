<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses'     => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret'   => env('SPARKPOST_SECRET'),
    ],

    'stripe'  => [
        'model'  => App\User::class ,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook'       => [
        'client_id'     => '741761969350102',
        'client_secret' => '24d8eee2965101a3fe7fdb4047465f04',
        'redirect'      => 'https://www.kiostix.com/callback/facebook',
    ],
    'google'         => [
        'client_id'     => '973061431941-knbthqb4u2m1006ijlfgdlqmk6mh4ptd.apps.googleusercontent.com',
        'client_secret' => 'EEXU82LhEOaysKNzGz0p-6VV',
        'redirect'      => 'https://www.kiostix.com/callback/google',
    ],

];
